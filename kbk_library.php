<?php

/**
<th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
 * Function used retrieve data from tables
 *
 * @param String $query The SQL query
 * @param mysqli $con   The SQL connection
 * 
 * @return Array
 */

function retrieveData($query, $con)
{
    $queryResult = $con->query($query);
    $result = [];
    if ($queryResult) {
        while ($row = $queryResult->fetch_array(MYSQLI_ASSOC)) {
            array_push($result, $row);
        }
    }

    if (gettype($queryResult) === "mysqli_result") {
        $queryResult->free_result();
    }

    return $result;
}

function insertData($query, $con)
{
    $result = $con->query($query);
    return $result;
}

function spinningWageCalculation($ar)
{
    //This function is used for calculating the wage that pays every month
    $result = [];

    $result['sliver'] = round(($ar['noh'] * $ar['sliver']), 2);
    $result['wage'] = round(($ar['noh'] * $ar['rate']), 2);
    $result['wf'] = round(($ar['noh'] * $ar['wf']), 2);
    $result['wfrec'] = round($ar['wfrec'], 2);
    $result['adrec'] = round($ar['adrec'], 2);
    $result['kcrec'] = round($ar['kcrec'], 2);
    $result['covrec'] = round($ar['covrec'], 2);
    $result['lic'] = round($ar['lic'], 2);
    $result['netwages'] = round(($result['wage'] - $result['wf'] - $result['wfrec'] - $result['adrec'] - $result['kcrec'] - $result['covrec'] - $result['lic']), 2);
    $result['hdwage'] = round((($result['wage'] / $ar['wno']) * $ar['hno']), 2);
    $result['totwages'] = round($result['netwages'] + $result['hdwage']);
    $result['sv'] = $result['sliver'] * $ar['sliver'];
    $result['val'] = $ar['noh'] * $ar['rate'];

    return $result;
}


function spinningIncentivesCalculation($ar, $da)
{
    // Used for calculating the wages paid when the govenment sanctioned
    $result = [];
    $result['esi'] = round(($ar['noh'] * $ar['esi']), 2);
    $result['yi'] = round($ar['noh'] * $ar['yi'], 2);
    $result['mw'] = round($ar['noh'] * $ar['mw'], 2);
    $result['dadays'] = intval($ar['noh'] / $ar['target']);
    if ($result['dadays'] > $da) {
        $result['dadays'] = $da;
    }
    $result['dawages'] = round($result['dadays'] * $ar['dawages'], 2);
    $result['totmw'] = round($result['mw'] + $result['dawages'], 2);
    $result['netmw'] = round($result['totmw'] - $ar['wage'], 2);


    return $result;
}

function twistingWageCalculation($ar)
{
    //This function is used for calculating the wage that pays every month
    $result = [];

    $result['yarn'] = round(($ar['noh'] * $ar['yarn']), 2);
    $result['wage'] = round(($ar['noh'] * $ar['rate']), 2);
    $result['wf'] = round(($ar['noh'] * $ar['wf']), 2);
    $result['wfrec'] = round($ar['wfrec'], 2);
    $result['adrec'] = round($ar['adrec'], 2);
    $result['kcrec'] = round($ar['kcrec'], 2);
    $result['covrec'] = round($ar['covrec'], 2);
    $result['lic'] = round($ar['lic'], 2);
    $result['netwages'] = round(($result['wage'] - $result['wf'] - $result['wfrec'] - $result['adrec'] - $result['kcrec'] - $result['covrec'] - $result['lic']), 2);
    $result['hdwage'] = round((($result['wage'] / $ar['wno']) * $ar['hno']), 2);
    $result['totwages'] = round($result['netwages'] + $result['hdwage']);
    $result['yv'] = $result['yarn'] * $ar['yarn'];
    $result['val'] = $ar['noh'] * $ar['rate'];

    return $result;
}


function twistingIncentivesCalculation($ar, $da)
{
    // Used for calculating the wages paid when the govenment sanctioned
    $result = [];
    $result['esi'] = round(($ar['noh'] * $ar['esi']), 2);
    $result['yi'] = round($ar['noh'] * $ar['yi'], 2);
    $result['mw'] = round($ar['noh'] * $ar['mw'], 2);
    $result['dadays'] = intval($ar['noh'] / $ar['target']);
    if ($result['dadays'] > $da) {
        $result['dadays'] = $da;
    }
    $result['dawages'] = round($result['dadays'] * $ar['dawages'], 2);
    $result['totmw'] = round($result['mw'] + $result['dawages'], 2);
    $result['netmw'] = round($result['totmw'] - $ar['wage'], 2);


    return $result;
}

function weavingWageCalculation($ar)
{
    //This function is used for calculating the wages that pays every month

    $result = [];
    $result['wage'] = round(($ar['metre'] * $ar['wage']), 2);
    $result['wf'] = round(($result['wage'] * ($ar['wf'] / 100)), 2);
    $result['wfrec'] = round($ar['wfrec'], 2);
    $result['adrec'] = round($ar['adrec'], 2);
    $result['kcrec'] = round($ar['kcrec'], 2);
    $result['covrec'] = round($ar['covrec'], 2);
    $result['lic'] = round($ar['lic'], 2);
    $result['netwages'] = round(($result['wage'] - $result['wf'] - $result['wfrec'] - $result['adrec'] - $result['kcrec'] - $result['covrec'] - $result['lic']), 2);
    $result['hdwage'] = round((($result['wage'] / $ar['wno']) * $ar['hno']), 2);
    $result['totwages'] = round($result['netwages'] + $result['hdwage']);
    $result['sqmtr'] = $ar['metre'] * $ar['sqmtr'];
    $result['prime'] = $ar['prime'] * $ar['metre'];
    $result['value'] = $ar['value'] * $ar['metre'];

    return $result;
}


function weavingIncentivesCalculation($ar, $da)
{
    // Used for calculating the wages paid when the govenment sanctioned
    $result = [];
    $result['esi'] = round(($ar['wage'] * $ar['esi']) / 100, 2);
    $result['warpyi'] = round($ar['metre'] * $ar['warpyi'] * $ar['warp_tot'], 2);
    $result['weftyi'] = round($ar['metre'] * $ar['weftyi'] * $ar['weft_tot'], 2);
    $result['mw'] = round($ar['metre'] * $ar['mw'], 2);
    $result['dadays'] = intval($ar['metre'] / $ar['target']);
    if ($result['dadays'] > $da) {
        $result['dadays'] = $da;
    }
    $result['dawages'] = round($result['dadays'] * $ar['dawages'], 2);
    $result['totmw'] = round($result['mw'] + $result['dawages'], 2);
    $result['netmw'] = round($result['totmw'] - $ar['wage'], 2);


    return $result;
}


function weavingYICalculation($ar)
{
    $result = [];
    $result['weft_tot'] = $ar['weft_grey'] + $ar['weft_colour'] + $ar['weft_black'] + $ar['weft_bl'];
    $result['warp_tot'] = $ar['warp_grey'] + $ar['warp_colour'] + $ar['warp_black'] + $ar['warp_bl'];

    return $result;
}

function yarnConsumptionCalculation($ar, $mtr)
{
    $result = [];
    $result['warp_grey']    = $ar['warp_grey'] * $mtr;
    $result['warp_colour']  = $ar['warp_colour'] * $mtr;
    $result['warp_black']   = $ar['warp_black'] * $mtr;
    $result['warp_bl']      = $ar['warp_bl'] * $mtr;
    $result['warp_tot']     = $result['warp_grey'] + $result['warp_colour'] + $result['warp_black'] + $result['warp_bl'];
    $result['weft_grey']    = $ar['weft_grey'] * $mtr;
    $result['weft_colour']  = $ar['weft_colour'] * $mtr;
    $result['weft_black']   = $ar['weft_black'] * $mtr;
    $result['weft_bl']      = $ar['weft_bl'] * $mtr;
    $result['weft_tot']     = $result['weft_grey'] + $result['weft_colour'] + $result['weft_black'] + $result['weft_bl'];
    return $result;
}

function preprocessingWageCalculation($ar)
{
    $result = [];

    // $result['swage']=$ar['snoh']*$ar['swage'];
    $result['swage'] = $ar['snoh'] * $ar['swage'];
    $result['bwage'] = $ar['bnoh'] * $ar['bwage'];
    $result['wwage'] = $ar['wnoh'] * $ar['wwage'];
    $result['totnoh'] = $ar['snoh'] + $ar['bnoh'] + $ar['wnoh'];
    $result['wage'] = round($result['swage'] + $result['bwage'] + $result['wwage'], 2);
    $result['wf'] = round(($result['wage'] * ($ar['wf'] / 100)), 2);
    $result['wfrec'] = round($ar['wfrec'], 2);
    $result['adrec'] = round($ar['adrec'], 2);
    $result['kcrec'] = round($ar['kcrec'], 2);
    $result['covrec'] = round($ar['covrec'], 2);
    $result['lic'] = round($ar['lic'], 2);
    $result['netwages'] = round(($result['wage'] - $result['wf'] - $result['wfrec'] - $result['adrec'] - $result['kcrec'] - $result['covrec'] - $result['lic']), 2);
    $result['hdwage'] = round((($result['wage'] / $ar['wno']) * $ar['hno']), 2);
    $result['totwages'] = round($result['netwages'] + $result['hdwage']);
    return $result;
}

function preprocessingIncentivesCalculation($ar)
{
    // Used for calculating the wages paid when the govenment sanctioned
    $result = [];

    $result['syi'] = round($ar['snoh'] * $ar['syi'], 2);
    $result['byi'] = round($ar['bnoh'] * $ar['byi'], 2);
    $result['wyi'] = round($ar['wnoh'] * $ar['wyi'], 2);
    $result['yi']  = $result['syi'] + $result['byi'] + $result['wyi'];
    $result['smw'] = round($ar['snoh'] * $ar['smw'], 2);
    $result['bmw'] = round($ar['bnoh'] * $ar['bmw'], 2);
    $result['wmw'] = round($ar['wnoh'] * $ar['wmw'], 2);
    $result['mw']  = $result['smw'] + $result['bmw'] + $result['wmw'];
    $result['sdadays'] = intval($ar['snoh'] / $ar['starget']);
    $result['bdadays'] = intval($ar['bnoh'] / $ar['btarget']);
    $result['wdadays'] = intval($ar['wnoh'] / $ar['wtarget']);
    $result['dadays'] = $result['sdadays'] + $result['bdadays'] + $result['wdadays'];
    if ($result['dadays'] > $ar['twd']) {
        $result['dadays'] = $ar['twd'];
    }
    $result['esi'] = round(($ar['wage'] * $ar['esi']) / 100, 2);
    $result['dawages'] = round($result['dadays'] * $ar['dawages'], 2);
    $result['totmw'] = $result['smw'] + $result['bmw'] + $result['wmw'] + $result['dawages'];
    $result['netmw'] = round($result['totmw'] - $ar['wage'], 2);


    return $result;
}

function preprocessingTypeWage($ar)
{
    $result1 = $ar['grey'] + $ar['col'] + $ar['bl'] + $ar['black'];
    return $result1;
}
