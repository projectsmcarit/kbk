-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 17, 2021 at 08:40 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE USER IF NOT EXISTS 'developer'@'localhost' IDENTIFIED BY 'passwd#6789';

CREATE DATABASE IF NOT EXISTS kbk;

GRANT ALL PRIVILEGES ON kbk . * TO 'developer'@'localhost';

