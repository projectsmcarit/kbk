<?php

include("../connection.php");
include("../kbk_library.php");

$artisan_id = $_POST['artisanId'];
$no_of_peices = $_POST['nop'];
$weaving_code = $_POST['weavingVariety'];
$meter = $_POST['meter'];
$hdate = $_POST['hdate'];
$sql_retrive = "SELECT * FROM kbk_weaving WHERE artisan_id = '$artisan_id'";
$result =  $con->query($sql_retrive);
$result_retrive = $result->fetch_assoc();
// $no_of_peices = $result_retrive['nop'];
// $meter = $result_retrive['mtr'];
// $weaving_id = $result_retrive['weaving_id'];
// $wages = $result_retrive['wages'];
$weaving_type = $result_retrive['w_id'];
$attendance = $result_retrive['attendance'];
// $sdate = $result_retrive['s_date'];
$sdate = $hdate;
$unit_code = $result_retrive['unit_code'];
$status = $result_retrive['status'];
$res = retrieveData("SELECT w_type FROM kbk_weaving_type WHERE w_id=$weaving_type", $con);
$w_type = $res[0]['w_type'];
$res7 = retrieveData("SELECT weaving_id,wf,wef,countable,length,width,prime_cost,rate FROM kbk_weaving_variety where weaving_code='$weaving_code' and wef = (SELECT max(wef) from kbk_weaving_variety where weaving_code ='$weaving_code' and wef<='$sdate')", $con);
$res3 = retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$sdate'", $con);
$wno = $res3[0]['total_wd'];
$res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no FROM kbk_deduction WHERE d_date='$sdate' AND artisan_id=$artisan_id AND work_id=2", $con);
$count = $res7[0]['countable'];
if ($count == 1) {
    $meter = $no_of_peices * $res7[0]['length'];
}
$vid = intval($res7[0]['weaving_id']);

$wef = $res7[0]['wef'];
// echo $vid . " " . $wef;
$res9 = retrieveData("SELECT w_id,w_wages FROM kbk_weaving_type where weaving_id='$vid' and w_type='$w_type' and wef='$wef'", $con);
echo $res9[0]['w_wages'];
// $wid = intval($res9[0]['w_id']);

$war1 = [
    'metre'  => $meter,
    'wage'   => $res9[0]['w_wages'],
    'wf'     => $res7[0]['wf'],
    'wfrec'  => floatval($res12[0]['wf_rec']),
    'adrec'  => floatval($res12[0]['ad_rec']),
    'kcrec'  => floatval($res12[0]['credit_rec']),
    'covrec' => floatval($res12[0]['cov_rec']),
    'lic'    => floatval($res12[0]['lic']),
    'hno'    => $res3[0]['holiday_no'],
    'wno'    => $wno,
    'sqmtr'  => $res7[0]['width'],
    'prime'  => $res7[0]['prime_cost'],
    'value'  => $res7[0]['rate'],
];
$calc3 = weavingWageCalculation($war1);
$wages = intval($calc3['wage']);

$sql_insert = "INSERT INTO kbk_weaving (s_date, wages, attendance, nop, mtr, artisan_id, weaving_id, w_id, unit_code, status) VALUES ('$sdate', $wages, '$attendance', '$no_of_peices', '$meter', '$artisan_id', '$vid', '$weaving_type', '$unit_code', '$status')";
$result1 = $con->query($sql_insert);

if ($result1) {
    echo "<script>alert('successfull');</script>";
    echo "<script>window.location.href='view_month_entry.php'</script>";
} else {
    echo "<script>alert('unsuccessfull');</script>";
    // echo "<script>window.location.href='view_month_entry.php'</script>";
}
