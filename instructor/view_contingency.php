<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Contingency</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    
    <?php 
   include('iheader.php');  
   ?>
  </head>
  <body>
    <!-- Start your project here-->
    <form name="contigency" action="contingency.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    <img
                      src="../images/Gandhi_statue.jpg"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .30rem; border-bottom-left-radius:.30rem;"
                    />
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                        <h3 class="mb-5 text-uppercase">Contingency</h3>
      
                     <div>
                     <div class="col-md-6 mb-4">
    
    <select class="form-select" name="unitid" aria-label="Default select example" id="unitid">
        <option disabled selected>-- <span style="color:#ff0000">*</span>Select Unit Name--</option>
        <?php
            include('../connection.php'); // Using database connection file here
            $unid= mysqli_query($con, "SELECT unit_code,unit_name From kbk_unit");  // Use select query here 

            while($unit = mysqli_fetch_array($unid))
              {
               echo "<option value='". $unit['unit_code'] ."'>" .$unit['unit_code']." ".$unit['unit_name'] ."</option>";  // displaying data in option menu
              }	
          ?>  
      </select>
    </div>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="text"  name="voucherno" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">Voucher_No</label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="water" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">Water</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="electricity" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">Electricity</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="landexps" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">Land_exps </label>
                            </div>
                          </div>
                      </div>
      
                      
      
                     
      
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="bldgrepair" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">Bldg_repair</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="stationary" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">Stationary </label>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="autocharge" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">Auto_Charge</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="miscexps" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">Misc_exps</label>
                          </div>
                        </div>
                      </div>
                     
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="charkarepair" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">Charka_repair</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="loomrepair" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">Loom_repair</label>
                          </div>
                        </div>
                      </div>
      
                      <div class="d-flex justify-content-end pt-3">
                        <button type="submit" name="submit" class="btn btn-warning btn-lg ms-2">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
    <!-- End your project here-->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
