<?php
 include('../connection.php');
 include('../kbk_library.php');
 include('view_weaving_list.php');
 session_start();


 $workid=$_SESSION['worktype'];
 $unitcode=$_SESSION['unit'];
 $hdate = $_SESSION['wdate'];  
 
 
 //$_SESSION['a']=$hdate;
 $res1= retrieveData("SELECT artisan_id,artisan_code,artisan_name From kbk_artisan where unit_code='$unitcode' and status=1",$con);
 $_SESSION['i']=$res1;
 
 $res3= retrieveData("SELECT holiday_no,total_wd From kbk_holidays where h_date='$hdate'",$con);
 $res4= retrieveData("SELECT type From kbk_work_type where work_id=$workid",$con);
 $_SESSION['t']=$workid;
 $_SESSION['unitcode']=$unitcode;
 $res5= retrieveData("SELECT unit_name From kbk_unit where unit_code='$unitcode'",$con);
 
?>
<!DOCTYPE html>
<html lang="en">

  <head>
  <?php
 include('iheader.php');
 
 ?>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <?php
    echo '<title>'.$res4[0]['type'].'</title>';
    ?>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    
  </head>
  <body>
    <!-- Start your project here-->
    <form name="spinningentry" action="spinning_entry.php" method="post"> 
    <header>
      <!-- Navbar -->
 
   
      
      </header>
    
    
    <section class="h-100 bg-dark" style="padding-left: 0px;width: 105rem" >
      <div class="py-5 h-100" style="width: 100rem;padding-left: 20px;">
        <div class="row d-flex justify-content-center align-items-center h-100" style="width: 100rem; padding-left: 10px;">
          <div class="col" >
            <div class="card card-registration my-4" style="width: 100rem;padding-left: 0px;" >
              <div class="row g-0">
               
                 <div class="card-body text-black" style="padding-left: 30px;">
                   <h3 class="mb-5 text-uppercase">Monthly Entry</h3> </div>           

  <div class="row" style="padding-left: 70px;">
     
     <div class="col-md-2 mb-2">                       
     <label class="form-label" >Month and Year</label>                   
     </div>
     <div class="col-md-2 mb-2">
       <div class="form-outline ">
       <?php 
         echo '<select class="form-select" name="hdate" >';
         $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         $m1= strval(date('m',strtotime($hdate)));
         $y1= strval(date('Y',strtotime($hdate)));
         echo '<option value="'.$hdate.'">'.$months_name[strval($m1)-1]." ".$y1.'</option></select>'."\n";
       ?>
       </div>
     </div>
     </div>
<div class="row" style="padding-left: 70px;">
 
 <div class="col-md-2 mb-2">                       
  <label class="form-label" >Work Type</label>                   
  </div>
  <div class="col-md-2 mb-2">
    <div class="form-outline ">
    <?php
      echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="'.$res4[0]['type'].'" readonly/>';                   
    ?>
    </div>
  </div>
  
  <div class="col-md-2 mb-2">                       
  <label class="form-label" >Unit ID</label>                   
  </div>
  <div class="col-md-2 mb-2">
    <div class="form-outline ">
    <?php
      echo '<input type="text" name="unitid" class="form-control form-control-lg" value="'.$res5[0]['unit_name'].'" readonly />';                    
    ?>
    </div>
  </div>
  </div>



 <div class="row" style="padding-left: 70px;">
 
 <div class="col-md-2 mb-2">                       
  <label class="form-label">Number Of Holidays</label>                   
  </div>
  <div class="col-md-2 mb-2">
    <div class="form-outline ">
        <?php
        echo '<input type="text" name="hno" class="form-control form-control-lg" value="'.$res3[0]['holiday_no'].'" readonly/>';                    
        ?>
    </div>
  </div>
  
  <div class="col-md-2 mb-2">                       
  <label class="form-label">Number Of Working Days</label>                   
  </div>
  <div class="col-md-2 mb-2">
    <div class="form-outline ">
    <?php
        echo '<input type="text" name="wno" class="form-control form-control-lg" value="'.$res3[0]['total_wd'].'" readonly />';
    ?>                  
    </div>
  </div>
  </div>
  
<?php

if(sizeof($res1)>0)
{
  if($res4[0]['type']=="Spinning")
  {
    $res2=retrieveData("select * from kbk_yarn_variety group by yarn_code",$con);
    //$res2= retrieveData("select * from kbk_yarn_variety group by yarn_code having wef in (select max(wef) from kbk_yarn_variety group by yarn_code having wef<='$hdate')",$con);
?>
<div style="padding-left: 20px;padding-right:20px;">
    <table class="table-bordered">
        <thead>
            <tr>
                <th>Sl NO</th>               
                <th>Artisan Code</th>
                <th>Artisan Name</th>
                <th>Yarn Count</th>
                <th><span style="color:#ff0000">*</span>No. of Hanks</th>
                <th>Welfare Fund</th>
                <th>Welfare Recovery</th>
                <th>Adv. Recovery</th>
                <th>Covid Recovery</th>
                <th>Khadi cr. rec.</th>
                <th>LIC</th>
                <th><span style="color:#ff0000">*</span>Attendance</th>
            </tr>
        </thead>
        <tbody>
<?php
    for($i=0;$i<sizeof($res1);$i++)
    {
        echo '<tr><td>'.strval($i+1).'</td>';
        echo '<td>'.$res1[$i]['artisan_code'].'</td>';
        echo '<td>'.$res1[$i]['artisan_name'].'</td>';

        echo '<td> <select class="form-control-plaintext"  name="yarn'.strval($i+1).'" aria-label="Default select example"  required> <option disabled selected> </option>';
       
        for($j=0;$j<sizeof($res2);$j++)
        {
            echo "<option value='". $res2[$j]['yarn_code'] ."'>" .$res2[$j]['type'] ."</option>";  // displaying data in option menu
        }	
        echo '</select></td>';
        echo '<td><input type="text" name="noh'.strval($i+1).'" class="form-control-plaintext form-control-lg" required /></td>';

         echo '<td> <select class="form-control-plaintext"  name="wfyn'.strval($i+1).'" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';

        echo '<td><input type="text" name="wfrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="adrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="covrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="kcrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="lic'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="att'.strval($i+1).'" class="form-control-plaintext form-control-lg" required /></td></tr>';
    }


?>  



        </tbody>
        <tfoot></tfoot>
    </table> 
</div>



<?php
  }
  if($res4[0]['type']=="Weaving")
  {
    $res2= retrieveData("SELECT weaving_id,weaving_code,type,countable From kbk_weaving_variety group by weaving_code",$con);
    $res6=retrieveData("SELECT w_id,w_type From kbk_weaving_type group by w_type",$con);
   // $f=weaving_entry($res1,$res2,$res6,$unitcode);


      echo '<div style="padding-left: 20px;padding-right:20px;">
    <table class="table-bordered">
      <thead>
            <tr>
                <th>Sl NO</th>               
                <th style="width:120px">Artisan Code</th>
                <th style="width:150px">Artisan Name</th>
                <th style="width:120px">Variety</th>
                <th>No. of Pieces</th>
                <th>Metre</th>
                <th>Welfare Fund</th>
                <th>Welfare Recovery</th>
                <th>Adv. Recovery</th>
                <th>Covid Recovery</th>
                <th>Khadi cr. rec.</th>
                <th>LIC</th>
                <th>Attendance</th>
                <th>weaving Type</th>
            </tr>
        </thead>
        <tbody>';

    for($i=0;$i<sizeof($res1);$i++)
    {
        echo '<tr><td>'.strval($i+1).'</td>';
        echo '<td>'.$res1[$i]['artisan_code'].'</td>';
        echo '<td>'.$res1[$i]['artisan_name'].'</td>';

         
        echo '<td> <select class="form-control-plaintext"  name="variety'.strval($i+1).'" aria-label="Default select example" required> <option disabled selected> </option>';
       
        for($j=0;$j<sizeof($res2);$j++)
        {
            echo "<option value='". $res2[$j]['weaving_code'] ."'>" .$res2[$j]['type'] ."</option>";  // displaying data in option menu
        } 
        echo '</select></td>';
        echo '<td><input type="text" name="nop'.strval($i+1).'" class="form-control-plaintext form-control-lg" value="0"/></td>';

        echo '<td><input type="text" name="metre'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
         echo '<td> <select class="form-control-plaintext"  name="wfyn'.strval($i+1).'" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
        echo '<td><input type="text" name="wfrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="adrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="covrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="kcrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="lic'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td><input type="text" name="att'.strval($i+1).'" class="form-control-plaintext form-control-lg" required /></td>';
         echo '<td> <select class="form-control-plaintext"  name="wt'.strval($i+1).'" aria-label="Default select example" required> <option disabled selected> </option>';
       
        for($k=0;$k<sizeof($res6);$k++)
        {
            echo "<option value='". $res6[$k]['w_type'] ."'>" .$res6[$k]['w_type'] ."</option>";  // displaying data in option menu
        } 


        echo '</td></tr>';
    }





       echo '</tbody>
        <tfoot></tfoot>
    </table> 
</div>';
  }
if($res4[0]['type']=="Preprocessing")
{
   $res2= retrieveData("SELECT rm_code,type From kbk_preprocessing_variety group by rm_code",$con);
   echo '<div style="padding-left: 20px;padding-right:20px;">
    <table class="table-bordered" >
      <thead>
            <tr>
              <th colspan="4"></th>
              <th colspan="4" style="text-align: center;">Sizing</th>
              <th colspan="4" style="text-align: center;">Bobin Winding</th>
              <th colspan="4" style="text-align: center;">Warping</th>
              <th colspan="6"></th>
            </tr>
            <tr>
                <th>Sl NO</th>               
                <th style="width:120px">Artisan Code</th>
                <th style="width:130px">Artisan Name</th>
                <th>Variety Code</th>

                <th>Grey</th>
                <th>Col</th>
                <th>Bl</th>
                <th>Black</th>

                <th>Grey</th>
                <th>Col</th>
                <th>Bl</th>
                <th>Black</th>

                <th>Grey</th>
                <th>Col</th>
                <th>Bl</th>
                <th>Black</th>

                <th>Welfare Fund</th>
                <th>Welfare Recovery</th>
                <th>Adv. Recovery</th>
                <th>Covid Recovery</th>
                <th>Khadi cr. rec.</th>
                <th>LIC</th>
                <th style="width:60px">Attendance</th>
            </tr>
        </thead>
        <tbody>';

   for($i=0;$i<sizeof($res1);$i++)
   {
        echo '<tr><td>'.strval($i+1).'</td>';
        echo '<td>'.$res1[$i]['artisan_code'].'</td>';
        echo '<td>'.$res1[$i]['artisan_name'].'</td>';
        echo '<td style="width:70px">';
        echo "<select class='form-control-plaintext'  name='variety".strval($i+1)."' aria-label='Default select example' required><option disabled selected></option>";
       
        for($j=0;$j<sizeof($res2);$j++)
        {
            echo "<option value='". $res2[$j]['rm_code'] ."'>" .$res2[$j]['type'] ."</option>";  // displaying data in option menu
        } 
        echo '</select></td>';
        echo '<td ><input type="text" name="sgrey'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="scol'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="sbl'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="sblack'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td><input type="text" name="bgrey'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="bcol'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="bbl'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="bblack'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td><input type="text" name="wgrey'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="wcol'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="wbl'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="wblack'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td> <select class="form-control-plaintext"  name="wfyn'.strval($i+1).'" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
        
        echo '<td><input type="text" name="wfrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="adrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="covrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="kcrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="lic'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td><input type="text" name="att'.strval($i+1).'" class="form-control-plaintext form-control-lg"  required /></td></tr>';

   }//loop closing of preprocessing

       echo '</tbody>
        <tfoot></tfoot>
    </table> 
</div>';

}//preprocessing if closing
if($res4[0]['type']=="Twisting")
  {
    $res2=retrieveData("select * from kbk_twisting_variety group by twisting_code",$con);
    //$res2= retrieveData("select * from kbk_yarn_variety group by yarn_code having wef in (select max(wef) from kbk_yarn_variety group by yarn_code having wef<='$hdate')",$con);
?>
<div style="padding-left: 20px;padding-right:20px;">
    <table class="table-bordered">
        <thead>
            <tr>
                <th>Sl NO</th>               
                <th>Artisan Code</th>
                <th>Artisan Name</th>
                <th>Yarn Count</th>
                <th><span style="color:#ff0000">*</span>No. of Hanks</th>
                <th>Welfare Fund</th>
                <th>Welfare Recovery</th>
                <th>Adv. Recovery</th>
                <th>Covid Recovery</th>
                <th>Khadi cr. rec.</th>
                <th>LIC</th>
                <th><span style="color:#ff0000">*</span>Attendance</th>
            </tr>
        </thead>
        <tbody>
<?php
    for($i=0;$i<sizeof($res1);$i++)
    {
        echo '<tr><td>'.strval($i+1).'</td>';
        echo '<td>'.$res1[$i]['artisan_code'].'</td>';
        echo '<td>'.$res1[$i]['artisan_name'].'</td>';

        echo '<td> <select class="form-control-plaintext"  name="yarn'.strval($i+1).'" aria-label="Default select example"  required> <option disabled selected> </option>';
       
        for($j=0;$j<sizeof($res2);$j++)
        {
            echo "<option value='". $res2[$j]['twisting_code'] ."'>" .$res2[$j]['type'] ."</option>";  // displaying data in option menu
        }	
        echo '</select></td>';
        echo '<td><input type="text" name="noh'.strval($i+1).'" class="form-control-plaintext form-control-lg" required /></td>';

         echo '<td> <select class="form-control-plaintext"  name="wfyn'.strval($i+1).'" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';

        echo '<td><input type="text" name="wfrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="adrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="covrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="kcrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="lic'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="att'.strval($i+1).'" class="form-control-plaintext form-control-lg" required /></td></tr>';
    }


?>  



        </tbody>
        <tfoot></tfoot>
    </table> 
</div>



<?php
  }

}// res1 closing






?>
<div class="d-flex justify-content-end pt-3" style="padding-bottom: 20px;padding-right: 10px">
    <button class="btn btn-light btn-lg" onclick="history.go(-2)">Back</button>
    <button type="reset" class="btn btn-light btn-lg ms-2">Reset all</button>
    <input type="submit" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Submit">

</div>



                  </div>          
                </div>
              </div>                
            </div>
          </div>            
        </div>
      </div>
    </section>
 

  </form>
        <!-- End your project here-->
        <script type="text/javascript" src="../admin/validation.js"></script>

<!-- MDB -->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
