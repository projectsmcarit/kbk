<?php
 include('../connection.php');
 include('../kbk_library.php');
 session_start();
 $username=$_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Work Entry</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
  <?php   
        include('iheader.php')
      ?>
    <!-- Start your project here-->
    <form name="spinningentry" action="" method="post"> 
    <header>
      <!-- Navbar -->
      
   
      
      </header>
    
    
    <section class="h-100 bg-dark">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col">
            <div class="card card-registration my-4">
              <div class="row g-0">
               <div class="col-xl-6">
                 <div class="card-body p-md-5 text-black">
                   <h3 class="mb-5 text-uppercase">Monthly Entry</h3>
<div>
                    <div class="row">
                     <div class="col-md-3 mb-3">                       
                       <label class="form-label" for="worktype"><span style="color:#ff0000">*</span>Work Type</label>                   
                     </div>  
                     <div class="col-md-6 mb-4">
                      <select class="form-select" name="worktype" aria-label="Default select example" required>
                       <?php    
                        echo "<option value=''>-- Select work--</option>";           
                        $types=retrieveData("SELECT work_id,type From kbk_work_type",$con);  // Use select query here 
                        for($i=0;$i<sizeof($types);$i++)
                         {
                           echo "<option value='". $types[$i]['work_id'] ."'>" .$types[$i]['type'] ."</option>";  // displaying data in option menu
                         }	
                         
                       ?>  
                      </select>
                     </div>
                    </div>

<div class="row">

    <div class="col-md-3 mb-3">                       
       <label class="form-label" for="unit"><span style="color:#ff0000">*</span>Unit Name</label>                   
    </div>
                      
    <div class="col-md-6 mb-4">
    
        <select class="form-select" name="unit" aria-label="Default select example" required>
         <?php    
             echo "<option value=''>-- Select Unit--</option>";           
             $units= retrieveData("SELECT unit_code,unit_name From kbk_unit where instructor_id=$username",$con);  // Use select query here  
             for($i=0;$i<sizeof($units);$i++)
               {
                echo "<option value='".$units[$i]['unit_code']."'>" .$units[$i]['unit_code']." ".$units[$i]['unit_name'] ."</option>";  // displaying data in option menu
               }	
          ?>  
        </select>
    </div>

</div>

<div class="row">

    <div class="col-md-3 mb-3">                       
       <label class="form-label" for="unit"><span style="color:#ff0000">*</span>Month and Year</label>                   
    </div>
                      
    <div class="col-md-6 mb-4">
    
        <select class="form-select" name="wdate" aria-label="Default select example" required>
         <?php    
                echo "<option value=''>-- Select --</option>";           
                $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $res1=retrieveData("SELECT  h_date FROM kbk_holidays",$con);

                
                for ($i = 0; $i < sizeof($res1); $i++)
                {
                   $m1[$i]= strval(date('m',strtotime($res1[$i]['h_date'])));
                   $y1[$i]= strval(date('Y',strtotime($res1[$i]['h_date'])));
                   echo '<option value="'.$res1[$i]['h_date'].'">'.$months_name[strval($m1[$i])-1]." ".$y1[$i].'</option>'."\n";
                } 
               
                	
          ?>  
        </select>
    </div>

</div>



<div class="row">
  <div class="col-md-5 mb-4">
  </div>
  <div class="col-md-4 mb-4"> 
    <div class="d-flex justify-content-end pt-1">
      <input type="submit"   class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="entry" value="Submit">
    </div>
  </div>
</div>
</div>





    
                    
    
                    
                  </div>          
                </div>


              </div>                
            </div>
          </div>            
        </div>
      </div>
    </section>
 

  </form>

  <?php

    if(isset($_POST['entry']))
    {
       $workid=$_POST['worktype'];
       $unitcode=$_POST['unit'];
       $hdate = $_POST['wdate']; 
       $_SESSION['worktype']=$workid;
       $_SESSION['unit']=$unitcode;
       $_SESSION['wdate']=$hdate;
       $res2=retrieveData("SELECT * from kbk_work_type where work_id=$workid",$con);
     
       $c='kbk_'.strtolower($res2[0]['type']);
       $res1=retrieveData("SELECT * from $c where s_date='$hdate' and unit_code='$unitcode'",$con);

    
       if(sizeof($res1)>0)
       {
        echo "<script>window.location.href='view_work_details_edit.php'</script>";  
       }
       else
       {
        echo "<script>window.location.href='view_month_list.php'</script>";  
       }

    }

  ?>
        <!-- End your project here-->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
