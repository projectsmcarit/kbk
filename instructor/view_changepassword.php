<!DOCTYPE html>
<?php 
    include('../connection.php');
?>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Change Password</title>
    <!-- MDB icon -->
    <!--<link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon" /> -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <script>
		function validatePassword() {
			// Get the password input element
			var passwordInput = document.getElementById("password");

			// Get the password and password error message elements
			var password = passwordInput.value;
			var passwordError = document.getElementById("passwordError");

			// Reset the error message
			passwordError.innerHTML = "";
      const submitButton = document.getElementById("submit");

			// Validate the password
			if (password.length < 8) {
				passwordError.innerHTML = "Password must be at least 8 characters long";
        submitButton.disabled = true;
        return;
			} else if (!/\d/.test(password)) {
				passwordError.innerHTML = "Password must contain at least one digit";
        submitButton.disabled = true;
        return;
			} else if (!/[a-z]/.test(password)) {
				passwordError.innerHTML = "Password must contain at least one lowercase letter";
        submitButton.disabled = true;
        return;
			} else if (!/[A-Z]/.test(password)) {
				passwordError.innerHTML = "Password must contain at least one uppercase letter";
        submitButton.disabled = true;
        return;
			} else if (!/\W/.test(password)) {
				passwordError.innerHTML = "Password must contain at least one special character";
        submitButton.disabled = true;
        return;
			} else{
        passwordError.innerHTML = "";
        submitButton.disabled = false;
        return;
      }
		}
	</script>
  </head>
  <body>

    <!-- Start your project here-->
    <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <div class="col-md-6 col-lg-5 d-none d-md-block">
                  <img
                    src="../images/1.jpg"
                    alt="login form"
                    class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                  />
                </div>
                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                  <div class="card-body p-4 p-lg-5 text-black">
    
                    <form action="changepassword.php" method="post"> 
    
                      <div class="d-flex align-items-center mb-3 pb-1">
                        
                        <span class="h1 fw-bold mb-0">Change Password</span>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" id="form2Example17" name="uname" class="form-control form-control-lg" required="" />
                        <label class="form-label" for="form2Example17">User Name</label>
                      </div>
    
                      <div class="form-outline mb-4">
                        <input type="password" id="form2Example27" name="opsw" class="form-control form-control-lg" required="" />
                        <label class="form-label" for="form2Example27">Old Password</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="password" id="password" name="npsw" class="form-control form-control-lg" required="" onkeyup="validatePassword()" />
                        <span id="passwordError"></span>
                        <label class="form-label" for="form2Example27">New Password</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="password" id="form2Example27" name="cpsw" class="form-control form-control-lg" required="" />
                        <label class="form-label" for="form2Example27">Confirm Password</label>
                      </div>
                        
                      <div class="pt-1 mb-4">
                        <input type="submit" class="btn btn-dark btn-lg btn-block" name="submit" value="Submit">
                      </div>
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
