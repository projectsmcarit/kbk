
<?php
//include('../connection.php');
//include('../kbk_library.php');
// change $res2 - res is the result of yarn variety table , here need weaving variety tbl
function weaving_entry($res1,$res2,$res6,$unitid)
{
	
	echo '<div>
    <table class="table-bordered">
      <thead>
            <tr>
                <th>Sl NO</th>               
                <th>Artisan Code</th>
                <th>Artisan Name</th>
                <th>Variety</th>
                <th>No. of Pieces</th>
                <th>Metre</th>
                <th>Welfare Recovery</th>
                <th>Adv. Recovery</th>
                <th>Covid Recovery</th>
                <th>Khadi cr. rec.</th>
                <th>LIC</th>
                <th>Attendance</th>
                <th>weaving Type</th>
            </tr>
        </thead>
        <tbody>';

    for($i=0;$i<sizeof($res1);$i++)
    {
        echo '<tr><td>'.strval($i+1).'</td>';
        echo '<td>'.$unitid.$res1[$i]['artisan_id'].'</td>';
        echo '<td>'.$res1[$i]['artisan_name'].'</td>';

        echo "<td> <select class='form-control-plaintext'  name='variety".strval($i+1)."' aria-label='Default select example'>
         <option disabled selected> </option>";
       
        for($j=0;$j<sizeof($res2);$j++)
        {
            echo "<option value='". $res2[$j]['weaving_code'] ."'>" .$res2[$j]['type'] ."</option>";  // displaying data in option menu
        }	
        echo '</select></td>';
        echo '<td><input type="text" name="nop'.strval($i+1).'" class="form-control-plaintext form-control-lg" value="0"/></td>';

        echo '<td><input type="text" name="metre'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="wfrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="adrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="covrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="kcrec'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
        echo '<td><input type="text" name="lic'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';

        echo '<td><input type="text" name="att'.strval($i+1).'" class="form-control-plaintext form-control-lg" /></td>';
         echo '<td> <select class="form-control-plaintext"  name="wt'.strval($i+1).'" aria-label="Default select example"> <option disabled selected> </option>';
       
        for($k=0;$k<sizeof($res6);$k++)
        {
            echo "<option value='". $res6[$k]['w_type'] ."'>" .$res6[$k]['w_type'] ."</option>";  // displaying data in option menu
        }	


        echo '</td></tr>';
    }





       echo '</tbody>
        <tfoot></tfoot>
    </table> 
</div>';


return 1;
}


?>
