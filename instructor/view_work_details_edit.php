<?php
include('../connection.php');
include('../kbk_library.php');
//include('view_weaving_list.php');
session_start();

$worktype = $_SESSION['worktype'];
$unit = $_SESSION['unit'];
$hdate = $_SESSION['wdate'];

$res3 = retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'", $con);
$res4 = retrieveData("SELECT type From kbk_work_type where work_id=$worktype", $con);
$res5 = retrieveData("SELECT unit_code,unit_name From kbk_unit where unit_code='$unit'", $con);

$unitdemo = $res5[0]['unit_name'];
$_SESSION['unitcode'] = $unit;

$a = strtolower($res4[0]['type']);
$b = "kbk_" . $a;
$res1 = retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and status=1 and artisan_id in (select artisan_id from $b where s_date='$hdate')", $con);
$_SESSION['i'] = $res1;

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include('iheader.php');
  ?>

  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <?php
  echo '<title>' . $res4[0]['type'] . '</title>';
  ?>
  <!-- MDB icon -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />
  <!-- Bootstrap 4.3 -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 2px;
      padding-bottom: 2px;
      text-align: left;
      background-color: #04AA6D;
      color: white;
    }
  </style>
  <!-- Bootstrap 4.3 javascript -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <script type="text/javascript" src="../js/mdb.min.js"></script>

  <!-- Start your project here-->

  <form name="spinninglistrep" action="work_details_edit.php" method="POST">

    <section class="" style="padding-top: 30px;">
      <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col">
            <div class="">
              <div class="row g-0">
                <div id="container_content">
                  <div class="text-black">
                    <h3 class="mb-5 text-uppercase">Monthly <?php echo $res4[0]['type']; ?> Report</h3>

                    <div class="row">

                      <div class="col-md-2 mb-2">
                        <label class="form-label">Month and Year</label>
                      </div>
                      <div class="col-md-3 mb-2">
                        <div class="form-outline ">
                          <?php
                          echo '<select class="form-select" name="hdate" >';
                          $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                          $m1 = strval(date('m', strtotime($hdate)));
                          $y1 = strval(date('Y', strtotime($hdate)));
                          echo '<option value="' . $hdate . '">' . $months_name[strval($m1) - 1] . " " . $y1 . '</option></select>' . "\n";
                          $htmldata = $months_name[strval($m1) - 1];
                          $_SESSION["mydata4"] = $htmldata;
                          $htmldata = $y1;
                          $_SESSION["mydata5"] = $htmldata;

                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="row">

                      <div class="col-md-2 mb-2">
                        <label class="form-label">Work Type</label>
                      </div>
                      <div class="col-md-3 mb-2">
                        <div class="form-outline ">
                          <?php
                          echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="' . $res4[0]['type'] . '" readonly/>';
                          $htmldata = $res4[0]['type'];
                          $_SESSION["mydata2"] = $htmldata;
                          ?>
                        </div>
                      </div>

                      <div class="col-md-2 mb-2">
                        <label class="form-label">Unit ID</label>
                      </div>
                      <div class="col-md-3 mb-2">
                        <div class="form-outline ">
                          <?php
                          echo '<input type="text" name="unitid" class="form-control form-control-lg" value="' . $unitdemo . '" readonly />';
                          $htmldata = $unitdemo;
                          $_SESSION["mydata1"] = $htmldata;

                          ?>
                        </div>
                      </div>
                    </div>



                    <div class="row">

                      <div class="col-md-2 mb-2">
                        <label class="form-label">Number Of Holidays</label>
                      </div>
                      <div class="col-md-3 mb-2">
                        <div class="form-outline ">
                          <?php
                          echo '<input type="text" name="hno" class="form-control form-control-lg" value="' . $res3[0]['holiday_no'] . '" readonly/>';
                          $_SESSION["holy_days"] = $res3[0]['holiday_no'];
                          ?>
                        </div>
                      </div>

                      <div class="col-md-2 mb-2">
                        <label class="form-label">Number Of Working Days</label>
                      </div>
                      <div class="col-md-3 mb-2">
                        <div class="form-outline ">
                          <?php
                          echo '<input type="text" name="wno" class="form-control form-control-lg" value="' . $res3[0]['total_wd'] . '" readonly />';
                          $_SESSION["working_days"] = $res3[0]['total_wd'];
                          ?>
                        </div>
                      </div>
                    </div>




                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div style="padding: 0px 50px 30px 40px;">

        <?php
        if ($res4[0]['type'] == "Spinning") {
          $sum = array(
            'no' => 0,
            'meter'    => 0,
            'wfrec' => 0,
            'adrec' => 0

          );
          $res13 = retrieveData("SELECT * from kbk_yarn_variety group by yarn_code", $con);
          if (sizeof($res1) > 0) {
        ?>

            <table class="table-bordered" id="customers">
              <thead>
                <tr>
                  <th>SI No.</th>
                  <th>Artisan Code</th>
                  <th style="width:130px">Artisan Name</th>
                  <th style="width:100px">Variety</th>
                  <th style="width:100px">No. of Hanks</th>


                  <th style="width:100px">Welfare Fund</th>
                  <th style="width:100px">w/f Recovery</th>
                  <th style="width:100px">Adv Recovery</th>
                  <th style="width:100px">Khadi Cr. Rec.</th>
                  <th style="width:100px">Covid adv.</th>
                  <th style="width:100px">LIC</th>


                  <!-- <th scope="col">ESI Contr</th>
                          <th scope="col">Yarn Incentive</th>
                          <th scope="col">Minimum Wages</th>
                          <th scope="col">DA Days</th>
                          <th scope="col">DA Wages</th>
                          <th scope="col">Total Minimum Wages</th>
                          <th scope="col">Net Minimum Wages</th> -->
                  <th style="width:100px">Attendance</th>

                </tr>
              </thead>
              <tbody>
                <?php

                $htmldata = "";
                for ($i = 0; $i < sizeof($res1); $i++) {

                  echo '<tr style="text-align:center"><td>' . strval($i + 1) . '</td>';
                  echo '<td style="text-align:left">' . $res1[$i]['artisan_code'] . '</td>';
                  echo '<td style="text-align:left">' . $res1[$i]['artisan_name'] . '</td>';

                  $temp1 = strval($res1[$i]['artisan_id']);
                  $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id,status From kbk_spinning where s_date='$hdate' and artisan_id=$temp1", $con);
                  $temp2 = $res6[0]['yarn_id'];
                  $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=1", $con);
                  $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'", $con);

                  echo '<td> <select class="form-control-plaintext"  name="yarn' . strval($i + 1) . '" aria-label="Default select example"> <option value="' . $res7[0]['yarn_code'] . '" selected>' . $res7[0]['type'] . '</option>';
                  for ($j = 0; $j < sizeof($res13); $j++) {
                    echo "<option value='" . $res13[$j]['yarn_code'] . "'>" . $res13[$j]['type'] . "</option>";  // displaying data in option menu
                  }
                  echo '</select></td>';
                  echo '<td><input type="text" name="noh' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res6[0]['noh'] . '" required /></td>';
                  if ($res8[0]['wf_yes_no'] == 1) {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
                  } else {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option value="0" selected>NO</option><option value="1">YES</option></select></td>';
                  }
                  $wfund = 0;
                  if ($res8[0]['wf_yes_no'] == 1) {
                    $wfund = $res7[0]['wf'];
                  }
                  $sar1  = array(
                    'noh'    => $res6[0]['noh'],
                    'sliver' => $res7[0]['sliver'],
                    'rate'   => $res7[0]['rate'],
                    'wf'     => $wfund,
                    'wfrec'  => $res8[0]['wf_rec'],
                    'adrec'  => $res8[0]['ad_rec'],
                    'kcrec'  => $res8[0]['credit_rec'],
                    'covrec' => $res8[0]['cov_rec'],
                    'lic'    => $res8[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd'],
                    'sv'     => $res7[0]['sliver_value'],
                    'val'    => $res7[0]['value'],
                  );

                  $calc1 = spinningWageCalculation($sar1);

                  $sar2 = array(
                    'noh'     => $res6[0]['noh'],
                    'esi'     => $res7[0]['esi'],
                    'yi'      => $res7[0]['yi'],
                    'mw'      => $res7[0]['mw'],
                    'target'  => $res7[0]['target'],
                    'dawages' => $res3[0]['da_wages'],
                    'wage'    => $calc1['wage'],
                  );

                  $sum['no'] = $sum['no'] + $res6[0]['noh'];
                  $sum['wfrec'] = $sum['wfrec'] + $calc1['wfrec'];
                  $sum['adrec'] = $sum['adrec'] + $calc1['adrec'];

                  $htmldata .= '<tr><td>' . strval($i + 1) . '</td>';
                  $htmldata .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                  $htmldata .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                  $htmldata .= '<td>' . $res7[0]['type'] . '</td>';
                  $htmldata .= '<td>' . $res6[0]['noh'] . '</td>';
                  //$htmldata.='<td>'.$res7[0]['rate'].'</td>';
                  //$htmldata.='<td>'.$calc1['val'].'</td>';
                  //$htmldata.='<td>'.$calc1['sv'].'</td>';
                  //$htmldata.= '<td>'.$calc1['sliver'].'</td>';
                  //$htmldata.='<td>'.$calc1['wage'].'</td>';
                  //$htmldata.= '<td>'.$res8[0]['wf_yes_no'].'</td>';
                  if ($res8[0]['wf_yes_no'] == 1)
                    $htmldata .= '<td>' . $res8[0]['wf_yes_no'] = "YES" . '</td>';
                  else
                    $htmldata .= '<td>' . $res8[0]['wf_yes_no'] = "NO" . '</td>';
                  $htmldata .=  '<td>' . $calc1['wfrec'] . '</td>';
                  $htmldata .=  '<td>' . $calc1['adrec'] . '</td>';
                  $htmldata .=  '<td>' . $calc1['kcrec'] . '</td>';
                  $htmldata .= '<td>' . $calc1['covrec'] . '</td>';
                  $htmldata .= '<td>' . $calc1['lic'] . '</td>';
                  //$htmldata.= '<td>'.$calc1['netwages'].'</td>';
                  //$htmldata.=  '<td>'.$calc1['hdwage'].'</td>';
                  //$htmldata.= '<td>'.$calc1['totwages'].'</td>';
                  $htmldata .= '<td>' . $res6[0]['attendance'] . '</td>';
                  $htmldata .=  '</tr>';



                  echo '<td><input type="text" name="wfrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['wfrec'] . '"/></td>';
                  echo '<td><input type="text" name="adrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['adrec'] . '"/></td>';
                  echo '<td><input type="text" name="covrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['covrec'] . '"/></td>';
                  echo '<td><input type="text" name="kcrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['kcrec'] . '"/></td>';
                  echo '<td><input type="text" name="lic' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['lic'] . '"/></td>';
                  echo '<td><input type="text" name="att' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res6[0]['attendance'] . '" required /></td>';
                  echo '</tr>';
                }
                echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Total</td>';
                echo "<td>" . $sum['no'] . "</td>";
                echo "<td> </td>";
                echo "<td>" . $sum['wfrec'] . "</td>";
                echo "<td>" . $sum['adrec'] . "</td>";
                echo "<td colspan='4'> </td></tr>";

                echo "<td> </td></tr>";
                $htmldata .= '<tr style="text-align: center;"><td colspan="4"><b>Unit Total</b></td>';
                $htmldata .= "<td><b>" . $sum['no'] . "</b></td>";
                $htmldata .= "<td> </td>";
                //$htmldata.="<td><b>".$sum['val']."</b></td>";
                //$htmldata.="<td><b>".$sum['pr']."</b></td>";
                //$htmldata.= "<td><b>".$sum['sliver']."</b></td>";
                //$htmldata.= "<td><b>".$sum['wage']."</b></td>";
                //$htmldata.= "<td><b>".$sum['wf']."</b></td>";
                $htmldata .= "<td><b>" . $sum['wfrec'] . "</b></td>";
                $htmldata .= "<td><b>" . $sum['adrec'] . "</b></td>";
                // $htmldata.= "<td><b>".$sum['kcrec']."</b></td>";
                //$htmldata.= "<td><b>".$sum['covrec']."</b></td>";
                //$htmldata.= "<td><b>".$sum['lic']."</b></td>";
                //$htmldata.= "<td><b>".$sum['nw']."</b></td>";
                // $htmldata.= "<td><b>".$sum['hw']."</b></td>";
                //$htmldata.= "<td><b>".$sum['tw']."</b></td>";

                $htmldata .= "<td></td><td></td><td></td><td></td></tr>";
                $_SESSION["mydata"] = $htmldata;
                ?>



              </tbody>
              <tfoot></tfoot>
            </table>



          <?php


          } // if closing of if(sizeof($res1)>0)

        } // if closing of work type spinning
        if ($res4[0]['type'] == "Weaving") {
          $sum = array(
            'no' => 0,
            'meter'    => 0,
            'wfrec' => 0,
            'adrec' => 0

          ); {
            if (sizeof($res1) > 0) {
              echo '<table class="table-bordered" id="customers">

        <thead>
                        <tr>
                          <th >SI No.</th>
                          <th >Artisan Code</th>
                          <th style="min-width:130px">Artisan Name</th>
                          <th style="min-width:150px">Variety</th>
                          <th style="min-width:80px">No. of Pieces</th>
                          <th style="min-width:80px">Metre</th>    
                          <th style="min-width:80px">W/F Fund</th>                         
                          <th style="min-width:80px">w/f Recovery</th>
                          <th style="min-width:80px">Adv Recovery</th>
                          <th style="min-width:80px">Khadi Cr. Rec.</th>
                          <th style="min-width:80px">Covid adv.</th>
                          <th style="min-width:80px">LIC</th>
                          <th style="min-width:80px">Attendance</th>
                          <th style="min-width:130px">Weaving Type</th>
                        </tr>
                      </thead>
        <tbody>';
              $htmldata1 = "";
              $state = 0;
              for ($i = 0; $i < sizeof($res1); $i++) {
                $a_id = $res1[$i]['artisan_id'];
                // echo "<script> alert('" . $hdate . "'); </script>";
                $loop = retrieveData("SELECT wvg_id, artisan_id FROM kbk_weaving WHERE artisan_id = $a_id and s_date = '$hdate'", $con);
                // $state = 0;
                for ($l = 0; $l < sizeof($loop); $l++) {
                  $res2 = retrieveData("SELECT weaving_id,weaving_code,type,countable From kbk_weaving_variety group by weaving_code", $con);
                  $res6 = retrieveData("SELECT w_id,w_type From kbk_weaving_type group by w_type", $con);
                  // $temp1 = strval($res1[$i]['artisan_id']);
                  $temp1 = $loop[$l]['artisan_id'];
                  $wvg_id = $loop[$l]['wvg_id'];
                  // echo '<script> alert("' . $wvg_id . '"); </script>';
                  $res9 = retrieveData("SELECT attendance, nop, mtr, weaving_id, w_id, status FROM kbk_weaving WHERE wvg_id=$wvg_id AND s_date='$hdate' AND artisan_id=$temp1", $con);
                  $temp2 = $res9[0]['weaving_id'];
                  $temp3 = intval($res9[0]['w_id']);

                  $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type FROM kbk_weaving_type WHERE w_id=$temp3", $con);
                  $res11 = retrieveData("SELECT weaving_code,type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft FROM kbk_weaving_variety WHERE weaving_id=$temp2", $con);
                  $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no FROM kbk_deduction WHERE d_date='$hdate' AND artisan_id=$temp1 AND work_id=2", $con);
                  $wfund = 0;
                  if ($res12[0]['wf_yes_no'] == 1) {
                    $wfund = $res11[0]['wf'];
                  }
                  $war1 = [
                    'nop' => $res9[0]['nop'],
                    'metre'  => $res9[0]['mtr'],
                    'wage'   => $res10[0]['w_wages'],
                    'wf'     => $wfund,
                    'wfrec'  => $res12[0]['wf_rec'],
                    'adrec'  => $res12[0]['ad_rec'],
                    'kcrec'  => $res12[0]['credit_rec'],
                    'covrec' => $res12[0]['cov_rec'],
                    'lic'    => $res12[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd'],
                    'sqmtr'  => $res11[0]['width'],
                    'prime'  => $res11[0]['prime_cost'],
                    'value'  => $res11[0]['rate'],
                  ];
                  $calc3 = weavingWageCalculation($war1);

                  $war2 = [
                    'metre'   => $res9[0]['mtr'],
                    'esi'     => $res11[0]['esi'],
                    'warpyi'  => $res10[0]['warp_yi'],
                    'weftyi'  => $res10[0]['weft_yi'],
                    'mw'      => $res10[0]['min_wages'],
                    'target'  => $res10[0]['target'],
                    'dawages' => $res3[0]['da_wages'],
                    'wage'    => $calc3['wage'],
                  ];

                  $sum['no'] = $sum['no'] + $res9[0]['nop'];
                  $sum['meter'] = $sum['meter'] + $res9[0]['mtr'];
                  $sum['wfrec'] = $sum['wfrec'] + $calc3['wfrec'];
                  $sum['adrec'] = $sum['adrec'] + $calc3['adrec'];


                  echo '<tr  style="text-align:center"><td>' . strval($i + 1) . '</td>';
                  echo '<td style="text-align:left">' . $res1[$i]['artisan_code'] . '</td>';
                  echo '<td style="text-align:left">' . $res1[$i]['artisan_name'] . '</td>';
                  // echo '<td style="text-align:left">'.$res11[0]['type'].'</td>';
                  // echo '<td>'.$res9[0]['nop'].'</td>';
                  // echo '<td>'.$res9[0]['mtr'].'</td>';
                  // echo '<td>'.$calc3['sqmtr'].'</td>';
                  // echo '<td style="text-align:left">'.$res10[0]['w_type'].'</td>';
                  // echo '<td>'.$res10[0]['w_wages'].'</td>';
                  // echo '<td>'.$calc3['wage'].'</td>';
                  // echo '<td>'.$calc3['wf'].'</td>';
                  // echo '<td>'.$calc3['wfrec'].'</td>';
                  // echo '<td>'.$calc3['adrec'].'</td>';
                  // echo '<td>'.$calc3['kcrec'].'</td>';
                  // echo '<td>'.$calc3['covrec'].'</td>';
                  // echo '<td>'.$calc3['lic'].'</td>';
                  // echo '<td>'.$calc3['netwages'].'</td>';
                  // echo '<td>'.$calc3['hdwage'].'</td>';
                  // echo '<td>'.$calc3['totwages'].'</td>';

                  // echo '<td>'.$res9[0]['attendance'].'</td>';
                  // echo '</tr>';
                  // $lp = 0;
                  // echo '<input type="hidden" name="wvg_id" value="' . $wvg_id . '"/>';
                  echo "<td> <select class='form-control-plaintext'  name='variety" . strval($state + 1) . "' aria-label='Default select example'>";
                  echo '<option value="' . $res11[0]['weaving_code'] . '" selected>' . $res11[0]['type'] . '</option>';
                  for ($j = 0; $j < sizeof($res2); $j++) {
                    echo "<option value='" . $res2[$j]['weaving_code'] . "'>" . $res2[$j]['type'] . "</option>";  // displaying data in option menu
                  }
                  echo '</select></td>';
                  echo '<td><input type="hidden" name="wvg_id' . strval($state + 1) . '" value="' . $wvg_id . '"/><input type="text" name="nop' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res9[0]['nop'] . '"/></td>';

                  echo '<td><input type="text" name="metre' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res9[0]['mtr'] . '"/></td>';

                  if ($res12[0]['wf_yes_no'] == 1) {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
                  } else {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option value="0" selected>NO</option><option value="1">YES</option></select></td>';
                  }


                  echo '<td><input type="text" name="wfrec' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc3['wfrec'] . '"/></td>';
                  echo '<td><input type="text" name="adrec' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc3['adrec'] . '"/></td>';
                  echo '<td><input type="text" name="covrec' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc3['covrec'] . '"/></td>';
                  echo '<td><input type="text" name="kcrec' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc3['kcrec'] . '"/></td>';
                  echo '<td><input type="text" name="lic' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc3['lic'] . '"/></td>';

                  echo '<td><input type="text" name="att' . strval($state + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res9[0]['attendance'] . '"/></td>';
                  echo '<td> <select class="form-control-plaintext"  name="wt' . strval($i + 1) . '" aria-label="Default select example">';
                  echo "<option value='" . $res10[0]['w_type'] . "' selected>" . $res10[0]['w_type'] . "</option>'";

                  for ($k = 0; $k < sizeof($res6); $k++) {
                    echo "<option value='" . $res6[$k]['w_type'] . "'>" . $res6[$k]['w_type'] . "</option>";  // displaying data in option menu
                  }



                  echo '</td></tr>';

                  $htmldata1 .= '<tbody><tr><td style="overflow: hidden;">' . strval($i + 1) . '</td>';
                  $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res1[$i]['artisan_code'] . '</td>';
                  $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res1[$i]['artisan_name'] . '</td>';
                  $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res11[0]['type'] . '</td>';
                  $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res9[0]['nop'] . '</td>';
                  $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res9[0]['mtr'] . '</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['sqmtr'].'</td>';
                  //$htmldata1.= '<td colspan="4" style="overflow: hidden;">'.$res10[0]['w_type'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$res10[0]['w_wages'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['prime'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['value'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['wage'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$res12[0]['wf_yes_no'].'</td>';
                  if ($res12[0]['wf_yes_no'] == 1)
                    $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res12[0]['wf_yes_no'] = "YES" . '</td>';
                  else
                    $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res12[0]['wf_yes_no'] = "NO" . '</td>';
                  $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['wfrec'] . '</td>';
                  $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['adrec'] . '</td>';
                  $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['kcrec'] . '</td>';
                  $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['covrec'] . '</td>';
                  $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['lic'] . '</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['netwages'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['hdwage'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['totwages'].'</td>';
                  $htmldata1 .=  '<td colspan="2" style="overflow: hidden;">' . $res9[0]['attendance'] . '</td>';
                  $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res10[0]['w_type'] . '</td>';
                  $htmldata1 .=  '</tr></tbody>';
                  $state = $state + 1;
                }
              }
            }
            echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
            echo "<td>" . $sum['no'] . "</td>";
            echo "<td>" . $sum['meter'] . "</td>";
            echo "<td> </td>";
            echo "<td>" . $sum['wfrec'] . "</td>";
            echo "<td>" . $sum['adrec'] . "</td>";
            echo "<td colspan='5'> </td></tr>";
            $htmldata1 .= '<tr style="text-align: center;"><td colspan="13"><b>Unit Total</b></td>';

            $htmldata1 .= "<td colspan='2'><b>" . $sum['no'] . "</b></td>";
            $htmldata1 .= "<td colspan='2'><b>" . $sum['meter'] . "</b></td>";
            //$htmldata1.= "<td colspan='2'><b>".$sum['sqm']."</b></td>";
            //$htmldata1.= "<td colspan='6'></td>";
            // $htmldata1.= "<td colspan='2'><b>".$sum['pr']."</b></td>";
            //$htmldata1.= "<td colspan='2'><b>".$sum['val']."</b></td>";
            //$htmldata1.= "<td colspan='2'><b>".$sum['wage']."</b></td>";
            //$htmldata1.= "<td colspan='2'><b>".$sum['wf']."</b></td>";
            $htmldata1 .= "<td colspan='2'> </td> ";
            $htmldata1 .= "<td><b>" . $sum['wfrec'] . "</b></td>";
            $htmldata1 .= "<td><b>" . $sum['adrec'] . "</b></td>";
            //$htmldata1.= "<td><b>".$sum['kcrec']."</b></td>";
            //$htmldata1.= "<td><b>".$sum['covrec']."</b></td>";
            //$htmldata1.= "<td><b>".$sum['lic']."</b></td>";
            $htmldata1 .= "<td colspan='11'></td></tr></tbody><tfoot></tfoot></table>";
            $_SESSION['mydata3'] = $htmldata1;
            // echo "<td></td></tr>";
            echo "</tbody>
        <tfoot></tfoot>
    </table>";
          }
        } // weaving end

        if ($res4[0]['type'] == "Preprocessing") {
          $htmldata2 = "";
          $yarnsum = array(
            'warp_grey'    => 0,
            'warp_colour'  => 0,
            'warp_black'   => 0,
            'warp_bl'      => 0,
            'weft_grey'    => 0,
            'weft_colour'  => 0,
            'weft_black'   => 0,
            'weft_bl'      => 0,
          );
          $rmsum = array(
            's_grey'    => 0,
            's_colour'  => 0,
            's_black'   => 0,
            's_bl'      => 0,
            'b_grey'    => 0,
            'b_colour'  => 0,
            'b_black'   => 0,
            'b_bl'      => 0,
            'w_grey'    => 0,
            'w_colour'  => 0,
            'w_black'   => 0,
            'w_bl'      => 0,
          );
          $sum = array(
            'wfrec' => 0,
            'adrec' => 0
          );
          if (sizeof($res1) > 0) {
          ?>
            <table class="table-bordered" id="customers">

              <thead>
                <tr>
                  <th colspan="4" style="min-width: center"></th>
                  <th colspan="4" style="text-align:center">Sizing</th>
                  <th colspan="4" style="text-align:center">Bobin Winding</th>
                  <th colspan="4" style="text-align:center">Warping</th>
                  <th colspan="7"></th>
                </tr>
                <tr>
                  <th scope="col">SI No.</th>
                  <th scope="col">Artisan Code</th>
                  <th style="min-width: 130px">Artisan Name</th>
                  <th style="min-width:80px">Variety</th>

                  <th style="min-width:80px">Grey</th>
                  <th style="min-width:80px">Colour</th>
                  <th style="min-width:80px">Bl</th>
                  <th style="min-width:80px">Black</th>


                  <th style="min-width:80px">Grey</th>
                  <th style="min-width:80px">Colour</th>
                  <th style="min-width:80px">Bl</th>
                  <th style="min-width:80px">Black</th>


                  <th style="min-width:80px">Grey</th>
                  <th style="min-width:80px">Colour</th>
                  <th style="min-width:80px">Bl</th>
                  <th style="min-width:80px">Black</th>


                  <th style="min-width:80px">W/F Fund</th>
                  <th style="min-width:80px">W/F Rec.</th>
                  <th style="min-width:80px">Adv Rec.</th>
                  <th style="min-width:80px">Khadi Cr. Rec.</th>
                  <th style="min-width:80px">Covid adv. Rec.</th>
                  <th style="min-width:80px">LIC</th>


                  <th style="min-width:80px">Attendance</th>

                </tr>
              </thead>
              <tbody>

                <?php

                for ($i = 0; $i < sizeof($res1); $i++) {


                  $artid = intval($res1[$i]['artisan_id']);

                  $res6 =  retrieveData("SELECT wages,attendance,pre_id,preprocessing_id,status From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'", $con);

                  $preid = intval($res6[0]['pre_id']);
                  $preprocessingid = intval($res6[0]['preprocessing_id']);
                  $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3", $con);
                  $res9 = retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid", $con);
                  $res10 = retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid", $con);

                  $par1 = [
                    'grey'  => $res10[0]['s_grey'],
                    'col'   => $res10[0]['s_col'],
                    'bl'    => $res10[0]['s_bl'],
                    'black' => $res10[0]['s_black'],
                  ];
                  $sizetotnoh = preprocessingTypeWage($par1);
                  $par1 = [
                    'grey'  => $res10[0]['b_grey'],
                    'col'   => $res10[0]['b_col'],
                    'bl'    => $res10[0]['b_bl'],
                    'black' => $res10[0]['b_black'],
                  ];
                  $bobintotnoh = preprocessingTypeWage($par1);
                  $par1 = [
                    'grey'  => $res10[0]['w_grey'],
                    'col'   => $res10[0]['w_col'],
                    'bl'    => $res10[0]['w_bl'],
                    'black' => $res10[0]['w_black'],
                  ];
                  $warptotnoh = preprocessingTypeWage($par1);

                  $wfund = 0;
                  if ($res8[0]['wf_yes_no'] == 1) {
                    $wfund = $res9[0]['wf'];
                  }

                  $par2 = [
                    'snoh'  => $sizetotnoh,
                    'bnoh'  => $bobintotnoh,
                    'wnoh'  => $warptotnoh,
                    'swage' => $res9[0]['s_wages'],
                    'bwage' => $res9[0]['b_wages'],
                    'wwage' => $res9[0]['w_wages'],
                    'wf'    => $wfund,
                    'wfrec' => $res8[0]['wf_rec'],
                    'adrec' => $res8[0]['ad_rec'],
                    'kcrec' => $res8[0]['credit_rec'],
                    'covrec' => $res8[0]['cov_rec'],
                    'lic'   => $res8[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd'],
                  ];

                  $calc7 = preprocessingWageCalculation($par2);

                  $rmsum['s_grey']  = $rmsum['s_grey'] + $res10[0]['s_grey'];
                  $rmsum['s_colour'] = $rmsum['s_colour'] + $res10[0]['s_col'];
                  $rmsum['s_black'] = $rmsum['s_black'] + $res10[0]['s_black'];
                  $rmsum['s_bl']    = $rmsum['s_bl'] + $res10[0]['s_bl'];
                  //$rmsum['s_tot']   =$rmsum['s_tot']+$sizetotnoh;
                  //$rmsum['s_wage']  =$rmsum['s_wage']+$calc7['swage'];
                  //$rmsum['s_yi']    =$rmsum['s_yi']+$calc8['syi'];
                  $rmsum['b_grey']  = $rmsum['b_grey'] + $res10[0]['b_grey'];
                  $rmsum['b_colour'] = $rmsum['b_colour'] + $res10[0]['b_col'];
                  $rmsum['b_black'] = $rmsum['b_black'] + $res10[0]['b_black'];
                  $rmsum['b_bl']    = $rmsum['b_bl'] + $res10[0]['b_bl'];
                  //$rmsum['b_tot']   =$rmsum['b_tot']+$bobintotnoh;
                  //$rmsum['b_wage']  =$rmsum['b_wage']+$calc7['bwage'];
                  //$rmsum['b_yi']    =$rmsum['b_yi']+$calc8['byi'];
                  $rmsum['w_grey']  = $rmsum['w_grey'] + $res10[0]['w_grey'];
                  $rmsum['w_colour'] = $rmsum['w_colour'] + $res10[0]['w_col'];
                  $rmsum['w_black'] = $rmsum['w_black'] + $res10[0]['w_black'];
                  $rmsum['w_bl']    = $rmsum['w_bl'] + $res10[0]['w_bl'];
                  //$rmsum['w_tot']   =$rmsum['w_tot']+$warptotnoh;
                  //$rmsum['w_wage']  =$rmsum['w_wage']+$calc7['wwage'];
                  //$rmsum['w_yi']    =$rmsum['w_yi']+$calc8['wyi'];
                  //$sum['wf']=$sum['wf']+$calc7['wf'];
                  $sum['wfrec'] = $sum['wfrec'] + $calc7['wfrec'];
                  $sum['adrec'] = $sum['adrec'] + $calc7['adrec'];


                  echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                  echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';
                  echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';

                  $res13 = retrieveData("SELECT rm_code,type From kbk_preprocessing_variety group by rm_code", $con);
                  echo "<td><select class='form-control-plaintext'  name='variety" . strval($i + 1) . "' aria-label='Default select example' required><option selected value=" . $res9[0]['rm_code'] . ">" . $res9[0]['type'] . "</option>";

                  for ($j = 0; $j < sizeof($res13); $j++) {
                    echo "<option value='" . $res13[$j]['rm_code'] . "'>" . $res13[$j]['type'] . "</option>";
                    //$htmldata2.= '<td colspan="4" style="overflow: hidden;">'.$res10[0]['type'].'</td>'; // displaying data in option menu
                  }
                  echo '</select></td>';
                  echo '<td ><input type="text" name="sgrey' . strval($i + 1) . '" value="' . $res10[0]['s_grey'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="scol' . strval($i + 1) . '" value="' . $res10[0]['s_col'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="sbl' . strval($i + 1) . '" value="' . $res10[0]['s_bl'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="sblack' . strval($i + 1) . '" value="' . $res10[0]['s_black'] . '" class="form-control-plaintext form-control-lg" /></td>';

                  echo '<td ><input type="text" name="bgrey' . strval($i + 1) . '" value="' . $res10[0]['b_grey'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="bcol' . strval($i + 1) . '" value="' . $res10[0]['b_col'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="bbl' . strval($i + 1) . '" value="' . $res10[0]['b_bl'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="bblack' . strval($i + 1) . '" value="' . $res10[0]['b_black'] . '" class="form-control-plaintext form-control-lg" /></td>';

                  echo '<td ><input type="text" name="wgrey' . strval($i + 1) . '" value="' . $res10[0]['w_grey'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="wcol' . strval($i + 1) . '" value="' . $res10[0]['w_col'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="wbl' . strval($i + 1) . '" value="' . $res10[0]['w_bl'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="wblack' . strval($i + 1) . '" value="' . $res10[0]['w_black'] . '" class="form-control-plaintext form-control-lg" /></td>';


                  if ($res8[0]['wf_yes_no'] == 1) {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
                  } else {
                    echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option value="0" selected>NO</option><option value="1">YES</option></select></td>';
                  }
                  echo '<td><input type="text" name="wfrec' . strval($i + 1) . '" value="' . $res8[0]['wf_rec'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="adrec' . strval($i + 1) . '" value="' . $res8[0]['ad_rec'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="covrec' . strval($i + 1) . '" value="' . $res8[0]['credit_rec'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="kcrec' . strval($i + 1) . '" value="' . $res8[0]['cov_rec'] . '" class="form-control-plaintext form-control-lg" /></td>';
                  echo '<td><input type="text" name="lic' . strval($i + 1) . '" value="' . $res8[0]['lic'] . '" class="form-control-plaintext form-control-lg" /></td>';

                  echo '<td><input type="text" name="att' . strval($i + 1) . '" value="' . $res6[0]['attendance'] . '" class="form-control-plaintext form-control-lg"  required /></td></tr>';

                  $htmldata2 .= '<tbody><tr><td>' . strval($i + 1) . '</td>';
                  $htmldata2 .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                  $htmldata2 .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                  $htmldata2 .= '<td>' . $res9[0]['type'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['s_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['s_col'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['s_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['s_black'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['b_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['b_col'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['b_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['b_black'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['w_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['w_col'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['w_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $res10[0]['w_black'] . '</td>';
                  if ($res8[0]['wf_yes_no'] == 1)
                    $htmldata2 .= '<td>' . $res8[0]['wf_yes_no'] = "YES" . '</td>';
                  else
                    $htmldata2 .= '<td>' . $res8[0]['wf_yes_no'] = "NO" . '</td>';
                  $htmldata2 .= '<td>' . $res8[0]['wf_rec'] . '</td>';
                  $htmldata2 .= '<td>' . $res8[0]['ad_rec'] . '</td>';
                  $htmldata2 .= '<td>' . $res8[0]['credit_rec'] . '</td>';
                  $htmldata2 .= '<td>' . $res8[0]['cov_rec'] . '</td>';
                  $htmldata2 .= '<td>' . $res8[0]['lic'] . '</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['netwages'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['hdwage'].'</td>';
                  //$htmldata1.= '<td colspan="2" style="overflow: hidden;">'.$calc3['totwages'].'</td>';
                  $htmldata2 .= '<td>' . $res6[0]['attendance'] . '</td>';
                  //$htmldata2.=  '</tr></tbody>';



                }
                echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
                echo '<td>' . $rmsum['s_grey'] . '</td>';
                echo '<td>' . $rmsum['s_colour'] . '</td>';
                echo '<td>' . $rmsum['s_bl'] . '</td>';
                echo '<td>' . $rmsum['s_black'] . '</td>';
                //echo '<td>'.$rmsum['s_tot'].'</td>';
                //echo '<td></td>';
                //echo '<td>'.$rmsum['s_wage'].'</td>';
                //echo '<td>'.$rmsum['s_yi'].'</td>';
                echo '<td>' . $rmsum['b_grey'] . '</td>';
                echo '<td>' . $rmsum['b_colour'] . '</td>';
                echo '<td>' . $rmsum['b_bl'] . '</td>';
                echo '<td>' . $rmsum['b_black'] . '</td>';
                //echo '<td>'.$rmsum['b_tot'].'</td>';
                //echo '<td></td>';
                //echo '<td>'.$rmsum['b_wage'].'</td>';
                //echo '<td>'.$rmsum['b_yi'].'</td>';
                echo '<td>' . $rmsum['w_grey'] . '</td>';
                echo '<td>' . $rmsum['w_colour'] . '</td>';
                echo '<td>' . $rmsum['w_bl'] . '</td>';
                echo '<td>' . $rmsum['w_black'] . '</td>';
                //echo '<td>'.$rmsum['w_tot'].'</td>';
                //echo '<td></td>';
                //echo '<td>'.$rmsum['w_wage'].'</td>';
                //echo '<td>'.$rmsum['w_yi'].'</td>';
                //echo '<td>'.$sum['no'].'</td>';
                //echo "<td>".$sum['wf']."</td>";
                echo '<td></td>';
                echo "<td>" . $sum['wfrec'] . "</td>";
                echo "<td>" . $sum['adrec'] . "</td>";
                echo "<td colspan='4'> </td></tr>";
                $htmldata2 .= '<tr style="text-align:center;"><td colspan="4"><b>Unit Total</b></td>';

                $htmldata2 .= '<td><b>' . $rmsum['s_grey'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['s_colour'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['s_bl'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['s_black'] . '</b></td>';
                //$htmldata1.= "<td colspan='6'></td>";
                $htmldata2 .= '<td><b>' . $rmsum['b_grey'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['b_colour'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['b_bl'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['b_black'] . '</b></td>';

                $htmldata2 .= '<td><b>' . $rmsum['w_grey'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['w_colour'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['w_bl'] . '</b></td>';
                $htmldata2 .= '<td><b>' . $rmsum['w_black'] . '</b></td>';
                $htmldata2 .= '<td></td>';
                $htmldata2 .= "<td><b>" . $sum['wfrec'] . "</b></td>";
                $htmldata2 .= "<td><b>" . $sum['adrec'] . "</b></td>";
                $htmldata2 .= '<td></td>';
                $htmldata2 .= '<td></td>';
                $htmldata2 .= '<td></td>';
                $htmldata2 .= '<td></td>';
                $_SESSION['mydata'] = $htmldata2;


                echo "</tbody>
        <tfoot></tfoot>
    </table>";
              }
            }

            if ($res4[0]['type'] == "Twisting") {
              $sum = array(
                'no' => 0,
                'meter'    => 0,
                'wfrec' => 0,
                'adrec' => 0

              );
              $res13 = retrieveData("SELECT * from kbk_twisting_variety group by twisting_code", $con);
              if (sizeof($res1) > 0) {
                ?>

                <table class="table-bordered" id="customers">
                  <thead>
                    <tr>
                      <th>SI No.</th>
                      <th>Artisan Code</th>
                      <th style="width:130px">Artisan Name</th>
                      <th style="width:100px">Variety</th>
                      <th style="width:100px">No. of Hanks</th>


                      <th style="width:100px">Welfare Fund</th>
                      <th style="width:100px">w/f Recovery</th>
                      <th style="width:100px">Adv Recovery</th>
                      <th style="width:100px">Khadi Cr. Rec.</th>
                      <th style="width:100px">Covid adv.</th>
                      <th style="width:100px">LIC</th>


                      <!-- <th scope="col">ESI Contr</th>
                          <th scope="col">Yarn Incentive</th>
                          <th scope="col">Minimum Wages</th>
                          <th scope="col">DA Days</th>
                          <th scope="col">DA Wages</th>
                          <th scope="col">Total Minimum Wages</th>
                          <th scope="col">Net Minimum Wages</th> -->
                      <th style="width:100px">Attendance</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php

                    $htmldata = "";
                    for ($i = 0; $i < sizeof($res1); $i++) {

                      echo '<tr style="text-align:center"><td>' . strval($i + 1) . '</td>';
                      echo '<td style="text-align:left">' . $res1[$i]['artisan_code'] . '</td>';
                      echo '<td style="text-align:left">' . $res1[$i]['artisan_name'] . '</td>';

                      $temp1 = strval($res1[$i]['artisan_id']);
                      $res6 =  retrieveData("SELECT wages,attendance,noh,twist_id From kbk_twisting where s_date='$hdate' and artisan_id=$temp1", $con);
                      $temp2 = $res6[0]['twist_id'];
                      $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=4", $con);
                      $res7 = retrieveData("SELECT * From kbk_twisting_variety where twist_id='$temp2'", $con);

                      echo '<td> <select class="form-control-plaintext"  name="yarn' . strval($i + 1) . '" aria-label="Default select example"> <option value="' . $res7[0]['twisting_code'] . '" selected>' . $res7[0]['type'] . '</option>';
                      for ($j = 0; $j < sizeof($res13); $j++) {
                        echo "<option value='" . $res13[$j]['twisting_code'] . "'>" . $res13[$j]['type'] . "</option>";  // displaying data in option menu
                      }
                      echo '</select></td>';
                      echo '<td><input type="text" name="noh' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res6[0]['noh'] . '" required /></td>';
                      if ($res8[0]['wf_yes_no'] == 1) {
                        echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option selected value="1">YES</option><option value="0">NO</option></select></td>';
                      } else {
                        echo '<td> <select class="form-control-plaintext"  name="wfyn' . strval($i + 1) . '" aria-label="Default select example"  required> <option value="0" selected>NO</option><option value="1">YES</option></select></td>';
                      }
                      $wfund = 0;
                      if ($res8[0]['wf_yes_no'] == 1) {
                        $wfund = $res7[0]['wf'];
                      }
                      $sar1  = array(
                        'noh'    => $res6[0]['noh'],
                        'yarn' => $res7[0]['yarn'],
                        'rate'   => $res7[0]['rate'],
                        'wf'     => $wfund,
                        'wfrec'  => $res8[0]['wf_rec'],
                        'adrec'  => $res8[0]['ad_rec'],
                        'kcrec'  => $res8[0]['credit_rec'],
                        'covrec' => $res8[0]['cov_rec'],
                        'lic'    => $res8[0]['lic'],
                        'hno'    => $res3[0]['holiday_no'],
                        'wno'    => $res3[0]['total_wd'],
                        'yv'     => $res7[0]['yarn_value'],
                        'val'    => $res7[0]['value'],
                      );

                      $calc1 = twistingWageCalculation($sar1);

                      $sar2 = array(
                        'noh'     => $res6[0]['noh'],
                        'esi'     => $res7[0]['esi'],
                        'yi'      => $res7[0]['yi'],
                        'mw'      => $res7[0]['mw'],
                        'target'  => $res7[0]['target'],
                        'dawages' => $res3[0]['da_wages'],
                        'wage'    => $calc1['wage'],
                      );

                      $sum['no'] = $sum['no'] + $res6[0]['noh'];
                      $sum['wfrec'] = $sum['wfrec'] + $calc1['wfrec'];
                      $sum['adrec'] = $sum['adrec'] + $calc1['adrec'];

                      $htmldata .= '<tr><td>' . strval($i + 1) . '</td>';
                      $htmldata .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                      $htmldata .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                      $htmldata .= '<td>' . $res7[0]['type'] . '</td>';
                      $htmldata .= '<td>' . $res6[0]['noh'] . '</td>';
                      //$htmldata.='<td>'.$res7[0]['rate'].'</td>';
                      //$htmldata.='<td>'.$calc1['val'].'</td>';
                      //$htmldata.='<td>'.$calc1['sv'].'</td>';
                      //$htmldata.= '<td>'.$calc1['sliver'].'</td>';
                      //$htmldata.='<td>'.$calc1['wage'].'</td>';
                      //$htmldata.= '<td>'.$res8[0]['wf_yes_no'].'</td>';
                      if ($res8[0]['wf_yes_no'] == 1)
                        $htmldata .= '<td>' . $res8[0]['wf_yes_no'] = "YES" . '</td>';
                      else
                        $htmldata .= '<td>' . $res8[0]['wf_yes_no'] = "NO" . '</td>';
                      $htmldata .=  '<td>' . $calc1['wfrec'] . '</td>';
                      $htmldata .=  '<td>' . $calc1['adrec'] . '</td>';
                      $htmldata .=  '<td>' . $calc1['kcrec'] . '</td>';
                      $htmldata .= '<td>' . $calc1['covrec'] . '</td>';
                      $htmldata .= '<td>' . $calc1['lic'] . '</td>';
                      //$htmldata.= '<td>'.$calc1['netwages'].'</td>';
                      //$htmldata.=  '<td>'.$calc1['hdwage'].'</td>';
                      //$htmldata.= '<td>'.$calc1['totwages'].'</td>';
                      $htmldata .= '<td>' . $res6[0]['attendance'] . '</td>';
                      $htmldata .=  '</tr>';



                      echo '<td><input type="text" name="wfrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['wfrec'] . '"/></td>';
                      echo '<td><input type="text" name="adrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['adrec'] . '"/></td>';
                      echo '<td><input type="text" name="covrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['covrec'] . '"/></td>';
                      echo '<td><input type="text" name="kcrec' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['kcrec'] . '"/></td>';
                      echo '<td><input type="text" name="lic' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $calc1['lic'] . '"/></td>';
                      echo '<td><input type="text" name="att' . strval($i + 1) . '" class="form-control-plaintext form-control-lg" value="' . $res6[0]['attendance'] . '" required /></td>';
                      echo '</tr>';
                    }
                    echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Total</td>';
                    echo "<td>" . $sum['no'] . "</td>";
                    echo "<td> </td>";
                    echo "<td>" . $sum['wfrec'] . "</td>";
                    echo "<td>" . $sum['adrec'] . "</td>";
                    echo "<td colspan='4'> </td></tr>";

                    echo "<td> </td></tr>";
                    $htmldata .= '<tr style="text-align: center;"><td colspan="4"><b>Unit Total</b></td>';
                    $htmldata .= "<td><b>" . $sum['no'] . "</b></td>";
                    $htmldata .= "<td> </td>";
                    //$htmldata.="<td><b>".$sum['val']."</b></td>";
                    //$htmldata.="<td><b>".$sum['pr']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['sliver']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['wage']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['wf']."</b></td>";
                    $htmldata .= "<td><b>" . $sum['wfrec'] . "</b></td>";
                    $htmldata .= "<td><b>" . $sum['adrec'] . "</b></td>";
                    // $htmldata.= "<td><b>".$sum['kcrec']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['covrec']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['lic']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['nw']."</b></td>";
                    // $htmldata.= "<td><b>".$sum['hw']."</b></td>";
                    //$htmldata.= "<td><b>".$sum['tw']."</b></td>";

                    $htmldata .= "<td></td><td></td><td></td><td></td></tr>";
                    $_SESSION["mydata"] = $htmldata;
                    ?>



                  </tbody>
                  <tfoot></tfoot>
                </table>



            <?php


              } // if closing of if(sizeof($res1)>0)

            }




            ?>

      </div>

      <!-- <div class="text-center" >
  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
<a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
<a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
</div>
 -->
      <?php
      $spinning_status = retrieveData("SELECT status FROM kbk_spinning WHERE s_date = '$hdate'", $con);
      $weaving_status = retrieveData("SELECT status FROM kbk_weaving WHERE s_date = '$hdate'", $con);
      $twisting_status = retrieveData("SELECT status FROM kbk_twisting WHERE s_date = '$hdate'", $con);
      $preprocessing_status = retrieveData("SELECT status FROM kbk_preprocessing WHERE s_date = '$hdate'", $con);
      if (($res4[0]['type'] == "Spinning") && ($spinning_status[0]['status'] != 1)) {
      ?>
        <div class="d-flex justify-content-end pt-3">
          <!--  <button class="btn btn-light btn-lg" onclick="history.go(-1)">Back</button> -->
          <button type="reset" class="btn btn-light btn-lg ms-2">Reset all</button>
          <input type="submit" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Edit">

        </div>
      <?php
      } elseif (($res4[0]['type'] == "Weaving") && ($weaving_status[0]['status'] != 1)) {
      ?>
        <div class="d-flex justify-content-end pt-3">
          <!--  <button class="btn btn-light btn-lg" onclick="history.go(-1)">Back</button> -->
          <button type="reset" class="btn btn-light btn-lg ms-2">Reset all</button>
          <input type="submit" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Edit">

        </div>
      <?php
      } elseif (($res4[0]['type'] == "Preprocessing") && ($preprocessing_status[0]['status'] != 1)) {
      ?>
        <div class="d-flex justify-content-end pt-3">
          <!--  <button class="btn btn-light btn-lg" onclick="history.go(-1)">Back</button> -->
          <button type="reset" class="btn btn-light btn-lg ms-2">Reset all</button>
          <input type="submit" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Edit">

        </div>
      <?php
      } elseif (($res4[0]['type'] == "Twisting") && ($twisting_status[0]['status'] != 1)) {
      ?>
        <div class="d-flex justify-content-end pt-3">
          <!--  <button class="btn btn-light btn-lg" onclick="history.go(-1)">Back</button> -->
          <button type="reset" class="btn btn-light btn-lg ms-2">Reset all</button>
          <input type="submit" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Edit">

        </div>
      <?php
      } else {
        echo "<br>";
      }
      ?>
      <div>
        <label></label>
      </div>
      <div class="text-center">
        <!--  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">-->
        <!--<a target="_blank" class="btn btn-info btn_print" href="view_details.php">View Details</a>-->
        <button type="submit" class="btn btn-info btn_print" formaction="view_details.php" name="entry">View Details</button>
        <a target="_blank" class="btn btn-info btn_print" href="view_work_details_pdf.php">Generate PDF and Print</a>
      </div>

    </section>

  </form>
  <!-- Adding more varity of weaving to artisan -->
  <?php
  if($res4[0]['type']=="Weaving"){
  ?>
  <center>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="margin-top: 10px;">
      weaving variety
    </button>
  </center>
  <?php
  }
  ?>
  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <form method="POST" action="add_weaving_variety.php">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Add More Weaving Variety to Artisan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="artisanCode">Artisan</label>
              <select class="custom-select" name="artisanId" id="artisanId">
                <?php
                for ($i = 0; $i < sizeof($res1); $i++) {
                ?>
                  <option value="<?= $res1[$i]['artisan_id']; ?>"><?php echo $res1[$i]['artisan_code'] . ' ' . $res1[$i]['artisan_name']; ?></option>
                <?php
                }
                ?>
              </select>
              <label for="weavingVariety">Weaving Variety</label>
              <select class="custom-select" name="weavingVariety" id="weavingVariety">
                <?php
                for ($i = 0; $i < sizeof($res2); $i++) {
                ?>
                  <option value="<?= $res2[$i]['weaving_code']; ?>"><?php echo $res2[$i]['type']; ?></option>
                <?php
                }
                ?>
              </select><br><br>
              <label for="nop">Number of peices</label>
              <input type="text" name="nop" id="nop"><br>
              <label for="meter">Meter</label>
              <input type="text" name="meter" id="meter">
              <input type="hidden" name="hdate" id="hdate" value="<?php echo $hdate; ?>">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" name="submit">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End your project here-->

  <!-- MDB -->
  <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
  <!-- Custom scripts -->
  <!--<script type="text/javascript"></script>-->
  <script type="text/javascript">
    window.history.forward();

    function noBack() {
      window.history.forward();
    }
  </script>
</body>

</html>