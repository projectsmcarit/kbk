
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Instructor Home</title>
    <!-- MDB icon -->
   
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->
<!--Main Navigation-->
<header>
    <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:darkslategrey; position:fixed;z-index:1; right:0;left: 0;margin-bottom: 50px;"  >
    <!-- Container wrapper -->
    <div class="container-fluid" style="z-index: 1;" >
      <!-- Toggle button -->
      <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i class="fas fa-bars"></i>
      </button>
  
      <!-- Collapsible wrapper -->
     
        <!-- Left links -->
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="view_instructor_index.php" style="color: white">Home</a>
          </li>
          <!-- Navbar dropdown -->
        

  <li class="nav-item">
    <a class="nav-link" href="view_month_entry.php" style="color: white">Work</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="view_contingency.php" style="color: white">Contingency</a>
  </li>
        <!-- Left links -->
      </div>
      <!-- Collapsible wrapper -->
  
      
  
        
        <!-- Avatar -->
        <div class="dropdown">
          <a
            class="dropdown-toggle d-flex align-items-center hidden-arrow"
            href="#"
            id="navbarDropdownMenuAvatar"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            <img
              src="../images/1.jpg"
              class="rounded-circle"
              height="25"
              alt="Khadi Board Kottayam"
              loading="lazy"
            />
          </a>
          <ul
            class="dropdown-menu dropdown-menu-end"
            aria-labelledby="navbarDropdownMenuAvatar"
          >
          <a class="dropdown-item" href="./view_changepassword.php">Change Password</a>
          <a class="dropdown-item" href="../logout.php">Logout</a>
              
            </li>
          </ul>
        </div>
      </div>
      <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
  </nav>
 <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>