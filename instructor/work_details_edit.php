<?php
include('../connection.php');
include('../kbk_library.php');
session_start();
if (isset($_POST['submit'])) {
  //  $hdate=date("Y/m/d",strtotime($_SESSION['a']));
  $hdate = date("Y/m/d", strtotime($_POST['hdate']));

  $worktype = $_POST['worktype']; //
  $unitcode = $_SESSION['unitcode'];
  $hno = $_POST['hno'];
  $wno = $_POST['wno'];
  $j = $_SESSION['i'];
  // $t=$_SESSION['t'];
  // print_r($j);
  $res5 = retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays WHERE h_date='$hdate'", $con);

  if ($worktype == "Spinning") {
    $sar = [];
    for ($i = 0; $i < sizeof($j); $i++) {
      $artid  = intval($j[$i]['artisan_id']); //from session variable
      $yarn   = $_POST['yarn' . strval($i + 1)]; // yarn code
      $noh    = intval($_POST['noh' . strval($i + 1)]); // no. of hanks
      $wfrec  = floatval($_POST['wfrec' . strval($i + 1)]);
      $adrec  = floatval($_POST['adrec' . strval($i + 1)]);
      $covrec = floatval($_POST['covrec' . strval($i + 1)]);
      $kcrec  = floatval($_POST['kcrec' . strval($i + 1)]);
      $att    = intval($_POST['att' . strval($i + 1)]);
      $lic    = floatval($_POST['lic' . strval($i + 1)]);
      $wfyn   = intval($_POST['wfyn' . strval($i + 1)]);

      $res6 = retrieveData("SELECT yarn_id,rate,sliver_value,value,sliver,esi,wf,yi,target,mw from kbk_yarn_variety where yarn_code='$yarn' and wef = (SELECT max(wef) from kbk_yarn_variety where yarn_code='$yarn' and wef<='$hdate')", $con);


      $sar1 = [
        'noh'    => $noh,
        'sliver' => $res6[0]['sliver'],
        'rate'   => $res6[0]['rate'],
        'wf'     => $res6[0]['wf'],
        'wfrec'  => $wfrec,
        'adrec'  => $adrec,
        'kcrec'  => $kcrec,
        'covrec' => $covrec,
        'lic'    => $lic,
        'hno'    => $hno,
        'wno'    => $wno,
        'sv'     => $res6[0]['sliver_value'],
        'val'    => $res6[0]['value'],
      ];
      //print_r($sar1);

      $calc1 = spinningWageCalculation($sar1);



      //   $sar2 = [
      //     noh     => $noh,
      //     esi     => $res6[0]['esi'],
      //     yi      => $res6[0]['yi'],
      //     mw      => $res6[0]['mw'],
      //     target  => $res6[0]['target'],
      //     dawages => $res5[0]['da_wages'],
      //     wage    => $calc1['wage'],
      // ];


      //   $calc2=spinningIncentivesCalculation($sar2);

      //printf(sizeof($sar)); echo '<br>';
      //print_r($sar1);echo '<br>';
      //print_r($sar2);echo '<br>';
      //print_r($calc1);echo '<br>';
      // print_r($calc2);echo '<br>';
      $wage = $calc1['wage'];
      // $yi=$calc2['yi'];
      $hwage = $calc1['hdwage'];
      $y = $res6[0]['yarn_id'];
      //echo $unitcode;
      //$res1=insertData("INSERT INTO kbk_spinning(s_date,wages,attendance,noh,artisan_id,yarn_id,unit_code) VALUES ('$hdate',$wage,$att,$noh,$artid,$y,'$unitcode')",$con);
      $sql1 = "UPDATE kbk_spinning SET wages=$wage, noh=$noh, yarn_id=$y, attendance=$att  WHERE s_date='$hdate' and artisan_id=$artid and unit_code='$unitcode'";
      $res1 = $con->query($sql1);

      //var_dump($res1);echo "ffgfff";
      //var_dump($hdate);echo "dfjhgdj";
      //var_dump($artid);echo "fdfgd";
      // $res2=insertData("INSERT INTO kbk_addition(h_wages,artisan_id,a_date,work_id)VALUES ($hwage,$artid,'$hdate',$t)",$con);
      $sql2 = "UPDATE kbk_addition SET h_wages=$hwage WHERE a_date='$hdate' and artisan_id=$artid and work_id=1";
      $res2 = $con->query($sql2);
      //var_dump($hwage);
      // $res3=insertData("INSERT INTO kbk_deduction(ad_rec,credit_rec,cov_rec,wf_rec,artisan_id,d_date,lic,work_id)VALUES($adrec,$kcrec,$covrec,$wfrec,$artid,'$hdate',$lic,$t)",$con);
      $sql3 = "UPDATE kbk_deduction SET ad_rec=$adrec, credit_rec=$kcrec, cov_rec=$covrec, wf_rec=$wfrec, lic=$lic, wf_yes_no=$wfyn  WHERE d_date='$hdate' and artisan_id=$artid and work_id=1";
      $res3 = $con->query($sql3);
      if (!($res1 && $res2 && $res3)) {

        echo "<script>alert('Error!!! Try Again')</script>";
        echo "<script>window.location.href='view_work_details_edit.php'</script>";
        //break;
      }
    }
    if ($res1 && $res2 && $res3) {
      echo "<script>alert('success')</script>";
      echo "<script>window.location.href='view_instructor_index.php'</script>";
    }
  }
  if ($worktype == "Weaving") {
    $state = 0;
    for ($i = 0; $i < sizeof($j); $i++) {
      $a_id = $j[$i]['artisan_id'];
      // echo "<br>" . $a_id;
      $loop = retrieveData("SELECT * FROM kbk_weaving WHERE artisan_id = $a_id AND s_date='$hdate'", $con);
      // $state = 0;
      for ($l = 0; $l < sizeof($loop); $l++) {
        // var_dump($_POST['wvg_id']);
        $wvg_id = intval($_POST['wvg_id' . strval($state + 1)]);
        $artid  = intval($j[$i]['artisan_id']);
        $code  = $_POST['variety' . strval($state + 1)]; // yarn code
        $nop    = isset($_POST['nop' . strval($state + 1)]) ? intval($_POST['nop' . strval($state + 1)]) : 0; // no. of pieces if the variety is countable
        $metre  = isset($_POST['metre' . strval($state + 1)]) ? intval($_POST['metre' . strval($state + 1)]) : 0;
        $wfrec  = floatval($_POST['wfrec' . strval($state + 1)]);
        $adrec  = floatval($_POST['adrec' . strval($state + 1)]);
        $covrec = floatval($_POST['covrec' . strval($state + 1)]);
        $kcrec  = floatval($_POST['kcrec' . strval($state + 1)]);
        $att    = intval($_POST['att' . strval($state + 1)]);
        $wt     = $_POST['wt' . strval($i + 1)];
        $lic    = floatval($_POST['lic' . strval($state + 1)]);
        $wfyn   = intval($_POST['wfyn' . strval($state + 1)]);

        echo $wvg_id . " " . $nop . " " . $metre . " " . $artid . "<br>";

        //echo $code; echo '<br>';
        //var_dump($hdate);
        $res7 = retrieveData("SELECT weaving_id,wf,wef,countable,length,width,prime_cost,rate FROM kbk_weaving_variety where weaving_code='$code' and wef = (SELECT max(wef) from kbk_weaving_variety where weaving_code ='$code' and wef<='$hdate')", $con);
        //print_r($res7);echo '<br>';
        $count = $res7[0]['countable'];
        if ($count == 1) {
          $metre = $nop * $res7[0]['length'];
        }
        // $res8=retrieveData("SELECT * FROM kbk_yarn_comsumption where w_id=$variety",$con);
        $vid = intval($res7[0]['weaving_id']);
        $wef = $res7[0]['wef'];
        //var_dump($vid);echo '<br>';
        //var_dump($wef);echo '<br>';
        //var_dump($wt);echo '<br>';
        $res9 = retrieveData("SELECT w_id,w_wages FROM kbk_weaving_type where weaving_id='$vid' and w_type='$wt' and wef='$wef'", $con);
        //var_dump($vid);echo '<br>';
        //print_r($res9);echo '<br>';

        $wid = intval($res9[0]['w_id']);; //find the w_id by using the w_type, wef date, and weaving variety 

        $war1 = [
          'metre'  => $metre,
          'wage'   => $res9[0]['w_wages'],
          'wf'     => $res7[0]['wf'],
          'wfrec'  => $wfrec,
          'adrec'  => $adrec,
          'kcrec'  => $kcrec,
          'covrec' => $covrec,
          'lic'    => $lic,
          'hno'    => $hno,
          'wno'    => $wno,
          'sqmtr'  => $res7[0]['width'],
          'prime'  => $res7[0]['prime_cost'],
          'value'  => $res7[0]['rate'],
        ];

        $calc3 = weavingWageCalculation($war1);

        //   $war2 = [
        //     metre   => $metre,
        //     esi     => $res9[0]['esi'],
        //     warpyi  => $res9[0]['warp_yi'],
        //     weftyi  => $res9[0]['weft_yi'],
        //     mw      => $res9[0]['min_wages'],
        //     target  => $res9[0]['target'],
        //     dawages => $res5[0]['da_wages'],
        //     wage    => $calc3['wage'],
        // ];
        //echo '<br>';//var_dump($artid);echo '<br>';var_dump($unitcode);echo '<br>';
        $wages = intval($calc3['wage']);
        $hdwage = intval($calc3['hdwage']);
        $a[] = [$hdate, $wages, $att, $nop, $metre, $artid, $vid, $wid];
        //print_r($a);
        //$res1 = insertData("INSERT INTO kbk_weaving(s_date,wages,attendance,nop,mtr,artisan_id,weaving_id,w_id,unit_code) VALUES ('$hdate',$wages,$att,$nop,$metre,$artid,$vid,$wid,'$unitcode')",$con);
        $sql1 = "UPDATE kbk_weaving SET wages=$wages, nop=$nop, mtr=$metre, w_id=$wid, weaving_id=$vid, attendance=$att  WHERE wvg_id=$wvg_id AND s_date='$hdate' AND artisan_id=$artid AND unit_code='$unitcode'";
        $res1 = $con->query($sql1);
        $sql2 = "UPDATE kbk_addition SET h_wages=$hdwage WHERE a_date='$hdate' and artisan_id=$artid and work_id=2";
        $res2 = $con->query($sql2);
        //var_dump($hdate);
        $sql3 = "UPDATE kbk_deduction SET ad_rec=$adrec, credit_rec=$kcrec, cov_rec=$covrec, wf_rec=$wfrec, lic=$lic, wf_yes_no=$wfyn  WHERE d_date='$hdate' and artisan_id=$artid and work_id=2";
        $res3 = $con->query($sql3);

        if (!($res1 && $res2 && $res3)) {

          echo "<script>alert('Error!!! Try Again')</script>";
          echo "<script>window.location.href='view_work_details_edit.php'</script>";
          //break;
        }
        $state = $state + 1;
      }
    }
    if ($res1 && $res2 && $res3) {
      echo "<script>alert('success')</script>";
      echo "<script>window.location.href='view_instructor_index.php'</script>";
    }
  }

  if ($worktype == "Preprocessing") {
    for ($i = 0; $i < sizeof($j); $i++) {


      $artid  = $j[$i]['artisan_id'];
      $code  = $_POST['variety' . strval($i + 1)]; // preprocessing code

      $sgrey    = isset($_POST['sgrey' . strval($i + 1)]) ? intval($_POST['sgrey' . strval($i + 1)]) : 0;
      $scol    = isset($_POST['scol' . strval($i + 1)]) ? intval($_POST['scol' . strval($i + 1)]) : 0;
      $sbl    = isset($_POST['sbl' . strval($i + 1)]) ? intval($_POST['sbl' . strval($i + 1)]) : 0;
      $sblack    = isset($_POST['sblack' . strval($i + 1)]) ? intval($_POST['sblack' . strval($i + 1)]) : 0;

      $bgrey    = (isset($_POST['bgrey' . strval($i + 1)])) ? intval($_POST['bgrey' . strval($i + 1)]) : 0;
      $bcol    = isset($_POST['bcol' . strval($i + 1)]) ? intval($_POST['bcol' . strval($i + 1)]) : 0;
      $bbl    = isset($_POST['bbl' . strval($i + 1)]) ? intval($_POST['bbl' . strval($i + 1)]) : 0;
      $bblack    = isset($_POST['bblack' . strval($i + 1)]) ? intval($_POST['bblack' . strval($i + 1)]) : 0;
      $wgrey    = isset($_POST['wgrey' . strval($i + 1)]) ? intval($_POST['wgrey' . strval($i + 1)]) : 0;
      $wcol    = isset($_POST['wcol' . strval($i + 1)]) ? intval($_POST['wcol' . strval($i + 1)]) : 0;
      $wbl    = isset($_POST['wbl' . strval($i + 1)]) ? intval($_POST['wbl' . strval($i + 1)]) : 0;
      $wblack    = isset($_POST['wblack' . strval($i + 1)]) ? intval($_POST['wblack' . strval($i + 1)]) : 0;


      $wfrec  = floatval($_POST['wfrec' . strval($i + 1)]);
      $adrec  = floatval($_POST['adrec' . strval($i + 1)]);
      $covrec = floatval($_POST['covrec' . strval($i + 1)]);
      $kcrec  = floatval($_POST['kcrec' . strval($i + 1)]);
      $att    = intval($_POST['att' . strval($i + 1)]);
      $lic    = floatval($_POST['lic' . strval($i + 1)]);
      $wfyn   = intval($_POST['wfyn' . strval($i + 1)]);

      $par1 = [
        'grey'  => $sgrey,
        'col'   => $scol,
        'bl'    => $sbl,
        'black' => $sblack,
      ];
      $sizetotnoh = preprocessingTypeWage($par1);
      $par1 = [
        'grey'  => $bgrey,
        'col'   => $bcol,
        'bl'    => $bbl,
        'black' => $bblack,
      ];

      $bobintotnoh = preprocessingTypeWage($par1);
      $par1 = [
        'grey'  => $wgrey,
        'col'   => $wcol,
        'bl'    => $wbl,
        'black' => $wblack,
      ];
      $warptotnoh = preprocessingTypeWage($par1);
      $res10 = retrieveData("SELECT * FROM kbk_preprocessing_variety where rm_code='$code' and wef = (SELECT max(wef) from kbk_preprocessing_variety where rm_code ='$code' and wef<='$hdate')", $con);


      $preid = $res10[0]['pre_id'];

      $par2 = [
        'snoh'  => $sizetotnoh,
        'bnoh'  => $bobintotnoh,
        'wnoh'  => $warptotnoh,
        'swage' => $res10[0]['s_wages'],
        'bwage' => $res10[0]['b_wages'],
        'wwage' => $res10[0]['w_wages'],
        'wf'    => $res10[0]['wf'],
        'wfrec' => $wfrec,
        'adrec' => $adrec,
        'kcrec' => $kcrec,
        'covrec' => $covrec,
        'lic'   => $lic,
        'hno'   => $hno,
        'wno'   => $wno,

      ];

      $calc4 = preprocessingWageCalculation($par2);
      $wages = $calc4['wage'];
      $hdwage = $calc4['hdwage'];
      $res11 = $con->query("UPDATE kbk_preprocessing SET wages=$wages, attendance=$att, pre_id=$preid WHERE s_date='$hdate' and artisan_id=$artid and unit_code='$unitcode'");

      $demo = array($hdate, $wages, $att, $preid, $artid, $unitcode);

      $res12 = retrieveData("SELECT preprocessing_id FROM kbk_preprocessing where s_date='$hdate' and pre_id=$preid and artisan_id=$artid and unit_code='$unitcode'", $con);
      $preprocessingid = $res12[0]['preprocessing_id'];

      $res13 = $con->query("UPDATE kbk_preprocessing_type_entry SET b_grey=$bgrey, b_col=$bcol, b_black=$bblack, b_bl=$bbl, s_grey=$sgrey, s_col=$scol, s_black=$sblack, s_bl=$sbl, w_grey=$wgrey, w_col=$wcol, w_black=$wblack, w_bl=$wbl WHERE preprocessing_id=$preprocessingid");
      $sql2 = "UPDATE kbk_addition SET h_wages=$hdwage WHERE a_date='$hdate' and artisan_id=$artid and work_id=3";
      $res2 = $con->query($sql2);
      $sql3 = "UPDATE kbk_deduction SET ad_rec=$adrec, credit_rec=$kcrec, cov_rec=$covrec, wf_rec=$wfrec, lic=$lic, wf_yes_no=$wfyn  WHERE d_date='$hdate' and artisan_id=$artid and work_id=3";
      $res3 = $con->query($sql3);

      if (!($res11 && $res2 && $res3 && $res13)) {

        echo "<script>alert('Error!!! Try Again')</script>";
        //echo "<script>window.location.href='view_spinning_entry.php'</script>";
        //break;
      }
    }
    if ($res11 && $res2 && $res3 && $res13) {
      echo "<script>alert('success')</script>";
      echo "<script>window.location.href='view_instructor_index.php'</script>";
    }
  }
  if ($worktype == "Twisting") {
    $sar = [];
    for ($i = 0; $i < sizeof($j); $i++) {
      $artid  = intval($j[$i]['artisan_id']); //from session variable
      $yarn   = $_POST['yarn' . strval($i + 1)]; // yarn code
      $noh    = intval($_POST['noh' . strval($i + 1)]); // no. of hanks
      $wfrec  = floatval($_POST['wfrec' . strval($i + 1)]);
      $adrec  = floatval($_POST['adrec' . strval($i + 1)]);
      $covrec = floatval($_POST['covrec' . strval($i + 1)]);
      $kcrec  = floatval($_POST['kcrec' . strval($i + 1)]);
      $att    = intval($_POST['att' . strval($i + 1)]);
      $lic    = floatval($_POST['lic' . strval($i + 1)]);
      $wfyn   = intval($_POST['wfyn' . strval($i + 1)]);

      $res6 = retrieveData("SELECT twist_id,rate,yarn_value,value,yarn,esi,wf,yi,target,mw from kbk_twisting_variety where twisting_code='$yarn' and wef = (SELECT max(wef) from kbk_twisting_variety where twisting_code='$yarn' and wef<='$hdate')", $con);


      $sar1 = [
        'noh'    => $noh,
        'yarn' => $res6[0]['yarn'],
        'rate'   => $res6[0]['rate'],
        'wf'     => $res6[0]['wf'],
        'wfrec'  => $wfrec,
        'adrec'  => $adrec,
        'kcrec'  => $kcrec,
        'covrec' => $covrec,
        'lic'    => $lic,
        'hno'    => $hno,
        'wno'    => $wno,
        'yv'     => $res6[0]['yarn_value'],
        'val'    => $res6[0]['value'],
      ];
      //print_r($sar1);

      $calc1 = twistingWageCalculation($sar1);



      //   $sar2 = [
      //     noh     => $noh,
      //     esi     => $res6[0]['esi'],
      //     yi      => $res6[0]['yi'],
      //     mw      => $res6[0]['mw'],
      //     target  => $res6[0]['target'],
      //     dawages => $res5[0]['da_wages'],
      //     wage    => $calc1['wage'],
      // ];


      //   $calc2=spinningIncentivesCalculation($sar2);

      //printf(sizeof($sar)); echo '<br>';
      //print_r($sar1);echo '<br>';
      //print_r($sar2);echo '<br>';
      //print_r($calc1);echo '<br>';
      // print_r($calc2);echo '<br>';
      $wage = $calc1['wage'];
      // $yi=$calc2['yi'];
      $hwage = $calc1['hdwage'];
      $y = $res6[0]['twist_id'];
      //echo $unitcode;
      //$res1=insertData("INSERT INTO kbk_spinning(s_date,wages,attendance,noh,artisan_id,yarn_id,unit_code) VALUES ('$hdate',$wage,$att,$noh,$artid,$y,'$unitcode')",$con);
      $sql1 = "UPDATE kbk_twisting SET wages=$wage, noh=$noh, twist_id=$y, attendance=$att  WHERE s_date='$hdate' and artisan_id=$artid and unit_code='$unitcode'";
      $res1 = $con->query($sql1);

      //var_dump($res1);echo "ffgfff";
      //var_dump($hdate);echo "dfjhgdj";
      //var_dump($artid);echo "fdfgd";
      // $res2=insertData("INSERT INTO kbk_addition(h_wages,artisan_id,a_date,work_id)VALUES ($hwage,$artid,'$hdate',$t)",$con);
      $sql2 = "UPDATE kbk_addition SET h_wages=$hwage WHERE a_date='$hdate' and artisan_id=$artid and work_id=4";
      $res2 = $con->query($sql2);
      //var_dump($hwage);
      // $res3=insertData("INSERT INTO kbk_deduction(ad_rec,credit_rec,cov_rec,wf_rec,artisan_id,d_date,lic,work_id)VALUES($adrec,$kcrec,$covrec,$wfrec,$artid,'$hdate',$lic,$t)",$con);
      $sql3 = "UPDATE kbk_deduction SET ad_rec=$adrec, credit_rec=$kcrec, cov_rec=$covrec, wf_rec=$wfrec, lic=$lic, wf_yes_no=$wfyn  WHERE d_date='$hdate' and artisan_id=$artid and work_id=4";
      $res3 = $con->query($sql3);
      if (!($res1 && $res2 && $res3)) {

        echo "<script>alert('Error!!! Try Again')</script>";
        echo "<script>window.location.href='view_work_details_edit.php'</script>";
        //break;
      }
    }
    if ($res1 && $res2 && $res3) {
      echo "<script>alert('success')</script>";
      echo "<script>window.location.href='view_instructor_index.php'</script>";
    }
  }
}
