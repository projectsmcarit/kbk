<?php
include('../connection.php');
include('../kbk_library.php');
session_start();

require_once("../admin/dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$html=""; 
$html.='<html>';
$html.='<body>';
$html.='<form>'; 
$html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
$html.='<br>';
$r1=$_SESSION["mydata2"];
$r2=$_SESSION["mydata4"];
$r3=$_SESSION["mydata5"];
$r4=$_SESSION["unitcode_instructor"];

$html.='<h3 align="center">'.$r1.' Wages For '.$_SESSION["mydata1"].' Unit('.$r4.') '.$r2.' '.$r3.'</h3>';
$html.='<table style="border-collapse:collapse" border="0" width=100%><th>Number of Holidays : '.$_SESSION["holy_days"].'</th><th style="text-align:right;">Number of Working days : '.$_SESSION["working_days"].'</th></table>';

              if($r1=="Spinning")
              {
                $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
<thead>
                <tr style="width:10.17mm">
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Variety</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Hanks</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Fund</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Rec</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Adv Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Khadi Credit Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm"> Covid Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">LIC</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Attendance</th>

                </tr>
              </thead>';
                $html.=$_SESSION["mydata"];
                $html.="<new>";
                $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed" border="0" align="center" width=100%>';
                   $html.="<tr>
                      <td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td>
                      </tr>";
                   $html.='<tr><td style="height:25px"></td><td></td></tr>
                      <tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";

                  $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';

              }
              else if($r1=="Weaving")
              {
                $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
<thead>
                <tr>
                  <th >SI No.</th>
                  <th style="overflow: hidden;" colspan="4">Artisan Code</th>
                  <th style="overflow: hidden;" colspan="4">Artisan Name</th>
                  <th style="overflow: hidden;" colspan="4">Variety</th>
                  <th style="overflow: hidden;" colspan="2">No. of Pieces</th>
                  <th style="overflow: hidden;" colspan="2">Metre</th>
                  
                  <th style="overflow: hidden;" colspan="2">W/F Fund</th>
                  <th style="overflow: hidden;">w/f Rec </th>
                  <th style="overflow: hidden;">Adv Rec </th>
                  <th style="overflow: hidden;">KhadiCr</th>
                  <th style="overflow: hidden;">Covidad</th>
                  <th style="overflow: hidden;">LIC</th>
                  <th style="overflow: hidden;" colspan="2">Attendance</th>
                  <th style="overflow: hidden;" colspan="4">Weaving Type</th>
                </tr>
              </thead>';
                $html.=$_SESSION["mydata3"];
                $html.=$_SESSION['weaving_var_sum'];
                   $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="0" align="center" width=67rem>';
                   $html.="<tr>
                      <td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td>
                      </tr>";
                   $html.='<tr><td style="height:25px"></td><td></td></tr>
                      <tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";

                  $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';


              }
              else if($r1=="Preprocessing")
              {
                $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
                <thead>
                        <tr>
                          <th colspan="4" style="min-width: center"></th>
                          <th colspan="4" style="text-align:center">Sizing</th>
                          <th colspan="4" style="text-align:center">Bobin Winding</th>
                          <th colspan="4" style="text-align:center">Warping</th>
                          <th colspan="7"></th>
                        </tr>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th style="min-width: 130px">Artisan Name</th>
                          <th style="min-width:80px">Variety</th>

                          <th style="min-width:80px">Grey</th>
                          <th style="min-width:80px">Colour</th>
                          <th style="min-width:80px">Bl</th>
                          <th style="min-width:80px">Black</th>
                      

                          <th style="min-width:80px">Grey</th>
                          <th style="min-width:80px">Colour</th>
                          <th style="min-width:80px">Bl</th>
                          <th style="min-width:80px">Black</th>
                  

                          <th style="min-width:80px">Grey</th>
                          <th style="min-width:80px">Colour</th>
                          <th style="min-width:80px">Bl</th>
                          <th style="min-width:80px">Black</th>
                        

                          <th style="min-width:80px">W/F Fund</th>
                          <th style="min-width:80px">W/F Rec.</th>
                          <th style="min-width:80px">Adv Rec.</th>
                          <th style="min-width:80px">Khadi Cr. Rec.</th>
                          <th style="min-width:80px">Covid adv. Rec.</th>
                          <th style="min-width:80px">LIC</th>
                       
                          
                          <th style="min-width:80px">Attendance</th>
  
                        </tr>
                      </thead>';
                      $html.=$_SESSION["mydata"];
   // $html.="<new>";
    $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed" border="0" align="center" width=100%>';
    $html.="<tr><td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td></tr>";
    $html.='<tr><td style="height:25px"></td><td></td></tr><tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";
    $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';

              }
              else if($r1=="Twisting")
              {
                $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
<thead>
                <tr style="width:10.17mm">
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Variety</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Hanks</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Fund</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Rec</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Adv Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Khadi Credit Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm"> Covid Rec </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">LIC</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Attendance</th>

                </tr>
              </thead>';
                $html.=$_SESSION["mydata"];
                $html.="<new>";
                $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed" border="0" align="center" width=100%>';
                   $html.="<tr>
                      <td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td>
                      </tr>";
                   $html.='<tr><td style="height:25px"></td><td></td></tr>
                      <tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";

                  $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';

              }
    $html.='</table>';
$html.="</form>";
$html.="</body>";
$html.="</html>";
$dompdf->loadHtml(html_entity_decode($html));	
$dompdf->setPaper('A4', 'landscape'); //portrait or landscape
$dompdf->render();
ob_end_clean();
$dompdf->stream("Monthly Report",array("Attachment"=>0));

?>    