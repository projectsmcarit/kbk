-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `kbk`;

DROP TABLE IF EXISTS `kbk_addition`;
CREATE TABLE `kbk_addition` (
  `addition_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `h_wages` decimal(10,2) NOT NULL,
  `artisan_id` bigint(20) NOT NULL,
  `a_date` date NOT NULL,
  `work_id` int(11) NOT NULL,
  PRIMARY KEY (`addition_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `work_id` (`work_id`),
  CONSTRAINT `kbk_addition_ibfk_1` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_addition_ibfk_3` FOREIGN KEY (`work_id`) REFERENCES `kbk_work_type` (`work_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_addition` (`addition_id`, `h_wages`, `artisan_id`, `a_date`, `work_id`) VALUES
(1,	67.67,	3,	'2022-07-01',	1),
(2,	62.50,	4,	'2022-07-01',	1),
(3,	67.67,	5,	'2022-07-01',	1),
(4,	77.50,	7,	'2022-07-01',	1),
(5,	62.85,	3,	'2022-07-01',	2),
(6,	78.15,	4,	'2022-07-01',	2),
(7,	153.23,	5,	'2022-07-01',	2),
(8,	70.58,	7,	'2022-07-01',	2),
(9,	5.75,	3,	'2022-07-01',	3),
(10,	5.75,	4,	'2022-07-01',	3),
(11,	5.75,	5,	'2022-07-01',	3),
(12,	5.75,	7,	'2022-07-01',	3)
ON DUPLICATE KEY UPDATE `addition_id` = VALUES(`addition_id`), `h_wages` = VALUES(`h_wages`), `artisan_id` = VALUES(`artisan_id`), `a_date` = VALUES(`a_date`), `work_id` = VALUES(`work_id`);

DROP TABLE IF EXISTS `kbk_artisan`;
CREATE TABLE `kbk_artisan` (
  `artisan_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artisan_code` varchar(20) NOT NULL,
  `artisan_name` varchar(20) NOT NULL,
  `contact_no` bigint(10) NOT NULL,
  `aadhar_no` bigint(12) NOT NULL,
  `aadhar` varchar(200) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `doj` date NOT NULL,
  `dob` date NOT NULL,
  `addressln1` varchar(100) NOT NULL,
  `addressln2` varchar(100) DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `pin_number` int(6) NOT NULL,
  `father_spouse` varchar(20) NOT NULL,
  `lic` varchar(20) DEFAULT NULL,
  `esi` varchar(20) DEFAULT NULL,
  `account_no` varchar(20) NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `ifsc` varchar(20) NOT NULL,
  `wf_reg_no` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `unit_code` varchar(20) NOT NULL,
  `caste` varchar(10) NOT NULL,
  `mmdi` varchar(20) DEFAULT NULL,
  `esi_centr` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`artisan_id`),
  UNIQUE (`aadhar_no`),
  KEY `unit_id` (`unit_code`),
  KEY `esi_centr` (`esi_centr`),
  CONSTRAINT `kbk_artisan_ibfk_1` FOREIGN KEY (`unit_code`) REFERENCES `kbk_unit` (`unit_code`) ON DELETE CASCADE,
  CONSTRAINT `kbk_artisan_ibfk_2` FOREIGN KEY (`esi_centr`) REFERENCES `kbk_esi_center` (`esi_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_artisan` (`artisan_id`, `artisan_code`, `artisan_name`, `contact_no`, `aadhar_no`, `aadhar`, `photo`, `doj`, `dob`, `addressln1`, `addressln2`, `gender`, `pin_number`, `father_spouse`, `lic`, `esi`, `account_no`, `bank_name`, `ifsc`, `wf_reg_no`, `email`, `unit_code`, `caste`, `mmdi`, `esi_centr`, `status`) VALUES
(1,	'kbk/ktm/s/01',	'Arun',	9874563214,	785469532584,	'',	'',	'2021-06-04',	'1990-11-25',	'kuttoorichalil',	'kozhikkodpo',	'Male',	745896,	'Boss',	'lic/2021/145',	'esi/2021/114',	'014201456201',	'SBI',	'SIBL0000136',	'wf/2021/154',	'arun@gmail.com',	'kbk/ktm/s',	'OEC',	'mm/2021/124',	1,	0),
(2,	'kbk/pmy/s/01',	'joel',	9947457966,	785469537777,	'',	'',	'2018-03-24',	'1985-01-18',	'mannampuzhayath',	'cherukol',	'Male',	690104,	'Johnson',	'lic/2018/110',	'esi/2018/111',	'0001364587450014',	'SIB',	'SIBL0000256',	'wf/2018/155',	'joel@gmail.com',	'kbk/pmy/s',	'OBC',	'mm/2018/121',	1,	1),
(3,	'kbk/ktm/s/02',	'franly',	9947457975,	785469537555,	'',	'',	'2018-03-24',	'1985-01-18',	'mannapuzha',	'thrissur',	'Male',	690104,	'francis',	'lic/2018/115',	'esi/2018/117',	'0001364587455555',	'SIB',	'SIBL0000256',	'wf/2018/177',	'franly@gmail.com',	'kbk/ktm/s',	'OBC',	'mm/2018/224',	1,	1),
(4,	'kbk/ktm/s/03',	'vishak',	8943876265,	25632,	'hardware_specication.png',	'Screenshot 2022-04-06 121521.png',	'2022-04-20',	'2022-05-12',	'Kudukkappara',	'Monippally',	'Male',	686636,	'Prasad',	'54545645',	'64464',	'53652554',	'SBI',	'545454',	'54545465',	'vishak@gmail.com',	'kbk/ktm/s',	'OBC',	'256',	1,	1),
(5,	'kbk/ktm/s/04',	'vishak p',	45,	56223,	'WhatsApp Image 2022-03-23 at 10.07.20 AM.jpeg',	'wallpaperflare.com_wallpaper.jpg',	'2022-04-28',	'2022-04-21',	'Kudukkappara',	'Monippally',	'Male',	56556,	'Prasad',	'1452369',	'64464',	'0000522',	'SBI',	'fdf5454',	'54545465',	'vishak@gmail.com',	'kbk/ktm/s',	'OBC',	'562',	1,	1),
(6,	'kbk/mkd/s/01',	'rahul',	7645245686,	475587669949,	'level1.2.png',	'Level 0.png',	'2022-02-26',	'1996-06-25',	'panenghat',	'kaiparambu',	'Male',	354652,	'john',	'677683715',	'110095',	'123456789',	'cbi',	'CBIN0282785',	'45464646',	'rahul@gmail.com',	'kbk/mkd/s',	'GENERAL',	'2021124',	1,	1),
(7,	'kbk/ktm/s/05',	'Nabeel',	7559910524,	858585858585,	'Screenshot from 2022-04-22 10-43-51.png',	'Screenshot from 2022-04-22 10-43-51.png',	'2022-05-05',	'2022-05-04',	'arambram',	'arambram',	'',	673571,	'nazir',	'56546565',	'45543554',	'44112200013708',	'canara bank',	'CNRB0001234',	'565656566556',	'admin@gmail.com',	'kbk/ktm/s',	'OBC',	'89778787879',	1,	1)
ON DUPLICATE KEY UPDATE `artisan_id` = VALUES(`artisan_id`), `artisan_code` = VALUES(`artisan_code`), `artisan_name` = VALUES(`artisan_name`), `contact_no` = VALUES(`contact_no`), `aadhar_no` = VALUES(`aadhar_no`), `aadhar` = VALUES(`aadhar`), `photo` = VALUES(`photo`), `doj` = VALUES(`doj`), `dob` = VALUES(`dob`), `addressln1` = VALUES(`addressln1`), `addressln2` = VALUES(`addressln2`), `gender` = VALUES(`gender`), `pin_number` = VALUES(`pin_number`), `father_spouse` = VALUES(`father_spouse`), `lic` = VALUES(`lic`), `esi` = VALUES(`esi`), `account_no` = VALUES(`account_no`), `bank_name` = VALUES(`bank_name`), `ifsc` = VALUES(`ifsc`), `wf_reg_no` = VALUES(`wf_reg_no`), `email` = VALUES(`email`), `unit_code` = VALUES(`unit_code`), `caste` = VALUES(`caste`), `mmdi` = VALUES(`mmdi`), `esi_centr` = VALUES(`esi_centr`), `status` = VALUES(`status`);

DROP TABLE IF EXISTS `kbk_centre`;
CREATE TABLE `kbk_centre` (
  `centre_code` varchar(20) NOT NULL,
  `centre_name` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pin_no` int(6) NOT NULL,
  `po_name` varchar(20) NOT NULL,
  `panchayath` varchar(20) DEFAULT NULL,
  `block` varchar(20) DEFAULT NULL,
  `village` varchar(20) DEFAULT NULL,
  `land_no` varchar(20) DEFAULT NULL,
  `building_no` varchar(20) DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  `ownership` varchar(20) DEFAULT NULL,
  `machinery` varchar(100) DEFAULT NULL,
  `furniture` varchar(100) DEFAULT NULL,
  `electricity` varchar(20) DEFAULT NULL,
  `water` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`centre_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `kbk_centre` (`centre_code`, `centre_name`, `address`, `pin_no`, `po_name`, `panchayath`, `block`, `village`, `land_no`, `building_no`, `area`, `ownership`, `machinery`, `furniture`, `electricity`, `water`) VALUES
(' kbk/krza',	'karukachal',	'asda',	686636,	'karukachal',	'kottayam',	'manarcad',	'manarcad',	'gcvchg',	'nvjv',	'200',	'Owned',	'50200',	'mbmas',	'mnm',	'hvbh'),
('kbk/ktm',	'Kottayam',	'Kottayamad',	655896,	'Kottayam',	'kottayam',	'kottaya',	'kottayam',	'01/562',	'205',	'20.60',	'own',	'50000',	'10000',	'ksebl1245368',	'kwal54785'),
('kbk/mkd',	'Manarkad',	'Manarkadadr',	758692,	'Manarkadpo',	'Manarkadpan',	'kottayam',	'Manarkadvil',	'21/45',	'123',	'10.25',	'Rent',	'40000',	'15000',	'ksebl3452',	'ksw758642'),
('kbk/pmy',	'pampady',	'pampadyadr',	545879,	'pampadypo',	'pampadypan',	'kottayam',	'pampadyvil',	'02/45',	'65',	'50.20',	'owned',	'120000',	'40000',	'ksebl245638',	'kwal45685')
ON DUPLICATE KEY UPDATE `centre_code` = VALUES(`centre_code`), `centre_name` = VALUES(`centre_name`), `address` = VALUES(`address`), `pin_no` = VALUES(`pin_no`), `po_name` = VALUES(`po_name`), `panchayath` = VALUES(`panchayath`), `block` = VALUES(`block`), `village` = VALUES(`village`), `land_no` = VALUES(`land_no`), `building_no` = VALUES(`building_no`), `area` = VALUES(`area`), `ownership` = VALUES(`ownership`), `machinery` = VALUES(`machinery`), `furniture` = VALUES(`furniture`), `electricity` = VALUES(`electricity`), `water` = VALUES(`water`);

DROP TABLE IF EXISTS `kbk_contigency`;
CREATE TABLE `kbk_contigency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` varchar(20) NOT NULL,
  `voucher_no` varchar(20) DEFAULT NULL,
  `water` decimal(10,2) DEFAULT NULL,
  `electricity` decimal(10,2) DEFAULT NULL,
  `land_exps` decimal(10,2) DEFAULT NULL,
  `bldg_repair` decimal(10,2) DEFAULT NULL,
  `stationary` decimal(10,2) DEFAULT NULL,
  `auto_charge` decimal(10,2) DEFAULT NULL,
  `mIsc_exps` decimal(10,2) DEFAULT NULL,
  `charka_repair` decimal(10,2) DEFAULT NULL,
  `loom_repair` decimal(10,2) DEFAULT NULL,
  `c_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `kbk_deduction`;
CREATE TABLE `kbk_deduction` (
  `deduction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ad_rec` decimal(10,2) DEFAULT '0.00',
  `credit_rec` decimal(10,2) DEFAULT '0.00',
  `cov_rec` decimal(10,2) DEFAULT '0.00',
  `wf_rec` decimal(10,2) DEFAULT '0.00',
  `artisan_id` bigint(20) NOT NULL,
  `d_date` date NOT NULL,
  `lic` decimal(10,2) NOT NULL,
  `wf_yes_no` int(1) NOT NULL,
  `work_id` int(11) NOT NULL,
  PRIMARY KEY (`deduction_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `work_id` (`work_id`),
  CONSTRAINT `kbk_deduction_ibfk_1` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_deduction_ibfk_2` FOREIGN KEY (`work_id`) REFERENCES `kbk_work_type` (`work_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_deduction` (`deduction_id`, `ad_rec`, `credit_rec`, `cov_rec`, `wf_rec`, `artisan_id`, `d_date`, `lic`, `wf_yes_no`, `work_id`) VALUES
(1,	0.00,	0.00,	0.00,	0.00,	3,	'2022-07-01',	0.00,	1,	1),
(2,	0.00,	0.00,	0.00,	0.00,	4,	'2022-07-01',	0.00,	1,	1),
(3,	0.00,	0.00,	0.00,	0.00,	5,	'2022-07-01',	0.00,	1,	1),
(4,	0.00,	0.00,	0.00,	0.00,	7,	'2022-07-01',	0.00,	1,	1),
(5,	0.00,	0.00,	0.00,	0.00,	3,	'2022-07-01',	0.00,	1,	2),
(6,	0.00,	0.00,	0.00,	0.00,	4,	'2022-07-01',	0.00,	1,	2),
(7,	0.00,	0.00,	0.00,	0.00,	5,	'2022-07-01',	0.00,	1,	2),
(8,	0.00,	0.00,	0.00,	0.00,	7,	'2022-07-01',	0.00,	1,	2),
(9,	0.00,	0.00,	0.00,	0.00,	3,	'2022-07-01',	0.00,	1,	3),
(10,	0.00,	0.00,	0.00,	0.00,	4,	'2022-07-01',	0.00,	1,	3),
(11,	0.00,	0.00,	0.00,	0.00,	5,	'2022-07-01',	0.00,	1,	3),
(12,	0.00,	0.00,	0.00,	0.00,	7,	'2022-07-01',	0.00,	1,	3),
(13,	0.00,	0.00,	0.00,	0.00,	3,	'2022-07-01',	0.00,	1,	4),
(14,	0.00,	0.00,	0.00,	0.00,	4,	'2022-07-01',	0.00,	1,	4),
(15,	0.00,	0.00,	0.00,	0.00,	5,	'2022-07-01',	0.00,	1,	4),
(16,	0.00,	0.00,	0.00,	0.00,	7,	'2022-07-01',	0.00,	1,	4)
ON DUPLICATE KEY UPDATE `deduction_id` = VALUES(`deduction_id`), `ad_rec` = VALUES(`ad_rec`), `credit_rec` = VALUES(`credit_rec`), `cov_rec` = VALUES(`cov_rec`), `wf_rec` = VALUES(`wf_rec`), `artisan_id` = VALUES(`artisan_id`), `d_date` = VALUES(`d_date`), `lic` = VALUES(`lic`), `wf_yes_no` = VALUES(`wf_yes_no`), `work_id` = VALUES(`work_id`);

DROP TABLE IF EXISTS `kbk_esi_center`;
CREATE TABLE `kbk_esi_center` (
  `esi_id` int(11) NOT NULL AUTO_INCREMENT,
  `center_name` varchar(20) NOT NULL,
  PRIMARY KEY (`esi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_esi_center` (`esi_id`, `center_name`) VALUES
(1,	'kottayam'),
(2,	' pampady')
ON DUPLICATE KEY UPDATE `esi_id` = VALUES(`esi_id`), `center_name` = VALUES(`center_name`);

DROP TABLE IF EXISTS `kbk_holidays`;
CREATE TABLE `kbk_holidays` (
  `holiday_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `holiday_no` int(3) NOT NULL,
  `h_date` date NOT NULL,
  `total_wd` int(3) NOT NULL,
  `da_wages` decimal(10,2) NOT NULL,
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_holidays` (`holiday_id`, `holiday_no`, `h_date`, `total_wd`, `da_wages`) VALUES
(1,	2,	'2022-07-01',	24,	54.00)
ON DUPLICATE KEY UPDATE `holiday_id` = VALUES(`holiday_id`), `holiday_no` = VALUES(`holiday_no`), `h_date` = VALUES(`h_date`), `total_wd` = VALUES(`total_wd`), `da_wages` = VALUES(`da_wages`);

DROP TABLE IF EXISTS `kbk_instructor`;
CREATE TABLE `kbk_instructor` (
  `instructor_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pin` int(6) NOT NULL,
  `phone` bigint(10) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `wages` decimal(10,2) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`instructor_id`),
  UNIQUE (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_instructor` (`instructor_id`, `name`, `address`, `pin`, `phone`, `email`, `wages`, `username`, `password`) VALUES
(1,	'Aneesh',	'chirayathu',	686691,	6282945638,	'aneesh@gmail.com',	12000.00,	'aneesh',	'aneesh'),
(2,	'Nabeel',	'',	673571,	7559910574,	'nabeel@gmail.com',	20000.00,	'nabeel',	'nabeel'),
(3,	'Athul',	'Kopparambil',	650324,	8596748596,	'athul@gmail.com',	10000.00,	'athul',	'athul'),
(4,	'nabeel',	'kudukkappara',	673571,	7559910574,	'vishak@gmail.com',	1500.00,	'vishak',	'vishak'),
(5,	'Franly',	'kudukkappara',	680546,	9539283477,	'nabrel@gmail.com',	500.00,	'franly',	'franly'),
(6,	'Joel',	'mavelikara',	632584,	4578123654,	'joel@gmail.com',	10000.00,	'joel',	'joel'),
(7,	'Bayek',	'Siwa',	690104,	9876543219,	'bayek@gmail.com',	20000.00,	'bayekofsiwa',	'bayekofsiwa*99'),
(8,	'rony',	'joseph',	586254,	8657423165,	'rony@gmail.com',	560.00,	'rony',	'Rony@1998')
ON DUPLICATE KEY UPDATE `instructor_id` = VALUES(`instructor_id`), `name` = VALUES(`name`), `address` = VALUES(`address`), `pin` = VALUES(`pin`), `phone` = VALUES(`phone`), `email` = VALUES(`email`), `wages` = VALUES(`wages`), `username` = VALUES(`username`), `password` = VALUES(`password`);

DROP TABLE IF EXISTS `kbk_login`;
CREATE TABLE `kbk_login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `kbk_login` (`username`, `password`, `user_type`) VALUES
('admin',	'admin',	'admin')
ON DUPLICATE KEY UPDATE `username` = VALUES(`username`), `password` = VALUES(`password`), `user_type` = VALUES(`user_type`);

DROP TABLE IF EXISTS `kbk_preprocessing`;
CREATE TABLE `kbk_preprocessing` (
  `preprocessing_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `s_date` date NOT NULL,
  `wages` decimal(12,4) NOT NULL,
  `attendance` int(11) NOT NULL,
  `pre_id` bigint(20) NOT NULL,
  `artisan_id` bigint(20) NOT NULL,
  `unit_code` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`preprocessing_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `unit_code` (`unit_code`),
  KEY `pre_id` (`pre_id`),
  CONSTRAINT `kbk_preprocessing_ibfk_1` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_preprocessing_ibfk_2` FOREIGN KEY (`unit_code`) REFERENCES `kbk_unit` (`unit_code`) ON DELETE CASCADE,
  CONSTRAINT `kbk_preprocessing_ibfk_3` FOREIGN KEY (`pre_id`) REFERENCES `kbk_preprocessing_variety` (`pre_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_preprocessing` (`preprocessing_id`, `s_date`, `wages`, `attendance`, `pre_id`, `artisan_id`, `unit_code`, `status`) VALUES
(1,	'2022-07-01',	69.0000,	20,	1,	3,	'kbk/ktm/s', 0),
(2,	'2022-07-01',	69.0000,	20,	1,	4,	'kbk/ktm/s', 0),
(3,	'2022-07-01',	69.0000,	20,	1,	5,	'kbk/ktm/s', 0),
(4,	'2022-07-01',	69.0000,	20,	1,	7,	'kbk/ktm/s', 0)
ON DUPLICATE KEY UPDATE `preprocessing_id` = VALUES(`preprocessing_id`), `s_date` = VALUES(`s_date`), `wages` = VALUES(`wages`), `attendance` = VALUES(`attendance`), `pre_id` = VALUES(`pre_id`), `artisan_id` = VALUES(`artisan_id`), `unit_code` = VALUES(`unit_code`);

DROP TABLE IF EXISTS `kbk_preprocessing_type_entry`;
CREATE TABLE `kbk_preprocessing_type_entry` (
  `pre_entry_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `b_grey` decimal(10,4) NOT NULL,
  `b_col` decimal(10,4) NOT NULL,
  `b_black` decimal(10,4) NOT NULL,
  `b_bl` decimal(10,4) NOT NULL,
  `s_grey` decimal(10,4) NOT NULL,
  `s_col` decimal(10,4) NOT NULL,
  `s_black` decimal(10,4) NOT NULL,
  `s_bl` decimal(10,4) NOT NULL,
  `w_grey` decimal(10,4) NOT NULL,
  `w_col` decimal(10,4) NOT NULL,
  `w_black` decimal(10,4) NOT NULL,
  `w_bl` decimal(10,4) NOT NULL,
  `preprocessing_id` bigint(20) NOT NULL,
  PRIMARY KEY (`pre_entry_id`),
  KEY `preprocessing_id` (`preprocessing_id`),
  CONSTRAINT `kbk_preprocessing_type_entry_ibfk_1` FOREIGN KEY (`preprocessing_id`) REFERENCES `kbk_preprocessing` (`preprocessing_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_preprocessing_type_entry` (`pre_entry_id`, `b_grey`, `b_col`, `b_black`, `b_bl`, `s_grey`, `s_col`, `s_black`, `s_bl`, `w_grey`, `w_col`, `w_black`, `w_bl`, `preprocessing_id`) VALUES
(1,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	1),
(2,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	2),
(3,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	3),
(4,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	0.0000,	0.0000,	20.0000,	0.0000,	4)
ON DUPLICATE KEY UPDATE `pre_entry_id` = VALUES(`pre_entry_id`), `b_grey` = VALUES(`b_grey`), `b_col` = VALUES(`b_col`), `b_black` = VALUES(`b_black`), `b_bl` = VALUES(`b_bl`), `s_grey` = VALUES(`s_grey`), `s_col` = VALUES(`s_col`), `s_black` = VALUES(`s_black`), `s_bl` = VALUES(`s_bl`), `w_grey` = VALUES(`w_grey`), `w_col` = VALUES(`w_col`), `w_black` = VALUES(`w_black`), `w_bl` = VALUES(`w_bl`), `preprocessing_id` = VALUES(`preprocessing_id`);

DROP TABLE IF EXISTS `kbk_preprocessing_variety`;
CREATE TABLE `kbk_preprocessing_variety` (
  `pre_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rm_code` varchar(20) NOT NULL,
  `type` varchar(100) NOT NULL,
  `s_wages` decimal(10,4) NOT NULL,
  `s_min_wages` decimal(10,4) NOT NULL,
  `s_target` decimal(10,4) NOT NULL,
  `s_yi` decimal(10,4) NOT NULL,
  `b_wages` decimal(10,4) NOT NULL,
  `b_min_wages` decimal(10,4) NOT NULL,
  `b_target` decimal(10,4) NOT NULL,
  `b_yi` decimal(10,4) NOT NULL,
  `w_wages` decimal(10,4) NOT NULL,
  `w_min_wages` decimal(10,4) NOT NULL,
  `w_target` decimal(10,4) NOT NULL,
  `w_yi` decimal(10,4) NOT NULL,
  `wf` decimal(10,4) NOT NULL,
  `ai` decimal(10,4) NOT NULL,
  `esi` decimal(10,4) NOT NULL,
  `wef` date NOT NULL,
  PRIMARY KEY (`pre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_preprocessing_variety` (`pre_id`, `rm_code`, `type`, `s_wages`, `s_min_wages`, `s_target`, `s_yi`, `b_wages`, `b_min_wages`, `b_target`, `b_yi`, `w_wages`, `w_min_wages`, `w_target`, `w_yi`, `wf`, `ai`, `esi`, `wef`) VALUES
(1,	'RM/S/33',	'33s',	1.1400,	1.1100,	324.0000,	0.2000,	1.2600,	4.5000,	80.0000,	0.2000,	1.0500,	1.6000,	250.0000,	0.2000,	12.0000,	10.0000,	6.5000,	'2022-04-01')
ON DUPLICATE KEY UPDATE `pre_id` = VALUES(`pre_id`), `rm_code` = VALUES(`rm_code`), `type` = VALUES(`type`), `s_wages` = VALUES(`s_wages`), `s_min_wages` = VALUES(`s_min_wages`), `s_target` = VALUES(`s_target`), `s_yi` = VALUES(`s_yi`), `b_wages` = VALUES(`b_wages`), `b_min_wages` = VALUES(`b_min_wages`), `b_target` = VALUES(`b_target`), `b_yi` = VALUES(`b_yi`), `w_wages` = VALUES(`w_wages`), `w_min_wages` = VALUES(`w_min_wages`), `w_target` = VALUES(`w_target`), `w_yi` = VALUES(`w_yi`), `wf` = VALUES(`wf`), `ai` = VALUES(`ai`), `esi` = VALUES(`esi`), `wef` = VALUES(`wef`);

DROP TABLE IF EXISTS `kbk_spinning`;
CREATE TABLE `kbk_spinning` (
  `spinning_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `s_date` date NOT NULL,
  `wages` decimal(10,2) NOT NULL,
  `attendance` int(3) NOT NULL,
  `noh` int(11) NOT NULL,
  `artisan_id` bigint(20) NOT NULL,
  `yarn_id` int(11) NOT NULL,
  `unit_code` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spinning_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `yarn_code` (`yarn_id`),
  KEY `unit_code` (`unit_code`),
  CONSTRAINT `kbk_spinning_ibfk_3` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_spinning_ibfk_4` FOREIGN KEY (`yarn_id`) REFERENCES `kbk_yarn_variety` (`yarn_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_spinning_ibfk_5` FOREIGN KEY (`unit_code`) REFERENCES `kbk_unit` (`unit_code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_spinning` (`spinning_id`, `s_date`, `wages`, `attendance`, `noh`, `artisan_id`, `yarn_id`, `unit_code`, `status`) VALUES
(1,	'2022-07-01',	812.00,	20,	100,	3,	11,	'kbk/ktm/s', 0),
(2,	'2022-07-01',	750.00,	20,	100,	4,	10,	'kbk/ktm/s', 0),
(3,	'2022-07-01',	812.00,	20,	100,	5,	11,	'kbk/ktm/s', 0),
(4,	'2022-07-01',	930.00,	20,	100,	7,	12,	'kbk/ktm/s', 0)
ON DUPLICATE KEY UPDATE `spinning_id` = VALUES(`spinning_id`), `s_date` = VALUES(`s_date`), `wages` = VALUES(`wages`), `attendance` = VALUES(`attendance`), `noh` = VALUES(`noh`), `artisan_id` = VALUES(`artisan_id`), `yarn_id` = VALUES(`yarn_id`), `unit_code` = VALUES(`unit_code`);

DROP TABLE IF EXISTS `kbk_twisting`;
CREATE TABLE `kbk_twisting` (
  `twisting_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `s_date` date NOT NULL,
  `wages` decimal(10,2) NOT NULL,
  `attendance` int(3) NOT NULL,
  `noh` int(11) NOT NULL,
  `artisan_id` bigint(20) NOT NULL,
  `twist_id` int(11) NOT NULL,
  `unit_code` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`twisting_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `twisting_code` (`twist_id`),
  KEY `unit_code` (`unit_code`),
  CONSTRAINT `kbk_twisting_ibfk_3` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_twisting_ibfk_4` FOREIGN KEY (`twist_id`) REFERENCES `kbk_twisting_variety` (`twist_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_twisting_ibfk_5` FOREIGN KEY (`unit_code`) REFERENCES `kbk_unit` (`unit_code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_twisting` (`twisting_id`, `s_date`, `wages`, `attendance`, `noh`, `artisan_id`, `twist_id`, `unit_code`, `status`) VALUES
(1,	'2022-07-01',	812.00,	20,	100,	3,	11,	'kbk/ktm/s', 0),
(2,	'2022-07-01',	750.00,	20,	100,	4,	10,	'kbk/ktm/s', 0),
(3,	'2022-07-01',	812.00,	20,	100,	5,	11,	'kbk/ktm/s', 0),
(4,	'2022-07-01',	930.00,	20,	100,	7,	12,	'kbk/ktm/s', 0)
ON DUPLICATE KEY UPDATE `twisting_id` = VALUES(`twisting_id`), `s_date` = VALUES(`s_date`), `wages` = VALUES(`wages`), `attendance` = VALUES(`attendance`), `noh` = VALUES(`noh`), `artisan_id` = VALUES(`artisan_id`), `twist_id` = VALUES(`twist_id`), `unit_code` = VALUES(`unit_code`);

DROP TABLE IF EXISTS `kbk_unit`;
CREATE TABLE `kbk_unit` (
  `unit_code` varchar(20) NOT NULL,
  `unit_name` varchar(20) NOT NULL,
  `activity` varchar(20) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `centre_id` varchar(20) NOT NULL,
  PRIMARY KEY (`unit_code`),
  KEY `instructor_id` (`instructor_id`),
  KEY `centre_id` (`centre_id`),
  CONSTRAINT `kbk_unit_ibfk_1` FOREIGN KEY (`instructor_id`) REFERENCES `kbk_instructor` (`instructor_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_unit_ibfk_2` FOREIGN KEY (`centre_id`) REFERENCES `kbk_centre` (`centre_code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `kbk_unit` (`unit_code`, `unit_name`, `activity`, `instructor_id`, `centre_id`) VALUES
('kbk/ktm/s',	'kottayam',	'spinning',	1,	'kbk/ktm'),
('kbk/mkd/s',	'manarkad spinning',	'spinning',	2,	'kbk/mkd'),
('kbk/pmy/s',	'pampady',	'spinning',	1,	'kbk/pmy')
ON DUPLICATE KEY UPDATE `unit_code` = VALUES(`unit_code`), `unit_name` = VALUES(`unit_name`), `activity` = VALUES(`activity`), `instructor_id` = VALUES(`instructor_id`), `centre_id` = VALUES(`centre_id`);

DROP TABLE IF EXISTS `kbk_weaving`;
CREATE TABLE `kbk_weaving` (
  `wvg_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date` date NOT NULL,
  `wages` decimal(10,2) NOT NULL,
  `attendance` int(3) NOT NULL,
  `nop` int(11) NOT NULL,
  `mtr` decimal(10,2) NOT NULL,
  `artisan_id` bigint(20) NOT NULL,
  `weaving_id` int(11) NOT NULL,
  `w_id` int(11) NOT NULL,
  `unit_code` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wvg_id`),
  KEY `artisan_id` (`artisan_id`),
  KEY `fw_id` (`w_id`),
  KEY `weaving_id` (`weaving_id`),
  KEY `unit_code` (`unit_code`),
  CONSTRAINT `kbk_weaving_ibfk_11` FOREIGN KEY (`weaving_id`) REFERENCES `kbk_weaving_variety` (`weaving_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_weaving_ibfk_12` FOREIGN KEY (`w_id`) REFERENCES `kbk_weaving_type` (`w_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_weaving_ibfk_13` FOREIGN KEY (`unit_code`) REFERENCES `kbk_unit` (`unit_code`) ON DELETE CASCADE,
  CONSTRAINT `kbk_weaving_ibfk_9` FOREIGN KEY (`artisan_id`) REFERENCES `kbk_artisan` (`artisan_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_weaving` (`wvg_id`, `s_date`, `wages`, `attendance`, `nop`, `mtr`, `artisan_id`, `weaving_id`, `w_id`, `unit_code`, `status`) VALUES
(1,	'2022-07-01',	754.20,	20,	0,	30.00,	3,	13,	38,	'kbk/ktm/s', 0),
(2,	'2022-07-01',	937.80,	20,	0,	30.00,	4,	14,	41,	'kbk/ktm/s', 0),
(3,	'2022-07-01',	1838.70,	20,	30,	54.00,	5,	15,	44,	'kbk/ktm/s', 0),
(4,	'2022-07-01',	846.90,	20,	0,	30.00,	7,	16,	47,	'kbk/ktm/s', 0)
ON DUPLICATE KEY UPDATE `wvg_id` = VALUES(`wvg_id`), `s_date` = VALUES(`s_date`), `wages` = VALUES(`wages`), `attendance` = VALUES(`attendance`), `nop` = VALUES(`nop`), `mtr` = VALUES(`mtr`), `artisan_id` = VALUES(`artisan_id`), `weaving_id` = VALUES(`weaving_id`), `w_id` = VALUES(`w_id`), `unit_code` = VALUES(`unit_code`);

DROP TABLE IF EXISTS `kbk_weaving_type`;
CREATE TABLE `kbk_weaving_type` (
  `w_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_wages` decimal(10,2) NOT NULL,
  `warp_yi` decimal(10,2) NOT NULL,
  `weft_yi` decimal(10,2) NOT NULL,
  `min_wages` decimal(10,2) NOT NULL,
  `target` decimal(10,2) NOT NULL,
  `w_type` varchar(20) NOT NULL,
  `weaving_id` int(11) NOT NULL,
  `wef` date NOT NULL,
  PRIMARY KEY (`w_id`),
  KEY `code` (`weaving_id`),
  CONSTRAINT `kbk_weaving_type_ibfk_1` FOREIGN KEY (`weaving_id`) REFERENCES `kbk_weaving_variety` (`weaving_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_weaving_type` (`w_id`, `w_wages`, `warp_yi`, `weft_yi`, `min_wages`, `target`, `w_type`, `weaving_id`, `wef`) VALUES
(20,	25.14,	1.20,	1.80,	91.99,	4.00,	'RM WARP WEAVING',	7,	'2022-04-01'),
(21,	18.00,	1.20,	1.20,	72.10,	6.00,	'MEAR WEAVING',	7,	'2022-04-01'),
(22,	35.10,	1.80,	1.80,	111.46,	3.00,	'FULL WEAVING',	7,	'2022-04-01'),
(23,	31.26,	1.20,	1.80,	91.99,	4.00,	'RM WARP WEAVING',	8,	'2022-04-01'),
(24,	24.12,	1.20,	1.20,	72.10,	6.00,	'MEAR WEAVING',	8,	'2022-04-01'),
(25,	41.22,	1.80,	1.80,	111.46,	3.00,	'FULL WEAVING',	8,	'2022-04-01'),
(26,	34.05,	1.20,	1.80,	94.22,	4.00,	'RM WARP WEAVING',	9,	'2022-04-01'),
(27,	29.32,	1.20,	1.20,	78.92,	5.00,	'MEAR WEAVING',	9,	'2022-04-01'),
(28,	45.34,	1.80,	1.80,	115.85,	3.00,	'FULL WEAVING',	9,	'2022-04-01'),
(29,	28.23,	1.20,	1.80,	69.16,	5.00,	'RM WARP WEAVING',	10,	'2022-04-01'),
(30,	24.12,	1.20,	1.20,	55.90,	8.00,	'MEAR WEAVING',	10,	'2022-04-01'),
(31,	38.38,	1.80,	1.80,	88.63,	4.00,	'FULL WEAVING',	10,	'2022-04-01'),
(32,	15.48,	1.20,	1.80,	68.20,	5.00,	'RM WARP WEAVING',	11,	'2022-04-01'),
(33,	12.95,	1.20,	1.20,	60.04,	7.00,	'MEAR WEAVING',	11,	'2022-04-01'),
(34,	21.87,	1.80,	1.80,	80.46,	4.00,	'FULL WEAVING',	11,	'2022-04-01'),
(35,	34.05,	1.20,	1.80,	94.22,	4.00,	'RM WARP WEAVING',	12,	'2022-04-01'),
(36,	29.32,	1.20,	1.20,	78.92,	5.00,	'MEAR WEAVING',	12,	'2022-04-01'),
(37,	45.34,	1.80,	1.80,	115.85,	5.00,	'FULL WEAVING',	12,	'2022-04-01'),
(38,	25.14,	1.20,	1.80,	91.99,	4.00,	'RM WARP WEAVING',	13,	'2022-05-01'),
(39,	18.00,	1.20,	1.20,	72.10,	6.00,	'MEAR WEAVING',	13,	'2022-05-01'),
(40,	35.10,	1.80,	1.80,	111.46,	3.00,	'FULL WEAVING',	13,	'2022-05-01'),
(41,	31.26,	1.20,	1.80,	91.99,	4.00,	'RM WARP WEAVING',	14,	'2022-05-01'),
(42,	24.12,	1.20,	1.20,	72.10,	6.00,	'MEAR WEAVING',	14,	'2022-05-01'),
(43,	41.22,	1.80,	1.80,	111.46,	3.00,	'FULL WEAVING',	14,	'2022-05-01'),
(44,	34.05,	1.20,	1.80,	94.22,	4.00,	'RM WARP WEAVING',	15,	'2022-05-01'),
(45,	29.32,	1.20,	1.20,	78.92,	5.00,	'MEAR WEAVING',	15,	'2022-05-01'),
(46,	45.34,	1.80,	1.80,	115.85,	3.00,	'FULL WEAVING',	15,	'2022-05-01'),
(47,	28.23,	1.20,	1.80,	69.16,	5.00,	'RM WARP WEAVING',	16,	'2022-05-01'),
(48,	24.12,	1.20,	1.20,	55.90,	8.00,	'MEAR WEAVING',	16,	'2022-05-01'),
(49,	38.38,	1.80,	1.80,	88.63,	4.00,	'FULL WEAVING',	16,	'2022-05-01'),
(50,	15.48,	1.20,	1.80,	68.20,	5.00,	'RM WARP WEAVING',	17,	'2022-05-01'),
(51,	12.95,	1.20,	1.20,	60.04,	7.00,	'MEAR WEAVING',	17,	'2022-05-01'),
(52,	21.87,	1.80,	1.80,	80.46,	4.00,	'FULL WEAVING',	17,	'2022-05-01'),
(53,	34.05,	1.20,	1.80,	94.22,	4.00,	'RM WARP WEAVING',	18,	'2022-05-01'),
(54,	29.32,	1.20,	1.20,	78.92,	5.00,	'MEAR WEAVING',	18,	'2022-05-01'),
(55,	45.34,	1.80,	1.80,	115.85,	3.00,	'FULL WEAVING',	18,	'2022-05-01')
ON DUPLICATE KEY UPDATE `w_id` = VALUES(`w_id`), `w_wages` = VALUES(`w_wages`), `warp_yi` = VALUES(`warp_yi`), `weft_yi` = VALUES(`weft_yi`), `min_wages` = VALUES(`min_wages`), `target` = VALUES(`target`), `w_type` = VALUES(`w_type`), `weaving_id` = VALUES(`weaving_id`), `wef` = VALUES(`wef`);

DROP TABLE IF EXISTS `kbk_weaving_variety`;
CREATE TABLE `kbk_weaving_variety` (
  `weaving_id` int(11) NOT NULL AUTO_INCREMENT,
  `weaving_code` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `countable` int(1) NOT NULL,
  `length` decimal(10,4) NOT NULL,
  `wf` decimal(10,4) NOT NULL,
  `esi` decimal(10,4) NOT NULL,
  `prime_cost` decimal(10,2) NOT NULL,
  `width` decimal(10,2) NOT NULL,
  `wef` date NOT NULL,
  `ai` decimal(10,4) NOT NULL,
  `yarn_warp` int(11) NOT NULL,
  `yarn_weft` int(11) NOT NULL,
  `thread_group` varchar(20) NOT NULL,
  PRIMARY KEY (`weaving_id`),
  KEY `yarn_code` (`yarn_warp`),
  KEY `yarn_weft` (`yarn_weft`),
  CONSTRAINT `kbk_weaving_variety_ibfk_1` FOREIGN KEY (`yarn_warp`) REFERENCES `kbk_yarn_variety` (`yarn_id`) ON DELETE CASCADE,
  CONSTRAINT `kbk_weaving_variety_ibfk_2` FOREIGN KEY (`yarn_weft`) REFERENCES `kbk_yarn_variety` (`yarn_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_weaving_variety` (`weaving_id`, `weaving_code`, `type`, `rate`, `countable`, `length`, `wf`, `esi`, `prime_cost`, `width`, `wef`, `ai`, `yarn_warp`, `yarn_weft`, `thread_group`) VALUES
(7,	'W/NMC/01',	'1200*115 STDT Plain',	204.00,	0,	0.0000,	12.0000,	6.5000,	150.20,	1.15,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(8,	'W/NMC/02',	'1200*115 STDT Poplin',	258.00,	0,	0.0000,	12.0000,	6.5000,	190.67,	1.15,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(9,	'W/NMC/03',	'1320*127*1.80 Col Do',	248.00,	1,	1.8000,	12.0000,	6.5000,	183.33,	1.27,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(10,	'W/NMC/04',	'1200*115 Col ST',	216.00,	0,	0.0000,	12.0000,	6.5000,	159.56,	1.15,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(11,	'W/NMC/05',	'740*70*1.4 Thorth',	110.00,	1,	1.4000,	12.0000,	6.5000,	81.07,	0.70,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(12,	'W/NMC/06',	'1320*127*2 Col Dothy',	248.00,	1,	2.0000,	12.0000,	6.5000,	183.33,	1.27,	'2022-04-01',	10.0000,	7,	7,	'NMC'),
(13,	'W/NMC/01',	'1200*115 STDT Plain',	240.00,	0,	0.0000,	12.0000,	6.5000,	177.59,	1.15,	'2022-05-01',	10.0000,	7,	7,	'NMC'),
(14,	'W/NMC/02',	'1200*115 STDT Poplin',	305.00,	0,	0.0000,	12.0000,	6.5000,	226.31,	1.15,	'2022-05-01',	10.0000,	7,	7,	'NMC'),
(15,	'W/NMC/03',	'1320*127*1.8 Col Dot',	291.00,	1,	1.8000,	12.0000,	6.5000,	215.83,	1.27,	'2022-05-01',	10.0000,	7,	7,	'NMC'),
(16,	'W/NMC/04',	'1200*115 ST Col',	254.00,	0,	0.0000,	12.0000,	6.5000,	188.18,	1.15,	'2022-05-01',	10.0000,	7,	7,	'NMC'),
(17,	'W/NMC/05',	'740*70*1.40 Thorth',	129.00,	1,	1.4000,	12.0000,	6.5000,	94.94,	0.70,	'2022-05-01',	10.0000,	7,	7,	'NMC'),
(18,	'W/NMC/06',	'1320*127*2 Col Dothy',	291.00,	1,	2.0000,	12.0000,	6.5000,	215.83,	1.27,	'2022-05-01',	10.0000,	7,	7,	'NMC')
ON DUPLICATE KEY UPDATE `weaving_id` = VALUES(`weaving_id`), `weaving_code` = VALUES(`weaving_code`), `type` = VALUES(`type`), `rate` = VALUES(`rate`), `countable` = VALUES(`countable`), `length` = VALUES(`length`), `wf` = VALUES(`wf`), `esi` = VALUES(`esi`), `prime_cost` = VALUES(`prime_cost`), `width` = VALUES(`width`), `wef` = VALUES(`wef`), `ai` = VALUES(`ai`), `yarn_warp` = VALUES(`yarn_warp`), `yarn_weft` = VALUES(`yarn_weft`), `thread_group` = VALUES(`thread_group`);

DROP TABLE IF EXISTS `kbk_work_type`;
CREATE TABLE `kbk_work_type` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_work_type` (`work_id`, `type`) VALUES
(1,	'Spinning'),
(2,	'Weaving'),
(3,	'Preprocessing'),
(4, 'Twisting')
ON DUPLICATE KEY UPDATE `work_id` = VALUES(`work_id`), `type` = VALUES(`type`);

DROP TABLE IF EXISTS `kbk_yarn_consumption`;
CREATE TABLE `kbk_yarn_consumption` (
  `consumption_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `warp_grey` decimal(10,4) DEFAULT '0.0000',
  `warp_colour` decimal(10,4) DEFAULT '0.0000',
  `warp_black` decimal(10,4) DEFAULT '0.0000',
  `warp_bl` decimal(10,4) DEFAULT '0.0000',
  `weft_grey` decimal(10,4) DEFAULT '0.0000',
  `weft_colour` decimal(10,4) DEFAULT '0.0000',
  `weft_black` decimal(10,4) DEFAULT '0.0000',
  `weft_bl` decimal(10,4) DEFAULT '0.0000',
  `weaving_id` int(11) NOT NULL,
  `wef` date NOT NULL,
  PRIMARY KEY (`consumption_id`),
  KEY `weaving_id` (`weaving_id`),
  CONSTRAINT `kbk_yarn_consumption_ibfk_1` FOREIGN KEY (`weaving_id`) REFERENCES `kbk_weaving_variety` (`weaving_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_yarn_consumption` (`consumption_id`, `warp_grey`, `warp_colour`, `warp_black`, `warp_bl`, `weft_grey`, `weft_colour`, `weft_black`, `weft_bl`, `weaving_id`, `wef`) VALUES
(5,	2.7000,	0.0000,	0.0000,	0.0000,	3.9000,	0.0000,	0.0000,	0.0000,	7,	'2022-04-01'),
(6,	0.0000,	2.7000,	0.0000,	0.0000,	0.0000,	3.9000,	0.0000,	0.0000,	8,	'2022-04-01'),
(7,	0.0000,	3.0000,	0.0000,	0.0000,	0.0000,	3.0000,	0.0000,	0.0000,	9,	'2022-04-01'),
(8,	0.0000,	2.7000,	0.0000,	0.0000,	0.0000,	2.6000,	0.0000,	0.0000,	10,	'2022-04-01'),
(9,	1.7000,	0.0000,	0.0000,	0.0000,	1.6000,	0.0000,	0.0000,	0.0000,	11,	'2022-04-01'),
(10,	0.0000,	3.0000,	0.0000,	0.0000,	0.0000,	3.0000,	0.0000,	0.0000,	12,	'2022-04-01'),
(11,	2.7000,	0.0000,	0.0000,	0.0000,	3.9000,	0.0000,	0.0000,	0.0000,	13,	'2022-05-01'),
(12,	0.0000,	2.7000,	0.0000,	0.0000,	0.0000,	3.9000,	0.0000,	0.0000,	14,	'2022-05-01'),
(13,	0.0000,	3.0000,	0.0000,	0.0000,	0.0000,	3.0000,	0.0000,	0.0000,	15,	'2022-05-01'),
(14,	0.0000,	2.7000,	0.0000,	0.0000,	0.0000,	2.6000,	0.0000,	0.0000,	16,	'2022-05-01'),
(15,	1.7000,	0.0000,	0.0000,	0.0000,	1.6000,	0.0000,	0.0000,	0.0000,	17,	'2022-05-01'),
(16,	0.0000,	3.0000,	0.0000,	0.0000,	0.0000,	3.0000,	0.0000,	0.0000,	18,	'2022-05-01')
ON DUPLICATE KEY UPDATE `consumption_id` = VALUES(`consumption_id`), `warp_grey` = VALUES(`warp_grey`), `warp_colour` = VALUES(`warp_colour`), `warp_black` = VALUES(`warp_black`), `warp_bl` = VALUES(`warp_bl`), `weft_grey` = VALUES(`weft_grey`), `weft_colour` = VALUES(`weft_colour`), `weft_black` = VALUES(`weft_black`), `weft_bl` = VALUES(`weft_bl`), `weaving_id` = VALUES(`weaving_id`), `wef` = VALUES(`wef`);

DROP TABLE IF EXISTS `kbk_yarn_variety`;
CREATE TABLE `kbk_yarn_variety` (
  `yarn_id` int(11) NOT NULL AUTO_INCREMENT,
  `yarn_code` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `rate` decimal(10,4) NOT NULL,
  `sliver` decimal(10,4) NOT NULL,
  `value` decimal(10,4) NOT NULL,
  `sliver_value` decimal(10,4) NOT NULL,
  `esi` decimal(10,4) NOT NULL,
  `wf` decimal(10,4) NOT NULL,
  `yi` decimal(10,4) NOT NULL,
  `target` int(11) NOT NULL,
  `mw` decimal(10,4) NOT NULL,
  `ai` decimal(10,4) NOT NULL,
  `wef` date NOT NULL,
  PRIMARY KEY (`yarn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_yarn_variety` (`yarn_id`, `yarn_code`, `type`, `rate`, `sliver`, `value`, `sliver_value`, `esi`, `wf`, `yi`, `target`, `mw`, `ai`, `wef`) VALUES
(7,	'S/33',	'33s',	7.5000,	0.0310,	0.0000,	0.0000,	0.4900,	0.9000,	0.6000,	24,	14.9000,	0.7500,	'2022-04-01'),
(8,	'S/100',	'100s',	8.1200,	0.0120,	0.0000,	0.0000,	0.5300,	0.9700,	0.6000,	20,	17.9000,	0.8100,	'2022-04-01'),
(9,	'S/150',	'150s',	9.3000,	0.0068,	0.0000,	0.0000,	0.6000,	1.1200,	0.6000,	18,	20.1000,	0.9300,	'2022-04-01'),
(10,	'S/33',	'33',	7.5000,	0.0310,	20.4200,	345.0000,	0.4900,	0.9000,	0.6000,	24,	14.9000,	0.7500,	'2022-05-01'),
(11,	'S/100',	'100',	8.1200,	0.0120,	15.1400,	447.0000,	0.5300,	0.9700,	0.6000,	20,	17.9000,	0.8100,	'2022-05-01'),
(12,	'S/150',	'150',	9.3000,	0.0068,	16.5600,	656.0000,	0.6000,	1.1200,	0.6000,	18,	20.1000,	0.9300,	'2022-05-01')
ON DUPLICATE KEY UPDATE `yarn_id` = VALUES(`yarn_id`), `yarn_code` = VALUES(`yarn_code`), `type` = VALUES(`type`), `rate` = VALUES(`rate`), `sliver` = VALUES(`sliver`), `value` = VALUES(`value`), `sliver_value` = VALUES(`sliver_value`), `esi` = VALUES(`esi`), `wf` = VALUES(`wf`), `yi` = VALUES(`yi`), `target` = VALUES(`target`), `mw` = VALUES(`mw`), `ai` = VALUES(`ai`), `wef` = VALUES(`wef`);

DROP TABLE IF EXISTS `kbk_twisting_variety`;
CREATE TABLE `kbk_twisting_variety` (
  `twist_id` int(11) NOT NULL AUTO_INCREMENT,
  `twisting_code` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `rate` decimal(10,4) NOT NULL,
  `yarn` decimal(10,4) NOT NULL,
  `value` decimal(10,4) NOT NULL,
  `yarn_value` decimal(10,4) NOT NULL,
  `esi` decimal(10,4) NOT NULL,
  `wf` decimal(10,4) NOT NULL,
  `yi` decimal(10,4) NOT NULL,
  `target` int(11) NOT NULL,
  `mw` decimal(10,4) NOT NULL,
  `ai` decimal(10,4) NOT NULL,
  `wef` date NOT NULL,
  PRIMARY KEY (`twist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `kbk_twisting_variety` (`twist_id`, `twisting_code`, `type`, `rate`, `yarn`, `value`, `yarn_value`, `esi`, `wf`, `yi`, `target`, `mw`, `ai`, `wef`) VALUES
(7,	'S/33',	'33s',	7.5000,	0.0310,	0.0000,	0.0000,	0.4900,	0.9000,	0.6000,	24,	14.9000,	0.7500,	'2022-04-01'),
(8,	'S/100',	'100s',	8.1200,	0.0120,	0.0000,	0.0000,	0.5300,	0.9700,	0.6000,	20,	17.9000,	0.8100,	'2022-04-01'),
(9,	'S/150',	'150s',	9.3000,	0.0068,	0.0000,	0.0000,	0.6000,	1.1200,	0.6000,	18,	20.1000,	0.9300,	'2022-04-01'),
(10,	'S/33',	'33',	7.5000,	0.0310,	20.4200,	345.0000,	0.4900,	0.9000,	0.6000,	24,	14.9000,	0.7500,	'2022-05-01'),
(11,	'S/100',	'100',	8.1200,	0.0120,	15.1400,	447.0000,	0.5300,	0.9700,	0.6000,	20,	17.9000,	0.8100,	'2022-05-01'),
(12,	'S/150',	'150',	9.3000,	0.0068,	16.5600,	656.0000,	0.6000,	1.1200,	0.6000,	18,	20.1000,	0.9300,	'2022-05-01')
ON DUPLICATE KEY UPDATE `twist_id` = VALUES(`twist_id`), `twisting_code` = VALUES(`twisting_code`), `type` = VALUES(`type`), `rate` = VALUES(`rate`), `yarn` = VALUES(`yarn`), `value` = VALUES(`value`), `yarn_value` = VALUES(`yarn_value`), `esi` = VALUES(`esi`), `wf` = VALUES(`wf`), `yi` = VALUES(`yi`), `target` = VALUES(`target`), `mw` = VALUES(`mw`), `ai` = VALUES(`ai`), `wef` = VALUES(`wef`);

-- 2022-07-30 18:24:19
