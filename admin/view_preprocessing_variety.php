<?php
 include('../connection.php');
 include('../kbk_library.php');
 include('header.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Preprocessing Paramater View</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
          
   <style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
  text-align: center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 2px;
  padding-bottom: 2px;
  
  background-color: #04AA6D;
  color: white;

}
</style>

     
    
  </head>
<body>
<script type="text/javascript" src="../js/mdb.min.js"></script>
  <form name="spinningparaview" action="" method="post">    
    <section class="" style="padding-top: 30px;">
      <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col">
            <div class="">
              <div class="row g-0">
               <div  id="container_content">
                 <div class="text-black">
                  <h3 class="mb-5 text-uppercase">Preprocessing Parameters</h3>            

                  <table class="table-bordered " id="customers">
                    <thead>
                        <tr>
                          <th >SI No.</th>
                          <th style="min-width: 110px;">readymade Code</th>
                          <th >type</th>
                          <th >sizing wages</th>
                          <th >sizing minimum wages</th>
                          <th >sizing target</th>
                          <th >sizing yarn incentive</th>
                          <th style="min-width: 80px;">bobbin winding wages</th>
                          <th >bobbin winding minimum wages</th>
                          <th >bobbin winding target</th>
                          <th >bobbin winding yarn incentive</th>
                          <th >warping wages</th>
                          <th >warping minimum wages</th>
                          <th >warping target</th>
                          <th >warping yarn incentive</th>
                          <th >Welfare Fund</th>
                          <th >Annual Incentive</th>
                          <th style="min-width: 80px;">ESI</th>
                          <th style="min-width: 100px;">With Effect From</th>
                          <th style="background-color: white;border-color: white;"> </th>
                        </tr>
                    </thead>
                    <tbody>

<?php

    $res1 = retrieveData("SELECT * from kbk_preprocessing_variety",$con);
    for($i=0;$i<sizeof($res1);$i++)
    {
        
        //echo '<tr><td>'.strval($i+1).'</td>';
        echo '<tr><td >'.$res1[$i]['pre_id'].'</td>';
        echo '<td >'.$res1[$i]['rm_code'].'</td>';
        echo '<td >'.$res1[$i]['type'].'</td>';
        echo '<td>'.$res1[$i]['s_wages'].'</td>';
        echo '<td>'.$res1[$i]['s_min_wages'].'</td>';
        echo '<td>'.$res1[$i]['s_target'].'</td>';
        echo '<td>'.$res1[$i]['s_yi'].'</td>';
        echo '<td>'.$res1[$i]['b_wages'].'</td>';
        echo '<td>'.$res1[$i]['b_min_wages'].'</td>';
        echo  '<td>'.$res1[$i]['b_target'].'</td>';
        echo  '<td>'.$res1[$i]['b_yi'].'</td>';
        echo  '<td>'.$res1[$i]['w_wages'].'</td>';
        echo  '<td>'.$res1[$i]['w_min_wages'].'</td>';
        echo  '<td>'.$res1[$i]['w_target'].'</td>';
        echo  '<td>'.$res1[$i]['w_yi'].'</td>';
        echo  '<td>'.$res1[$i]['wf'].'</td>';
        echo  '<td>'.$res1[$i]['ai'].'</td>';
        echo  '<td>'.$res1[$i]['esi'].'</td>';
        echo  '<td>'.$res1[$i]['wef'].'</td>';

echo '<td style="border-color: white;background-color: white;"> </td></tr>';      
    }
    

?>  



        </tbody>
        <tfoot></tfoot>
    </table> 


                 </div>
                </div>
              </div>                
            </div>
          </div>            
        </div>
      </div>
    </section>
  </form>
</body>
</html>


<!-- <div style="padding: 0px 50px 30px 40px;">
 -->  

  
