<?php
include('../connection.php');
include('../kbk_library.php');
session_start();

require_once("dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$html=""; 
$html.='<html>';
$html.='<body>';
$html.='<form>'; 
$html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
$html.='<br>';
$html.='<h3 align="center">Minimum Wages Statement Of Spinning For The Month '.$_SESSION["mydata4"].' '.$_SESSION["mydata5"].'</h3>';
$html.='<br>';
$html.=$_SESSION["mydata3"];

$html.='<table style="border-collapse:collapse" border="1" align="center" width=47%>
<thead>
                <tr style="line-height:5.17mm">
                  <th align="center"  valign="middle"  align="center" halign="middle" style="width:20.17mm">SI No.</th>
                  <th align="center"  valign="middle"  align="center" halign="middle" style="width:20.17mm">Artisan Code</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">Artisan Name</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">Verity</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">No. of Hanks</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">Rate</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">Min Wages</th>
                  
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">DA Days</th>
                  <th align="center"  valign="middle" align="center" halign="middle" style="width:20.17mm">DA Wages</th>
                  
                  <th scope="col" align="center" halign="middle" style="width:20.17mm">Total Min Wages </th>
                  <th scope="col" align="center" halign="middle" style="width:20.17mm">Wages Paid</th>
                  <th scope="col" align="center" halign="middle" style="width:20.17mm">Net Min Wages</th>
                 
                </tr>
              </thead>';

              $html.=$_SESSION["mydata6"];

         $html.='</table>';

$html.="</form>";
$html.="</body>";
$html.="</html>";
$dompdf->loadHtml(html_entity_decode($html));	
$dompdf->setPaper('A4', 'landscape'); //portrait or landscape
$dompdf->render();
ob_end_clean();
$dompdf->stream("SPINING REPORT",array("Attachment"=>0));

?>