<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Weaving_Edit</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
   
     <?php 
   include('header.php');  
   ?>
  </head>
  <body>
    <!-- Start your project here-->
    <form name="Weaving_edit" action="weaving.php" method="post">
  <section class="h-100 " style="background-color:#00E6BF">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100" >
        <div class="col" style="background-color:#197F6E">
          <div class="card card-registration my-4" >

          <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase text-center text-primary" >Weaving Search</h3>
                    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                    <script>
$(document).ready(function(){
    $('.form-outline input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("weavingcode_search.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".form-outline").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
                  </script>
    
                    <div class="form-outline mb-4">
                      <input type="text" id="weavingcode" class="form-control form-control-lg" name="weavingcode" placeholder="search by weaving code"  autocomplete="off"/>
                      <label class="form-label" for="unit">Weaving Code</label>
                      <div class="result"></div>
                      
                    </div>
                    

                    
                      <div class="text-center">
                        <input type="submit" class="btn btn-warning btn-lg ms-2"  name="search" method="post" value="search">
                   
    </div>
                      
                    
                    </div>
              

            <div class="row g-0">
                
              <div class="col-xl-6">
                <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Weaving Variety</h3>
  
                    <div class="row">
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="text" name="code" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n">Weaving Code</label>
                    </div>
                  </div>
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="text" name="warpgrey" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n">Warp Grey in meter</label>
                    </div>
                  </div>
                  </div>
                  <div class="row">           
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="variety" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Variety</label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpcolour" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Colour in meter</label>
                      </div>
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="category" aria-label="Default select example" id="category">
                      <option disabled selected>-- Select --</option>
                      <option value="NMC">NMC</option>
                      <option value="MUSLIN">MUSLIN</option>
                      <option value="POLY">POLY</option>
                      <option value="SILK">SILK</option>
                      
                      </select>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpblack" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Black in meter</label>
                      </div>
                    </div>
                  </div>

                  <div class="d-md-flex justify-content-start align-items-center mb-4 py-2">
            <div class="form-group col-md-5">
              <input type="radio" id="measurable" name="lengthtype" value="measurable" checked onclick="EnableDisableTB()">
              Measurable</div>
            
            <div class="form-group col-md-5">
              <input type="radio" id="countable" name="lengthtype" value="countable"  onclick="EnableDisableTB()">
              Countable </div>
          </div>
          
          <div class="row">
            
            
            <div class="col-md-6 mb-4">
            <div class="form-outline">
              <input type="text" id="length" name="lengthofitem" disabled="disabled"  class="form-control form-control-lg" value="0">
              <label class="form-label" for="form3Example8">Length of item </label>
            </div>
            </div>
            <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpbl" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Bl in meter</label>
                      </div>
                    </div>
          </div>
    
          <div class="row">
                  <div class="col-md-6 mb-4">
                  <div class="form-outline ">
                    <input type="text" step="any" min="0" name="rate" class="form-control form-control-lg" />
                    <label class="form-label" for="form3Example8">Value Per Meter </label>
                  </div>
                  </div>
                   <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftgrey" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Grey in meter</label>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" step="any" min="0" name="primemeter" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Prime Cost Per Meter_ </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftcolour" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Colour in meter</label>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" step="any" min="0" name="sqrmeter" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n1">Width In Meter</label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftblack" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Black in meter</label>
                      </div>
                    </div>
                  </div>
  
                  
  
                 
  
                  <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftbl" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Bl in meter</label>
                      </div>
                    </div>
  </div>
                   <div class="row">
                   <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="annualincentive" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Annual Incentive </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="welfarefunds" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Welfare Funds </label>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
    
    <select class="form-select" name="warpcode" aria-label="Default select example">
      <option disabled selected>-- Select Warp Code --</option>
      <?php
                              include('../connection.php'); // Using database connection file here
                              $yarn= mysqli_query($con, "SELECT distinct yarn_code,type From kbk_yarn_variety");  // Use select query here 

                              while($yrn = mysqli_fetch_array($yarn))
                                {
                                 echo "<option value='". $yrn['yarn_code'] ."'>" .$yrn['type']." </option>";  // displaying data in option menu
                                }	
                            ?>  
    </select>

               </div>
               <div class="col-md-6 mb-4">
               <select class="form-select" name="weftcode" aria-label="Default select example">
      <option disabled selected>-- Select Weft Code--</option>
      <?php
                              include('../connection.php'); // Using database connection file here
                              $unid= mysqli_query($con, "SELECT distinct yarn_code,type From kbk_yarn_variety");  // Use select query here 

                              while($unit = mysqli_fetch_array($unid))
                                {
                                 echo "<option value='". $unit['yarn_code'] ."'>" .$unit['type']." </option>";  // displaying data in option menu
                                }	
                            ?>  
    </select>
                
                  </div>
          </div>
          <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="esi" id="esi" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">ESI</label>
                        </div>
                      </div>
                      <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">RM Warp Weaving</h3>
  
                    <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">RM Warp Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Weft Weaving yi</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
  </div>
                  
                  </div>
                  </div>
              </div>
              <div class="col-xl-6">
                <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Full Weaving</h3>
  
                    <div class="form-outline mb-4">
                        <input type="text" name="full_w_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">Full Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="full_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Full Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="full_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Full Weft Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>

                      <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Mear Weaving</h3>
  
                    <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">RM Warp Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Weft Weaving yi</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
  </div>
                      
                      
                  
              </div>
            </div>
            <div class="d-flex justify-content-end pt-3">
                  <input type="button"  name="edit" id="edit" class="btn btn-warning btn-lg ms-2" onclick=formUpdate() method="post" value="EDIT"></button>
                      <input type="submit" name="update" class="btn btn-warning btn-lg ms-2" value="UPDATE" >
                </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  </form>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
<?php
$wvcode = "";
$type  = "";
$rate  = "";
$countable= "";
$length = "";
$block = "";
$village = "";
$landno="";
$bno="";
$area="";
$ownership="";
$machinery="";
$furniture="";
$electricity="";
$water="";
$instid="";
 if(isset($_POST['search']))
 {
     $weavingcode=$_POST['weavingcode'];
     $sql = "select * from kbk_weaving_variety WHERE  weaving_code = '$weavingcode'"; 
     
     $result = mysqli_query($con, $sql);
     $row = mysqli_fetch_array($result);  
     
      $wvcode     = $row['weaving_code'];
      $type  = $row['type'];
      $rate      = $row['rate'];
      $countable   = $row['countable'];
      $length    = $row['length'];
      $wf    = $row['wf'];
      $prime_cost     = $row['prime_cost'];
      $width  = $row['width'];
      $wef      = $row['wef'];
      $ai  = $row['ai'];
      $yarn_warp    = $row['yarn_warp'];
      $yarn_weft    = $row['yarn_weft'];
      $thread_group  = $row['thread_group'];
      




 }
?>