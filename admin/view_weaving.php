<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>weaving</title>
    <!-- MDB icon -->
    <link rel="icon" href="img/insreg.png" type="image/x-icon" />
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->
    
    <header>
      <!-- Navbar -->
      <?php 
        include('header.php')
      ?>
      
      </header>
<!-- Tabs navs -->
<ul class="nav nav-tabs nav-fill mb-3" id="ex1" role="tablist">
    <li class="nav-item" role="presentation">
     <a
      class="nav-link active"
      id="ex2-tab-1"
      data-mdb-toggle="tab"
      href="#ex2-tabs-1"
      role="tab"
      aria-controls="ex2-tabs-1"
      aria-selected="true"
     >Weaving Variety</a
     >
    </li>

    <li class="nav-item" role="presentation">
      <a
        class="nav-link"
        id="ex2-tab-2"
        data-mdb-toggle="tab"
        href="#ex2-tabs-2"
        role="tab"
        aria-controls="ex2-tabs-2"
        aria-selected="false"
        >RM Warp Weaving</a
      >
    </li>
    <li class="nav-item" role="presentation">
      <a
        class="nav-link"
        id="ex2-tab-3"
        data-mdb-toggle="tab"
        href="#ex2-tabs-3"gandhi
        >Mear Weaving</a
      >
    </li>
    <li class="nav-item" role="presentation">
      <a
        class="nav-link "
        id="ex2-tab-4"
        data-mdb-toggle="tab"
        href="#ex2-tabs-4"
        role="tab"
        aria-controls="ex2-tabs-4"
        aria-selected="false"
        >Full Weaving</a
      >
    </li>
  </ul>
  <!-- Tabs navs -->
  
  <!-- Tabs content -->
  <div class="tab-content" id="ex2-content">
    <div
      class="tab-pane fade "
      id="ex2-tabs-4"
      role="tabpanel"
      aria-labelledby="ex2-tab-4"
    >
    <form name="Weaving" action="weaving.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    <img
                      src="../images/wgp.png"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                    />
    
    
                    
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                      <h3 class="mb-5 text-uppercase">Full Weaving</h3>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="full_w_id" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Full Weaving Id</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="full_w_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">Full Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="full_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Full Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="full_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Full Weft Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="code" aria-label="Default select example" id="code">
                          <option disabled selected>-- Weaving code --</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $cen= mysqli_query($con, "SELECT weaving_id From kbk_weaving_variety");  // Use select query here 

                              while($center = mysqli_fetch_array($cen))
                                {
                                 echo "<option value='". $center['weaving_id'] ."'>" .$center['weaving_id'] ." </option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <button type="reset" class="btn btn-light btn-lg">Reset</button>
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="fullsubmit">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>
    </div>


    <div
      class="tab-pane fade"
      id="ex2-tabs-2"
      role="tabpanel"
      aria-labelledby="ex2-tab-2"
    >
    <form name="Weaving" action="weaving.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    <img
                      src="../images/wgp.png"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                    />
    
    
                    
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                      <h3 class="mb-5 text-uppercase">RM Warp Weaving</h3>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_id" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">RM Warp Weaving Id</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">RM Warp Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Weft Weaving yi</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="code" aria-label="Default select example" id="code">
                          <option disabled selected>-- Weaving code --</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $cen= mysqli_query($con, "SELECT weaving_id From kbk_weaving_variety");  // Use select query here 

                              while($center = mysqli_fetch_array($cen))
                                {
                                 echo "<option value='". $center['weaving_id'] ."'>" .$center['weaving_id'] ." </option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <button type="reset" class="btn btn-light btn-lg">Reset</button>
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="rmsubmit">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>
    </div>


    <div
      class="tab-pane fade"
      id="ex2-tabs-3"
      role="tabpanel"
      aria-labelledby="ex2-tab-3"
    >
    <form name="Weaving" action="weaving.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    <img
                      src="../images/wgp.png"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                    />
    
    
                    
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                      <h3 class="mb-5 text-uppercase">Mear Weaving</h3>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="mear_w_id" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Mear Weaving Id</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="mear_w_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">Mear Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="mear_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Mear Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="mear_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">Mear Weft Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="code" aria-label="Default select example" id="code">
                          <option disabled selected>-- Weaving code --</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $cen= mysqli_query($con, "SELECT weaving_id From kbk_weaving_variety");  // Use select query here 

                              while($center = mysqli_fetch_array($cen))
                                {
                                 echo "<option value='". $center['weaving_id'] ."'>" .$center['weaving_id'] ." </option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
      
                      <div class="d-flex justify-content-end pt-3">
                        <button type="reset" class="btn btn-light btn-lg">Reset</button>
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="mearsubmit">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>
    </div>


    <div
    class="tab-pane fade show active"
    id="ex2-tabs-1"
    role="tabpanel"
    aria-labelledby="ex2-tab-1"
  >
  <form name="Weaving" action="weaving.php" method="post">
  <section class="h-100 bg-dark">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col">
          <div class="card card-registration my-4">
            <div class="row g-0">
              <div class="col-xl-6 d-none d-xl-block">
                
                <img
                  src="../images/wgp.png"
                  alt="Sample photo"
                  class="img-fluid"
                  style="border-top-left-radius: .30rem; border-bottom-left-radius: .30rem;"
                />
              </div>
              <div class="col-xl-6">
                <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Weaving Variety</h3>
  
                    <div class="row">
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="text" name="code" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n">Code</label>
                    </div>
                  </div>
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="text" name="warpgrey" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n">Warp Grey</label>
                    </div>
                  </div> 
                   </div> 
                   <div class="row">           
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="variety" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Variety</label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpcolour" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Colour</label>
                      </div>
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="category" aria-label="Default select example" id="category">
                      <option disabled selected>-- Select --</option>
                      <option value="SC">NMC</option>
                      <option value="ST">MUSLIN</option>
                      <option value="OBC">POLY</option>
                      <option value="OEC">SILK</option>
                      
                      </select>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpblack" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Black</label>
                      </div>
                    </div>
                  </div>
                           
                  <div class="d-md-flex justify-content-start align-items-center mb-4 py-2">
            <div class="form-group col-md-5">
              <input type="radio" id="measurable" name="lengthtype" value="measurable" checked onclick="EnableDisableTB()">
              Measurable</div>
            
            <div class="form-group col-md-5">
              <input type="radio" id="countable" name="lengthtype" value="countable"  onclick="EnableDisableTB()">
              Countable </div>
          </div>
          <div class="row">
            
            
            <div class="col-md-6 mb-4">
            <div class="form-outline">
              <input type="text" id="length" name="lengthofitem" disabled="disabled"  class="form-control form-control-lg" value="0">
              <label class="form-label" for="form3Example8">Length of item </label>
            </div>
            </div>
            <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="warpbl" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Warp Bl</label>
                      </div>
                    </div>
          </div>
    
          <script type="text/javascript">
    function EnableDisableTB() {
        var others = document.getElementById("countable");
        var length = document.getElementById("length");
        length.disabled = countable.checked ? false : true;
        length.value="";
        if (!length.disabled) {
            length.focus();
        }
    }
</script>        
                 <div class="row">
                  <div class="col-md-6 mb-4">
                  <div class="form-outline ">
                    <input type="text" step="any" min="0" name="rate" class="form-control form-control-lg" />
                    <label class="form-label" for="form3Example8">Value Per Meter </label>
                  </div>
                  </div>
                   <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftgrey" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Grey</label>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" step="any" min="0" name="primemeter" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Prime Cost Per Meter_ </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftcolour" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Colour</label>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" step="any" min="0" name="sqrmeter" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n1">Width In Meter</label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftblack" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Black</label>
                      </div>
                    </div>
                  </div>
  
                  
  
                 
  
                  <div class="row">
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="weftbl" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1n">Weft Bl</label>
                      </div>
                    </div>
  </div>
                   <div class="row">
                   <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="annualincentive" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Annual Incentive </label>
                      </div>
                    </div>
                    <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="text" name="welfarefunds" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">Welfare Funds </label>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-4">
    
    <select class="form-select" name="warpcode" aria-label="Default select example">
      <option disabled selected>-- Select Warp --</option>
      <?php
                              include('../connection.php'); // Using database connection file here
                              $yarn= mysqli_query($con, "SELECT yarn_id,type From kbk_yarn_variety");  // Use select query here 

                              while($yrn = mysqli_fetch_array($yarn))
                                {
                                 echo "<option value='". $yrn['yarn_id'] ."'>" .$yrn['type']." </option>";  // displaying data in option menu
                                }	
                            ?>  
    </select>

               </div>
               <div class="col-md-6 mb-4">
               <select class="form-select" name="weftcode" aria-label="Default select example">
      <option disabled selected>-- Select Weft Code--</option>
      <?php
                              include('../connection.php'); // Using database connection file here
                              $unid= mysqli_query($con, "SELECT yarn_id,type From kbk_yarn_variety");  // Use select query here 

                              while($unit = mysqli_fetch_array($unid))
                                {
                                 echo "<option value='". $unit['yarn_id'] ."'>" .$unit['type']." </option>";  // displaying data in option menu
                                }	
                            ?>  
    </select>
                
                  </div>
          </div>
  
                 
                  
  
                  <div class="d-flex justify-content-end pt-3">
                    <button type="submit" class="btn btn-warning btn-lg ms-2" name="wvsubmit">Submit</button>
                  </div>
  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  </form>
  </div>
  </div>
  <!-- Tabs content -->
  

    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
