<?php
 include('../connection.php');
 include('../kbk_library.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
   
 
      <title>Report</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
   
      <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <?php 
        include('header.php');
      ?>
</head>
<body>
<script type="text/javascript">
  function displayReport(temp)
  {
  
    const arr=['fru','mru','yiru','mwru','wfru','aru'];
    document.getElementById('buttonid').value=temp;
    document.getElementById('selectdetails').style.display="block";
     document.getElementById('unitname').setAttribute('hidden','true');
      document.getElementById('unit').setAttribute('disabled','true');
    if(arr.includes(temp))
    {
       document.getElementById('unitname').removeAttribute('hidden');
     document.getElementById('unit').removeAttribute('disabled');
       
      
    }
    
  }

</script>


<section class="" style="background-color: black;padding-top: 50px;">
   
      
<form action="calculation_view_main.php" method="post"> 
      <div class=" py-5">

       
            <div class="row" style="margin-left:  35px; ">
              
                <div class="card col-md-6 mb-4 py-5" style="padding-left:  50px; margin-left: 20px; ">
                 <div class="row" > 
                  <div class="col-md-4 mb-4"></div>
                  <div class="col-md-4 mb-4"><span style="padding-left:  20px;"> DISTRICT WISE</span></div>
                  <div class="col-md-4 mb-4"><span style="padding-left:  25px;"> UNIT WISE</span></div>
                </div>
                  <div class="row" > 
              <div class="col-md-4 mb-4">
                <label for="">Full Report View</label>
              </div>
              <div class="col-md-4 mb-4">
                  <input type="button" id="frd" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
              <div class="col-md-4 mb-4">
                  <input type="button" id="fru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
            </div>            
            <div class="row">
              <div class="col-md-4 mb-4">
               <label >Monthly Report</label>
              </div>
              <div class="col-md-4 mb-4">
                <input type="button" id="mrd" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
              <div class="col-md-4 mb-4">
                  <input type="button" id="mru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
            </div>  
            <div class="row">
              <div class="col-md-4 mb-4">
                <label >Yarn Incentive Report</label>
              </div>
              <div class="col-md-4 mb-4">
                <input type="button" id="yird" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
              <div class="col-md-4 mb-4">
                  <input type="button" id="yiru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
              </div>
             </div> 
             <div class="row">
                <div class="col-md-4 mb-4">
                  <label >Minimum Wages Report</label>
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="mwrd" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="mwru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
              </div>  
              <div class="row">
                <div class="col-md-4 mb-4">
                  <label >Welfare Fund Report</label>
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="wfrd" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="wfru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
              </div>  
              <div class="row">
                <div class="col-md-4 mb-4">
                  <label >Advance Report</label>
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="ard" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
                <div class="col-md-4 mb-4">
                  <input type="button" id="aru" value="Report" class="btn btn-secondary" onclick="displayReport(this.id)" style="width:150px;">
                </div>
              </div>  
            </div> 


           <div class="card col-md-5 mb-4" id="selectdetails" style="display:none; margin: 50px 30px; padding:   50px;max-height:  350px;" >
              <input type="text" name="buttonid" id="buttonid" hidden="true">         
       <div class="row">
                     <div class="col-md-5 mb-4">                       
                       <label class="form-label" for="worktype">Work Type</label>                   
                     </div>  
                     <div class="col-md-5 mb-4">
                      <select class="form-select" name="worktype" aria-label="Default select example" required>
                       <?php    
                        echo "<option value=''>-- Select work--</option>";           
                        $types=retrieveData("SELECT work_id,type From kbk_work_type",$con);  // Use select query here 
                        for($i=0;$i<sizeof($types);$i++)
                         {
                           echo "<option value='". $types[$i]['work_id'] ."'>" .$types[$i]['type'] ."</option>";  // displaying data in option menu
                         }  
                         
                       ?>  
                      </select>
                     </div>
                    </div>

<div class="row" id="unitname" >

    <div class="col-md-5 mb-4">                       
       <label class="form-label" for="unit">Unit Name</label>                   
    </div>
                      
    <div class="col-md-5 mb-4">
    
        <select class="form-select" name="unit" id="unit" aria-label="Default select example" required>
         <?php    
             echo "<option value=''>-- Select Unit--</option>";           
             $units= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);  // Use select query here  
             for($i=0;$i<sizeof($units);$i++)
               {
              
              echo "<option value='".$units[$i]['unit_code']."'>" .$units[$i]['unit_code']." ".$units[$i]['unit_name'] ."</option>";  // displaying data in option menu

              $htmldata= $unit[$i]['unit_name'];  
      $_SESSION["mydata1"]=$htmldata; 
               }  
              
          ?>  
        </select>
    </div>

</div>

<div class="row">

    <div class="col-md-5 mb-4">                       
       <label class="form-label" for="unit">Month and Year</label>                   
    </div>
                      
    <div class="col-md-5 mb-4">
    
        <select class="form-select" name="wdate" aria-label="Default select example" required>
         <?php    
                echo "<option value=''>-- Select --</option>";           
                $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $res1=retrieveData("SELECT  h_date FROM kbk_holidays",$con);

                
                for ($i = 0; $i < sizeof($res1); $i++)
                {
                   $m1[$i]= strval(date('m',strtotime($res1[$i]['h_date'])));
                   $y1[$i]= strval(date('Y',strtotime($res1[$i]['h_date'])));
                   echo '<option value="'.$res1[$i]['h_date'].'">'.$months_name[strval($m1[$i])-1]." ".$y1[$i].'</option>'."\n";
                } 
               
                  
          ?>  
        </select>
    </div>

</div>



<div class="row">
  <div class="col-md-4 mb-4">
  </div>
  <div class="col-md-4 mb-4"> 
    <div class=" justify-content-center">
      <input type="submit"   class="btn btn-warning btn-lg" name="entry" value="Submit">
    </div>
  </div>
</div>
</div>


           </div>
          </div>
        </div>
            

  </form> 
    </section>
    <script type="text/javascript" src="../js/mdb.min.js"></script> 

    </body>
</html>
