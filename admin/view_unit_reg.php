<?php
include('../connection.php');
include('../kbk_library.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Unit Registration</title>
    
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->


    

<form name="unitregistration" action="unit_reg.php" method="post">

    <!-- Navbar -->
<?php 
include('header.php') ;

?>
<!-- Navbar -->


    <section class="h-100 bg-dark">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col">
            <div class="card card-registration my-4">
              <div class="row g-0">
                <div class="col-xl-6 d-none d-xl-block">
                  <img
                    src="../images/Unit_Gandhi.jpg"
                    alt="Sample photo"
                    class="img-fluid"
                    style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                  readonly/>
                </div>
                <div class="col-xl-6">
                  <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">UNIT REGISTRATION</h3>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
    
                        <select class="form-select" aria-label="Default select example" name="centre_code" id="centre_code" onChange="takeSelectboxValue()">
                          <option value="" disabled selected >-- Centre Code --</option>
                          <?php
                          
                          $res1 = retrieveData("SELECT centre_code,centre_name From kbk_centre",$con);
                          print_r($res1);
                          for($i=0;$i<sizeof($res1);$i++)
                          { 
                            echo "<option value='". $res1[$i]['centre_code'] ."'>".$res1[$i]['centre_code']." ".$res1[$i]['centre_name'] ."</option>";
                          }
                          ?>
                
                      </select>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-md-6 mb-4">
                      <label class="form-label" for="form3Example1n"><span style="color:#ff0000">*</span>Unit Code</label>
                        <div class="form-outline">
                          <input type="text" name="unit_code" id="unit_code" class="form-control form-control-lg" required autocomplete=off/>
                          
                        </div>
                        <span id="errorname"></span>
                      </div>
                    </div>

                  
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="unit_name" id="unit_name" class="form-control form-control-lg" required/>
                          <label class="form-label" for="pin"><span style="color:#ff0000">*</span>Unit Name</label>
                        </div>
                        <span id="errorpin"></span>
                      </div>
                        </div>

    
                        <div class="row">
                  <div class="col-md-6 mb-4">
    
                        <select class="form-select" name="instructorid" id="instructorid"aria-label="Default select example" required>
                          <option disabled selected>-- Instructor ID --</option>
                          <?php
                          
                          $res1 = retrieveData("SELECT instructor_id,name From kbk_instructor",$con);
                          print_r($res1);
                          for($i=0;$i<sizeof($res1);$i++)
                          { 
                            echo "<option value='". $res1[$i]['instructor_id'] ."'>".$res1[$i]['instructor_id']." ".$res1[$i]['name'] ."</option>";
                          }
                          ?>
                        </select>
    
                      </div>
                    </div>

                    <div class="row">
                  <div class="col-md-6 mb-4">
    
                        <select class="form-select" name="activity" id="instructorid"aria-label="Default select example">
                          <option disabled selected>-- Activity --</option>
                          <?php
                          
                          $res1 = retrieveData("SELECT work_id,type From kbk_work_type",$con);
                          print_r($res1);
                          for($i=0;$i<sizeof($res1);$i++)
                          { 
                            echo "<option value='". $res1[$i]['work_id'] ."'>".$res1[$i]['work_id']." ".$res1[$i]['type'] ."</option>";
                          }
                          ?>
                        </select>
    
                      </div>
                    </div>

    
                    
    
                    
                    <!--Buttons-->
                    <div class="d-flex justify-content-end pt-3">
                      <button type="reset" class="btn btn-light btn-lg">Reset all</button>
                      <input type="submit"   class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Register">
                          <!--pop up-->
                      <div class="modal top fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-mdb-backdrop="true" data-mdb-keyboard="true">
                        <div class="modal-dialog  ">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Registered</h5>
                              
                            </div>
                            
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                                Close
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    <script type="text/javascript">
      function takeSelectboxValue()
      {
     var select=document.getElementById("centre_code").value;
    //  alert(select);
     document.getElementById("unit_code").value=select;
      }

    </script>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
    <script type="text/javascript" src="validation.js"></script>

  </body>
</html>
