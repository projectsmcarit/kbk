<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Unit Registration</title>
    
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->


    

<form name="unitregistration" action="centre_reg.php" method="post">

    <!-- Navbar -->
<?php include('header.php') 
?>
<!-- Navbar -->


    <section class="h-100 bg-dark">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col">
            <div class="card card-registration my-4">
              <div class="row g-0">
                <div class="col-xl-6 d-none d-xl-block">
                  <img
                    src="../images/Unit_Gandhi.jpg"
                    alt="Sample photo"
                    class="img-fluid"
                    style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                  readonly/>
                </div>
                <div class="col-xl-6">
                  <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Centre registration</h3>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="unitcode" class="form-control form-control-lg" id="unitcode"  required />
                          <label class="form-label" for="unitcode"><span style="color:#ff0000">*</span>Centre Code</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="unitname" id="name" class="form-control form-control-lg" onchange="nameValidate()" required/>
                          <label class="form-label" for="form3Example1n"><span style="color:#ff0000">*</span>Centre Name</label>
                        </div>
                        <span id="errorname"></span>
                      </div>
                    </div>

                    <div class="form-outline mb-4">
                      <input type="text" name="address" id="address" class="form-control form-control-lg" required />
                      <label class="form-label" for="address"><span style="color:#ff0000">*</span>Address</label>
                    </div>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" name="pin" id="pin" class="form-control form-control-lg"  onchange="pinValidate()" required/>
                          <label class="form-label" for="pin"><span style="color:#ff0000">*</span>Pin Number</label>
                        </div>
                        <span id="errorpin"></span>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="postoffice" id="postoffice" class="form-control form-control-lg" required />
                          <label class="form-label" for="postoffice"><span style="color:#ff0000">*</span>Post office Name</label>
                        </div>
                      </div>
                    </div>
    
                    
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="panchayath" id="panchayath" class="form-control form-control-lg" />
                          <label class="form-label" for="panchayath">Panchayath</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="block" id="block" class="form-control form-control-lg"/>
                          <label class="form-label" for="block">Block</label>
                        </div>
                      </div>
                    </div>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="village" id="village" class="form-control form-control-lg" />
                          <label class="form-label" for="village">Village</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="landnumber" id="landnumber" class="form-control form-control-lg"  />
                          <label class="form-label" for="landnumber">Land Number</label>
                        </div>
                      </div>
                    </div>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="buildingnumber" id="buildingnumber" class="form-control form-control-lg"  />
                          <label class="form-label" for="buildingnumber">Building Number</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="area" id="area" class="form-control form-control-lg" />
                          <label class="form-label" for="area">Area</label>
                        </div>
                        <span id="errorarea"></span>
                      </div>
                    </div>
    
                    
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
    
                        <select class="form-select" aria-label="Default select example" name="ownership">
                          <option value="1" disabled selected >Ownership</option>
                          <option value="free of rent">Free Of Rent</option>
                          <option value="Owned">Own</option>
                          <option value="Rent">Rent</option>
                        </select>
    
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="machinery" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Machinery</label>
                        </div>
                      </div>
                      
                    </div>


                    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="furniture" id="furniture" class="form-control form-control-lg"  />
                          <label class="form-label" for="furniture">Furniture</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="electricity"  id="electricity"  class="form-control form-control-lg"  />
                          <label class="form-label" for="electricity">Electricity</label>
                        </div>
                      </div>
                    </div>



                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="water" id="water" class="form-control form-control-lg" />
                          <label class="form-label" for="water">Water</label>
                        </div>
                      </div>
                    </div>

    
                    
    
                    
                    <!--Buttons-->
                    <div class="d-flex justify-content-end pt-3">
                      <button type="reset" class="btn btn-light btn-lg">Reset all</button>
                      <input type="submit"   class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Register">
                          <!--pop up-->
                      <div class="modal top fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-mdb-backdrop="true" data-mdb-keyboard="true">
                        <div class="modal-dialog  ">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Registered</h5>
                              
                            </div>
                            
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                                Close
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
    <script type="text/javascript" src="validation.js"></script>

  </body>
</html>
