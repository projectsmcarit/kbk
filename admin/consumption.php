<html>
    <body>
      <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Consumption</title>
    <?php
        include('header.php'); 
        ?>
    <h1 style="text-align:center;padding-top: 75px;">CONSUMPTION</h1>
      <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
    <div style="padding-top:2%;">
<table class="table table-hover text-nowrap" >

  <thead>
    <tr>
      <th scope="col">UNIT</th>
      <th scope="col">Initial Stock</th>
      <th scope="col">Unique Purchases</th>
      <th scope="col">Quantity</th>
      <th scope="col">Product Revenue</th>
      <th scope="col">Avg. Price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">U1</th>
      <td>18,492</td>
      <td>228</td>
      <td>350</td>
      <td>$4,787.64</td>
      <td>$13.68</td>
    </tr>
    <tr>
      <th scope="row">U2</th>
      <td>
        <span class="text-danger">
          <i class="fas fa-caret-down me-1"></i><span>-48.8%%</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>14.0%</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>46.4%</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>29.6%</span>
        </span>
      </td>
      <td>
        <span class="text-danger">
          <i class="fas fa-caret-down me-1"></i><span>-11.5%</span>
        </span>
      </td>
    </tr>
    <tr>
      <th scope="row">U3</th>
      <td>
        <span class="text-danger">
          <i class="fas fa-caret-down me-1"></i><span>-17,654</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>28</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>111</span>
        </span>
      </td>
      <td>
        <span class="text-success">
          <i class="fas fa-caret-up me-1"></i><span>$1,092.72</span>
        </span>
      </td>
      <td>
        <span class="text-danger">
          <i class="fas fa-caret-down me-1"></i><span>$-1.78</span>
        </span>
      </td>
    </tr>
  </tbody>
</table>
</div>
<script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
</body>
</html>