const form = document.getElementsById('artisanregistration');
const art_name = document.getElementById('art_name');
const email = document.getElementById('email');
const addressln1 = document.getElementById('addressln1');
const addressln2 = document.getElementById('addressln2');
const contact = document.getElementById('contact');
const pin = document.getElementById('pin');
const adar = document.getElementById('adar');
const aadharpic = document.getElementById('aadharpic');
const pic = document.getElementById('pic');
const dob = document.getElementById('dob');
const fos = document.getElementById('fos');
const gender = document.getElementById('gender');
const lic = document.getElementById('lic');
const esi = document.getElementById('esi');
const accno = document.getElementById('accno');
const bankname = document.getElementById('bankname');
const ifsc = document.getElementById('ifsc');
const wf = document.getElementById('wf');
const unitid = document.getElementById('unitid');
const doj = document.getElementById('doj');
const esi_centr = document.getElementById('esi_centr');
const mmid = document.getElementById('mmid');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    checkInputs();
});

function checkInputs() {
    const art_namevalue = art_name.value.trim();
    const emailvalue = email.value.trim();
    const addressln1value = addressln1.value.trim();
    const addressln2value = addressln2.value.trim();
    const contactvalue = contact.value.trim();
    const pinvalue = pin.value.trim();
    const adarvalue = adar.value.trim();
    const aadharpicvalue = aadharpic.value.trim();
    const picvalue = pic.value.trim();
    const dobvalue = dob.value.trim();
    const fosvalue = fos.value.trim();
    const gendervalue = gender.value.trim();
    const licvalue = lic.value.trim();
    const esivalue = esi.value.trim();
    const accnovalue = accno.value.trim();
    const banknamevalue = bankname.value.trim();
    const ifscvalue = ifsc.value.trim();
    const wfvalue = wf.value.trim();
    const unitidvalue = unitid.value.trim();
    const dojvalue = doj.value.trim();
    const esi_centrvalue = esi_centr.value.trim();
    const mmidvalue = mmid.value.trim();


    if (art_namevalue == '', emailvalue == '', addressln1value = '', addressln2value = '', contactvalue = '', pinvalue = '', adarvalue = '', aadharpicvalue = '', picvalue = '', dobvalue = '', fosvalue = '', gendervalue = '', esivalue = '', licvalue = '', accnovalue = '', banknamevalue = '', ifscvalue = '', wfvalue = '', unitidvalue = '', dojvalue = '', esi_centrvalue = '', mmidvalue = '') {
        setErrorFor(art_name, emailvalue, addressln1value, addressln2value, contactvalue, pinvalue, adarvalue, aadharpicvalue, picvalue, dobvalue, fosvalue, gendervalue, esivalue, licvalue, accnovalue, banknamevalue, ifscvalue, wfvalue, unitidvalue, dojvalue, esi_centrvalue, mmidvalue, 'Cannot be blank');
    } else {
        setSuccessFor(art_name);
    }
}

function nameValidate() {

    var name = document.getElementById("name").value;
    var letters = /^[A-Za-z\s]+$/;
    if (name.match(letters)) {
        document.getElementById("errorname").innerHTML = "";
        return true;
    } else {
        document.getElementById("name").value = "";
        document.getElementById("errorname").innerHTML = "<span style='color: red;'>" +
            "invalid name </span>";
        return false;
    }

}

function emailValidate() {
    var email = document.getElementById("email").value;
    var letters = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (email.match(letters)) {
        document.getElementById("erroremail").innerHTML = "";
        return true;
    } else {
        document.getElementById("email").value = "";
        document.getElementById("erroremail").innerHTML = "<span style='color: red;'>" +
            "invalid email</span>";
        return false;
    }

}

function phoneValidate() {
    var phone = document.getElementById("contact").value;
    var letters = /^[789]\d{9}$/;
    if (phone.match(letters)) {
        document.getElementById("errorphone").innerHTML = "";
        return true;
    } else {
        document.getElementById("contact").value = "";
        document.getElementById("errorphone").innerHTML = "<span style='color: red;'>" +
            "invalid phone number</span>";
        return false;
    }

}

function pinValidate() {
    var pin = document.getElementById("pin").value;
    var letters = /^[1-9][0-9]{5}$/;
    if (pin.match(letters)) {
        document.getElementById("errorpin").innerHTML = "";
        return true;
    } else {
        document.getElementById("pin").value = "";
        document.getElementById("errorpin").innerHTML = "<span style='color: red;'>" +
            "invalid pin number</span>";
        return false;
    }

}

function adharValidate() {
    var adhar = document.getElementById("adhar").value;
    var letters = /^[2-9]{1}\d{11}$/;

    if (adhar.match(letters)) {
        document.getElementById("erroradhar").innerHTML = "";
        return true;
    } else {
        document.getElementById("adhar").value = "";
        document.getElementById("erroradhar").innerHTML = "<span style='color: red;'>" +
            "invalid adhar number</span>";
        return false;
    }

}

function fosnameValidate() {

    var fsname = document.getElementById("fos").value;
    var letters = /^[A-Za-z\s]+$/;
    if (fsname.match(letters)) {
        document.getElementById("errorfos").innerHTML = "";
        return true;
    } else {
        document.getElementById("fos").value = "";
        document.getElementById("errorfos").innerHTML = "<span style='color: red;'>" +
            "invalid father/spouse name</span>";
        return false;
    }

}

function accnoValidate() {
    var accno = document.getElementById("accno").value;
    var letters = /^\d{9,18}$/;

    if (accno.match(letters)) {
        document.getElementById("erroraccno").innerHTML = "";
        return true;
    } else {
        document.getElementById("accno").value = "";
        document.getElementById("erroraccno").innerHTML = "<span style='color: red;'>" +
            "invalid account number</span>";
        return false;
    }

}

function bnameValidate() {

    var bname = document.getElementById("bankname").value;
    var letters = /^[A-Za-z\s]+$/;
    if (bname.match(letters)) {
        document.getElementById("errorbname").innerHTML = "";
        return true;
    } else {
        document.getElementById("bankname").value = "";
        document.getElementById("errorbname").innerHTML = "<span style='color: red;'>" +
            "invalid name</span>";
        return false;
    }

}


function ifscValidate() {
    var accno = document.getElementById("ifsc").value;
    var letters = /^[A-Za-z]{4}\d{7}$/;

    if (accno.match(letters)) {
        document.getElementById("errorifsc").innerHTML = "";
        return true;
    } else {
        document.getElementById("ifsc").value = "";
        document.getElementById("errorifsc").innerHTML = "<span style='color: red;'>" +
            "invalid ifsc</span>";
        return false;
    }

}

function passValidate() {
    var password = document.getElementById("pass").value;
    var paswd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,14}$/;
    if (password.match(paswd)) {
        document.getElementById("errorpass").innerHTML = "";
        return true;
    } else {
        document.getElementById("pass").value = "";
        document.getElementById("errorpass").innerHTML = "<span style='color: red;'>" +
            "invalid password</span>";
        return false;
    }
}

function passFocus() {
    document.getElementById("pass").placeholder = "7-15 charector with digits and special charector";
}

function matchPassword() {
    var pw1 = document.getElementById("pass").value;
    var pw2 = document.getElementById("cnfrmpass").value;
    if (pw1 != pw2) {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'not matching';
    } else {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'matching';
    }
}