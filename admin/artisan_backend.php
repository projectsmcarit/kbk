<html>

<head>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <style>
        .checked-0{
            background-color:red;
            color:whitesmoke
        }
        .checked-1{
            background-color:green;
            color:whitesmoke
        }
    </style>

</head>
<?php

include('../connection.php');

if (isset($_REQUEST["term"])) {
    // Prepare a select statement
    $sql = "SELECT * FROM kbk_artisan WHERE artisan_name LIKE ? OR artisan_code LIKE ? OR aadhar_no LIKE ? OR contact_no LIKE ?";
    
    if($stmt = mysqli_prepare($con, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "ssss", $param_term, $param_term, $param_term, $param_term);
        
        // Set parameters
        $param_term = '%' . $_REQUEST["term"] . '%';
        
        // Attempt to execute the prepared statement
        if (mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            // Check number of rows in the result set
            if (mysqli_num_rows($result) > 0) {
                // Fetch result rows as an associative array
?>
                <center>
                    <table class='table table-hover'>
                        <tr>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Aadhar_no</th>
                            <th>Contact_no</th>
                        </tr>
                        <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
                            <tr>
                                <td><mark><?php echo $row['artisan_id']; ?></mark></td>
                                <td><mark><?php echo $row['artisan_code']; ?></mark></td>
                                <td><mark><?php echo $row['artisan_name']; ?></mark></td>
                                <td><mark><?php echo $row['aadhar_no']; ?></mark></td>
                                <td><mark><?php echo $row['contact_no']; ?></mark></td>
                                <td><a href="view_artisan.php?id=<?php echo $row['artisan_id']; ?>"><input type="button" value="View/Update" class="btn btn-primary"></a></td>
                                <td><a href="update_artisan_status.php?id=<?php echo $row['artisan_id']; ?>&status=<?php echo $row['status']; ?>"><input type="button"  class="btn checked-<?php echo $row['status']; ?>" value="Enable/Disable"></a></td>
                                <td><a href="delete.php?id=<?php echo $row['artisan_id']; ?>" onclick="return confirm('Do yout want to delete the record');"><input type="button" value='Delete' class="btn btn-danger"></a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </center>
<?php
            } else {
                echo "<p>No matches found</p>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
        }
    }
    // Close statement
    mysqli_stmt_close($stmt);
}

// close connection
mysqli_close($con);
?>

</html>
