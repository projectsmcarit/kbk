<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>ESI CENTRE</title>
   
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    
      
   <?php 
   include('header.php');  
   ?>
   
   
  </head>
  <body>
    <!-- Start your project here-->
    <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <div class="col-md-6 col-lg-5 d-none d-md-block">
                  <img
                    src="../images/k-khadi.png"
                    alt="Holiday form"
                    class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                  />
                </div>
                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                  <div class="card-body p-4 p-lg-5 text-black">
    
                    <form action="esi_centre.php" method="post">  
    
                      <div class="d-flex align-items-center mb-3 pb-1">
                        
                        <span class="h3 fw-bold mb-5 text-uppercase">ESI CENTRE REGISTRATION</span>
                      </div>
                      
                      
                  
                      <div class="form-outline mb-4">
                      <label class="form-col-form-label-lg" for="esicentre" >Centre Name</label>
                        <input type="text"  name="esicentre" id="esicentre" class="form-fa-volume-control-phone form-control-lg" required/>
                       
                      </div>

                     
                         
                      <div class="pt-1 mb-4">
                        <input type="submit" name="submit" id="submit" value="SUBMIT" class="btn btn-secondary" />
                      </div>
                   </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>