

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Add/Remove Artisan</title>
   
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />


    
    
      
   <?php 
   include('header.php');  
   ?>
   
   
  </head>
  
  <body>
    <!-- Start your project here-->
    <!-- Tabs navs -->
<div style="padding-top: 50px;">
<ul class="nav nav-tabs nav-fill mb-3" id="ex1" role="tablist">
  <li class="nav-item" role="presentation">
    <a
      class="nav-link active"
      id="ex1-tab-1"
      data-mdb-toggle="tab"
      href="#ex1-tabs-1"
      role="tab"
      aria-controls="ex1-tabs-1"
      aria-selected="true"
      >REMOVE ARTISAN</a
    >
  </li>
  <li class="nav-item" role="presentation">
    <a
      class="nav-link"
      id="ex1-tab-2"
      data-mdb-toggle="tab"
      href="#ex1-tabs-2"
      role="tab"
      aria-controls="ex1-tabs-2"
      aria-selected="false"
      >ADD REMOVED ARTISAN</a
    >
  </li>
  
</ul></div>
<!-- Tabs navs -->

<!-- Tabs content -->
<div class="tab-content" id="ex1-content">
  <div
    class="tab-pane fade show active"
    id="ex1-tabs-1"
    role="tabpanel"
    aria-labelledby="ex1-tab-1"
  >
  
  <form name="remove_artisan" action="" method="post">
  <section class="vh-100" style="background-color:yellowgreen">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col">
          <div class="card card-registration my-4">
            <div class="row g-0">
              <div class="">
                <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Remove Artisan</h3>
                    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                    <script>
$(document).ready(function(){
    $('.form-outline input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("backend-search_unit.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".form-outline").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
                  </script>
                   <div class="col-md-6 mb-4">
                  <div class="form-outline mb-4">
                      <input type="text" id="search" class="form-control form-control-lg" name="unit" placeholder="search by unit id"  autocomplete="off"/>
                      <label class="form-label" for="unit">Unit Code</label>
                      <div class="result"></div>
                      
                    </div>
                   </div>
                    
                   
                    <div class="row">
                      <div class="col-md-6 mb-4">
                      <input type="submit" class="btn btn-warning btn-lg ms-2" name="search" value="SEARCH" onclick=myFunction() >
                     
                   </div></div>
                   <div class="row">
                   <?php
                    include('../connection.php');
                    include('../kbk_library.php');
                    if(isset($_POST['search']))
                               {
                         echo "<table class=table-danger >";
                        
                         $unit=$_POST['unit'];                          
                         $res=mysqli_query($con,"SELECT * FROM kbk_artisan WHERE unit_code='$unit' and  status=1");
                        
                         echo "<thead>";
                         echo "<tr>";
                         echo "<tr>";
                         echo "<th> </th>";
                         echo "<th> ARTISAN ID </th>";
                         echo "<th> ARTISAN NAME </th>";
                         echo "<th> ARTISAN CODE </th>";
                         echo "<th> ACTION </th>";
                         echo "<thead>";
                         echo "</tr>";


                         while($row=mysqli_fetch_array($res))
                         {
                          
                          echo "<tbody>";
                           echo "<td>"; echo "</td>";
                           echo "<td>"; echo $row["artisan_id"];echo "</td>";
                           echo "<td>"; echo $row["artisan_name"];echo "</td>";
                           echo "<td>"; echo $row["artisan_code"];echo "</td>";
                           echo "<td>"; ?><input type="checkbox" name="try[]" Value="<?php echo $row["artisan_id"]  ?>"  <?php echo "</td>";


                           echo"</tr>";
                           echo "</tbody>";
                           



                         }
                        

                         echo "</table>";
                       
                                 
                                }





                    ?>
                   </div>
      
                            
                            <div class="d-flex justify-content-end pt-3">
                        
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="remove" >Remove</button>
                      </div>
                      <?php
                      if(isset($_POST['remove']))
                      {
                        $box=$_POST['try'];
                        while ( list ($key,$val) = @each ($box) )
                        {
                          mysqli_query($con,"UPDATE kbk_artisan SET status=0 WHERE artisan_id=$val");
                        }
                        
                              } 
                      
                      
                      
                      ?>
                      
  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  </form>

  </div>
  <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">

  <form name="Add_Removed_Artisan" action="" method="post">
  <section class="vh-100" style="background-color:yellowgreen">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration">
                <div class="row g-0">
                  </div>
                  <div class="">
                    <div class="card-body p-md-5 text-black">
                      <h3 class="mb-5 text-uppercase"></h3>
                      <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                    <script>
$(document).ready(function(){
    $('.form-outline input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("backend-search_unit.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".form-outline").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
                  </script>
      <div class="col-md-6">
                      <div class="form-outline mb-4">
                      <input type="text" id="search" class="form-control form-control-lg" name="unit" placeholder="search by unit id"  autocomplete="off"/>
                      <label class="form-label" for="unit">Unit Code</label>
                      <div class="result"></div>
                      
                      </div>
</div> 
                    
                   
                    <div class="row">
                      <div class="col-md-6 mb-4">
                      <input type="submit" class="btn btn-warning btn-lg ms-2" name="search1" value="SEARCH" onclick=myFunction() >
                      </div></div>
                      <div class="row">
                      <?php
                      if(isset($_POST['search1']))
                               {
                                 
                          echo '<table class="table-danger" >';
                        
                         $unit=$_POST['unit'];                          
                         $res=mysqli_query($con,"SELECT * FROM kbk_artisan WHERE unit_code='$unit' and  status=0");
                        //  echo "<table>";
                         echo "<thead>";
                         echo "<tr>";
                         echo "<tr>";
                         echo "<th> </th>";
                         echo "<th> ARTISAN ID </th>";
                         echo "<th> ARTISAN NAME </th>";
                         echo "<th> ARTISAN CODE </th>";
                         echo "<th> ACTION </th>";
                         echo "<thead>";
                         echo "</tr>";


                         while($row=mysqli_fetch_array($res))
                         {
                         
                          echo "<tbody>";
                           echo "<td>"; echo "</td>";
                           echo "<td>"; echo $row["artisan_id"];echo "</td>";
                           echo "<td>"; echo $row["artisan_name"];echo "</td>";
                           echo "<td>"; echo $row["artisan_code"];echo "</td>";
                           echo "<td>"; ?><input type="checkbox" name="try[]" Value="<?php echo $row["artisan_id"]  ?>"  <?php echo "</td>";


                           echo"</tr>";
                           echo "</tbody>";
                           



                         }
                        

                         echo "</table>";
                       
                                 
                                }





                    ?>
                      
                              </div>
                   
                   

                   <div class="d-flex justify-content-end pt-3">
                        
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="add">add</button>
                        <?php
                      if(isset($_POST['add']))
                      {
                        $box=$_POST['try'];
                        while ( list ($key,$val) = @each ($box) )
                        {
                          //echo "<script> alert($val)</script>";
                          mysqli_query($con,"UPDATE kbk_artisan SET status=1 WHERE artisan_id=$val");
                        }
                        
                      
                              }  
                            
                      
                      
                      ?>
                      <script type="text/javascript">
                       $(function() {
  
  $('a[data-mdb-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('lastTab', $(this).attr('href'));
  });
  var lastTab = localStorage.getItem('lastTab');
  
  if (lastTab) {
    $('[href="' + lastTab + '"]').tab('show');
  }
  
});
                          
                        </script>

                      </div>
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>


  </div>
  
</div>
<!-- Tabs content -->
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>