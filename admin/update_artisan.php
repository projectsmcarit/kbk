<?php

// Includeing database connection
include("../connection.php");

if (isset($_POST['update'])) {
    $artisan_id      = $_POST['art_id_disp'];
    $artisan_name    = $_POST['art_name'];
    $contact_no      = $_POST['contact'];
    $aadhar_no       = $_POST['adar'];
    $doj             = $_POST['doj'];
    $dob             = $_POST['dob'];
    $addressln1      = $_POST['addressln1'];
    $addressln2      = $_POST['addressln2'];
    $gender          = $_POST['gender'];
    $pin_number      = $_POST['pin'];
    $father_spouse   = $_POST['fos'];
    $lic             = $_POST['lic'];
    $esi             = $_POST['esi'];
    $account_no      = $_POST['accno'];
    $bank_name       = $_POST['bankname'];
    $ifsc            = $_POST['ifsc'];
    $wf_reg_no       = $_POST['wf'];
    $email           = $_POST['email'];
    $unit_code       = $_POST['unitid'];
    $category        = $_POST['category'];
    $mmdi            = $_POST['mmdi'];
    $esi_centr       = $_POST['esi_centr'];
    $filename1 = $_FILES["aadharpic"]["name"];
    $tempname1 = $_FILES["aadharpic"]["tmp_name"];
    $filename2 = $_FILES["pic"]["name"];
    $tempname2 = $_FILES["pic"]["tmp_name"];
    $folder1 = "../upload_images/" . $filename1;
    $folder2 = "../upload_images/" . $filename2;
    move_uploaded_file($tempname1, $folder1);
    move_uploaded_file($tempname2, $folder2);
    $sql = "UPDATE kbk_artisan SET artisan_name='$artisan_name',contact_no=$contact_no,aadhar_no=$aadhar_no,aadhar='$filename1',photo='$filename2', doj='$doj', dob='$dob', addressln1='$addressln1', addressln2='$addressln2', gender='$gender', pin_number=$pin_number, father_spouse='$father_spouse', lic='$lic', esi='$esi', account_no='$account_no', bank_name='$bank_name', ifsc='$ifsc', wf_reg_no='$wf_reg_no', email='$email', unit_code='$unit_code', caste='$category', mmdi='$mmdi',esi_centr='$esi_centr' WHERE artisan_id = '$artisan_id'";
    $result = mysqli_query($con, $sql);

    if ($result > 0) {
        echo "<script> alert('success, " . $artisan_name . " updated') </script>";
        echo "<script> window.location.href='Artisan_View.php'</script>";
    } else {
        echo "<script> alert('Error try again !!') </script>";
        echo "<script> window.location.href='update_artisan.php'</script>";
    }
}
