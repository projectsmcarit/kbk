<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Artisans</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <style>
        .cont {
            background: white;
            position: relative;
            margin-top: 100px;
            padding-top: 25px;
            padding-bottom: 25px;
            margin-right: 50px;
            margin-left: 50px;
            margin-bottom: 500px
        }

        h4{
            color: black;
        }

        .table-wrapper {
            max-height: 310px;
            overflow: auto;
            display: inline-block;
        }

        .table-wrapper thead tr {
            position: sticky;
            top: 0;
        }

    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
</head>
<body>
    <header>
        <?php
            include("header.php");
            include("../connection.php");
        ?>   
    </header>
    <!-- <section class=""> -->
        <div
         id="intro"
         class="bg-image"
         style="
                background-image: url(../images/2.jpg);
                height: 100vh;
                "
         >
            <div class="mask text-white" style="background-color: rgba(0, 0, 0, 0.8)">
                <div class="container-flex cont">
                    <div class="row justify-content-center" >
                        <div class="col-md-12">
                            <div class="card mt-5" >
                                <div class="card-header" >
                                    <center><h4>ARTISANS</h4></center>
                                </div>
                                <center>
                                <div class="card-body" style="width: 1200px;" >
                                    <form action="" method="POST">
                                        <div class="row" >
                                            <div class="clo-md-4" >
                                                <div class="input-group mb-3">
                                                    <select class="form-select" name="sort">
                                                        <option value="">--Select option--</option>
                                                        <option value="name" <?php if(isset($_POST['sort']) && $_POST['sort'] == "name"){ echo "selected"; } ?> >Name</option>
                                                        <option value="gender" <?php if(isset($_POST['sort']) && $_POST['sort'] == "gender"){ echo "selected"; } ?> >Gender</option>
                                                        <option value="cast" <?php if(isset($_POST['sort']) && $_POST['sort'] == "cast"){ echo "selected"; } ?> >Cast</option>
                                                        <option value="unit" <?php if(isset($_POST['sort']) && $_POST['sort'] == "unit"){ echo "selected";} ?> >Unit</option>
                                                    </select>
                                                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">sort</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="table-responsive">
                                        <table id="tableData" class="table table-hover table-sm table-wrapper">
                                            <thead class="table-dark">
                                                <tr>
                                                    <th>Artisan Code</th>
                                                    <th>Unit Name</th>
                                                    <th>Name</th>
                                                    <th>Contact Number</th>
                                                    <th>Aadhar Number</th>
                                                    <th>Date of Joining</th>
                                                    <th>Date of Birth</th>
                                                    <th>Address</th>
                                                    <th>Father/Spous</th>
                                                    <th>Gender</th>
                                                    <th>Cast</th>
                                                    <th>Lic</th>
                                                    <th>Esi</th>
                                                    <th>Bank Name</th>
                                                    <th>Account Number</th>
                                                    <th>IFSC</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(isset($_POST['sort'])) {
                                                        if($_POST['sort'] == "name") {
                                                            $sort_option = "artisan_name";
                                                            // echo "<script> console.log('".$_GET['sort']."'); </script>";
                                                            $sql = "SELECT a.*, b.unit_name FROM kbk_artisan AS a INNER JOIN kbk_unit AS b ON (a.unit_code = b.unit_code) ORDER BY $sort_option";
                                                            $result = $con->query($sql);
                                                            // echo "<script> console.log('".$result->num_rows."'); </script>";
                                                            if($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                                    //echo "<script> console.log(Names are: $row['artisan_name']); </script>";
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['artisan_code']; ?></td>
                                                    <td><?php echo $row['unit_name']; ?></td>
                                                    <td><?php echo $row['artisan_name']; ?></td>
                                                    <td><?php echo $row['contact_no']; ?></td>
                                                    <td><?php echo $row['aadhar_no']; ?></td>
                                                    <td><?php echo $row['doj']; ?></td>
                                                    <td><?php echo $row['dob']; ?></td>
                                                    <td><?php echo $row['addressln1'] .", ". $row['addressln2'] .", ". $row['pin_number'];  ?></td>
                                                    <td><?php echo $row['father_spouse']; ?></td>
                                                    <td><?php echo $row['gender']; ?></td>
                                                    <td><?php echo $row['caste']; ?></td>
                                                    <td><?php echo $row['lic']; ?></td>
                                                    <td><?php echo $row['esi']; ?></td>
                                                    <td><?php echo $row['bank_name']; ?></td>
                                                    <td><?php echo $row['account_no']; ?></td>
                                                    <td><?php echo $row['ifsc']; ?></td>
                                                </tr>
                                                <?php
                                                                }
                                                            }
                                                        }
                                                        elseif($_POST['sort'] == "gender") {
                                                            $sort_option = "gender";
                                                            $sql = "SELECT a.*, b.unit_name FROM kbk_artisan AS a INNER JOIN kbk_unit AS b ON (a.unit_code = b.unit_code) ORDER BY $sort_option";
                                                            $result = $con->query($sql);
                                                            if($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {

                                                ?>
                                                <tr>
                                                    <td><?php echo $row['artisan_code']; ?></td>
                                                    <td><?php echo $row['unit_name']; ?></td>
                                                    <td><?php echo $row['artisan_name']; ?></td>
                                                    <td><?php echo $row['contact_no']; ?></td>
                                                    <td><?php echo $row['aadhar_no']; ?></td>
                                                    <td><?php echo $row['doj']; ?></td>
                                                    <td><?php echo $row['dob']; ?></td>
                                                    <td><?php echo $row['addressln1'] .", ". $row['addressln2'] .", ". $row['pin_number'];  ?></td>
                                                    <td><?php echo $row['father_spouse']; ?></td>
                                                    <td><?php echo $row['gender']; ?></td>
                                                    <td><?php echo $row['caste']; ?></td>
                                                    <td><?php echo $row['lic']; ?></td>
                                                    <td><?php echo $row['esi']; ?></td>
                                                    <td><?php echo $row['bank_name']; ?></td>
                                                    <td><?php echo $row['account_no']; ?></td>
                                                    <td><?php echo $row['ifsc']; ?></td>
                                                </tr>
                                                <?php
                                                                }
                                                            }
                                                        }
                                                        elseif($_POST['sort'] == "unit") {
                                                            $_sort_option = "unit_code";
                                                            $sql = $sql = "SELECT a.*, b.unit_name FROM kbk_artisan AS a INNER JOIN kbk_unit AS b ON (a.unit_code = b.unit_code) ORDER BY b.unit_name";
                                                            $result = $con->query($sql);
                                                            if ($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['artisan_code']; ?></td>
                                                    <td><?php echo $row['unit_name']; ?></td>
                                                    <td><?php echo $row['artisan_name']; ?></td>
                                                    <td><?php echo $row['contact_no']; ?></td>
                                                    <td><?php echo $row['aadhar_no']; ?></td>
                                                    <td><?php echo $row['doj']; ?></td>
                                                    <td><?php echo $row['dob']; ?></td>
                                                    <td><?php echo $row['addressln1'] .", ". $row['addressln2'] .", ". $row['pin_number'];  ?></td>
                                                    <td><?php echo $row['father_spouse']; ?></td>
                                                    <td><?php echo $row['gender']; ?></td>
                                                    <td><?php echo $row['caste']; ?></td>
                                                    <td><?php echo $row['lic']; ?></td>
                                                    <td><?php echo $row['esi']; ?></td>
                                                    <td><?php echo $row['bank_name']; ?></td>
                                                    <td><?php echo $row['account_no']; ?></td>
                                                    <td><?php echo $row['ifsc']; ?></td>
                                                </tr>
                                                <?php
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            $sort_option = "caste";
                                                            $sql = "SELECT a.*, b.unit_name FROM kbk_artisan AS a INNER JOIN kbk_unit AS b ON (a.unit_code = b.unit_code) ORDER BY $sort_option";
                                                            $result = $con->query($sql);
                                                            if($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['artisan_code']; ?></td>
                                                    <td><?php echo $row['unit_name']; ?></td>
                                                    <td><?php echo $row['artisan_name']; ?></td>
                                                    <td><?php echo $row['contact_no']; ?></td>
                                                    <td><?php echo $row['aadhar_no']; ?></td>
                                                    <td><?php echo $row['doj']; ?></td>
                                                    <td><?php echo $row['dob']; ?></td>
                                                    <td><?php echo $row['addressln1'] .", ". $row['addressln2'] .", ". $row['pin_number'];  ?></td>
                                                    <td><?php echo $row['father_spouse']; ?></td>
                                                    <td><?php echo $row['gender']; ?></td>
                                                    <td><?php echo $row['caste']; ?></td>
                                                    <td><?php echo $row['lic']; ?></td>
                                                    <td><?php echo $row['esi']; ?></td>
                                                    <td><?php echo $row['bank_name']; ?></td>
                                                    <td><?php echo $row['account_no']; ?></td>
                                                    <td><?php echo $row['ifsc']; ?></td>
                                                </tr>
                                                <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button class="btn btn-primary" onclick="Export();">EXPORT</button>
                                    </div>
                                </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script type="text/javascript">
        function Export() {
            $(document).ready(function () {
                $("#tableData").table2excel({
                    filename: "artisans.xls"
                });
            });
        }
    </script>
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <script type="text/javascript" src="validation.js"></script>    
</body>
</html>