<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>ARTISAN REGISTRATION</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    
    
<!-- Start your project here-->
    <form name="artisanregistration" action="artisan_reg.php" method="post" enctype="multipart/form-data" id="artisanregistration" > 
    <header>
      <!-- Navbar -->
      <?php  
      include('header.php');
      ?>
   
      
      </header>
    
    
    <section class="h-100 bg-dark">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col">
            <div class="card card-registration my-4">
              <div class="row g-0">
                <div class="col-xl-6 d-none d-xl-block">
                  <img
                    src="../images/Mahatma_Gandhi.jpg"
                    alt="Sample photo"
                    class="img-fluid"
                    style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                  />
                </div>
                <div class="col-xl-6">
                  <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Artisan Registration</h3>
                    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="art_name" id="name" class="form-control form-control-lg" onchange="nameValidate()"   required />
                          <label class="form-label" ><span style="color:#ff0000">*</span>Name</label>
                        </div>
                        <span id="errorname"></span>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="email" id="email" class="form-control form-control-lg"  onchange="emailValidate()"/>
                          <label class="form-label" for="form3Example1n">Email ID</label>
                        </div>
                        <span id="erroremail"></span>
                      </div>
                    </div>
    
                    <div class="form-outline mb-4">
                      <input type="text" name="addressln1" id="addressln1" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example8"><span style="color:#ff0000">*</span>Address Line 1</label>
                    </div>

                    <div class="form-outline mb-4">
                      <input type="text" name="addressln2" id="addressln2" class="form-control form-control-lg"/>
                      <label class="form-label" for="form3Example8">Address Line 2</label>
                    </div>

                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                      <input type="number" name="contact" min=1000000000 max=9999999999 id="contact" class="form-control form-control-lg" onchange="" required />
                      <label class="form-label" for="contact"><span style="color:#ff0000">*</span>Contact Number</label>
                    </div>
                    <span id="errorphone"></span>
                    </div>
                    <div class="col-md-6 mb-4">
    
                    <select class="form-select" name="category" aria-label="Default select example" id="category">
                    <option disabled selected >-- <span style="color:#ff0000">*</span>Select Caste --</option>
                    <option value="SC">SC</option>
                    <option value="ST">ST</option>
                    <option value="OBC">OBC</option>
                    <option value="OEC">OEC</option>
                    <option value="GENERAL">GENERAL</option>
                    <option value="GENERAL">OTHERS</option>
         
                    </select>
                    </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" name="pin" id="pin" class="form-control form-control-lg" onchange="pinValidate()" required />
                          <label class="form-label" for="pin"><span style="color:#ff0000">*</span>Pin Number</label>
                        </div>
                        <span id="errorpin"></span>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" name="adhar" id="adhar" class="form-control form-control-lg" onchange="adharValidate()" required />
                          <label class="form-label" for="form3Example1n1"><span style="color:#ff0000">*</span>Aadhar number</label>
                        </div>
                        <span id="erroradhar"></span>
                      </div>
                    </div>
    
                    
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <label for="formFile" class="form-label"><span style="color:#ff0000">*</span>Aadhar image</label>
                          <input class="form-control" name= "aadharpic" type="file" id="aadharpic" />
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <label for="formFile" class="form-label"><span style="color:#ff0000">*</span>Photo</label>
                          <input class="form-control" name="pic" type="file" id="pic" />
                        </div>
                      </div>
                    </div>
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="date" name="dob" id="dob" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1m1"><span style="color:#ff0000">*</span>Date of birth</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="fos" id="fos" class="form-control form-control-lg" onchange="fosnameValidate()"  />
                          <label class="form-label" for="form3Example1n1"><span style="color:#ff0000">*</span>Father/Spouse</label>
                        </div>
                        <span id="errorfos"></span>
                      </div>
                    </div>
    
                    <div class="d-md-flex justify-content-start align-items-center mb-4 py-2">

                  <h6 class="mb-0 me-4"><span style="color:#ff0000">*</span>Gender: </h6>

                  <div class="form-check form-check-inline mb-0 me-4">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="gender"
                      id="gender"
                      value="Female"
                    />
                    <label class="form-check-label" for="femaleGender">Female</label>
                  </div>

                  <div class="form-check form-check-inline mb-0 me-4">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="gender"
                      id="gender"
                      value="Male"
                    />
                    <label class="form-check-label" for="maleGender">Male</label>
                  </div>

                  <div class="form-check form-check-inline mb-0">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="gender"
                      id="gender"
                      value="Other"
                    />
                    <label class="form-check-label" for="otherGender">Other</label>
                  </div>

                </div>

                    
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="lic" id="lic" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1m1">LIC Number</label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="esi" id="esi" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">ESI Number</label>
                        </div>
                      </div>
                    </div>
    
                     <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="accno" id="accno" class="form-control form-control-lg" onchange="" required />
                          <label class="form-label" for="form3Example1m1"><span style="color:#ff0000">*</span>Bank account number</label>
                        </div>
                        <span id="erroraccno"></span>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="bankname" id="bankname" class="form-control form-control-lg" onchange="bnameValidate()" required />
                          <label class="form-label" for="form3Example1n1"><span style="color:#ff0000">*</span>Bank name</label>
                        </div>
                        <span id="errorbname"></span>
                      </div>
                    </div>
    
    
    
                    <div class="row">
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="ifsc" id="ifsc" class="form-control form-control-lg" onchange="" required />
                          <label class="form-label" for="form3Example1m1"><span style="color:#ff0000">*</span>IFSC</label>
                        </div>
                        <span id="errorifsc"></span>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="text" name="wf" id="wf" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Welfare number</label>
                        </div>
                      </div>
                    </div>
                     
                    <div class="row">
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="unitid" aria-label="Default select example" id="unitid">
                          <option disabled selected>-- <span style="color:#ff0000">*</span>Select Unit Name--</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $unid= mysqli_query($con, "SELECT unit_code,unit_name From kbk_unit");  // Use select query here 

                              while($unit = mysqli_fetch_array($unid))
                                {
                                 echo "<option value='". $unit['unit_code'] ."'>" .$unit['unit_code']." ".$unit['unit_name'] ."</option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="date" name="doj" id="doj" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1"><span style="color:#ff0000">*</span>Date of joining</label>
                        </div>
                      </div>
                      
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="esicenter" aria-label="Default select example" id="esicenter">
                          <option disabled selected>-- <span style="color:#ff0000">*</span>Select Esi Centre --</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $cen= mysqli_query($con, "SELECT esi_id,center_name From kbk_esi_center");  // Use select query here 

                              while($center = mysqli_fetch_array($cen))
                                {
                                 echo "<option value='". $center['esi_id'] ."'>" .$center['esi_id'] ." ".$center['center_name']. "</option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" name="mmid" id="mmid" class="form-control form-control-lg" min=0  />
                          <label class="form-label" for="form3Example1n1">MMDI Number</label>
                        </div>
                      </div>
                      
                    </div>

    
                    
    
                    <div class="d-flex justify-content-end pt-3">
                      <button type="reset" class="btn btn-light btn-lg">Reset all</button>
                      <input type="submit"   class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="submit" value="Register" onclick="validate()">

                      <div class="modal top fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-mdb-backdrop="true" data-mdb-keyboard="true">
                        <div class="modal-dialog  ">
                          <div class="modal-content">
                            <div class="modal-header">
                              <!--<h5 class="modal-title" id="exampleModalLabel">Registered Succesfully hello</h5>-->
                              
                            </div>
                            
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                               
                              
                            </div>
                          </div>
                        </div>
                      </div>

                      
                    </div>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </form>
        <!-- End your project here-->
        <!-- validation javascript -->
        <script type="text/javascript" src="validation.js"></script>

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
   

  </body>
</html>
