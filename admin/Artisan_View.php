<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>ARTISAN VIEW</title>
  <!-- MDB icon -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />
</head>

<body>
  <form>
    <header>
      <?php
      include("header.php");
      ?>
      <section class="" style="background-color: aquamarine;">
        <div class="container py-5">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-8">
                    <div class="card-body p-md-5 text-black">
                      <div class="from-outline mn-4">
                        <label class="form-label" for="form3Example1n1">Search Artisans using their Code/Name/Contact number/Aadhar number</label>
                        <input type='text' id='livesearch' class="form-control form-control-lg" name="artisan_id" />
                        <div id="suggestion"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
    </header>
  </form>
  <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function() {
      $("#livesearch").keyup(function() {
        var search = $(this).val();
        console.log(search);
        if (search == "") {
          $("#suggestion").html("")
        } else {
          $.ajax({
            type: 'POST',
            url: "artisan_backend.php",
            data: {
              term: search,
            },
            success: function(response) {
              $("#suggestion").show();
              $("#suggestion").html(response);
            }
          });
        }
      });
    });
  </script>
  <script type="text/javascript" src="../js/mdb.min.js"></script>
  <script type="text/javascript" src="validation.js"></script>
</body>

</html>