<?php

 $res3= retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'",$con);
 $res4= retrieveData("SELECT type From kbk_work_type where work_id=$worktype",$con);
$dsum = array(
    'sliver'   => 0, 
    'wage'     => 0,
    'wf'       => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
    'lic'      => 0,
    'nw'       => 0,
    'hw'       => 0,
    'tw'       => 0,
    'esi'      => 0,
    'yi'       => 0,
    'wyi'      => 0,
    'mw'       => 0,
    'dw'       => 0,
    'tmw'      => 0,
    'nmw'      => 0,
    );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <?php
    echo '<title>'.$res4[0]['type'].'</title>';
    ?>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
          
   <style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 2px;
  padding-bottom: 2px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>  
    
  </head>
  <body>

<script type="text/javascript" src="../js/mdb.min.js"></script>
  
    <!-- Start your project here-->
   
    <form name="spinninglistrep" action="spinning_pdf.php" method="post"> 
   
    
    
   
    <section class="" style="padding-top: 30px;">
      <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col">
            <div class="">
              <div class="row g-0">
               <div  id="container_content">
                 <div class="text-black">
                   <h3 class="mb-5 text-uppercase">Minimum Wage Report For <?php echo $res4[0]['type']; ?></h3>            

  <div class="row">
     
     <div class="col-md-3 mb-3">                       
     <label class="form-label" >Month and Year</label>                   
     </div>
     <div class="col-md-3 mb-2">
       <div class="form-outline ">
       <?php 
         echo '<select class="form-select" name="hdate" >';
         $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         $m1= strval(date('m',strtotime($hdate)));
         $y1= strval(date('Y',strtotime($hdate)));
         echo '<option value="'.$hdate.'">'.$months_name[strval($m1)-1]." ".$y1.'</option></select>'."\n";
         
       ?>
       </div>
     </div>
     </div>
<div class="row">
 
 <div class="col-md-3 mb-3">                       
  <label class="form-label" >Work Type</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
      echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="'.$res4[0]['type'].'" readonly/>';
                  
    ?>
    </div>
  </div>
  
<!--   <div class="col-md-2 mb-2">                       
  <label class="form-label" >Unit ID</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
      // echo '<input type="text" name="unitid" class="form-control form-control-lg" value="'.$unitdemo.'" readonly />';  
                 
    ?>
    </div>
  </div> -->
  </div>



 <div class="row">
 
 <div class="col-md-3 mb-3">                       
  <label class="form-label">Number Of Holidays</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
        <?php
        echo '<input type="text" name="hno" class="form-control form-control-lg" value="'.$res3[0]['holiday_no'].'" readonly/>';                    
        ?>
    </div>
  </div>
  
  <div class="col-md-3 mb-">                       
  <label class="form-label">Number Of Working Days</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
        echo '<input type="text" name="wno" class="form-control form-control-lg" value="'.$res3[0]['total_wd'].'" readonly />';
       
    ?>                  
    </div>
  </div>
  </div>




                  </div>          
                </div>
              </div>                
            </div>
          </div>            
        </div>
      </div>

<div style="padding: 0px 50px 30px 40px;">
  
<?php
if($res4[0]['type']=="Spinning")
{
 $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
 for($z=0;$z<sizeof($res5);$z++)
 {
 $usum = array(
    'sliver'   => 0, 
    'wage'     => 0,
    'wf'       => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
    'lic'      => 0,
    'nw'       => 0,
    'hw'       => 0,
    'tw'       => 0,
    'esi'      => 0,
    'yi'       => 0,
    'mw'       => 0,
    'dw'       => 0,
    'tmw'      => 0,
    'nmw'      => 0,
    );
 $unitdemo=$res5[$z]['unit_name'];
 $unit=$res5[$z]['unit_code'];


 $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_spinning where s_date='$hdate')",$con);
 
if(sizeof($res1)>0)
{
?>

    <table class="table-bordered " id="customers">
        <thead>
          <tr><th scope="col" colspan="12" style="background-color: #038857;"><?php echo strtoupper($unitdemo); ?></th></tr>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th scope="col">Artisan Name</th>
                          <th scope="col">Variety</th>
                          <th scope="col">No. of Hanks</th>
                          <th scope="col">Rate</th>
                          <!-- <th scope="col">Sliver Consumption</th> -->
                          <th scope="col">Wages Paid</th>
                       <!--    <th scope="col">W/F Fund</th>
                          <th scope="col">w/f Recovery</th>
                          <th scope="col">Adv Recovery</th>
                          <th scope="col">Khadi Cr. Rec.</th>
                          <th scope="col">Covid adv.</th>
                          <th scope="col">LIC</th>
                          <th scope="col">Net wages</th>
                          <th scope="col">Holiday wages</th>
                          <th scope="col">Total Wages</th>
                          <th scope="col">ESI Contr</th>
                          <th scope="col">Yarn Incentive</th> -->
                          <th scope="col">Minimum Wages</th>
                          <th scope="col">DA Days</th>
                          <th scope="col">DA Wages</th>
                          <th scope="col">Total Minimum Wages</th>
                          <th scope="col">Net Minimum Wages</th>
                          <!-- <th scope="col">Attendance</th>  -->
  
                        </tr>
                      </thead>
        <tbody>
<?php
    for($i=0;$i<sizeof($res1);$i++)
    {
        
        
        $temp1=strval($res1[$i]['artisan_id']);
        
        $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1",$con);
        $temp2=$res6[0]['yarn_id'];
        $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
        $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'",$con);
      
       
        $sar1  = array(
          'noh'    => $res6[0]['noh'],
          'sliver' => $res7[0]['sliver'],
          'rate'   => $res7[0]['rate'],
          'wf'     => $res7[0]['wf'],
          'wfrec'  => $res8[0]['wf_rec'],
          'adrec'  => $res8[0]['ad_rec'],
          'kcrec'  => $res8[0]['credit_rec'],
          'covrec' => $res8[0]['cov_rec'],
          'lic'    => $res8[0]['lic'],
          'hno'    => $res3[0]['holiday_no'],
          'wno'    => $res3[0]['total_wd']
        );

        $calc1=spinningWageCalculation($sar1);
      
        $sar2 = array(
          'noh'     => $res6[0]['noh'],
          'esi'     => $res7[0]['esi'],
          'yi'      => $res7[0]['yi'],
          'mw'      => $res7[0]['mw'],
          'target'  => $res7[0]['target'],
          'dawages' => $res3[0]['da_wages'],
          'wage'    => $calc1['wage'],
        );
        
$da=$res3[0]['total_wd'];
        $calc2=spinningIncentivesCalculation($sar2,$da);
        
        echo  '<tr><td>'.strval($i+1).'</td>';
        echo  '<td>'.$res1[$i]['artisan_code'].'</td>';
        echo  '<td>'.$res1[$i]['artisan_name'].'</td>';
        echo  '<td>'.$res7[0]['type'].'</td>';
        echo  '<td>'.$res6[0]['noh'].'</td>';
        echo  '<td>'.$res7[0]['rate'].'</td>';
        // echo  '<td>'.$calc1['sliver'].'</td>';
        echo  '<td>'.$calc1['wage'].'</td>';
        // echo  '<td>'.$calc1['wf'].'</td>';
        // echo  '<td>'.$calc1['wfrec'].'</td>';
        // echo  '<td>'.$calc1['adrec'].'</td>';
        // echo  '<td>'.$calc1['kcrec'].'</td>';
        // echo  '<td>'.$calc1['covrec'].'</td>';
        // echo  '<td>'.$calc1['lic'].'</td>';
        // echo  '<td>'.$calc1['netwages'].'</td>';
        // echo  '<td>'.$calc1['hdwage'].'</td>';
        // echo  '<td>'.$calc1['totwages'].'</td>';
        // echo  '<td>'.$calc2['esi'].'</td>';
        // echo  '<td>'.$calc2['yi'].'</td>';
        echo  '<td>'.$calc2['mw'].'</td>';
        echo  '<td>'.$calc2['dadays'].'</td>';
        echo  '<td>'.$calc2['dawages'].'</td>';
        echo  '<td>'.$calc2['totmw'].'</td>';
        echo  '<td>'.$calc2['netmw'].'</td>';
        // echo  '<td>'.$res6[0]['attendance'].'</td>';
        echo  '</tr>';
      
         $usum['wage']=$usum['wage']+$calc1['wage'];
         $usum['mw']=$usum['mw']+$calc2['mw'];
         $usum['dw']=$usum['dw']+$calc2['dawages'];
         $usum['tmw']=$usum['tmw']+$calc2['totmw'];
         $usum['nmw']=$usum['nmw']+$calc2['netmw'];

        
    }
   
   echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="6">Unit Total</td>';
  
    echo "<td>".$usum['wage']."</td>";
    echo "<td>".$usum['mw']."</td>";
    echo "<td> </td>";
    echo "<td>".$usum['dw']."</td>";
    echo "<td>".$usum['tmw']."</td>";
    echo "<td>".$usum['nmw']."</td>";
    echo "</tr>";
         $dsum['wage']=$dsum['wage']+$usum['wage'];
         $dsum['mw']=$dsum['mw']+$usum['mw'];
         $dsum['dw']=$dsum['dw']+$usum['dw'];
         $dsum['tmw']=$dsum['tmw']+$usum['tmw'];
         $dsum['nmw']=$dsum['nmw']+$usum['nmw']; 




}// if closing of if(sizeof($res1)>0)


}
echo '<tr style="background-color: #808080; color:white;text-align: center;"><td colspan="6">District Total</td>';
  
    echo "<td>".$dsum['wage']."</td>";

    echo "<td>".$dsum['mw']."</td>";
    echo "<td> </td>";
    echo "<td>".$dsum['dw']."</td>";
    echo "<td>".$dsum['tmw']."</td>";
    echo "<td>".$dsum['nmw']."</td>";
    echo "</tr>";

      echo '</tbody>
        <tfoot></tfoot>
    </table>'; 
  
// create for loop for total
}// if closing of work type spinning
if($res4[0]['type']=="Weaving")
{

  $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
 for($z=0;$z<sizeof($res5);$z++)
 {

 $unitdemo=$res5[$z]['unit_name'];
 $unit=$res5[$z]['unit_code'];
$usum = array(
    'wage'     => 0,
    'wf'       => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
    'lic'      => 0,
    'nw'       => 0,
    'hw'       => 0,
    'tw'       => 0,
    'esi'      => 0,
    'yi'       => 0,
    'wyi'      => 0,
    'mw'       => 0,
    'dw'       => 0,
    'tmw'      => 0,
    'nmw'      => 0,
    );

 $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_weaving where s_date='$hdate')",$con);
  if(sizeof($res1)>0)
{
  echo '<table class="table-bordered" id="customers">

        <thead>
                        
                        <tr><th scope="col" colspan="14" style="background-color: #038857;">'.strtoupper($unitdemo).'</th></tr>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th scope="col">Artisan Name</th>
                          <th scope="col">Variety</th>
                          <th scope="col">No. of Pieces</th>
                          <th scope="col">Metre</th>
                          
                          <th scope="col">Weaving Type</th>
                          <th scope="col">Rate</th>
                          
                          <th scope="col">Wages Paid</th>
                          <th scope="col">Minimum Wages</th>
                          <th scope="col">DA Days</th>
                          <th scope="col">DA Wages</th>
                          <th scope="col">Total Minimum Wages</th>
                          <th scope="col">Net Minimum Wages</th>
                           
                        </tr>
                      </thead>
        <tbody>';
        for($i=0;$i<sizeof($res1);$i++)
        {
          
          $temp1=strval($res1[$i]['artisan_id']);
          $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1",$con);
          $temp2 = $res9[0]['weaving_id'];
          $temp3 = $res9[0]['w_id'];
          $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3",$con);
          $res11 = retrieveData("SELECT type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2",$con);
          $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
          $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2",$con);
          $war3 =[
            'warp_grey'    => $res13[0]['warp_grey'],
            'warp_colour'  => $res13[0]['warp_colour'],
            'warp_black'   => $res13[0]['warp_black'],
            'warp_bl'      => $res13[0]['warp_bl'],
            'weft_grey'    => $res13[0]['weft_grey'],
            'weft_colour'  => $res13[0]['weft_colour'],
            'weft_black'   => $res13[0]['weft_black'],
            'weft_bl'      => $res13[0]['weft_bl'],
          ];
          $war1 = [
            'metre'  => $res9[0]['mtr'],
            'wage'   => $res10[0]['w_wages'],
            'wf'     => $res11[0]['wf'],
            'wfrec'  => $res12[0]['wf_rec'],
            'adrec'  => $res12[0]['ad_rec'],
            'kcrec'  => $res12[0]['credit_rec'],
            'covrec' => $res12[0]['cov_rec'],
            'lic'    => $res12[0]['lic'],
            'hno'    => $res3[0]['holiday_no'],
            'wno'    => $res3[0]['total_wd'],
            'sqmtr'  => $res11[0]['width'],
          ];
          $calc3=weavingWageCalculation($war1);

          $war2 = [
                'metre'   => $res9[0]['mtr'],
                'esi'     => $res11[0]['esi'],
                'warpyi'  => $res10[0]['warp_yi'],
                'weftyi'  => $res10[0]['weft_yi'],
                'mw'      => $res10[0]['min_wages'],
                'target'  => $res10[0]['target'],
                'dawages' => $res3[0]['da_wages'],
                'wage'    => $calc3['wage'],
                'warp_tot'=> $calc5['warp_tot'],
                'weft_tot'=> $calc5['weft_tot'],
            ];
$da=$res3[0]['total_wd'];
          $calc4=weavingIncentivesCalculation($war2,$da);

          echo '<tr><td>'.strval($i+1).'</td>';
          echo '<td>'.$res1[$i]['artisan_code'].'</td>';
          echo '<td>'.$res1[$i]['artisan_name'].'</td>';
          echo '<td>'.$res11[0]['type'].'</td>';
          echo '<td>'.$res9[0]['nop'].'</td>';
          echo '<td>'.$res9[0]['mtr'].'</td>';
          // echo '<td>'.$calc3['sqmtr'].'</td>';
          echo '<td>'.$res10[0]['w_type'].'</td>';
          echo '<td>'.$res10[0]['w_wages'].'</td>';
           echo '<td>'.$calc3['wage'].'</td>';
          // echo '<td>'.$calc3['wf'].'</td>';
          // echo '<td>'.$calc3['wfrec'].'</td>';
          // echo '<td>'.$calc3['adrec'].'</td>';
          // echo '<td>'.$calc3['kcrec'].'</td>';
          // echo '<td>'.$calc3['covrec'].'</td>';
          // echo '<td>'.$calc3['lic'].'</td>';
          // echo '<td>'.$calc3['netwages'].'</td>';
          // echo '<td>'.$calc3['hdwage'].'</td>';
          // echo '<td>'.$calc3['totwages'].'</td>';
          // echo '<td>'.$calc4['esi'].'</td>';
          // echo '<td>'.$calc4['warpyi'].'</td>';
          // echo '<td>'.$calc4['weftyi'].'</td>';
          echo '<td>'.$calc4['mw'].'</td>';
          echo '<td>'.$calc4['dadays'].'</td>';
          echo '<td>'.$calc4['dawages'].'</td>';
          echo '<td>'.$calc4['totmw'].'</td>';
          echo '<td>'.$calc4['netmw'].'</td>';
          // echo '<td>'.$res9[0]['attendance'].'</td>';
          echo '</tr>';
          $usum['wage']=$usum['wage']+$calc3['wage'];
         $usum['wf']=$usum['wf']+$calc3['wf'];
         $usum['wfrec']=$usum['wfrec']+$calc3['wfrec'];
         $usum['adrec']=$usum['adrec']+$calc3['adrec'];         
         $usum['kcrec']=$usum['kcrec']+$calc3['kcrec'];
         $usum['covrec']=$usum['covrec']+$calc3['covrec'];
         $usum['lic']=$usum['lic']+$calc3['lic'];
         $usum['nw']=$usum['nw']+$calc3['netwages'];
         $usum['hw']=$usum['hw']+$calc3['hdwage'];
         $usum['tw']=$usum['tw']+$calc3['totwages'];
         $usum['esi']=$usum['esi']+$calc4['esi'];
         $usum['yi']=$usum['yi']+$calc4['warpyi'];
         $usum['wyi']=$usum['wyi']+$calc4['weftyi'];
         $usum['mw']=$usum['mw']+$calc4['mw'];
         $usum['dw']=$usum['dw']+$calc4['dawages'];
         $usum['tmw']=$usum['tmw']+$calc4['totmw'];
         $usum['nmw']=$usum['nmw']+$calc4['netmw'];

        }
        echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="8">Unit Total</td>';
    echo "<td>".$usum['wage']."</td>";

    echo "<td>".$usum['mw']."</td>";
    echo "<td> </td>";
    echo "<td>".$usum['dw']."</td>";
    echo "<td>".$usum['tmw']."</td>";
    echo "<td>".$usum['nmw']."</td>";
    echo "</tr>";

        
         $dsum['wage']=$dsum['wage']+$usum['wage'];
         $dsum['wf']=$dsum['wf']+$usum['wf'];
         $dsum['wfrec']=$dsum['wfrec']+$usum['wfrec'];
         $dsum['adrec']=$dsum['adrec']+$usum['adrec'];
         $dsum['kcrec']=$dsum['kcrec']+$usum['kcrec'];
         $dsum['covrec']=$dsum['covrec']+$usum['covrec'];
         $dsum['lic']=$dsum['lic']+$usum['lic'];
         $dsum['nw']=$dsum['nw']+$usum['nw'];
         $dsum['hw']=$dsum['hw']+$usum['hw'];
         $dsum['tw']=$dsum['tw']+$usum['tw'];
         $dsum['esi']=$dsum['esi']+$usum['esi'];
         $dsum['yi']=$dsum['yi']+$usum['yi'];
         $dsum['wyi']=$dsum['wyi']+$usum['wyi'];
         $dsum['mw']=$dsum['mw']+$usum['mw'];
         $dsum['dw']=$dsum['dw']+$usum['dw'];
         $dsum['tmw']=$dsum['tmw']+$usum['tmw'];
         $dsum['nmw']=$dsum['nmw']+$usum['nmw'];

      
      }
    }//  for loop iterator $z different unit's details display
 echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="8">District Total</td>';
    echo "<td>".$dsum['wage']."</td>";
   
    echo "<td>".$dsum['mw']."</td>";
    echo "<td> </td>";
    echo "<td>".$dsum['dw']."</td>";
    echo "<td>".$dsum['tmw']."</td>";
    echo "<td>".$dsum['nmw']."</td>";
    echo "</tr>";
echo "</tbody>
        <tfoot></tfoot>
    </table>";

}





?>

</div>

<!-- <div class="text-center" >
  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
<a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
<a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
</div>
 -->


    </section>
    
  </form>
<?php 


?>
        <!-- End your project here-->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
