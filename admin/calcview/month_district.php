<?php

  $res3= retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'",$con);
  $res4= retrieveData("SELECT type From kbk_work_type where work_id=$worktype",$con);

  $dsum = array(
    'sliver'   => 0, 
    'wage'     => 0,
    'wf'       => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
    'lic'      => 0,
    'nw'       => 0,
    'hw'       => 0,
    'tw'       => 0,
    'no'       => 0
  );

  $drmsum=array(
    's_grey'    => 0,
    's_colour'  => 0,
    's_black'   => 0,
    's_bl'      => 0,
    's_tot'     => 0,
    's_wage'    => 0,
    's_yi'      => 0,
    'b_grey'    => 0,
    'b_colour'  => 0,
    'b_black'   => 0,
    'b_bl'      => 0,
    'b_tot'     => 0,
    'b_wage'    => 0,
    'b_yi'      => 0,
    'w_grey'    => 0,
    'w_colour'  => 0,
    'w_black'   => 0,
    'w_bl'      => 0,
    'w_tot'     => 0,         
    'w_wage'    => 0,
    'w_yi'      => 0,
  );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <?php
      echo '<title>'.$res4[0]['type'].'</title>';
    ?>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"/>
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"/>
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
          
    <style>
      #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #customers tr:nth-child(even){background-color: #f2f2f2;}

      #customers tr:hover {background-color: #ddd;}

      #customers th {
        padding-top: 2px;
        padding-bottom: 2px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
      }
    </style> 

  </head>
  <body>
    <script type="text/javascript" src="../js/mdb.min.js"></script>

    <!-- Start your project here-->
    <form name="spinninglistrep" action="spinning_pdf.php" method="post"> 
      <section class="" style="padding-top: 30px;">
        <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="">
                <div class="row g-0">
                  <div  id="container_content">
                    <div class="text-black">
                      <h3 class="mb-5 text-uppercase">Monthly <?php echo $res4[0]['type']; ?> Report</h3>            

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label" >Month and Year</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php 
                              echo '<select class="form-select" name="hdate" >';
                              $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                              $m1= strval(date('m',strtotime($hdate)));
                              $y1= strval(date('Y',strtotime($hdate)));
                              echo '<option value="'.$hdate.'">'.$months_name[strval($m1)-1]." ".$y1.'</option></select>'."\n";
                              
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label" >Work Type</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                              echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="'.$res4[0]['type'].'" readonly/>';
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label">Number Of Holidays</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                              echo '<input type="text" name="hno" class="form-control form-control-lg" value="'.$res3[0]['holiday_no'].'" readonly/>';                    
                            ?>
                          </div>
                        </div>
  
                        <div class="col-md-3 mb-">                       
                          <label class="form-label">Number Of Working Days</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                                echo '<input type="text" name="wno" class="form-control form-control-lg" value="'.$res3[0]['total_wd'].'" readonly />'; 
                            ?>                  
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style="padding: 0px 50px 30px 40px;">
  
          <?php
            if($res4[0]['type']=="Spinning"){
            $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
            
            for($z=0;$z<sizeof($res5);$z++){
            $usum = array(
              'sliver'   => 0, 
              'wage'     => 0,
              'wf'       => 0,
              'wfrec'    => 0,
              'adrec'    => 0,
              'kcrec'    => 0,
              'covrec'   => 0,
              'lic'      => 0,
              'nw'       => 0,
              'hw'       => 0,
              'tw'       => 0,     
            );

            $unitdemo=$res5[$z]['unit_name'];
            $unit=$res5[$z]['unit_code'];

            $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_spinning where s_date='$hdate')",$con);

            if(sizeof($res1)>0){
          ?>

          <table class="table-bordered " id="customers">
            <thead>
              <tr><th scope="col" colspan="18" style="background-color: #038857;"><?php echo strtoupper($unitdemo); ?></th></tr>
              <tr>
                <th scope="col">SI No.</th>
                <th scope="col">Artisan Code</th>
                <th scope="col">Artisan Name</th>
                <th scope="col">Variety</th>
                <th scope="col">No. of Hanks</th>
                <th scope="col">Rate</th>
                <th scope="col">Sliver Consumption</th>
                <th scope="col">Wages</th>
                <th scope="col">W/F Fund</th>
                <th scope="col">w/f Recovery</th>
                <th scope="col">Adv Recovery</th>
                <th scope="col">Khadi Cr. Rec.</th>
                <th scope="col">Covid adv.</th>
                <th scope="col">LIC</th>
                <th scope="col">Net wages</th>
                <th scope="col">Holiday wages</th>
                <th scope="col">Total Wages</th>
                <!--  <th scope="col">ESI Contr</th>
                <th scope="col">Yarn Incentive</th>
                <th scope="col">Minimum Wages</th>
                <th scope="col">DA Days</th>
                <th scope="col">DA Wages</th>
                <th scope="col">Total Minimum Wages</th>
                <th scope="col">Net Minimum Wages</th> -->
                <th scope="col">Attendance</th> 
              </tr>
            </thead>

            <tbody>
              <?php
                for($i=0;$i<sizeof($res1);$i++){
        
                  echo '<tr><td>'.strval($i+1).'</td>';
                  echo '<td>'.$res1[$i]['artisan_code'].'</td>';
                  echo '<td>'.$res1[$i]['artisan_name'].'</td>';
                  $temp1=strval($res1[$i]['artisan_id']);
                  
                  $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1",$con);
                  $temp2=$res6[0]['yarn_id'];
                  $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=1",$con);
                  $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'",$con);
                  echo '<td>'.$res7[0]['type'].'</td>';
                  echo '<td>'.$res6[0]['noh'].'</td>';
                  echo '<td>'.$res7[0]['rate'].'</td>';
       
                  $sar1  = array(
                    'noh'    => $res6[0]['noh'],
                    'sliver' => $res7[0]['sliver_value'],
                    'rate'   => $res7[0]['rate'],
                    'wf'     => $res7[0]['wf'],
                    'val'    => $res7[0]['value'],
                    'wfrec'  => $res8[0]['wf_rec'],
                    'adrec'  => $res8[0]['ad_rec'],
                    'kcrec'  => $res8[0]['credit_rec'],
                    'covrec' => $res8[0]['cov_rec'],
                    'lic'    => $res8[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd']
                  );

                  $calc1=spinningWageCalculation($sar1);
                
                  $sar2 = array(
                    'noh'     => $res6[0]['noh'],
                    'esi'     => $res7[0]['esi'],
                    'yi'      => $res7[0]['yi'],
                    'mw'      => $res7[0]['mw'],
                    'target'  => $res7[0]['target'],
                    'dawages' => $res3[0]['da_wages'],
                    'wage'    => $calc1['wage'],
                  );
        
                  $da=$res3[0]['total_wd'];
                  $calc2=spinningIncentivesCalculation($sar2,$da);

                  echo '<td>'.$calc1['sliver'].'</td>';
                  echo '<td>'.$calc1['wage'].'</td>';
                  echo  '<td>'.$calc1['wf'].'</td>';
                  echo  '<td>'.$calc1['wfrec'].'</td>';
                  echo  '<td>'.$calc1['adrec'].'</td>';
                  echo  '<td>'.$calc1['kcrec'].'</td>';
                  echo '<td>'.$calc1['covrec'].'</td>';
                  echo  '<td>'.$calc1['lic'].'</td>';
                  echo  '<td>'.$calc1['netwages'].'</td>';
                  echo  '<td>'.$calc1['hdwage'].'</td>';
                  echo  '<td>'.$calc1['totwages'].'</td>';
                  // echo  '<td>'.$calc2['esi'].'</td>';
                  // echo  '<td>'.$calc2['yi'].'</td>';
                  // echo  '<td>'.$calc2['mw'].'</td>';
                  // echo  '<td>'.$calc2['dadays'].'</td>';
                  // echo  '<td>'.$calc2['dawages'].'</td>';
                  // echo  '<td>'.$calc2['totmw'].'</td>';
                  // echo  '<td>'.$calc2['netmw'].'</td>';
                  echo  '<td>'.$res6[0]['attendance'].'</td></tr>';
        
                  $usum['sliver']=$usum['sliver']+$calc1['sliver'];
                  $usum['wage']=$usum['wage']+$calc1['wage'];
                  $usum['wf']=$usum['wf']+$calc1['wf'];
                  $usum['wfrec']=$usum['wfrec']+$calc1['wfrec'];
                  $usum['adrec']=$usum['adrec']+$calc1['adrec'];
                  $usum['kcrec']=$usum['kcrec']+$calc1['kcrec'];
                  $usum['covrec']=$usum['covrec']+$calc1['covrec'];
                  $usum['lic']=$usum['lic']+$calc1['lic'];
                  $usum['nw']=$usum['nw']+$calc1['netwages'];
                  $usum['hw']=$usum['hw']+$calc1['hdwage'];
                  $usum['tw']=$usum['tw']+$calc1['totwages'];
                }

                echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="6">Unit Total</td>';
                echo "<td>".$usum['sliver']."</td>";
                echo "<td>".$usum['wage']."</td>";
                echo "<td>".$usum['wf']."</td>";
                echo "<td>".$usum['wfrec']."</td>";
                echo "<td>".$usum['adrec']."</td>";
                echo "<td>".$usum['kcrec']."</td>";
                echo "<td>".$usum['covrec']."</td>";
                echo "<td>".$usum['lic']."</td>";
                echo "<td>".$usum['nw']."</td>";
                echo "<td>".$usum['hw']."</td>";
                echo "<td>".$usum['tw']."</td>";
                echo "<td> </td></tr>";

                $dsum['sliver']=$dsum['sliver']+$usum['sliver'];
                $dsum['wage']=$dsum['wage']+$usum['wage'];
                $dsum['wf']=$dsum['wf']+$usum['wf'];
                $dsum['wfrec']=$dsum['wfrec']+$usum['wfrec'];
                $dsum['adrec']=$dsum['adrec']+$usum['adrec'];
                $dsum['kcrec']=$dsum['kcrec']+$usum['kcrec'];
                $dsum['covrec']=$dsum['covrec']+$usum['covrec'];
                $dsum['lic']=$dsum['lic']+$usum['lic'];
                $dsum['nw']=$dsum['nw']+$usum['nw'];
                $dsum['hw']=$dsum['hw']+$usum['hw'];
                $dsum['tw']=$dsum['tw']+$usum['tw'];

              }// if closing of if(sizeof($res1)>0)
            }

            echo '<tr style="background-color: #808080; color:white;text-align: center;"><td colspan="6">District Total</td>';
            echo "<td>".$dsum['sliver']."</td>";
            echo "<td>".$dsum['wage']."</td>";
            echo "<td>".$dsum['wf']."</td>";
            echo "<td>".$dsum['wfrec']."</td>";
            echo "<td>".$dsum['adrec']."</td>";
            echo "<td>".$dsum['kcrec']."</td>";
            echo "<td>".$dsum['covrec']."</td>";
            echo "<td>".$dsum['lic']."</td>";
            echo "<td>".$dsum['nw']."</td>";
            echo "<td>".$dsum['hw']."</td>";
            echo "<td>".$dsum['tw']."</td>";
            echo "<td> </td></tr>";
            echo '</tbody><tfoot></tfoot></table> ';
          }// if closing of work type spinning

          if($res4[0]['type']=="Weaving"){
            $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
            
            for($z=0;$z<sizeof($res5);$z++){
              $unitdemo=$res5[$z]['unit_name'];
              $unit=$res5[$z]['unit_code'];
            
              $usum = array(
                'wage'     => 0,
                'wf'       => 0,
                'wfrec'    => 0,
                'adrec'    => 0,
                'kcrec'    => 0,
                'covrec'   => 0,
                'lic'      => 0,
                'nw'       => 0,
                'hw'       => 0,
                'tw'       => 0,
              );

              $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_weaving where s_date='$hdate')",$con);
            
              if(sizeof($res1)>0){
                echo '<table class="table-bordered" id="customers">
                  <thead>     
                    <tr><th scope="col" colspan="20" style="background-color: #038857;">'.strtoupper($unitdemo).'</th></tr>
                    <tr>
                      <th scope="col">SI No.</th>
                      <th scope="col">Artisan Code</th>
                      <th scope="col">Artisan Name</th>
                      <th scope="col">Variety</th>
                      <th scope="col">No. of Pieces</th>
                      <th scope="col">Metre</th>
                      <th scope="col">Square Metre</th>
                      <th scope="col">Weaving Type</th>
                      <th scope="col">Rate</th>
                      
                      <th scope="col">Wages</th>
                      <th scope="col">W/F Fund</th>
                      <th scope="col">w/f Recovery</th>
                      <th scope="col">Adv Recovery</th>
                      <th scope="col">Khadi Cr. Rec.</th>
                      <th scope="col">Covid adv.</th>
                      <th scope="col">LIC</th>
                      <th scope="col">Net wages</th>
                      <th scope="col">Holiday wages</th>
                      <th scope="col">Total Wages</th>
              
                      <th scope="col">Attendance</th>
                    </tr>
                  </thead>
                <tbody>';

                for($i=0;$i<sizeof($res1);$i++){
          
                  $temp1=strval($res1[$i]['artisan_id']);
                  $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1",$con);
                  $temp2 = $res9[0]['weaving_id'];
                  $temp3 = $res9[0]['w_id'];
                  $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3",$con);
                  $res11 = retrieveData("SELECT type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2",$con);
                  $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
                  $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2",$con);
                  
                  $war3 =[
                    'warp_grey'    => $res13[0]['warp_grey'],
                    'warp_colour'  => $res13[0]['warp_colour'],
                    'warp_black'   => $res13[0]['warp_black'],
                    'warp_bl'      => $res13[0]['warp_bl'],
                    'weft_grey'    => $res13[0]['weft_grey'],
                    'weft_colour'  => $res13[0]['weft_colour'],
                    'weft_black'   => $res13[0]['weft_black'],
                    'weft_bl'      => $res13[0]['weft_bl'],
                  ];

                  $calc5 = weavingYICalculation($war3);

                  $war1 = [
                    'metre'  => $res9[0]['mtr'],
                    'wage'   => $res10[0]['w_wages'],
                    'wf'     => $res11[0]['wf'],
                    'prime'  => $res11[0]['prime_cost'],
                    'wfrec'  => $res12[0]['wf_rec'],
                    'adrec'  => $res12[0]['ad_rec'],
                    'kcrec'  => $res12[0]['credit_rec'],
                    'covrec' => $res12[0]['cov_rec'],
                    'lic'    => $res12[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd'],
                    'sqmtr'  => $res11[0]['width'],
                    'value'  => 0
                  ];

                  $calc3=weavingWageCalculation($war1);

                  $war2 = [
                    'metre'   => $res9[0]['mtr'],
                    'esi'     => $res11[0]['esi'],
                    'warpyi'  => $res10[0]['warp_yi'],
                    'weftyi'  => $res10[0]['weft_yi'],
                    'mw'      => $res10[0]['min_wages'],
                    'target'  => $res10[0]['target'],
                    'dawages' => $res3[0]['da_wages'],
                    'wage'    => $calc3['wage'],
                    'warp_tot'=> $calc5['warp_tot'],
                    'weft_tot'=> $calc5['weft_tot'],
                  ];

                  $da=$res3[0]['total_wd'];
                  $calc4=weavingIncentivesCalculation($war2,$da);

                  echo '<tr><td>'.strval($i+1).'</td>';
                  echo '<td>'.$res1[$i]['artisan_code'].'</td>';
                  echo '<td>'.$res1[$i]['artisan_name'].'</td>';
                  echo '<td>'.$res11[0]['type'].'</td>';
                  echo '<td>'.$res9[0]['nop'].'</td>';
                  echo '<td>'.$res9[0]['mtr'].'</td>';
                  echo '<td>'.$calc3['sqmtr'].'</td>';
                  echo '<td>'.$res10[0]['w_type'].'</td>';
                  echo '<td>'.$res10[0]['w_wages'].'</td>';
                  echo '<td>'.$calc3['wage'].'</td>';
                  echo '<td>'.$calc3['wf'].'</td>';
                  echo '<td>'.$calc3['wfrec'].'</td>';
                  echo '<td>'.$calc3['adrec'].'</td>';
                  echo '<td>'.$calc3['kcrec'].'</td>';
                  echo '<td>'.$calc3['covrec'].'</td>';
                  echo '<td>'.$calc3['lic'].'</td>';
                  echo '<td>'.$calc3['netwages'].'</td>';
                  echo '<td>'.$calc3['hdwage'].'</td>';
                  echo '<td>'.$calc3['totwages'].'</td>';
                  // echo '<td>'.$calc4['esi'].'</td>';
                  // echo '<td>'.$calc4['warpyi'].'</td>';
                  // echo '<td>'.$calc4['weftyi'].'</td>';
                  // echo '<td>'.$calc4['mw'].'</td>';
                  // echo '<td>'.$calc4['dadays'].'</td>';
                  // echo '<td>'.$calc4['dawages'].'</td>';
                  // echo '<td>'.$calc4['totmw'].'</td>';
                  // echo '<td>'.$calc4['netmw'].'</td>';
                  echo '<td>'.$res9[0]['attendance'].'</td></tr>';

                  $usum['wage']=$usum['wage']+$calc3['wage'];
                  $usum['wf']=$usum['wf']+$calc3['wf'];
                  $usum['wfrec']=$usum['wfrec']+$calc3['wfrec'];
                  $usum['adrec']=$usum['adrec']+$calc3['adrec'];         
                  $usum['kcrec']=$usum['kcrec']+$calc3['kcrec'];
                  $usum['covrec']=$usum['covrec']+$calc3['covrec'];
                  $usum['lic']=$usum['lic']+$calc3['lic'];
                  $usum['nw']=$usum['nw']+$calc3['netwages'];
                  $usum['hw']=$usum['hw']+$calc3['hdwage'];
                  $usum['tw']=$usum['tw']+$calc3['totwages'];
                }

                echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="9">Unit Total</td>';
                echo "<td>".$usum['wage']."</td>";
                echo "<td>".$usum['wf']."</td>";
                echo "<td>".$usum['wfrec']."</td>";
                echo "<td>".$usum['adrec']."</td>";
                echo "<td>".$usum['kcrec']."</td>";
                echo "<td>".$usum['covrec']."</td>";
                echo "<td>".$usum['lic']."</td>";
                echo "<td>".$usum['nw']."</td>";
                echo "<td>".$usum['hw']."</td>";
                echo "<td>".$usum['tw']."</td>";
                echo "<td> </td></tr>";

                $dsum['wage']=$dsum['wage']+$usum['wage'];
                $dsum['wf']=$dsum['wf']+$usum['wf'];
                $dsum['wfrec']=$dsum['wfrec']+$usum['wfrec'];
                $dsum['adrec']=$dsum['adrec']+$usum['adrec'];
                $dsum['kcrec']=$dsum['kcrec']+$usum['kcrec'];
                $dsum['covrec']=$dsum['covrec']+$usum['covrec'];
                $dsum['lic']=$dsum['lic']+$usum['lic'];
                $dsum['nw']=$dsum['nw']+$usum['nw'];
                $dsum['hw']=$dsum['hw']+$usum['hw'];
                $dsum['tw']=$dsum['tw']+$usum['tw'];
              }
            }//  for loop iterator $z different unit's details display
    
            echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="9">District Total</td>';
            echo "<td>".$dsum['wage']."</td>";
            echo "<td>".$dsum['wf']."</td>";
            echo "<td>".$dsum['wfrec']."</td>";
            echo "<td>".$dsum['adrec']."</td>";
            echo "<td>".$dsum['kcrec']."</td>";
            echo "<td>".$dsum['covrec']."</td>";
            echo "<td>".$dsum['lic']."</td>";
            echo "<td>".$dsum['nw']."</td>";
            echo "<td>".$dsum['hw']."</td>";
            echo "<td>".$dsum['tw']."</td>";
            echo "<td> </td></tr>";
            echo "</tbody><tfoot></tfoot></table>";
          }

          if($res4[0]['type']=="Preprocessing"){
            $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
  
            for($z=0;$z<sizeof($res5);$z++) {// loop for units
              //$excel_var[$z][0][0]=0;

              $sum = array(
                'no'       => 0,
                'wage'     => 0,
                'wf'       => 0,
                'wfrec'    => 0,
                'adrec'    => 0,
                'kcrec'    => 0,
                'covrec'   => 0,
                'lic'      => 0,
                'nw'       => 0,
                'hw'       => 0,
                'tw'       => 0,
                'esi'      => 0,
                'yi'       => 0,
                'mw'       => 0,
                'dw'       => 0,
                'tmw'      => 0,
                'nmw'      => 0,
              );

              $rmsum=array(
                's_grey'    => 0,
                's_colour'  => 0,
                's_black'   => 0,
                's_bl'      => 0,
                's_tot'     => 0,
                's_wage'    => 0,
                's_yi'      => 0,
                'b_grey'    => 0,
                'b_colour'  => 0,
                'b_black'   => 0,
                'b_bl'      => 0,
                'b_tot'     => 0,
                'b_wage'    => 0,
                'b_yi'      => 0,
                'w_grey'    => 0,
                'w_colour'  => 0,
                'w_black'   => 0,
                'w_bl'      => 0,
                'w_tot'     => 0,         
                'w_wage'    => 0,
                'w_yi'      => 0,
              );

              $unitdemo=$res5[$z]['unit_name'];
              $unit=$res5[$z]['unit_code'];
              $dunitname[$z]=$unitdemo;

              $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name,account_no,ifsc,bank_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_preprocessing where s_date='$hdate')",$con);
          
              if(sizeof($res1)>0){
                  echo '<table class="table-bordered" id="customers">
                    <thead>
                      <tr><th scope="col" colspan="49" style="background-color: #038857;">'.strtoupper($unitdemo).'</th></tr>
                      <tr>
                        <th colspan="4"></th>
                        <th colspan="8" style="text-align:center">Sizing</th>
                        <th colspan="8" style="text-align:center">Bobin Winding</th>
                        <th colspan="8" style="text-align:center">Warping</th>
                        <th colspan="21"></th>
                      </tr>
                      <tr>
                        <th scope="col">SI No.</th>
                        <th scope="col">Artisan Code</th>
                        <th scope="col">Artisan Name</th>
                        <th scope="col">Variety</th>

                        <th scope="col">Grey</th>
                        <th scope="col">Colour</th>
                        <th scope="col">Bl</th>
                        <th scope="col">Black</th>
                        <th scope="col">Total</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Wage</th>
                        <th scope="col">YI</th>

                        <th scope="col">Grey</th>
                        <th scope="col">Colour</th>
                        <th scope="col">Bl</th>
                        <th scope="col">Black</th>
                        <th scope="col">Total</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Wage</th>
                        <th scope="col">YI</th>

                        <th scope="col">Grey</th>
                        <th scope="col">Colour</th>
                        <th scope="col">Bl</th>
                        <th scope="col">Black</th>
                        <th scope="col">Total</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Wage</th>
                        <th scope="col">YI</th>
                      
                        <th scope="col">Wages</th>

                        <th scope="col">W/F Fund</th>
                        <th scope="col">W/F Rec.</th>
                        <th scope="col">Adv Rec.</th>
                        <th scope="col">Khadi Cr. Rec.</th>
                        <th scope="col">Covid adv. Rec.</th>
                        <th scope="col">LIC</th>
                        <th scope="col">Net wages</th>
                        <th scope="col">Holiday wages</th>
                        <th scope="col">Total Wages</th>

                        <th scope="col">Attendance</th>
                      </tr>
                    </thead>
                  <tbody>';

                  for($i=0;$i<sizeof($res1);$i++){

                    $artid=intval($res1[$i]['artisan_id']);
                    $res6 =  retrieveData("SELECT wages,attendance,pre_id,preprocessing_id From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'",$con);
                    $preid=intval($res6[0]['pre_id']);
                    $preprocessingid=intval($res6[0]['preprocessing_id']);
                    $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3",$con);
                    $res9=retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid",$con);
                    $res10=retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid",$con);

                    $par1 = [
                      'grey'  => $res10[0]['s_grey'],
                      'col'   => $res10[0]['s_col'],
                      'bl'    => $res10[0]['s_bl'],
                      'black' => $res10[0]['s_black'],
                    ];

                    $sizetotnoh=preprocessingTypeWage($par1);

                    $par1 = [
                      'grey'  => $res10[0]['b_grey'],
                      'col'   => $res10[0]['b_col'],
                      'bl'    => $res10[0]['b_bl'],
                      'black' => $res10[0]['b_black'],
                    ];

                    $bobintotnoh=preprocessingTypeWage($par1);

                    $par1 = [
                      'grey'  => $res10[0]['w_grey'],
                      'col'   => $res10[0]['w_col'],
                      'bl'    => $res10[0]['w_bl'],
                      'black' => $res10[0]['w_black'],
                    ];
    
                    $warptotnoh=preprocessingTypeWage($par1);

                    $wfund=0;
                    if($res8[0]['wf_yes_no']==1){
                      $wfund=$res9[0]['wf'];
                    }

                    $par2 = [
                      'snoh'  => $sizetotnoh,
                      'bnoh'  => $bobintotnoh,
                      'wnoh'  => $warptotnoh,
                      'swage' => $res9[0]['s_wages'],
                      'bwage' => $res9[0]['b_wages'],
                      'wwage' => $res9[0]['w_wages'],
                      'wf'    => $wfund,
                      'wfrec' => $res8[0]['wf_rec'],
                      'adrec' => $res8[0]['ad_rec'],
                      'kcrec' => $res8[0]['credit_rec'],
                      'covrec'=> $res8[0]['cov_rec'],
                      'lic'   => $res8[0]['lic'],
                      'hno'    => $res3[0]['holiday_no'],
                      'wno'    => $res3[0]['total_wd'],
                    ];

                    $calc7=preprocessingWageCalculation($par2);

                    $par3 = [
                      'snoh'     => $sizetotnoh,
                      'bnoh'     => $bobintotnoh,
                      'wnoh'     => $warptotnoh,
                      'esi'      => $res9[0]['esi'],
                      'syi'      => $res9[0]['s_yi'],
                      'byi'      => $res9[0]['b_yi'],
                      'wyi'      => $res9[0]['w_yi'],  
                      'smw'      => $res9[0]['s_min_wages'],
                      'bmw'      => $res9[0]['b_min_wages'],
                      'wmw'      => $res9[0]['w_min_wages'],
                      'starget'  => $res9[0]['s_target'],
                      'btarget'  => $res9[0]['b_target'],
                      'wtarget'  => $res9[0]['w_target'],
                      'dawages'  => $res3[0]['da_wages'],
                      'wage'     => $calc7['wage'],
                      'twd'      => $res3[0]['total_wd'],
                      'tnoh'     => $calc7['totnoh'],   
                    ];

                    $calc8=preprocessingIncentivesCalculation($par3);

                    echo '<tr style="text-align: center;"><td>'.strval($i+1).'</td>';
                    echo '<td style="text-align: left;">'.$res1[$i]['artisan_code'].'</td>';
                    echo '<td style="text-align: left;">'.$res1[$i]['artisan_name'].'</td>';
                    echo '<td style="text-align: left;">'.$res9[0]['type'].'</td>';
                    echo '<td ">'.$res10[0]['s_grey'].'</td>';
                    echo '<td ">'.$res10[0]['s_col'].'</td>';
                    echo '<td ">'.$res10[0]['s_bl'].'</td>';
                    echo '<td ">'.$res10[0]['s_black'].'</td>';
                    echo '<td ">'.$sizetotnoh.'</td>';
                    echo '<td ">'.$res9[0]['s_wages'].'</td>';
                    echo '<td ">'.$calc7['swage'].'</td>';
                    echo '<td ">'.$calc8['syi'].'</td>';

                    echo '<td ">'.$res10[0]['b_grey'].'</td>';
                    echo '<td ">'.$res10[0]['b_col'].'</td>';
                    echo '<td ">'.$res10[0]['b_bl'].'</td>';
                    echo '<td ">'.$res10[0]['b_black'].'</td>';
                    echo '<td ">'.$bobintotnoh.'</td>';
                    echo '<td ">'.$res9[0]['b_wages'].'</td>';
                    echo '<td ">'.$calc7['bwage'].'</td>';
                    echo '<td ">'.$calc8['byi'].'</td>';

                    echo '<td ">'.$res10[0]['w_grey'].'</td>';
                    echo '<td ">'.$res10[0]['w_col'].'</td>';
                    echo '<td ">'.$res10[0]['w_bl'].'</td>';
                    echo '<td ">'.$res10[0]['w_black'].'</td>';
                    echo '<td ">'.$warptotnoh.'</td>';
                    echo '<td ">'.$res9[0]['w_wages'].'</td>';
                    echo '<td ">'.$calc7['wwage'].'</td>';
                    echo '<td ">'.$calc8['wyi'].'</td>';

                    echo '<td ">'.$calc7['wage'].'</td>';
                    echo '<td ">'.$calc7['wf'].'</td>';
                    echo '<td ">'.$res8[0]['wf_rec'].'</td>';
                    echo '<td ">'.$res8[0]['ad_rec'].'</td>';
                    echo '<td ">'.$res8[0]['credit_rec'].'</td>';
                    echo '<td ">'.$res8[0]['cov_rec'].'</td>';
                    echo '<td ">'.$res8[0]['lic'].'</td>';
                    echo '<td ">'.$calc7['netwages'].'</td>';
                    echo '<td ">'.$calc7['hdwage'].'</td>';
                    echo '<td ">'.$calc7['totwages'].'</td>';
                    echo '<td ">'.$res6[0]['attendance'].'</td></tr>';

                    $rmsum['s_grey']  =$rmsum['s_grey']+$res10[0]['s_grey'];
                    $rmsum['s_colour']=$rmsum['s_colour']+$res10[0]['s_col'];
                    $rmsum['s_black'] =$rmsum['s_black']+$res10[0]['s_black'];
                    $rmsum['s_bl']    =$rmsum['s_bl']+$res10[0]['s_bl'];
                    $rmsum['s_tot']   =$rmsum['s_tot']+$sizetotnoh;
                    $rmsum['s_wage']  =$rmsum['s_wage']+$calc7['swage'];
                    $rmsum['s_yi']    =$rmsum['s_yi']+$calc8['syi'];
                    $rmsum['b_grey']  =$rmsum['b_grey']+$res10[0]['b_grey'];
                    $rmsum['b_colour']=$rmsum['b_colour']+$res10[0]['b_col'];
                    $rmsum['b_black'] =$rmsum['b_black']+$res10[0]['b_black'];
                    $rmsum['b_bl']    =$rmsum['b_bl']+$res10[0]['b_bl'];
                    $rmsum['b_tot']   =$rmsum['b_tot']+$bobintotnoh;
                    $rmsum['b_wage']  =$rmsum['b_wage']+$calc7['bwage'];
                    $rmsum['b_yi']    =$rmsum['b_yi']+$calc8['byi'];
                    $rmsum['w_grey']  =$rmsum['w_grey']+$res10[0]['w_grey'];
                    $rmsum['w_colour']=$rmsum['w_colour']+$res10[0]['w_col'];
                    $rmsum['w_black'] =$rmsum['w_black']+$res10[0]['w_black'];
                    $rmsum['w_bl']    =$rmsum['w_bl']+$res10[0]['w_bl'];
                    $rmsum['w_tot']   =$rmsum['w_tot']+$warptotnoh;
                    $rmsum['w_wage']  =$rmsum['w_wage']+$calc7['wwage'];
                    $rmsum['w_yi']    =$rmsum['w_yi']+$calc8['wyi'];

                    $sum['wf']=$sum['wf']+$calc7['wf'];
                    $sum['wfrec']=$sum['wfrec']+$calc7['wfrec'];
                    $sum['adrec']=$sum['adrec']+$calc7['adrec'];         
                    $sum['kcrec']=$sum['kcrec']+$calc7['kcrec'];
                    $sum['covrec']=$sum['covrec']+$calc7['covrec'];
                    $sum['lic']=$sum['lic']+$calc7['lic'];
                    $sum['nw']=$sum['nw']+$calc7['netwages'];
                    $sum['hw']=$sum['hw']+$calc7['hdwage'];
                    $sum['tw']=$sum['tw']+$calc7['totwages'];
                    $sum['tmw']=$sum['tmw']+$calc8['totmw'];
                    $sum['nmw']=$sum['nmw']+$calc8['netmw'];
                    $sum['no']=$sum['no']+$calc7['wage'];
                  }
                  echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
                  echo '<td>'.$rmsum['s_grey'].'</td>';
                  echo '<td>'.$rmsum['s_colour'].'</td>';
                  echo '<td>'.$rmsum['s_bl'].'</td>';
                  echo '<td>'.$rmsum['s_black'].'</td>';      
                  echo '<td>'.$rmsum['s_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$rmsum['s_wage'].'</td>';
                  echo '<td>'.$rmsum['s_yi'].'</td>';
                  echo '<td>'.$rmsum['b_grey'].'</td>';
                  echo '<td>'.$rmsum['b_colour'].'</td>';
                  echo '<td>'.$rmsum['b_bl'].'</td>';
                  echo '<td>'.$rmsum['b_black'].'</td>';      
                  echo '<td>'.$rmsum['b_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$rmsum['b_wage'].'</td>';
                  echo '<td>'.$rmsum['b_yi'].'</td>';
                  echo '<td>'.$rmsum['w_grey'].'</td>';
                  echo '<td>'.$rmsum['w_colour'].'</td>';
                  echo '<td>'.$rmsum['w_bl'].'</td>';
                  echo '<td>'.$rmsum['w_black'].'</td>';      
                  echo '<td>'.$rmsum['w_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$rmsum['w_wage'].'</td>';
                  echo '<td>'.$rmsum['w_yi'].'</td>';
                  echo '<td>'.$sum['no'].'</td>';
                  echo "<td>".$sum['wf']."</td>";
                  echo "<td>".$sum['wfrec']."</td>";
                  echo "<td>".$sum['adrec']."</td>";
                  echo "<td>".$sum['kcrec']."</td>";
                  echo "<td>".$sum['covrec']."</td>";
                  echo "<td>".$sum['lic']."</td>";
                  echo "<td>".$sum['nw']."</td>";
                  echo "<td>".$sum['hw']."</td>";
                  echo "<td>".$sum['tw']."</td>";
                  echo "<td></td><tr>";
                
                  $drmsum['s_grey']=$drmsum['s_grey']+$rmsum['s_grey'];
                  $drmsum['s_colour']=$drmsum['s_colour']+$rmsum['s_colour'];
                  $drmsum['s_bl']=$drmsum['s_bl']+$rmsum['s_bl'];
                  $drmsum['s_black']=$drmsum['s_black']+$rmsum['s_black'];   
                  $drmsum['s_tot']=$drmsum['s_tot']+$rmsum['s_tot'];
                  $drmsum['s_wage']=$drmsum['s_wage']+$rmsum['s_wage'];
                  $drmsum['s_yi']=$drmsum['s_yi']+$rmsum['s_yi'];
                  $drmsum['b_grey']=$drmsum['b_grey']+$rmsum['b_grey'];
                  $drmsum['b_colour']=$drmsum['b_colour']+$rmsum['b_colour'];
                  $drmsum['b_bl']=$drmsum['b_bl']+$rmsum['b_bl'];
                  $drmsum['b_black']=$drmsum['b_black']+$rmsum['b_black'];     
                  $drmsum['b_tot']=$drmsum['b_tot']+$rmsum['b_tot'];
                  $drmsum['b_wage']=$drmsum['b_wage']+$rmsum['b_wage'];
                  $drmsum['b_yi']=$drmsum['b_yi']+$rmsum['b_yi'];
                  $drmsum['w_grey']=$drmsum['w_grey']+$rmsum['w_grey'];
                  $drmsum['w_colour']=$drmsum['w_colour']+$rmsum['w_colour'];
                  $drmsum['w_bl']=$drmsum['w_bl']+$rmsum['w_bl'];
                  $drmsum['w_black']=$drmsum['w_black']+$rmsum['w_black'];  
                  $drmsum['w_tot']=$drmsum['w_tot']+$rmsum['w_tot'];
                  $drmsum['w_wage']=$drmsum['w_wage']+$rmsum['w_wage'];
                  $drmsum['w_yi']=$drmsum['w_yi']+$rmsum['w_yi'];
                  $dsum['wage']  =$dsum['wage']+$sum['no'];
                  $dsum['wf']    =$dsum['wf']+$sum['wf'];
                  $dsum['wfrec'] =$dsum['wfrec']+$sum['wfrec'];
                  $dsum['adrec'] =$dsum['adrec']+$sum['adrec'];
                  $dsum['kcrec'] =$dsum['kcrec']+$sum['kcrec'];
                  $dsum['covrec']=$dsum['covrec']+$sum['covrec'];
                  $dsum['lic']   =$dsum['lic']+$sum['lic'];
                  $dsum['nw']    =$dsum['nw']+$sum['nw']; 
                  $dsum['hw']    =$dsum['hw']+$sum['hw'];
                  $dsum['tw']    =$dsum['tw']+$sum['tw'];
                  //$dsum['no']    =$dsum['']

              }
            }
              
                  echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="4">District Total</td>';
                  echo '<td>'.$drmsum['s_grey'].'</td>';
                  echo '<td>'.$drmsum['s_colour'].'</td>';
                  echo '<td>'.$drmsum['s_bl'].'</td>';
                  echo '<td>'.$drmsum['s_black'].'</td>';      
                  echo '<td>'.$drmsum['s_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$drmsum['s_wage'].'</td>';
                  echo '<td>'.$drmsum['s_yi'].'</td>';
                  echo '<td>'.$drmsum['b_grey'].'</td>';
                  echo '<td>'.$drmsum['b_colour'].'</td>';
                  echo '<td>'.$drmsum['b_bl'].'</td>';
                  echo '<td>'.$drmsum['b_black'].'</td>';      
                  echo '<td>'.$drmsum['b_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$drmsum['b_wage'].'</td>';
                  echo '<td>'.$drmsum['b_yi'].'</td>';
                  echo '<td>'.$drmsum['w_grey'].'</td>';
                  echo '<td>'.$drmsum['w_colour'].'</td>';
                  echo '<td>'.$drmsum['w_bl'].'</td>';
                  echo '<td>'.$drmsum['w_black'].'</td>';      
                  echo '<td>'.$drmsum['w_tot'].'</td>';
                  echo '<td></td>';
                  echo '<td>'.$drmsum['w_wage'].'</td>';
                  echo '<td>'.$drmsum['w_yi'].'</td>';
                  echo '<td>'.$dsum['no'].'</td>';
                  echo "<td>".$dsum['wf']."</td>";
                  echo "<td>".$dsum['wfrec']."</td>";
                  echo "<td>".$dsum['adrec']."</td>";
                  echo "<td>".$dsum['kcrec']."</td>";
                  echo "<td>".$dsum['covrec']."</td>";
                  echo "<td>".$dsum['lic']."</td>";
                  echo "<td>".$dsum['nw']."</td>";
                  echo "<td>".$dsum['hw']."</td>";
                  echo "<td>".$dsum['tw']."</td>";
                  echo "<td></td></tr>";
                  echo "</tbody><tfoot></tfoot></table>"; 
          }
        ?>

</div>

<!-- <div class="text-center" >
  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
<a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
<a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
</div>
 -->
 <div class="text-center" >
  
<a target="_blank" class="btn btn-info btn_print" href="./calcview/month_district_pdf.php">Wages Report</a>
</div>


    </section>
    
  </form>
<?php 


?>
        <!-- End your project here-->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
