<?php
  include('../connection.php');
  include('../kbk_library.php');
  session_start();

  require_once("../dompdf/autoload.inc.php");
  use Dompdf\Dompdf;
  $dompdf = new Dompdf();

  $html=""; 
  $html.='<html>';
  $html.='<body>';
  $html.='<form>'; 
  $html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
  $html.='<br>';
  $r1=$_SESSION["mydata2"];
  $r2=$_SESSION["mydata4"];
  $r3=$_SESSION["mydata5"];
  $html.='<h3 align="center">'.$r1.'Minimum Wages For '.$_SESSION["mydata1"].'</h3>';
  $html.='<h3 align="center">'.$r2.' '.$r3.'</h3>';
  if($r1=="Spinning"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
      <thead>
        <tr style="width:10.17mm">
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Sl No.</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Verity</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Hanks</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Wages Paid</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Minimum Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">DA Days</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">DA Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Minimum Wages </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Net Minimum Wages </th>
        </tr>
      </thead>';

    $html.=$_SESSION["mydata"];
  }

  else if($r1=="Weaving"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
      <thead>
        <tr style="width:10.17mm">
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Sl No.</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Verity</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Pieces</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Metre</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Weaving Type</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Wages Paid</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Minimum Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">DA Days</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">DA Wages </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Minimum Wages </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Net Minimum Wages </th>
        </tr>
      </thead>';

    $html.=$_SESSION["mydata3"];
  }
              
  else if($r1="Preprocessing"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
      <thead>
        <tr style="width:10.17mm">
          <th colspan="4"></th>
          <th colspan="8">Sizing</th>
          <th colspan="8">Bobin Winding</th>
          <th colspan="8">Wraping</th>
          <th colspan="15"></th>
        </tr>
        <tr style="width:10.17mm">
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Sl No.</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Artisan Code</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Artisan Name</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Variety</th> 
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >W/F Fund</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >W/F Recovery</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Adv Recovery</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Khadi Credit Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" > Covid Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >LIC</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Net wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Holiday wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Minimum Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >DA Days</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >DA Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total Minimum Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Net Minimum Wages</th>
        </tr>
      </thead>';

    $html.=$_SESSION["myData"];
  }

  $html.="</form>";
  $html.="</body>";
  $html.="</html>";
  $dompdf->loadHtml(html_entity_decode($html));	
  $dompdf->setPaper('A4', 'landscape'); //portrait or landscape
  $dompdf->render();
  ob_end_clean();
  $dompdf->stream("Minimum Wage Report",array("Attachment"=>0));

?>    