<?php
  include('../connection.php');
  include('../kbk_library.php');
  session_start();

  require_once("../dompdf/autoload.inc.php");
  use Dompdf\Dompdf;
  $dompdf = new Dompdf();

  $html=""; 
  $html.='<html>';
  $html.='<body>';
  $html.='<form>'; 
  $html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
  $html.='<br>';
  $r1=$_SESSION["mydata2"];
  $r2=$_SESSION["mydata4"];
  $r3=$_SESSION["mydata5"];
  $r4=$_SESSION["unitcode_instructor"];

  $html.='<h3 align="center">'.$r1.' Wages For '.$_SESSION["mydata1"].' Unit('.$r4.') '.$r2.' '.$r3.'</h3>';
  $html.='<table style="border-collapse:collapse" border="0" width=100%><th>Number of Holidays : '.$_SESSION["holy_days"].'</th><th style="text-align:right;">Number of Working days : '.$_SESSION["working_days"].'</th></table>';

  if($r1=="Spinning"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>

      <thead>
        <tr style="width:10.17mm">
          <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Verity</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Hanks</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Value</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Sliver Value</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Sliver Cons</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Fund</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Rec</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Adv Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Khadi Credit Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm"> Covid Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">LIC</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Net wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Holiday wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Attendance</th>
        </tr>
      </thead>';

    $html.=$_SESSION["mydata"];
    $html.="<new>";
    $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed" border="0" align="center" width=100%>';
    $html.="<tr><td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td></tr>";
    $html.='<tr><td style="height:25px"></td><td></td></tr><tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";
    $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';
  }

  else if($r1=="Weaving"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>

      <thead>
        <tr>
          <th >Sl No.</th>
          <th style="overflow: hidden;" colspan="4">Artisan Code</th>
          <th style="overflow: hidden;" colspan="4">Artisan Name</th>
          <th style="overflow: hidden;" colspan="4">Verity</th>
          <th style="overflow: hidden;" colspan="2">No. of Pieces</th>
          <th style="overflow: hidden;" colspan="2">Metre</th>
          <th style="overflow: hidden;" colspan="2">Square Metre</th>
          <th style="overflow: hidden;" colspan="4">Weaving Type</th>
          <th style="overflow: hidden;" colspan="2">Rate</th>
          <th style="overflow: hidden;" colspan="2">Prime Cost</th>
          <th style="overflow: hidden;" colspan="2">Value</th>
          <th style="overflow: hidden;" colspan="2">Wages</th>
          <th style="overflow: hidden;" colspan="2">W/F Fund</th>
          <th style="overflow: hidden;">w/f Rec </th>
          <th style="overflow: hidden;">Adv Rec </th>
          <th style="overflow: hidden;">KhadiCr</th>
          <th style="overflow: hidden;">Covidad</th>
          <th style="overflow: hidden;">LIC</th>
          <th style="overflow: hidden;" colspan="2">Net wages</th>
          <th style="overflow: hidden;" colspan="2">Holiday wages</th>
          <th style="overflow: hidden;" colspan="2">Total Wages</th>
          <th style="overflow: hidden;" colspan="2">Attendance</th>
        </tr>
      </thead>';

    $html.=$_SESSION["mydata3"];
    $html.='<br><table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
      <tr>
        <th colspan="9"></td>
        <th colspan="5">Warp</td>
        <th colspan="5">Weft</td>
      </tr>
      <tr>
        <th colspan="2" style="overflow: hidden;">Variety Code</th>
        <th colspan="2" style="overflow: hidden;">Variety Name</th>
        <th>Metre</th>
        <th>Square Metre</th>
        <th>Prime Cost</th>
        <th>Value</th>
        <th>Wages</th>
        <th>Grey</th>
        <th>Colour</th>
        <th>Black</th>
        <th>BL</th>
        <th>Total</th>
        <th>Grey</th>
        <th>Colour</th>
        <th>Black</th>
        <th>BL</th>
        <th>Total</th>
      </tr>';

    $html.=$_SESSION['weaving_var_sum'];
    $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="0" align="center" width=67rem>';
    $html.="<tr><td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td></tr>";
    $html.='<tr><td style="height:25px"></td><td></td></tr><tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";
    $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';
  }

  else if($r1=="Preprocessing"){
    $html.='<table style="border-collapse:collapse;table-layout:fixed;word-wrap: break-word;" border="1" align="center" width=100%>
      <thead>
        <tr style="width:10.17mm">
          <th colspan="4"></th>
          <th colspan="8">Sizing</th>
          <th colspan="8">Bobin Winding</th>
          <th colspan="8">Warping</th>
          <th colspan="11"></th>
        </tr>
        <tr style="width:10.17mm">
          <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm">Variety</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Grey</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Colour</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >BI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Black</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Rate</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wage</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >YI</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >W/F Fund</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >W/F Recovery</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Adv Recovery</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Khadi Credit Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" > Covid Rec </th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >LIC</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Net wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Holiday wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Total Wages</th>
          <th scope="col" align="center" halign="middle" style="width:10.17mm" >Attendance</th>
        </tr>
      </thead>';

    $html.=$_SESSION["myData"];
    $html.='<br><br><table style="border-collapse:collapse;table-layout:fixed" border="0" align="center" width=100%>';
    $html.="<tr><td colspan='2'>Certified that the above items actually purchased from the artisan's and taken into the stock register of the unit by me</td></tr>";
    $html.='<tr><td style="height:25px"></td><td></td></tr><tr><td></td><td>'.$_SESSION["instructor_name"]."</td></tr>";
    $html.='<tr><td></td><td>Unit Instructor</td></tr></table>';
  }

  $html.="</table>";
  $html.="</form>";
  $html.="</body>";
  $html.="</html>";
  $dompdf->loadHtml(html_entity_decode($html));	
  $dompdf->setPaper("A4", "landscape"); //portrait or landscape
  $dompdf->render();
  ob_end_clean();
  $dompdf->stream("Monthly Report",array("Attachment"=>0));
?>    