<?php

  $res3= retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'",$con);
  $res4= retrieveData("SELECT type From kbk_work_type where work_id=$worktype",$con);
  $res14=retrieveData("SELECT yarn_code,type from kbk_yarn_variety group by yarn_code",$con);
  $res15=retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code",$con);
  $res16=retrieveData("SELECT rm_code,type from kbk_preprocessing_variety group by rm_code",$con);

  $excel_var = array(array(array()));
  $variety_array= array(array(array()));
  $wdsum=array();
  $dunitname=array();
  $spinning_data="";

  $spinning_array=array(array());

  for($k=0;$k<sizeof($res14);$k++){
    $temp4=$res14[$k]['yarn_code'];
    $spinning_array[$temp4]['noh']= 0;
    $spinning_array[$temp4]['val']= 0;
    $spinning_array[$temp4]['sliver']= 0;
    $spinning_array[$temp4]['sv']= 0;
    $spinning_array[$temp4]['wage']= 0;
  }
  
  $dyarn_array=array(array());

  for($k=0;$k<sizeof($res15);$k++){
    $temp4=$res15[$k]['weaving_code'];
    $dyarn_array[$temp4]['mtr']= 0;
    $dyarn_array[$temp4]['sqmtr']= 0;
    $dyarn_array[$temp4]['val']= 0;
    $dyarn_array[$temp4]['pc']= 0;
    $dyarn_array[$temp4]['warp_grey']= 0;
    $dyarn_array[$temp4]['warp_colour']= 0;
    $dyarn_array[$temp4]['warp_black']= 0;
    $dyarn_array[$temp4]['warp_bl']= 0;
    $dyarn_array[$temp4]['warp_tot']= 0;
    $dyarn_array[$temp4]['weft_grey']= 0;
    $dyarn_array[$temp4]['weft_colour']= 0;
    $dyarn_array[$temp4]['weft_black']= 0;
    $dyarn_array[$temp4]['weft_bl']= 0;
    $dyarn_array[$temp4]['weft_tot']= 0; 
  }

  $rmv_array=array(array());

  for($k=0;$k<sizeof($res16);$k++){
    $temp4=$res16[$k]['rm_code'];
    $rmv_array[$temp4]['s_grey']= 0;
    $rmv_array[$temp4]['s_colour']= 0;
    $rmv_array[$temp4]['s_black']= 0;
    $rmv_array[$temp4]['s_bl']= 0;
    $rmv_array[$temp4]['s_tot']= 0;
    $rmv_array[$temp4]['b_grey']= 0;
    $rmv_array[$temp4]['b_colour']= 0;
    $rmv_array[$temp4]['b_black']= 0;
    $rmv_array[$temp4]['b_bl']= 0;
    $rmv_array[$temp4]['b_tot']= 0;
    $rmv_array[$temp4]['w_grey']= 0;
    $rmv_array[$temp4]['w_colour']= 0;
    $rmv_array[$temp4]['w_black']= 0;
    $rmv_array[$temp4]['w_bl']= 0;
    $rmv_array[$temp4]['w_tot']= 0;
    $rmv_array[$temp4]['grand_tot']= 0;  
  }

  $dsum = array(
    'no'       => 0,
    'meter'    => 0,
    'sqm'      => 0,
    'sliver'   => 0, 
    'wage'     => 0,
    'wf'       => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
    'lic'      => 0,
    'nw'       => 0,
    'hw'       => 0,
    'tw'       => 0,
    'esi'      => 0,
    'yi'       => 0,
    'wyi'      => 0,
    'mw'       => 0,
    'dw'       => 0,
    'tmw'      => 0,
    'nmw'      => 0,
    'val'      => 0,
    'pr'       => 0,
    'sv'       => 0,
  );

  $dyarnsum=array(
    'warp_grey'    => 0,
    'warp_colour'  => 0,
    'warp_black'   => 0,
    'warp_bl'      => 0,
    'warp_tot'     => 0,
    'weft_grey'    => 0,
    'weft_colour'  => 0,
    'weft_black'   => 0,
    'weft_bl'      => 0,
    'weft_tot'     => 0,
  );

  $drmsum=array(
    's_grey'    => 0,
    's_colour'  => 0,
    's_black'   => 0,
    's_bl'      => 0,
    's_tot'     => 0,
    's_wage'    => 0,
    's_yi'      => 0,
    'b_grey'    => 0,
    'b_colour'  => 0,
    'b_black'   => 0,
    'b_bl'      => 0,
    'b_tot'     => 0,
    'b_wage'    => 0,
    'b_yi'      => 0,
    'w_grey'    => 0,
    'w_colour'  => 0,
    'w_black'   => 0,
    'w_bl'      => 0,
    'w_tot'     => 0,         
    'w_wage'    => 0,
    'w_yi'      => 0,
  );
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <?php
      echo '<title>'.$res4[0]['type'].'</title>';
    ?>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"/>
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"/>
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
          
    <style>
      #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #customers tr:nth-child(even){background-color: #f2f2f2;}

      #customers tr:hover {background-color: #ddd;}

      #customers th {
        padding-top: 2px;
        padding-bottom: 2px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
      }
    </style>  
    
  </head>
  <body>
    <script type="text/javascript" src="../js/mdb.min.js"></script>

    <!-- Start your project here-->
    <form name="spinninglistrep" action="spinning_pdf.php" method="post"> 
      <section class="" style="padding-top: 30px;">
        <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="">
                <div class="row g-0">
                  <div  id="container_content">
                    <div class="text-black">
                      <h3 class="mb-5 text-uppercase">Kottayam District Monthly <?php echo $res4[0]['type']; ?> Report</h3>            

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label" >Month and Year</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php 
                              echo '<select class="form-select" name="hdate" >';
                              $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                              $m1= strval(date('m',strtotime($hdate)));
                              $y1= strval(date('Y',strtotime($hdate)));
                              echo '<option value="'.$hdate.'">'.$months_name[strval($m1)-1]." ".$y1.'</option></select>'."\n";
                              $aaaa=strval($months_name[strval($m1)-1]." ".$y1);
                              $_SESSION["month_year"]=strtoupper($aaaa);
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label" >Work Type</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                              echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="'.$res4[0]['type'].'" readonly/>';
                              $_SESSION["work_type"]=$res4[0]['type'];         
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3 mb-3">                       
                          <label class="form-label">Number Of Holidays</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                              echo '<input type="text" name="hno" class="form-control form-control-lg" value="'.$res3[0]['holiday_no'].'" readonly/>';                    
                            ?>
                          </div>
                        </div>
  
                        <div class="col-md-3 mb-">                       
                          <label class="form-label">Number Of Working Days</label>                   
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                              echo '<input type="text" name="wno" class="form-control form-control-lg" value="'.$res3[0]['total_wd'].'" readonly />';
                            ?>                  
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div style="padding: 0px 50px 30px 40px;">
  
          <?php
            if($res4[0]['type']=="Spinning"){

              $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit ",$con);
              for($z=0;$z<sizeof($res5);$z++){
                $excel_var[$z][0][0]=0;

                $usum = array(
                  'noh'      => 0,
                  'val'      => 0,
                  'sv'       => 0,
                  'sliver'   => 0, 
                  'wage'     => 0,
                  'wf'       => 0,
                  'wfrec'    => 0,
                  'adrec'    => 0,
                  'kcrec'    => 0,
                  'covrec'   => 0,
                  'lic'      => 0,
                  'nw'       => 0,
                  'hw'       => 0,
                  'tw'       => 0,
                  'esi'      => 0,
                  'yi'       => 0,
                  'mw'       => 0,
                  'dw'       => 0,
                  'tmw'      => 0,
                  'nmw'      => 0,
                );

                $unitdemo=$res5[$z]['unit_name'];
                $unit=$res5[$z]['unit_code'];
                $dunitname[$z]=$unitdemo;

                $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name,account_no,ifsc,bank_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_spinning where s_date='$hdate')",$con);

                if(sizeof($res1)>0){
          ?>

          <table class="table-bordered " id="customers">
            <thead>
              <tr>
                <th scope="col" colspan="30" style="background-color: #038857;"><?php echo strtoupper($unitdemo);?></th>
              </tr>
              <tr>
                <th scope="col">SI No.</th>
                <th scope="col">Artisan Code</th>
                <th scope="col">Artisan Name</th>
                <th scope="col">Variety</th>
                <th scope="col">No. of Hanks</th>
                <th scope="col">Rate</th>
                <th scope="col">Value</th>
                <th scope="col">Sliver Value</th>
                <th scope="col">Sliver Consumption</th>
                <th scope="col">Wages</th>
                <th scope="col">W/F Fund</th>
                <th scope="col">w/f Recovery</th>
                <th scope="col">Adv Recovery</th>
                <th scope="col">Khadi Cr. Rec.</th>
                <th scope="col">Covid adv.</th>
                <th scope="col">LIC</th>
                <th scope="col">Net wages</th>
                <th scope="col">Holiday wages</th>
                <th scope="col">Total Wages</th>
                <th scope="col">ESI Contr</th>
                <th scope="col">Yarn Incentive</th>
                <th scope="col">Minimum Wages</th>
                <th scope="col">DA Days</th>
                <th scope="col">DA Wages</th>
                <th scope="col">Total Minimum Wages</th>
                <th scope="col">Net Minimum Wages</th>
                <th scope="col">Attendance</th> 
                <th scope="col">Account Number</th> 
                <th scope="col">IFSC</th> 
                <th scope="col">Bank Name</th> 
              </tr>
            </thead>
            <tbody>
              <?php 
                $yarn_array=array(array());
                $spinning_data="";

                for($k=0;$k<sizeof($res14);$k++){
                  $temp4=$res14[$k]['yarn_code'];
                  $yarn_array[$temp4]['noh']= 0;
                  $yarn_array[$temp4]['val']= 0;
                  $yarn_array[$temp4]['sliver']= 0;
                  $yarn_array[$temp4]['sv']= 0;
                  $yarn_array[$temp4]['wage']= 0;
                }

                for($i=0;$i<sizeof($res1);$i++){ // loop for artisans in a un.it
                  echo '<tr style="text-align:center;"><td>'.strval($i+1).'</td>';
                  echo '<td>'.$res1[$i]['artisan_code'].'</td>';
                  echo '<td>'.$res1[$i]['artisan_name'].'</td>';

                  $temp1=strval($res1[$i]['artisan_id']);
                  $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1",$con);
                  $temp2=$res6[0]['yarn_id'];
                  $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=1",$con);
                  $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'",$con);
      
                  echo '<td>'.$res7[0]['type'].'</td>';
                  echo '<td>'.$res6[0]['noh'].'</td>';
                  echo '<td>'.$res7[0]['rate'].'</td>';

                  $wfund=0;
                  if($res8[0]['wf_yes_no']==1){
                    $wfund=$res7[0]['wf'];
                  }
    
                  $sar1  = array(
                    'noh'    => $res6[0]['noh'],
                    'sliver' => $res7[0]['sliver'],
                    'rate'   => $res7[0]['rate'],
                    'wf'     => $wfund,
                    'wfrec'  => $res8[0]['wf_rec'],
                    'adrec'  => $res8[0]['ad_rec'],
                    'kcrec'  => $res8[0]['credit_rec'],
                    'covrec' => $res8[0]['cov_rec'],
                    'lic'    => $res8[0]['lic'],
                    'hno'    => $res3[0]['holiday_no'],
                    'wno'    => $res3[0]['total_wd'],
                    'val'    => $res7[0]['value'],
                    'sv'     => $res7[0]['sliver_value'],
                  );

                  $calc1=spinningWageCalculation($sar1);
      
                  $sar2 = array(
                    'noh'     => $res6[0]['noh'],
                    'esi'     => $res7[0]['esi'],
                    'yi'      => $res7[0]['yi'],
                    'mw'      => $res7[0]['mw'],
                    'target'  => $res7[0]['target'],
                    'dawages' => $res3[0]['da_wages'],
                    'wage'    => $calc1['wage'],
                  );
            
                  $da=$res3[0]['total_wd'];
                  $calc2=spinningIncentivesCalculation($sar2,$da);

                  echo '<td>'.$calc1['val'].'</td>';
                  echo '<td>'.$calc1['sv'].'</td>';
                  echo '<td>'.$calc1['sliver'].'</td>';
                  echo '<td>'.$calc1['wage'].'</td>';
                  echo  '<td>'.$calc1['wf'].'</td>';
                  echo  '<td>'.$calc1['wfrec'].'</td>';
                  echo  '<td>'.$calc1['adrec'].'</td>';
                  echo  '<td>'.$calc1['kcrec'].'</td>';
                  echo '<td>'.$calc1['covrec'].'</td>';
                  echo  '<td>'.$calc1['lic'].'</td>';
                  echo  '<td>'.$calc1['netwages'].'</td>';
                  echo  '<td>'.$calc1['hdwage'].'</td>';
                  echo  '<td>'.$calc1['totwages'].'</td>';
                  echo  '<td>'.$calc2['esi'].'</td>';
                  echo  '<td>'.$calc2['yi'].'</td>';
                  echo  '<td>'.$calc2['mw'].'</td>';
                  echo  '<td>'.$calc2['dadays'].'</td>';
                  echo  '<td>'.$calc2['dawages'].'</td>';
                  echo  '<td>'.$calc2['totmw'].'</td>';
                  echo  '<td>'.$calc2['netmw'].'</td>';
                  echo  '<td>'.$res6[0]['attendance'].'</td>';
                  echo  '<td>'.$res1[$i]['account_no'].'</td>';
                  echo  '<td>'.$res1[$i]['ifsc'].'</td>';
                  echo  '<td>'.$res1[$i]['bank_name'].'</td></tr>';
    
                  $usum['noh']=$usum['noh']+$res6[0]['noh'];
                  $usum['val']=$usum['val']+$calc1['val'];
                  $usum['sv']=$usum['sv']+$calc1['sv'];
                  $usum['sliver']=$usum['sliver']+$calc1['sliver'];
                  $usum['wage']=$usum['wage']+$calc1['wage'];
                  $usum['wf']=$usum['wf']+$calc1['wf'];
                  $usum['wfrec']=$usum['wfrec']+$calc1['wfrec'];
                  $usum['adrec']=$usum['adrec']+$calc1['adrec'];
                  $usum['kcrec']=$usum['kcrec']+$calc1['kcrec'];
                  $usum['covrec']=$usum['covrec']+$calc1['covrec'];
                  $usum['lic']=$usum['lic']+$calc1['lic'];
                  $usum['nw']=$usum['nw']+$calc1['netwages'];
                  $usum['hw']=$usum['hw']+$calc1['hdwage'];
                  $usum['tw']=$usum['tw']+$calc1['totwages'];
                  $usum['esi']=$usum['esi']+$calc2['esi'];
                  $usum['yi']=$usum['yi']+$calc2['yi'];
                  $usum['mw']=$usum['mw']+$calc2['mw'];
                  $usum['dw']=$usum['dw']+$calc2['dawages'];
                  $usum['tmw']=$usum['tmw']+$calc2['totmw'];
                  $usum['nmw']=$usum['nmw']+$calc2['netmw'];

                  $temp5=$res7[0]['yarn_code'];
                  $yarn_array[$temp5]['noh']=$yarn_array[$temp5]['noh']+$res6[0]['noh'];
                  $yarn_array[$temp5]['val']=$yarn_array[$temp5]['val']+$calc1['val'];
                  $yarn_array[$temp5]['sliver']=$yarn_array[$temp5]['sliver']+$calc1['sliver'];
                  $yarn_array[$temp5]['sv']=$yarn_array[$temp5]['sv']+$calc1['sv'];
                  $yarn_array[$temp5]['wage']=$yarn_array[$temp5]['wage']+$calc1['wage'];

                  $excel_var[$z][$i][0]=intval($i+1);
                  $excel_var[$z][$i][1]=$res1[$i]['artisan_code'];
                  $excel_var[$z][$i][2]=$res1[$i]['artisan_name'];
                  $excel_var[$z][$i][3]=$res7[0]['type'];
                  $excel_var[$z][$i][4]=$res6[0]['noh'];
                  $excel_var[$z][$i][5]=$res7[0]['rate'];
                  $excel_var[$z][$i][6]=$calc1['val'];
                  $excel_var[$z][$i][7]=$calc1['sv'];
                  $excel_var[$z][$i][8]=$calc1['sliver'];
                  $excel_var[$z][$i][9]=$calc1['wage'];
                  $excel_var[$z][$i][10]=$calc1['wf'];
                  $excel_var[$z][$i][11]=$calc1['wfrec'];
                  $excel_var[$z][$i][12]=$calc1['adrec'];
                  $excel_var[$z][$i][13]=$calc1['kcrec'];
                  $excel_var[$z][$i][14]=$calc1['covrec'];
                  $excel_var[$z][$i][15]=$calc1['lic'];
                  $excel_var[$z][$i][16]=$calc1['netwages'];
                  $excel_var[$z][$i][17]=$calc1['hdwage'];
                  $excel_var[$z][$i][18]=$calc1['totwages'];
                  $excel_var[$z][$i][19]=$calc2['esi'];
                  $excel_var[$z][$i][20]=$calc2['yi'];
                  $excel_var[$z][$i][21]=$calc2['mw'];
                  $excel_var[$z][$i][22]=$calc2['dadays'];
                  $excel_var[$z][$i][23]=$calc2['dawages'];
                  $excel_var[$z][$i][24]=$calc2['totmw'];
                  $excel_var[$z][$i][25]=$calc2['netmw'];
                  $excel_var[$z][$i][26]=$res6[0]['attendance'];
                  $excel_var[$z][$i][27]=$res1[$i]['account_no'];
                  $excel_var[$z][$i][28]=$res1[$i]['ifsc'];
                  $excel_var[$z][$i][29]=$res1[$i]['bank_name'];
                }

                for($k=0;$k<sizeof($res14);$k++){
                  $temp4=$res14[$k]['yarn_code'];
                  if($yarn_array[$temp4]['noh']!=0){
                    echo '<tr style="text-align:center;">';
                    echo '<td colspan="2" >'.$res14[$k]['yarn_code'].'</td>';
                    echo '<td colspan="2" >'.$res14[$k]['type'].'</td>';
                    echo '<td>'.$yarn_array[$temp4]['noh'].'</td>';
                    echo '<td></td>';
                    echo '<td>'.$yarn_array[$temp4]['val'].'</td>';
                    echo '<td>'.$yarn_array[$temp4]['sv'].'</td>';
                    echo '<td>'.$yarn_array[$temp4]['sliver'].'</td>';
                    echo '<td>'.$yarn_array[$temp4]['wage'].'</td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
                
                    $variety_array[$z]=$yarn_array;
                    $spinning_array[$temp4]['noh']=$spinning_array[$temp4]['noh']+$yarn_array[$temp4]['noh'];
                    $spinning_array[$temp4]['val']=$spinning_array[$temp4]['val']+$yarn_array[$temp4]['val'];
                    $spinning_array[$temp4]['sliver']=$spinning_array[$temp4]['sliver']+$yarn_array[$temp4]['sliver'];
                    $spinning_array[$temp4]['sv']=$spinning_array[$temp4]['sv']+$yarn_array[$temp4]['sv'];
                    $spinning_array[$temp4]['wage']=$spinning_array[$temp4]['wage']+$yarn_array[$temp4]['wage'];
                  }
                }
                    
                echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="4">Unit Total</td>';
                echo "<td>".$usum['noh']."</td>";
                echo "<td></td>";
                echo "<td>".$usum['val']."</td>";
                echo "<td>".$usum['sv']."</td>";
                echo "<td>".$usum['sliver']."</td>";
                echo "<td>".$usum['wage']."</td>";
                echo "<td>".$usum['wf']."</td>";
                echo "<td>".$usum['wfrec']."</td>";
                echo "<td>".$usum['adrec']."</td>";
                echo "<td>".$usum['kcrec']."</td>";
                echo "<td>".$usum['covrec']."</td>";
                echo "<td>".$usum['lic']."</td>";
                echo "<td>".$usum['nw']."</td>";
                echo "<td>".$usum['hw']."</td>";
                echo "<td>".$usum['tw']."</td>";
                echo "<td>".$usum['esi']."</td>";
                echo "<td>".$usum['yi']."</td>";
                echo "<td>".$usum['mw']."</td>";
                echo "<td> </td>";
                echo "<td>".$usum['dw']."</td>";
                echo "<td>".$usum['tmw']."</td>";
                echo "<td>".$usum['nmw']."</td>";
                echo "<td colspan='4'> </td></tr>";

                $excel_var[$z][$i][4]=$usum['noh'];
                $excel_var[$z][$i][6]=$usum['val'];
                $excel_var[$z][$i][7]=$usum['sv'];
                $excel_var[$z][$i][8]=$usum['sliver'];
                $excel_var[$z][$i][9]=$usum['wage'];
                $excel_var[$z][$i][10]=$usum['wf'];
                $excel_var[$z][$i][11]=$usum['wfrec'];
                $excel_var[$z][$i][12]=$usum['adrec'];
                $excel_var[$z][$i][13]=$usum['kcrec'];
                $excel_var[$z][$i][14]=$usum['covrec'];
                $excel_var[$z][$i][15]=$usum['lic'];
                $excel_var[$z][$i][16]=$usum['nw'];
                $excel_var[$z][$i][17]=$usum['hw'];
                $excel_var[$z][$i][18]=$usum['tw'];
                $excel_var[$z][$i][19]=$usum['esi'];
                $excel_var[$z][$i][20]=$usum['yi'];
                $excel_var[$z][$i][21]=$usum['mw'];
                $excel_var[$z][$i][23]=$usum['dw'];
                $excel_var[$z][$i][24]=$usum['tmw'];
                $excel_var[$z][$i][25]=$usum['nmw'];

                echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="4">Grand Total</td>';
                echo "<td>".$usum['noh']."</td>";
                echo "<td> </td>";
                echo "<td>".$usum['val']."</td>";
                echo "<td>".$usum['sv']."</td>";
                echo "<td>".$usum['sliver']."</td>";
                echo "<td>".$usum['wage']."</td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td> </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";

                $dsum['no']=$dsum['no']+$usum['noh'];
                $dsum['val']=$dsum['val']+$usum['val'];
                $dsum['sv']=$dsum['sv']+$usum['sv'];
                $dsum['sliver']=$dsum['sliver']+$usum['sliver'];
                $dsum['wage']=$dsum['wage']+$usum['wage'];
                $dsum['wf']=$dsum['wf']+$usum['wf'];
                $dsum['wfrec']=$dsum['wfrec']+$usum['wfrec'];
                $dsum['adrec']=$dsum['adrec']+$usum['adrec'];
                $dsum['kcrec']=$dsum['kcrec']+$usum['kcrec'];
                $dsum['covrec']=$dsum['covrec']+$usum['covrec'];
                $dsum['lic']=$dsum['lic']+$usum['lic'];
                $dsum['nw']=$dsum['nw']+$usum['nw'];
                $dsum['hw']=$dsum['hw']+$usum['hw'];
                $dsum['tw']=$dsum['tw']+$usum['tw'];
                $dsum['esi']=$dsum['esi']+$usum['esi'];
                $dsum['yi']=$dsum['yi']+$usum['yi'];
                $dsum['mw']=$dsum['mw']+$usum['mw'];
                $dsum['dw']=$dsum['dw']+$usum['dw'];
                $dsum['tmw']=$dsum['tmw']+$usum['tmw'];
                $dsum['nmw']=$dsum['nmw']+$usum['nmw']; 

                for($k=0;$k<sizeof($res14);$k++){
                  $temp4=$res14[$k]['yarn_code'];
                  if($spinning_array[$temp4]['noh']!=0){
                    echo '<tr style="text-align:center;">';
                    echo '<td colspan="2" >'.$res14[$k]['yarn_code'].'</td>';
                    echo '<td colspan="2" >'.$res14[$k]['type'].'</td>';
                    echo '<td>'.$spinning_array[$temp4]['noh'].'</td>';
                    echo '<td></td>';
                    echo '<td>'.$spinning_array[$temp4]['val'].'</td>';
                    echo '<td>'.$spinning_array[$temp4]['sv'].'</td>';
                    echo '<td>'.$spinning_array[$temp4]['sliver'].'</td>';
                    echo '<td>'.$spinning_array[$temp4]['wage'].'</td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
                  }
                }
              }
            }

                echo '<tr style="background-color: #808080; color:white;text-align: center;"><td colspan="4">District Total</td>';
                echo "<td>".$dsum['no']."</td>";
                echo "<td></td>";
                echo "<td>".$dsum['val']."</td>";
                echo "<td>".$dsum['sv']."</td>";
                echo "<td>".$dsum['sliver']."</td>";
                echo "<td>".$dsum['wage']."</td>";
                echo "<td>".$dsum['wf']."</td>";
                echo "<td>".$dsum['wfrec']."</td>";
                echo "<td>".$dsum['adrec']."</td>";
                echo "<td>".$dsum['kcrec']."</td>";
                echo "<td>".$dsum['covrec']."</td>";
                echo "<td>".$dsum['lic']."</td>";
                echo "<td>".$dsum['nw']."</td>";
                echo "<td>".$dsum['hw']."</td>";
                echo "<td>".$dsum['tw']."</td>";
                echo "<td>".$dsum['esi']."</td>";
                echo "<td>".$dsum['yi']."</td>";
                echo "<td>".$dsum['mw']."</td>";
                echo "<td> </td>";
                echo "<td>".$dsum['dw']."</td>";
                echo "<td>".$dsum['tmw']."</td>";
                echo "<td>".$dsum['nmw']."</td>";
                echo "<td colspan='4'> </td></tr>";
            
                $_SESSION["spinning_excel_data"]=$excel_var;
                $_SESSION["spinning_variety_data"]=$variety_array;
                $_SESSION["district_total_spinning_variety"]=$spinning_array;
                $_SESSION["district_total_spinning"]=$dsum;
                $_SESSION["unitname"]=$dunitname;
              ?>

            </tbody>
            <tfoot></tfoot>
          </table> 

          <?php
            }
            if($res4[0]['type']=="Weaving"){
              $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
              for($z=0;$z<sizeof($res5);$z++){
                $excel_var[$z][0][0]=0;

                $usum = array(
                  'no'       => 0,
                  'meter'    => 0,
                  'sqm'      => 0, 
                  'wage'     => 0,
                  'wf'       => 0,
                  'wfrec'    => 0,
                  'adrec'    => 0,
                  'kcrec'    => 0,
                  'covrec'   => 0,
                  'lic'      => 0,
                  'nw'       => 0,
                  'hw'       => 0,
                  'tw'       => 0,
                  'esi'      => 0,
                  'yi'       => 0,
                  'wyi'      => 0,
                  'mw'       => 0,
                  'dw'       => 0,
                  'tmw'      => 0,
                  'nmw'      => 0,
                  'val'      => 0,
                  'pr'       => 0,
                );

                $yarnsum=array(
                  'warp_grey'    => 0,
                  'warp_colour'  => 0,
                  'warp_black'   => 0,
                  'warp_bl'      => 0,
                  'warp_tot'     => 0,
                  'weft_grey'    => 0,
                  'weft_colour'  => 0,
                  'weft_black'   => 0,
                  'weft_bl'      => 0,
                  'weft_tot'     => 0,
                );

                $unitdemo=$res5[$z]['unit_name'];
                $unit=$res5[$z]['unit_code'];
                $dunitname[$z]=$unitdemo;

                $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name,account_no,ifsc,bank_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_weaving where s_date='$hdate')",$con);
                
                if(sizeof($res1)>0){
                  echo '<table class="table-bordered" id="customers">
                    <thead>
                      <tr>
                        <th scope="col" colspan="43" style="background-color: #038857;">'.strtoupper($unitdemo).'</th>
                      </tr>
                      <tr>
                        <th colspan="29"></th>
                        <th colspan="5" style="text-align:center">Warp</th>
                        <th colspan="5" style="text-align:center">Weft</th>
                        <th colspan="4"></th>
                      </tr>
                      <tr>
                        <th scope="col">SI No.</th>
                        <th scope="col">Artisan Code</th>
                        <th scope="col">Artisan Name</th>
                        <th scope="col">Variety</th>
                        <th scope="col">No. of Pieces</th>
                        <th scope="col">Metre</th>
                        <th scope="col">Square Metre</th>
                        <th scope="col">Weaving Type</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Prime Cost</th>
                        <th scope="col">Value</th>
                        <th scope="col">Wages</th>
                        <th scope="col">W/F Fund</th>
                        <th scope="col">w/f Recovery</th>
                        <th scope="col">Adv Recovery</th>
                        <th scope="col">Khadi Cr. Rec.</th>
                        <th scope="col">Covid adv.</th>
                        <th scope="col">LIC</th>
                        <th scope="col">Net wages</th>
                        <th scope="col">Holiday wages</th>
                        <th scope="col">Total Wages</th>
                        <th scope="col">ESI Contr</th>
                        <th scope="col">Warp Yarn Incentive</th>
                        <th scope="col">Weft Yarn Incentive</th>
                        <th scope="col">Minimum Wages</th>
                        <th scope="col">DA Days</th>
                        <th scope="col">DA Wages</th>
                        <th scope="col">Total Minimum Wages</th>
                        <th scope="col">Net Minimum Wages</th>
                        <th scope="col">Grey</th>
                        <th scope="col">Colour</th>
                        <th scope="col">Black</th>
                        <th scope="col">BL</th>
                        <th scope="col">Total</th>
                        <th scope="col">Grey</th>
                        <th scope="col">Colour</th>
                        <th scope="col">Black</th>
                        <th scope="col">BL</th>
                        <th scope="col">Total</th>
                        <th scope="col">Attendance</th>
                        <th scope="col">Account Number</th> 
                        <th scope="col">IFSC</th> 
                        <th scope="col">Bank Name</th> 
                      </tr>
                    </thead>
                    <tbody>';

                    $res14=retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code",$con);
                    $yarn_array=array(array());

                    for($k=0;$k<sizeof($res14);$k++){
                      $temp4=$res14[$k]['weaving_code'];
                      $yarn_array[$temp4]['mtr']= 0;
                      $yarn_array[$temp4]['sqmtr']= 0;
                      $yarn_array[$temp4]['val']= 0;
                      $yarn_array[$temp4]['pc']= 0;
                      $yarn_array[$temp4]['warp_grey']= 0;
                      $yarn_array[$temp4]['warp_colour']= 0;
                      $yarn_array[$temp4]['warp_black']= 0;
                      $yarn_array[$temp4]['warp_bl']= 0;
                      $yarn_array[$temp4]['warp_tot']= 0;
                      $yarn_array[$temp4]['weft_grey']= 0;
                      $yarn_array[$temp4]['weft_colour']= 0;
                      $yarn_array[$temp4]['weft_black']= 0;
                      $yarn_array[$temp4]['weft_bl']= 0;
                      $yarn_array[$temp4]['weft_tot']= 0;  
                    }

                    for($i=0;$i<sizeof($res1);$i++){
                      $temp1=strval($res1[$i]['artisan_id']);
                      $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1",$con);
                      $temp2 = $res9[0]['weaving_id'];
                      $temp3 = $res9[0]['w_id'];
                      $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3",$con);
                      $res11 = retrieveData("SELECT weaving_code,type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2",$con);
                      $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
                      $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2",$con);

                      $war3 =[
                        'warp_grey'    => $res13[0]['warp_grey'],
                        'warp_colour'  => $res13[0]['warp_colour'],
                        'warp_black'   => $res13[0]['warp_black'],
                        'warp_bl'      => $res13[0]['warp_bl'],
                        'weft_grey'    => $res13[0]['weft_grey'],
                        'weft_colour'  => $res13[0]['weft_colour'],
                        'weft_black'   => $res13[0]['weft_black'],
                        'weft_bl'      => $res13[0]['weft_bl'],
                      ];

                      $calc5 = weavingYICalculation($war3);

                      $wfund=0;
                      if($res12[0]['wf_yes_no']==1){
                        $wfund=$res11[0]['wf'];
                      }

                      $war1 = [
                        'metre'  => $res9[0]['mtr'],
                        'wage'   => $res10[0]['w_wages'],
                        'wf'     => $wfund,
                        'wfrec'  => $res12[0]['wf_rec'],
                        'adrec'  => $res12[0]['ad_rec'],
                        'kcrec'  => $res12[0]['credit_rec'],
                        'covrec' => $res12[0]['cov_rec'],
                        'lic'    => $res12[0]['lic'],
                        'hno'    => $res3[0]['holiday_no'],
                        'wno'    => $res3[0]['total_wd'],
                        'sqmtr'  => $res11[0]['width'],
                        'prime'  => $res11[0]['prime_cost'],
                        'value'  => $res11[0]['rate'],
                      ];

                      $calc3=weavingWageCalculation($war1);

                      $war2 = [
                        'metre'   => $res9[0]['mtr'],
                        'esi'     => $res11[0]['esi'],
                        'warpyi'  => $res10[0]['warp_yi'],
                        'weftyi'  => $res10[0]['weft_yi'],
                        'mw'      => $res10[0]['min_wages'],
                        'target'  => $res10[0]['target'],
                        'dawages' => $res3[0]['da_wages'],
                        'wage'    => $calc3['wage'],
                        'warp_tot'=> $calc5['warp_tot'],
                        'weft_tot'=> $calc5['weft_tot'],
                        
                      ];

                      $da=$res3[0]['total_wd'];
                      $calc4=weavingIncentivesCalculation($war2,$da);
                      $calc6=yarnConsumptionCalculation($war3,$war1['metre']);

                      echo '<tr><td>'.strval($i+1).'</td>';
                      echo '<td>'.$res1[$i]['artisan_code'].'</td>';
                      echo '<td>'.$res1[$i]['artisan_name'].'</td>';
                      echo '<td>'.$res11[0]['type'].'</td>';
                      echo '<td>'.$res9[0]['nop'].'</td>';
                      echo '<td>'.$res9[0]['mtr'].'</td>';
                      echo '<td>'.$calc3['sqmtr'].'</td>';
                      echo '<td>'.$res10[0]['w_type'].'</td>';
                      echo '<td>'.$res10[0]['w_wages'].'</td>';
                      echo '<td>'.$calc3['prime'].'</td>';
                      echo '<td>'.$calc3['value'].'</td>';
                      echo '<td>'.$calc3['wage'].'</td>';
                      echo '<td>'.$calc3['wf'].'</td>';
                      echo '<td>'.$calc3['wfrec'].'</td>';
                      echo '<td>'.$calc3['adrec'].'</td>';
                      echo '<td>'.$calc3['kcrec'].'</td>';
                      echo '<td>'.$calc3['covrec'].'</td>';
                      echo '<td>'.$calc3['lic'].'</td>';
                      echo '<td>'.$calc3['netwages'].'</td>';
                      echo '<td>'.$calc3['hdwage'].'</td>';
                      echo '<td>'.$calc3['totwages'].'</td>';
                      echo '<td>'.$calc4['esi'].'</td>';
                      echo '<td>'.$calc4['warpyi'].'</td>';
                      echo '<td>'.$calc4['weftyi'].'</td>';
                      echo '<td>'.$calc4['mw'].'</td>';
                      echo '<td>'.$calc4['dadays'].'</td>';
                      echo '<td>'.$calc4['dawages'].'</td>';
                      echo '<td>'.$calc4['totmw'].'</td>';
                      echo '<td>'.$calc4['netmw'].'</td>';

                      echo '<td>'.$calc6['warp_grey'].'</td>';
                      echo '<td>'.$calc6['warp_colour'].'</td>';
                      echo '<td>'.$calc6['warp_black'].'</td>';
                      echo '<td>'.$calc6['warp_bl'].'</td>';
                      echo '<td>'.$calc6['warp_tot'].'</td>';
                      echo '<td>'.$calc6['weft_grey'].'</td>';
                      echo '<td>'.$calc6['weft_colour'].'</td>';
                      echo '<td>'.$calc6['weft_black'].'</td>';
                      echo '<td>'.$calc6['weft_bl'].'</td>';
                      echo '<td>'.$calc6['weft_tot'].'</td>';

                      echo '<td>'.$res9[0]['attendance'].'</td>';
                      echo '<td>'.$res1[$i]['account_no'].'</td>';
                      echo '<td>'.$res1[$i]['ifsc'].'</td>';
                      echo '<td>'.$res1[$i]['bank_name'].'</td>';

                      $excel_var[$z][$i][0] = strval($i+1);
                      $excel_var[$z][$i][1] = $res1[$i]['artisan_code'];
                      $excel_var[$z][$i][2] = $res1[$i]['artisan_name'];
                      $excel_var[$z][$i][3] = $res11[0]['type'];
                      $excel_var[$z][$i][4] = $res9[0]['nop'];
                      $excel_var[$z][$i][5] = $res9[0]['mtr'];
                      $excel_var[$z][$i][6] = $calc3['sqmtr'];
                      $excel_var[$z][$i][7] = $res10[0]['w_type'];
                      $excel_var[$z][$i][8] = $res10[0]['w_wages'];
                      $excel_var[$z][$i][9] = $calc3['prime'];
                      $excel_var[$z][$i][10] = $calc3['value'];
                      $excel_var[$z][$i][11] = $calc3['wage'];
                      $excel_var[$z][$i][12] = $calc3['wf'];
                      $excel_var[$z][$i][13] = $calc3['wfrec'];
                      $excel_var[$z][$i][14] = $calc3['adrec'];
                      $excel_var[$z][$i][15] = $calc3['kcrec'];
                      $excel_var[$z][$i][16] = $calc3['covrec'];
                      $excel_var[$z][$i][17] = $calc3['lic'];
                      $excel_var[$z][$i][18] = $calc3['netwages'];
                      $excel_var[$z][$i][19] = $calc3['hdwage'];
                      $excel_var[$z][$i][20] = $calc3['totwages'];
                      $excel_var[$z][$i][21] = $calc4['esi'];
                      $excel_var[$z][$i][22] = $calc4['warpyi'];
                      $excel_var[$z][$i][23] = $calc4['weftyi'];
                      $excel_var[$z][$i][24] = $calc4['mw'];
                      $excel_var[$z][$i][25] = $calc4['dadays'];
                      $excel_var[$z][$i][26] = $calc4['dawages'];
                      $excel_var[$z][$i][27] = $calc4['totmw'];
                      $excel_var[$z][$i][28] = $calc4['netmw'];

                      $excel_var[$z][$i][29] = $calc6['warp_grey'];
                      $excel_var[$z][$i][30] = $calc6['warp_colour'];
                      $excel_var[$z][$i][31] = $calc6['warp_black'];
                      $excel_var[$z][$i][32] = $calc6['warp_bl'];
                      $excel_var[$z][$i][33] = $calc6['warp_tot'];
                      $excel_var[$z][$i][34] = $calc6['weft_grey'];
                      $excel_var[$z][$i][35] = $calc6['weft_colour'];
                      $excel_var[$z][$i][36] = $calc6['weft_black'];
                      $excel_var[$z][$i][37] = $calc6['weft_bl'];
                      $excel_var[$z][$i][38] = $calc6['weft_tot'];

                      $excel_var[$z][$i][39] = $res9[0]['attendance'];
                      $excel_var[$z][$i][40] = $res1[$i]['account_no'];
                      $excel_var[$z][$i][41] = $res1[$i]['ifsc'];
                      $excel_var[$z][$i][42] = $res1[$i]['bank_name'];

                      $usum['no']=$usum['no']+$res9[0]['nop'];
                      $usum['meter']=$usum['meter']+$res9[0]['mtr'];
                      $usum['sqm']=$usum['sqm']+$calc3['sqmtr'];
                      $usum['wage']=$usum['wage']+$calc3['wage'];
                      $usum['val']=$usum['val']+$calc3['value'];
                      $usum['pr']=$usum['pr']+$calc3['prime'];
                      $usum['wf']=$usum['wf']+$calc3['wf'];
                      $usum['wfrec']=$usum['wfrec']+$calc3['wfrec'];
                      $usum['adrec']=$usum['adrec']+$calc3['adrec'];         
                      $usum['kcrec']=$usum['kcrec']+$calc3['kcrec'];
                      $usum['covrec']=$usum['covrec']+$calc3['covrec'];
                      $usum['lic']=$usum['lic']+$calc3['lic'];
                      $usum['nw']=$usum['nw']+$calc3['netwages'];
                      $usum['hw']=$usum['hw']+$calc3['hdwage'];
                      $usum['tw']=$usum['tw']+$calc3['totwages'];
                      $usum['esi']=$usum['esi']+$calc4['esi'];
                      $usum['yi']=$usum['yi']+$calc4['warpyi'];
                      $usum['wyi']=$usum['wyi']+$calc4['weftyi'];
                      $usum['mw']=$usum['mw']+$calc4['mw'];
                      $usum['dw']=$usum['dw']+$calc4['dawages'];
                      $usum['tmw']=$usum['tmw']+$calc4['totmw'];
                      $usum['nmw']=$usum['nmw']+$calc4['netmw'];
                  
                      $yarnsum['warp_grey']=$yarnsum['warp_grey']+$calc6['warp_grey'];
                      $yarnsum['warp_colour']=$yarnsum['warp_colour']+$calc6['warp_colour'];
                      $yarnsum['warp_black']=$yarnsum['warp_black']+$calc6['warp_black'];
                      $yarnsum['warp_bl']=$yarnsum['warp_bl']+$calc6['warp_bl'];
                      $yarnsum['warp_tot']=$yarnsum['warp_tot']+$calc6['warp_tot'];
                      $yarnsum['weft_grey']=$yarnsum['weft_grey']+$calc6['weft_grey'];
                      $yarnsum['weft_colour']=$yarnsum['weft_colour']+$calc6['weft_colour'];
                      $yarnsum['weft_black']=$yarnsum['weft_black']+$calc6['weft_black'];
                      $yarnsum['weft_bl']=$yarnsum['weft_bl']+$calc6['weft_bl'];
                      $yarnsum['weft_tot']=$yarnsum['weft_tot']+$calc6['weft_tot'];


                      $temp5=$res11[0]['weaving_code'];
                      $yarn_array[$temp5]['mtr']=$yarn_array[$temp5]['mtr']+$res9[0]['mtr'];
                      $yarn_array[$temp5]['sqmtr']=$yarn_array[$temp5]['sqmtr']+$calc3['sqmtr'];
                      $yarn_array[$temp5]['val']=$yarn_array[$temp5]['val']+$calc3['value'];
                      $yarn_array[$temp5]['pc']=$yarn_array[$temp5]['pc']+$calc3['prime'];
                      $yarn_array[$temp5]['warp_grey']=$yarn_array[$temp5]['warp_grey']+$calc6['warp_grey'];
                      $yarn_array[$temp5]['warp_colour']=$yarn_array[$temp5]['warp_colour']+$calc6['warp_colour'];
                      $yarn_array[$temp5]['warp_black']=$yarn_array[$temp5]['warp_black']+$calc6['warp_black'];
                      $yarn_array[$temp5]['warp_bl']=$yarn_array[$temp5]['warp_bl']+$calc6['warp_bl'];
                      $yarn_array[$temp5]['warp_tot']=$yarn_array[$temp5]['warp_tot']+$calc6['warp_tot'];
                      $yarn_array[$temp5]['weft_grey']=$yarn_array[$temp5]['weft_grey']+$calc6['weft_grey'];
                      $yarn_array[$temp5]['weft_colour']=$yarn_array[$temp5]['weft_colour']+$calc6['weft_colour'];
                      $yarn_array[$temp5]['weft_black']=$yarn_array[$temp5]['weft_black']+$calc6['weft_black'];
                      $yarn_array[$temp5]['weft_bl']=$yarn_array[$temp5]['weft_bl']+$calc6['weft_bl'];
                      $yarn_array[$temp5]['weft_tot']=$yarn_array[$temp5]['weft_tot']+$calc6['weft_tot'];
                    }

                    
                    echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
                    echo "<td>".$usum['no']."</td>";
                    echo "<td>".$usum['meter']."</td>";
                    echo "<td>".$usum['sqm']."</td>";
                    echo '<td colspan="2"> </td>';
                    echo "<td>".$usum['pr']."</td>";
                    echo "<td>".$usum['val']."</td>";
                    echo "<td>".$usum['wage']."</td>";
                    echo "<td>".$usum['wf']."</td>";
                    echo "<td>".$usum['wfrec']."</td>";
                    echo "<td>".$usum['adrec']."</td>";
                    echo "<td>".$usum['kcrec']."</td>";
                    echo "<td>".$usum['covrec']."</td>";
                    echo "<td>".$usum['lic']."</td>";
                    echo "<td>".$usum['nw']."</td>";
                    echo "<td>".$usum['hw']."</td>";
                    echo "<td>".$usum['tw']."</td>";
                    echo "<td>".$usum['esi']."</td>";
                    echo "<td>".$usum['yi']."</td>";
                    echo "<td>".$usum['wyi']."</td>";
                    echo "<td>".$usum['mw']."</td>";
                    echo "<td> </td>";
                    echo "<td>".$usum['dw']."</td>";
                    echo "<td>".$usum['tmw']."</td>";
                    echo "<td>".$usum['nmw']."</td>";

                    echo "<td>".$yarnsum['warp_grey']."</td>";
                    echo "<td>".$yarnsum['warp_colour']."</td>";
                    echo "<td>".$yarnsum['warp_black']."</td>";
                    echo "<td>".$yarnsum['warp_bl']."</td>";
                    echo "<td>".$yarnsum['warp_tot']."</td>";
                    echo "<td>".$yarnsum['weft_grey']."</td>";
                    echo "<td>".$yarnsum['weft_colour']."</td>";
                    echo "<td>".$yarnsum['weft_black']."</td>";
                    echo "<td>".$yarnsum['weft_bl']."</td>";
                    echo "<td>".$yarnsum['weft_tot']."</td>";
                    echo "<td> </td><td> </td><td> </td><td> </td></tr>";

                    $excel_var[$z][$i][4] = $usum['no'];
                    $excel_var[$z][$i][5] = $usum['meter'];
                    $excel_var[$z][$i][6] = $usum['sqm'];

                    $excel_var[$z][$i][9] = $usum['pr'];
                    $excel_var[$z][$i][10] = $usum['val'];
                    $excel_var[$z][$i][11] = $usum['wage'];
                    $excel_var[$z][$i][12] = $usum['wf'];
                    $excel_var[$z][$i][13] = $usum['wfrec'];
                    $excel_var[$z][$i][14] = $usum['adrec'];
                    $excel_var[$z][$i][15] = $usum['kcrec'];
                    $excel_var[$z][$i][16] = $usum['covrec'];
                    $excel_var[$z][$i][17] = $usum['lic'];
                    $excel_var[$z][$i][18] = $usum['nw'];
                    $excel_var[$z][$i][19] = $usum['hw'];
                    $excel_var[$z][$i][20] = $usum['tw'];
                    $excel_var[$z][$i][21] = $usum['esi'];
                    $excel_var[$z][$i][22] = $usum['yi'];
                    $excel_var[$z][$i][23] = $usum['wyi'];
                    $excel_var[$z][$i][24] = $usum['mw'];
                  
                    $excel_var[$z][$i][26] = $usum['dw'];
                    $excel_var[$z][$i][27] = $usum['tmw'];
                    $excel_var[$z][$i][28] = $usum['nmw'];
                    $excel_var[$z][$i][29] = $yarnsum['warp_grey'];
                    $excel_var[$z][$i][30] = $yarnsum['warp_colour'];
                    $excel_var[$z][$i][31] = $yarnsum['warp_black'];
                    $excel_var[$z][$i][32] = $yarnsum['warp_bl'];
                    $excel_var[$z][$i][33] = $yarnsum['warp_tot'];
                    $excel_var[$z][$i][34] = $yarnsum['weft_grey'];
                    $excel_var[$z][$i][35] = $yarnsum['weft_colour'];
                    $excel_var[$z][$i][36] = $yarnsum['weft_black'];
                    $excel_var[$z][$i][37] = $yarnsum['weft_bl'];
                    $excel_var[$z][$i][38] = $yarnsum['weft_tot'];

                    for($k=0;$k<sizeof($res14);$k++){
                      $temp4=$res14[$k]['weaving_code'];
                      if($yarn_array[$temp4]['mtr']!=0){
                        echo '<tr style="text-align:center;">';
                        echo '<td colspan="2" >'.$res14[$k]['weaving_code'].'</td>';
                        echo '<td colspan="3" >'.$res14[$k]['type'].'</td>';
                        echo '<td>'.$yarn_array[$temp4]['mtr'].'</td>';
                        echo '<td>'.$yarn_array[$temp4]['sqmtr'].'</td>';
                        echo '<td colspan="2"></td>';
                        echo '<td>'.$yarn_array[$temp4]['pc'].'</td>';
                        echo '<td>'.$yarn_array[$temp4]['val'].'</td>'; 
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>'; 
                        echo '<td></td>';
                        echo '<td>'.$yarn_array[$temp4]['warp_grey'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['warp_colour'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['warp_black'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['warp_bl'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['warp_tot'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['weft_grey'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['weft_colour'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['weft_black'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['weft_bl'].'</td>'; 
                        echo '<td>'.$yarn_array[$temp4]['weft_tot'].'</td>';  
                        echo '<td></td><td> </td><td> </td><td> </td></tr>';

                        $variety_array[$z]=$yarn_array;

                        $temp5=$res14[$k]['weaving_code'];
                        $dyarn_array[$temp5]['mtr']=$dyarn_array[$temp5]['mtr']+$yarn_array[$temp5]['mtr'];
                        $dyarn_array[$temp5]['sqmtr']=$dyarn_array[$temp5]['sqmtr']+$yarn_array[$temp5]['sqmtr'];
                        $dyarn_array[$temp5]['val']=$dyarn_array[$temp5]['val']+$yarn_array[$temp5]['val'];
                        $dyarn_array[$temp5]['pc']=$dyarn_array[$temp5]['pc']+$yarn_array[$temp5]['pc'];
                        $dyarn_array[$temp5]['warp_grey']=$dyarn_array[$temp5]['warp_grey']+$yarn_array[$temp5]['warp_grey'];
                        $dyarn_array[$temp5]['warp_colour']=$dyarn_array[$temp5]['warp_colour']+$yarn_array[$temp5]['warp_colour'];
                        $dyarn_array[$temp5]['warp_black']=$dyarn_array[$temp5]['warp_black']+$yarn_array[$temp5]['warp_black'];
                        $dyarn_array[$temp5]['warp_bl']=$dyarn_array[$temp5]['warp_bl']+$yarn_array[$temp5]['warp_bl'];
                        $dyarn_array[$temp5]['warp_tot']=$dyarn_array[$temp5]['warp_tot']+$yarn_array[$temp5]['warp_tot'];
                        $dyarn_array[$temp5]['weft_grey']=$dyarn_array[$temp5]['weft_grey']+$yarn_array[$temp5]['weft_grey'];
                        $dyarn_array[$temp5]['weft_colour']=$dyarn_array[$temp5]['weft_colour']+$yarn_array[$temp5]['weft_colour'];
                        $dyarn_array[$temp5]['weft_black']=$dyarn_array[$temp5]['weft_black']+$yarn_array[$temp5]['weft_black'];
                        $dyarn_array[$temp5]['weft_bl']=$dyarn_array[$temp5]['weft_bl']+$yarn_array[$temp5]['weft_bl'];
                        $dyarn_array[$temp5]['weft_tot']=$dyarn_array[$temp5]['weft_tot']+$yarn_array[$temp5]['weft_tot'];
                      }
                    }

                    // echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit_Total</td>';
                    // echo "<td></td>";
                    // echo "<td>".$usum['meter']."</td>";
                    // echo "<td>".$usum['sqm']."</td>";
                    // echo '<td colspan="2"> </td>';
                    // echo "<td>".$usum['pr']."</td>";
                    // echo "<td>".$usum['val']."</td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td> </td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td>".$yarnsum['warp_grey']."</td>";
                    // echo "<td>".$yarnsum['warp_colour']."</td>";
                    // echo "<td>".$yarnsum['warp_black']."</td>";
                    // echo "<td>".$yarnsum['warp_bl']."</td>";
                    // echo "<td>".$yarnsum['warp_tot']."</td>";
                    // echo "<td>".$yarnsum['weft_grey']."</td>";
                    // echo "<td>".$yarnsum['weft_colour']."</td>";
                    // echo "<td>".$yarnsum['weft_black']."</td>";
                    // echo "<td>".$yarnsum['weft_bl']."</td>";
                    // echo "<td>".$yarnsum['weft_tot']."</td>";
                    // echo "<td> </td><td> </td><td> </td><td> </td></tr>";

                    $dsum['no']=$dsum['no']+$usum['no'];
                    $dsum['meter']=$dsum['meter']+$usum['meter'];
                    $dsum['sqm']=$dsum['sqm']+$usum['sqm'];
                    $dsum['val']=$dsum['val']+$usum['val'];
                    $dsum['pr']=$dsum['pr']+$usum['pr'];
                    
                    $dsum['wage']=$dsum['wage']+$usum['wage'];
                    $dsum['wf']=$dsum['wf']+$usum['wf'];
                    $dsum['wfrec']=$dsum['wfrec']+$usum['wfrec'];
                    $dsum['adrec']=$dsum['adrec']+$usum['adrec'];
                    $dsum['kcrec']=$dsum['kcrec']+$usum['kcrec'];
                    $dsum['covrec']=$dsum['covrec']+$usum['covrec'];
                    $dsum['lic']=$dsum['lic']+$usum['lic'];
                    $dsum['nw']=$dsum['nw']+$usum['nw'];
                    $dsum['hw']=$dsum['hw']+$usum['hw'];
                    $dsum['tw']=$dsum['tw']+$usum['tw'];
                    $dsum['esi']=$dsum['esi']+$usum['esi'];
                    $dsum['yi']=$dsum['yi']+$usum['yi'];
                    $dsum['wyi']=$dsum['wyi']+$usum['wyi'];
                    $dsum['mw']=$dsum['mw']+$usum['mw'];
                    $dsum['dw']=$dsum['dw']+$usum['dw'];
                    $dsum['tmw']=$dsum['tmw']+$usum['tmw'];
                    $dsum['nmw']=$dsum['nmw']+$usum['nmw'];

                    $dyarnsum['warp_grey']=$dyarnsum['warp_grey']+$yarnsum['warp_grey'];
                    $dyarnsum['warp_colour']=$dyarnsum['warp_colour']+$yarnsum['warp_colour'];
                    $dyarnsum['warp_black']=$dyarnsum['warp_black']+$yarnsum['warp_black'];
                    $dyarnsum['warp_bl']=$dyarnsum['warp_bl']+$yarnsum['warp_bl'];
                    $dyarnsum['warp_tot']=$dyarnsum['warp_tot']+$yarnsum['warp_tot'];
                    $dyarnsum['weft_grey']=$dyarnsum['weft_grey']+$yarnsum['weft_grey'];
                    $dyarnsum['weft_colour']=$dyarnsum['weft_colour']+$yarnsum['weft_colour'];
                    $dyarnsum['weft_black']=$dyarnsum['weft_black']+$yarnsum['weft_black'];
                    $dyarnsum['weft_bl']=$dyarnsum['weft_bl']+$yarnsum['weft_bl'];
                    $dyarnsum['weft_tot']=$dyarnsum['weft_tot']+$yarnsum['weft_tot'];
                 
                  }
                }
                     
                echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="4">District Total</td>';
                echo "<td>".$dsum['no']."</td>";
                echo "<td>".$dsum['meter']."</td>";
                echo "<td>".$dsum['sqm']."</td>";
                echo '<td colspan="2"> </td>';
                echo "<td>".$dsum['pr']."</td>";
                echo "<td>".$dsum['val']."</td>";
                echo "<td>".$dsum['wage']."</td>";
                echo "<td>".$dsum['wf']."</td>";
                echo "<td>".$dsum['wfrec']."</td>";
                echo "<td>".$dsum['adrec']."</td>";
                echo "<td>".$dsum['kcrec']."</td>";
                echo "<td>".$dsum['covrec']."</td>";
                echo "<td>".$dsum['lic']."</td>";
                echo "<td>".$dsum['nw']."</td>";
                echo "<td>".$dsum['hw']."</td>";
                echo "<td>".$dsum['tw']."</td>";
                echo "<td>".$dsum['esi']."</td>";
                echo "<td>".$dsum['yi']."</td>";
                echo "<td>".$dsum['wyi']."</td>";
                echo "<td>".$dsum['mw']."</td>";
                echo "<td> </td>";
                echo "<td>".$dsum['dw']."</td>";
                echo "<td>".$dsum['tmw']."</td>";
                echo "<td>".$dsum['nmw']."</td>";
                echo "<td>".$dyarnsum['warp_grey']."</td>";
                echo "<td>".$dyarnsum['warp_colour']."</td>";
                echo "<td>".$dyarnsum['warp_black']."</td>";
                echo "<td>".$dyarnsum['warp_bl']."</td>";
                echo "<td>".$dyarnsum['warp_tot']."</td>";
                echo "<td>".$dyarnsum['weft_grey']."</td>";
                echo "<td>".$dyarnsum['weft_colour']."</td>";
                echo "<td>".$dyarnsum['weft_black']."</td>";
                echo "<td>".$dyarnsum['weft_bl']."</td>";
                echo "<td>".$dyarnsum['weft_tot']."</td>";
                echo "<td> </td><td> </td><td> </td><td> </td></tr>";

                $wdsum[4]=$dsum['no'];
                $wdsum[5]=$dsum['meter'];
                $wdsum[6]=$dsum['sqm'];
                $wdsum[9]=$dsum['pr'];
                $wdsum[10]=$dsum['val'];
                $wdsum[11]=$dsum['wage'];
                $wdsum[12]=$dsum['wf'];
                $wdsum[13]=$dsum['wfrec'];
                $wdsum[14]=$dsum['adrec'];
                $wdsum[15]=$dsum['kcrec'];
                $wdsum[16]=$dsum['covrec'];
                $wdsum[17]=$dsum['lic'];
                $wdsum[18]=$dsum['nw'];
                $wdsum[19]=$dsum['hw'];
                $wdsum[20]=$dsum['tw'];
                $wdsum[21]=$dsum['esi'];
                $wdsum[22]=$dsum['yi'];
                $wdsum[23]=$dsum['wyi'];
                $wdsum[24]=$dsum['mw'];
                $wdsum[26]=$dsum['dw'];
                $wdsum[27]=$dsum['tmw'];
                $wdsum[28]=$dsum['nmw'];
                $wdsum[29]=$dyarnsum['warp_grey'];
                $wdsum[30]=$dyarnsum['warp_colour'];
                $wdsum[31]=$dyarnsum['warp_black'];
                $wdsum[32]=$dyarnsum['warp_bl'];
                $wdsum[33]=$dyarnsum['warp_tot'];
                $wdsum[34]=$dyarnsum['weft_grey'];
                $wdsum[35]=$dyarnsum['weft_colour'];
                $wdsum[36]=$dyarnsum['weft_black'];
                $wdsum[37]=$dyarnsum['weft_bl'];
                $wdsum[38]=$dyarnsum['weft_tot'];

                // for($k=0;$k<sizeof($res14);$k++){
                //   $temp4=$res15[$k]['weaving_code'];
                //   if($dyarn_array[$temp4]['mtr']!=0){
                //     echo '<tr style="text-align:center;">';
                //     echo '<td colspan="2" >'.$res15[$k]['weaving_code'].'</td>';
                //     echo '<td colspan="3" >'.$res15[$k]['type'].'</td>';
                //     echo '<td>'.$dyarn_array[$temp4]['mtr'].'</td>';
                //     echo '<td>'.$dyarn_array[$temp4]['sqmtr'].'</td>';
                //     echo '<td colspan="2"></td>';
                //     echo '<td>'.$dyarn_array[$temp4]['pc'].'</td>';
                //     echo '<td>'.$dyarn_array[$temp4]['val'].'</td>'; 
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>';
                //     echo '<td></td>'; 
                //     echo '<td></td>';
                //     echo '<td>'.$dyarn_array[$temp4]['warp_grey'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['warp_colour'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['warp_black'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['warp_bl'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['warp_tot'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['weft_grey'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['weft_colour'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['weft_black'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['weft_bl'].'</td>'; 
                //     echo '<td>'.$dyarn_array[$temp4]['weft_tot'].'</td>';  
                //     echo '<td></td><td> </td><td> </td><td> </td></tr>';          
                //   }
                // }

                    // echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="4">District Grand Total</td>';
                    // echo "<td></td>";
                    // echo "<td>".$dsum['meter']."</td>";
                    // echo "<td>".$dsum['sqm']."</td>";
                    // echo '<td colspan="2"> </td>';
                    // echo "<td>".$dsum['pr']."</td>";
                    // echo "<td>".$dsum['val']."</td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td> </td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td>".$dyarnsum['warp_grey']."</td>";
                    // echo "<td>".$dyarnsum['warp_colour']."</td>";
                    // echo "<td>".$dyarnsum['warp_black']."</td>";
                    // echo "<td>".$dyarnsum['warp_bl']."</td>";
                    // echo "<td>".$dyarnsum['warp_tot']."</td>";
                    // echo "<td>".$dyarnsum['weft_grey']."</td>";
                    // echo "<td>".$dyarnsum['weft_colour']."</td>";
                    // echo "<td>".$dyarnsum['weft_black']."</td>";
                    // echo "<td>".$dyarnsum['weft_bl']."</td>";
                    // echo "<td>".$dyarnsum['weft_tot']."</td>";
                    // echo "<td> </td><td> </td><td> </td><td> </td></tr>";
                    echo "</tbody><tfoot></tfoot></table>";

                    $_SESSION["weaving_excel_data"]=$excel_var;
                    $_SESSION["district_total_weaving"]=$wdsum;
                    $_SESSION["district_total_weaving_variety"]=$dyarn_array;
                    $_SESSION["weaving_variety_data"]=$variety_array;
                    $_SESSION["unitname"]=$dunitname;  
                }

                if($res4[0]['type']=="Preprocessing"){
                $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit",$con);
                for($z=0;$z<sizeof($res5);$z++) {// loop for units
                  $excel_var[$z][0][0]=0;

                  $sum = array(
                    'no'       => 0,
                    'wage'     => 0,
                    'wf'       => 0,
                    'wfrec'    => 0,
                    'adrec'    => 0,
                    'kcrec'    => 0,
                    'covrec'   => 0,
                    'lic'      => 0,
                    'nw'       => 0,
                    'hw'       => 0,
                    'tw'       => 0,
                    'esi'      => 0,
                    'yi'       => 0,
                    'mw'       => 0,
                    'dw'       => 0,
                    'tmw'      => 0,
                    'nmw'      => 0,
                  );

                  $rmsum=array(
                    's_grey'    => 0,
                    's_colour'  => 0,
                    's_black'   => 0,
                    's_bl'      => 0,
                    's_tot'     => 0,
                    's_wage'    => 0,
                    's_yi'      => 0,
                    'b_grey'    => 0,
                    'b_colour'  => 0,
                    'b_black'   => 0,
                    'b_bl'      => 0,
                    'b_tot'     => 0,
                    'b_wage'    => 0,
                    'b_yi'      => 0,
                    'w_grey'    => 0,
                    'w_colour'  => 0,
                    'w_black'   => 0,
                    'w_bl'      => 0,
                    'w_tot'     => 0,         
                    'w_wage'    => 0,
                    'w_yi'      => 0,
                  );

                  $unitdemo=$res5[$z]['unit_name'];
                  $unit=$res5[$z]['unit_code'];
                  $dunitname[$z]=$unitdemo;

                  $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name,account_no,ifsc,bank_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from kbk_preprocessing where s_date='$hdate')",$con);
          
                  if(sizeof($res1)>0){
                    echo '<table class="table-bordered" id="customers">
                      <thead>
                        <tr><th scope="col" colspan="49" style="background-color: #038857;">'.strtoupper($unitdemo).'</th></tr>
                        <tr>
                          <th colspan="4"></th>
                          <th colspan="8" style="text-align:center">Sizing</th>
                          <th colspan="8" style="text-align:center">Bobin Winding</th>
                          <th colspan="8" style="text-align:center">Warping</th>
                          <th colspan="21"></th>
                        </tr>
                            <tr>
                              <th scope="col">SI No.</th>
                              <th scope="col">Artisan Code</th>
                              <th scope="col">Artisan Name</th>
                              <th scope="col">Variety</th>

                              <th scope="col">Grey</th>
                              <th scope="col">Colour</th>
                              <th scope="col">Bl</th>
                              <th scope="col">Black</th>
                              <th scope="col">Total</th>
                              <th scope="col">Rate</th>
                              <th scope="col">Wage</th>
                              <th scope="col">YI</th>

                              <th scope="col">Grey</th>
                              <th scope="col">Colour</th>
                              <th scope="col">Bl</th>
                              <th scope="col">Black</th>
                              <th scope="col">Total</th>
                              <th scope="col">Rate</th>
                              <th scope="col">Wage</th>
                              <th scope="col">YI</th>

                              <th scope="col">Grey</th>
                              <th scope="col">Colour</th>
                              <th scope="col">Bl</th>
                              <th scope="col">Black</th>
                              <th scope="col">Total</th>
                              <th scope="col">Rate</th>
                              <th scope="col">Wage</th>
                              <th scope="col">YI</th>
                            
                              <th scope="col">Wages</th>

                              <th scope="col">W/F Fund</th>
                              <th scope="col">W/F Rec.</th>
                              <th scope="col">Adv Rec.</th>
                              <th scope="col">Khadi Cr. Rec.</th>
                              <th scope="col">Covid adv. Rec.</th>
                              <th scope="col">LIC</th>
                              <th scope="col">Net wages</th>
                              <th scope="col">Holiday wages</th>
                              <th scope="col">Total Wages</th>
                              <th scope="col">ESI Contr</th>
                              <th scope="col">Total Yarn Incentive</th>
                              
                              <th scope="col">Minimum Wages</th>
                              <th scope="col">DA Days</th>
                              <th scope="col">DA Wages</th>
                              <th scope="col">Total Minimum Wages</th>
                              <th scope="col">Net Minimum Wages</th>
                              
                              <th scope="col">Attendance</th>
                              <th scope="col">Account Number</th>
                              <th scope="col">IFSC</th>
                              <th scope="col">Bank Name</th>
        
                            </tr>
                          </thead>
                          <tbody>';
                          
                            $yarn_array=array(array());
                            for($k=0;$k<sizeof($res16);$k++){
                              $temp4=$res16[$k]['rm_code'];

                              $yarn_array[$temp4]['s_grey']= 0;
                              $yarn_array[$temp4]['s_colour']= 0;
                              $yarn_array[$temp4]['s_black']= 0;
                              $yarn_array[$temp4]['s_bl']= 0;
                              $yarn_array[$temp4]['s_tot']= 0;
                              $yarn_array[$temp4]['b_grey']= 0;
                              $yarn_array[$temp4]['b_colour']= 0;
                              $yarn_array[$temp4]['b_black']= 0;
                              $yarn_array[$temp4]['b_bl']= 0;
                              $yarn_array[$temp4]['b_tot']= 0;
                              $yarn_array[$temp4]['w_grey']= 0;
                              $yarn_array[$temp4]['w_colour']= 0;
                              $yarn_array[$temp4]['w_black']= 0;
                              $yarn_array[$temp4]['w_bl']= 0;
                              $yarn_array[$temp4]['w_tot']= 0;
                              $yarn_array[$temp4]['grand_tot']= 0; 
                            }

                        for($i=0;$i<sizeof($res1);$i++){
                          $artid=intval($res1[$i]['artisan_id']);
                          $res6 =  retrieveData("SELECT wages,attendance,pre_id,preprocessing_id From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'",$con);
                          $preid=intval($res6[0]['pre_id']);
                          $preprocessingid=intval($res6[0]['preprocessing_id']);
                          $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3",$con);
                          $res9=retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid",$con);
                          $res10=retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid",$con);

                          $par1 = [
                            'grey'  => $res10[0]['s_grey'],
                            'col'   => $res10[0]['s_col'],
                            'bl'    => $res10[0]['s_bl'],
                            'black' => $res10[0]['s_black'],
                          ];

                          $sizetotnoh=preprocessingTypeWage($par1);

                          $par1 = [
                            'grey'  => $res10[0]['b_grey'],
                            'col'   => $res10[0]['b_col'],
                            'bl'    => $res10[0]['b_bl'],
                            'black' => $res10[0]['b_black'],
                          ];

                          $bobintotnoh=preprocessingTypeWage($par1);

                          $par1 = [
                            'grey'  => $res10[0]['w_grey'],
                            'col'   => $res10[0]['w_col'],
                            'bl'    => $res10[0]['w_bl'],
                            'black' => $res10[0]['w_black'],
                          ];

                          $warptotnoh=preprocessingTypeWage($par1);

                          $wfund=0;
                          if($res8[0]['wf_yes_no']==1){
                            $wfund=$res9[0]['wf'];
                          }

                          $par2 = [
                            'snoh'  => $sizetotnoh,
                            'bnoh'  => $bobintotnoh,
                            'wnoh'  => $warptotnoh,
                            'swage' => $res9[0]['s_wages'],
                            'bwage' => $res9[0]['b_wages'],
                            'wwage' => $res9[0]['w_wages'],
                            'wf'    => $wfund,
                            'wfrec' => $res8[0]['wf_rec'],
                            'adrec' => $res8[0]['ad_rec'],
                            'kcrec' => $res8[0]['credit_rec'],
                            'covrec'=> $res8[0]['cov_rec'],
                            'lic'   => $res8[0]['lic'],
                            'hno'    => $res3[0]['holiday_no'],
                            'wno'    => $res3[0]['total_wd'],
                          ];

                          $calc7=preprocessingWageCalculation($par2);

                          $par3 = [
                            'snoh'     => $sizetotnoh,
                            'bnoh'     => $bobintotnoh,
                            'wnoh'     => $warptotnoh,
                            'esi'      => $res9[0]['esi'],
                            'syi'      => $res9[0]['s_yi'],
                            'byi'      => $res9[0]['b_yi'],
                            'wyi'      => $res9[0]['w_yi'],              
                            'smw'      => $res9[0]['s_min_wages'],
                            'bmw'      => $res9[0]['b_min_wages'],
                            'wmw'      => $res9[0]['w_min_wages'],
                            'starget'  => $res9[0]['s_target'],
                            'btarget'  => $res9[0]['b_target'],
                            'wtarget'  => $res9[0]['w_target'],
                            'dawages'  => $res3[0]['da_wages'],
                            'wage'     => $calc7['wage'],
                            'twd'      => $res3[0]['total_wd'],
                            'tnoh'     => $calc7['totnoh'],
                          ];

                          $calc8=preprocessingIncentivesCalculation($par3);

                          echo '<tr style="text-align: center;"><td>'.strval($i+1).'</td>';
                          echo '<td style="text-align: left;">'.$res1[$i]['artisan_code'].'</td>';
                          echo '<td style="text-align: left;">'.$res1[$i]['artisan_name'].'</td>';
                          echo '<td style="text-align: left;">'.$res9[0]['type'].'</td>';
                          echo '<td ">'.$res10[0]['s_grey'].'</td>';
                          echo '<td ">'.$res10[0]['s_col'].'</td>';
                          echo '<td ">'.$res10[0]['s_bl'].'</td>';
                          echo '<td ">'.$res10[0]['s_black'].'</td>';
                          echo '<td ">'.$sizetotnoh.'</td>';
                          echo '<td ">'.$res9[0]['s_wages'].'</td>';
                          echo '<td ">'.$calc7['swage'].'</td>';
                          echo '<td ">'.$calc8['syi'].'</td>';

                          echo '<td ">'.$res10[0]['b_grey'].'</td>';
                          echo '<td ">'.$res10[0]['b_col'].'</td>';
                          echo '<td ">'.$res10[0]['b_bl'].'</td>';
                          echo '<td ">'.$res10[0]['b_black'].'</td>';
                          echo '<td ">'.$bobintotnoh.'</td>';
                          echo '<td ">'.$res9[0]['b_wages'].'</td>';
                          echo '<td ">'.$calc7['bwage'].'</td>';
                          echo '<td ">'.$calc8['byi'].'</td>';

                          echo '<td ">'.$res10[0]['w_grey'].'</td>';
                          echo '<td ">'.$res10[0]['w_col'].'</td>';
                          echo '<td ">'.$res10[0]['w_bl'].'</td>';
                          echo '<td ">'.$res10[0]['w_black'].'</td>';
                          echo '<td ">'.$warptotnoh.'</td>';
                          echo '<td ">'.$res9[0]['w_wages'].'</td>';
                          echo '<td ">'.$calc7['wwage'].'</td>';
                          echo '<td ">'.$calc8['wyi'].'</td>';
      
                          echo '<td ">'.$calc7['wage'].'</td>';
                          echo '<td ">'.$calc7['wf'].'</td>';

                          echo '<td ">'.$res8[0]['wf_rec'].'</td>';
                          echo '<td ">'.$res8[0]['ad_rec'].'</td>';
                          echo '<td ">'.$res8[0]['credit_rec'].'</td>';
                          echo '<td ">'.$res8[0]['cov_rec'].'</td>';
                          echo '<td ">'.$res8[0]['lic'].'</td>';
                          
                          echo '<td ">'.$calc7['netwages'].'</td>';
                          echo '<td ">'.$calc7['hdwage'].'</td>';
                          echo '<td ">'.$calc7['totwages'].'</td>';
                          echo '<td ">'.$calc8['esi'].'</td>';
                          echo '<td ">'.$calc8['yi'].'</td>';
                          echo '<td ">'.$calc8['mw'].'</td>';
                          echo '<td ">'.$calc8['dadays'].'</td>';
                          echo '<td ">'.$calc8['dawages'].'</td>';
                          echo '<td ">'.$calc8['totmw'].'</td>';
                          echo '<td ">'.$calc8['netmw'].'</td>';
                          echo '<td ">'.$res6[0]['attendance'].'</td>';
                          
                          echo '<td>'.$res1[$i]['account_no'].'</td>';
                          echo '<td>'.$res1[$i]['ifsc'].'</td>';
                          echo '<td>'.$res1[$i]['bank_name'].'</td></tr>';

                          $excel_var[$z][$i][0] = strval($i+1);
                          $excel_var[$z][$i][1] =$res1[$i]['artisan_code'];
                          $excel_var[$z][$i][2] =$res1[$i]['artisan_name'];
                          $excel_var[$z][$i][3] =$res9[0]['type'];
                          $excel_var[$z][$i][4] =$res10[0]['s_grey'];
                          $excel_var[$z][$i][5] =$res10[0]['s_col'];
                          $excel_var[$z][$i][6] =$res10[0]['s_bl'];
                          $excel_var[$z][$i][7] =$res10[0]['s_black'];
                          $excel_var[$z][$i][8] =$sizetotnoh;
                          $excel_var[$z][$i][9] =$res9[0]['s_wages'];
                          $excel_var[$z][$i][10] =$calc7['swage'];
                          $excel_var[$z][$i][11] =$calc8['syi'];

                          $excel_var[$z][$i][12] =$res10[0]['b_grey'];
                          $excel_var[$z][$i][13] =$res10[0]['b_col'];
                          $excel_var[$z][$i][14] =$res10[0]['b_bl'];
                          $excel_var[$z][$i][15] =$res10[0]['b_black'];
                          $excel_var[$z][$i][16] =$bobintotnoh;
                          $excel_var[$z][$i][17] =$res9[0]['b_wages'];
                          $excel_var[$z][$i][18] =$calc7['bwage'];
                          $excel_var[$z][$i][19] =$calc8['byi'];

                          $excel_var[$z][$i][20] =$res10[0]['w_grey'];
                          $excel_var[$z][$i][21] =$res10[0]['w_col'];
                          $excel_var[$z][$i][22] =$res10[0]['w_bl'];
                          $excel_var[$z][$i][23] =$res10[0]['w_black'];
                          $excel_var[$z][$i][24] =$warptotnoh;
                          $excel_var[$z][$i][25] =$res9[0]['w_wages'];
                          $excel_var[$z][$i][26] =$calc7['wwage'];
                          $excel_var[$z][$i][27] =$calc8['wyi'];
                        
                          $excel_var[$z][$i][28] =$calc7['wage'];
                          $excel_var[$z][$i][29] =$calc7['wf'];

                          $excel_var[$z][$i][30] =$res8[0]['wf_rec'];
                          $excel_var[$z][$i][31] =$res8[0]['ad_rec'];
                          $excel_var[$z][$i][32] =$res8[0]['credit_rec'];
                          $excel_var[$z][$i][33] =$res8[0]['cov_rec'];
                          $excel_var[$z][$i][34] =$res8[0]['lic'];
      
                          $excel_var[$z][$i][35] =$calc7['netwages'];
                          $excel_var[$z][$i][36] =$calc7['hdwage'];
                          $excel_var[$z][$i][37] =$calc7['totwages'];
                          $excel_var[$z][$i][38] =$calc8['esi'];
                          $excel_var[$z][$i][39] =$calc8['yi'];
                          $excel_var[$z][$i][40] =$calc8['mw'];
                          $excel_var[$z][$i][41] =$calc8['dadays'];
                          $excel_var[$z][$i][42] =$calc8['dawages'];
                          $excel_var[$z][$i][43] =$calc8['totmw'];
                          $excel_var[$z][$i][44] =$calc8['netmw'];
                          $excel_var[$z][$i][45] =$res6[0]['attendance'];

                          $excel_var[$z][$i][46] =$res1[$i]['account_no'];
                          $excel_var[$z][$i][47] =$res1[$i]['ifsc'];
                          $excel_var[$z][$i][48] =$res1[$i]['bank_name'];

                          $rmsum['s_grey']  =$rmsum['s_grey']+$res10[0]['s_grey'];
                          $rmsum['s_colour']=$rmsum['s_colour']+$res10[0]['s_col'];
                          $rmsum['s_black'] =$rmsum['s_black']+$res10[0]['s_black'];
                          $rmsum['s_bl']    =$rmsum['s_bl']+$res10[0]['s_bl'];
                          $rmsum['s_tot']   =$rmsum['s_tot']+$sizetotnoh;
                          $rmsum['s_wage']  =$rmsum['s_wage']+$calc7['swage'];
                          $rmsum['s_yi']    =$rmsum['s_yi']+$calc8['syi'];
                          $rmsum['b_grey']  =$rmsum['b_grey']+$res10[0]['b_grey'];
                          $rmsum['b_colour']=$rmsum['b_colour']+$res10[0]['b_col'];
                          $rmsum['b_black'] =$rmsum['b_black']+$res10[0]['b_black'];
                          $rmsum['b_bl']    =$rmsum['b_bl']+$res10[0]['b_bl'];
                          $rmsum['b_tot']   =$rmsum['b_tot']+$bobintotnoh;
                          $rmsum['b_wage']  =$rmsum['b_wage']+$calc7['bwage'];
                          $rmsum['b_yi']    =$rmsum['b_yi']+$calc8['byi'];
                          $rmsum['w_grey']  =$rmsum['w_grey']+$res10[0]['w_grey'];
                          $rmsum['w_colour']=$rmsum['w_colour']+$res10[0]['w_col'];
                          $rmsum['w_black'] =$rmsum['w_black']+$res10[0]['w_black'];
                          $rmsum['w_bl']    =$rmsum['w_bl']+$res10[0]['w_bl'];
                          $rmsum['w_tot']   =$rmsum['w_tot']+$warptotnoh;
                          $rmsum['w_wage']  =$rmsum['w_wage']+$calc7['wwage'];
                          $rmsum['w_yi']    =$rmsum['w_yi']+$calc8['wyi'];


                          $sum['wf']=$sum['wf']+$calc7['wf'];
                          $sum['wfrec']=$sum['wfrec']+$calc7['wfrec'];
                          $sum['adrec']=$sum['adrec']+$calc7['adrec'];         
                          $sum['kcrec']=$sum['kcrec']+$calc7['kcrec'];
                          $sum['covrec']=$sum['covrec']+$calc7['covrec'];
                          $sum['lic']=$sum['lic']+$calc7['lic'];
                          $sum['nw']=$sum['nw']+$calc7['netwages'];
                          $sum['hw']=$sum['hw']+$calc7['hdwage'];
                          $sum['tw']=$sum['tw']+$calc7['totwages'];
                          $sum['esi']=$sum['esi']+$calc8['esi'];
                          $sum['yi']=$sum['yi']+$calc8['yi'];
                          $sum['mw']=$sum['mw']+$calc8['mw'];
                          $sum['dw']=$sum['dw']+$calc8['dawages'];
                          $sum['tmw']=$sum['tmw']+$calc8['totmw'];
                          $sum['nmw']=$sum['nmw']+$calc8['netmw'];
                          $sum['no']=$sum['no']+$calc7['wage'];

                          $temp5=$res9[0]['rm_code'];
                          $yarn_array[$temp5]['s_grey']=$yarn_array[$temp5]['s_grey']+$res10[0]['s_grey'];
                          $yarn_array[$temp5]['s_colour']=$yarn_array[$temp5]['s_colour']+$res10[0]['s_col'];
                          $yarn_array[$temp5]['s_black']=$yarn_array[$temp5]['s_black']+$res10[0]['s_black'];
                          $yarn_array[$temp5]['s_bl']=$yarn_array[$temp5]['s_bl']+$res10[0]['s_bl'];
                          $yarn_array[$temp5]['s_tot']=$yarn_array[$temp5]['s_tot']+$sizetotnoh;

                          $yarn_array[$temp5]['b_grey']=$yarn_array[$temp5]['b_grey']+$res10[0]['b_grey'];
                          $yarn_array[$temp5]['b_colour']=$yarn_array[$temp5]['b_colour']+$res10[0]['b_col'];
                          $yarn_array[$temp5]['b_black']=$yarn_array[$temp5]['b_black']+$res10[0]['b_black'];
                          $yarn_array[$temp5]['b_bl']=$yarn_array[$temp5]['b_bl']+$res10[0]['b_bl'];
                          $yarn_array[$temp5]['b_tot']=$yarn_array[$temp5]['b_tot']+$bobintotnoh;

                          $yarn_array[$temp5]['w_grey']=$yarn_array[$temp5]['w_grey']+$res10[0]['w_grey'];
                          $yarn_array[$temp5]['w_colour']=$yarn_array[$temp5]['w_colour']+$res10[0]['w_col'];
                          $yarn_array[$temp5]['w_black']=$yarn_array[$temp5]['w_black']+$res10[0]['w_black'];
                          $yarn_array[$temp5]['w_bl']=$yarn_array[$temp5]['w_bl']+$res10[0]['w_bl'];
                          $yarn_array[$temp5]['w_tot']=$yarn_array[$temp5]['w_tot']+$warptotnoh;
                          $yarn_array[$temp5]['grand_tot']=$sizetotnoh+$bobintotnoh+$warptotnoh;
                        }

                        echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
                        echo '<td>'.$rmsum['s_grey'].'</td>';
                        echo '<td>'.$rmsum['s_colour'].'</td>';
                        echo '<td>'.$rmsum['s_bl'].'</td>';
                        echo '<td>'.$rmsum['s_black'].'</td>';      
                        echo '<td>'.$rmsum['s_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$rmsum['s_wage'].'</td>';
                        echo '<td>'.$rmsum['s_yi'].'</td>';
                        echo '<td>'.$rmsum['b_grey'].'</td>';
                        echo '<td>'.$rmsum['b_colour'].'</td>';
                        echo '<td>'.$rmsum['b_bl'].'</td>';
                        echo '<td>'.$rmsum['b_black'].'</td>';      
                        echo '<td>'.$rmsum['b_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$rmsum['b_wage'].'</td>';
                        echo '<td>'.$rmsum['b_yi'].'</td>';
                        echo '<td>'.$rmsum['w_grey'].'</td>';
                        echo '<td>'.$rmsum['w_colour'].'</td>';
                        echo '<td>'.$rmsum['w_bl'].'</td>';
                        echo '<td>'.$rmsum['w_black'].'</td>';      
                        echo '<td>'.$rmsum['w_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$rmsum['w_wage'].'</td>';
                        echo '<td>'.$rmsum['w_yi'].'</td>';
                        echo '<td>'.$sum['no'].'</td>';
                        echo "<td>".$sum['wf']."</td>";
                        echo "<td>".$sum['wfrec']."</td>";
                        echo "<td>".$sum['adrec']."</td>";
                        echo "<td>".$sum['kcrec']."</td>";
                        echo "<td>".$sum['covrec']."</td>";
                        echo "<td>".$sum['lic']."</td>";
                        echo "<td>".$sum['nw']."</td>";
                        echo "<td>".$sum['hw']."</td>";
                        echo "<td>".$sum['tw']."</td>";
                        echo "<td>".$sum['esi']."</td>";
                        echo "<td>".$sum['yi']."</td>";
                        echo "<td>".$sum['mw']."</td>";
                        echo "<td> </td>";
                        echo "<td>".$sum['dw']."</td>";
                        echo "<td>".$sum['tmw']."</td>";
                        echo "<td>".$sum['nmw']."</td>";
                        echo "<td></td><td></td><td></td><td></td></tr>";

                        $drmsum['s_grey']=$drmsum['s_grey']+$rmsum['s_grey'];
                        $drmsum['s_colour']=$drmsum['s_colour']+$rmsum['s_colour'];
                        $drmsum['s_bl']=$drmsum['s_bl']+$rmsum['s_bl'];
                        $drmsum['s_black']=$drmsum['s_black']+$rmsum['s_black'];   
                        $drmsum['s_tot']=$drmsum['s_tot']+$rmsum['s_tot'];
                        $drmsum['s_wage']=$drmsum['s_wage']+$rmsum['s_wage'];
                        $drmsum['s_yi']=$drmsum['s_yi']+$rmsum['s_yi'];
                        $drmsum['b_grey']=$drmsum['b_grey']+$rmsum['b_grey'];
                        $drmsum['b_colour']=$drmsum['b_colour']+$rmsum['b_colour'];
                        $drmsum['b_bl']=$drmsum['b_bl']+$rmsum['b_bl'];
                        $drmsum['b_black']=$drmsum['b_black']+$rmsum['b_black'];     
                        $drmsum['b_tot']=$drmsum['b_tot']+$rmsum['b_tot'];
                        $drmsum['b_wage']=$drmsum['b_wage']+$rmsum['b_wage'];
                        $drmsum['b_yi']=$drmsum['b_yi']+$rmsum['b_yi'];
                        $drmsum['w_grey']=$drmsum['w_grey']+$rmsum['w_grey'];
                        $drmsum['w_colour']=$drmsum['w_colour']+$rmsum['w_colour'];
                        $drmsum['w_bl']=$drmsum['w_bl']+$rmsum['w_bl'];
                        $drmsum['w_black']=$drmsum['w_black']+$rmsum['w_black'];  
                        $drmsum['w_tot']=$drmsum['w_tot']+$rmsum['w_tot'];
                        $drmsum['w_wage']=$drmsum['w_wage']+$rmsum['w_wage'];
                        $drmsum['w_yi']=$drmsum['w_yi']+$rmsum['w_yi'];
                        $dsum['wage']  =$dsum['wage']+$sum['no'];
                        $dsum['wf']    =$dsum['wf']+$sum['wf'];
                        $dsum['wfrec'] =$dsum['wfrec']+$sum['wfrec'];
                        $dsum['adrec'] =$dsum['adrec']+$sum['adrec'];
                        $dsum['kcrec'] =$dsum['kcrec']+$sum['kcrec'];
                        $dsum['covrec']=$dsum['covrec']+$sum['covrec'];
                        $dsum['lic']   =$dsum['lic']+$sum['lic'];
                        $dsum['nw']    =$dsum['nw']+$sum['nw']; 
                        $dsum['hw']    =$dsum['hw']+$sum['hw'];
                        $dsum['tw']    =$dsum['tw']+$sum['tw'];
                        $dsum['esi']   =$dsum['esi']+$sum['esi'];
                        $dsum['yi']    =$dsum['yi']+$sum['yi'];
                        $dsum['mw']    =$dsum['mw']+$sum['mw'];
                        $dsum['dw']    =$dsum['dw']+$sum['dw'];
                        $dsum['tmw']   =$dsum['tmw']+$sum['tmw'];
                        $dsum['nmw']   =$dsum['nmw']+$sum['nmw'];

                        $excel_var[$z][$i][4]=$rmsum['s_grey'];
                        $excel_var[$z][$i][5]=$rmsum['s_colour'];
                        $excel_var[$z][$i][6]=$rmsum['s_bl'];
                        $excel_var[$z][$i][7]=$rmsum['s_black'];      
                        $excel_var[$z][$i][8]=$rmsum['s_tot'];
                  
                        $excel_var[$z][$i][10]=$rmsum['s_wage'];
                        $excel_var[$z][$i][11]=$rmsum['s_yi'];
                        $excel_var[$z][$i][12]=$rmsum['b_grey'];
                        $excel_var[$z][$i][13]=$rmsum['b_colour'];
                        $excel_var[$z][$i][14]=$rmsum['b_bl'];
                        $excel_var[$z][$i][15]=$rmsum['b_black'];      
                        $excel_var[$z][$i][16]=$rmsum['b_tot'];

                        $excel_var[$z][$i][18]=$rmsum['b_wage'];
                        $excel_var[$z][$i][19]=$rmsum['b_yi'];
                        $excel_var[$z][$i][20]=$rmsum['w_grey'];
                        $excel_var[$z][$i][21]=$rmsum['w_colour'];
                        $excel_var[$z][$i][22]=$rmsum['w_bl'];
                        $excel_var[$z][$i][23]=$rmsum['w_black'];      
                        $excel_var[$z][$i][24]=$rmsum['w_tot'];

                        $excel_var[$z][$i][26]=$rmsum['w_wage'];
                        $excel_var[$z][$i][27]=$rmsum['w_yi'];
                        $excel_var[$z][$i][28]=$sum['no'];
                        $excel_var[$z][$i][29]=$sum['wf'];
                        $excel_var[$z][$i][30]=$sum['wfrec'];
                        $excel_var[$z][$i][31]=$sum['adrec'];
                        $excel_var[$z][$i][32]=$sum['kcrec'];
                        $excel_var[$z][$i][33]=$sum['covrec'];
                        $excel_var[$z][$i][34]=$sum['lic'];
                        $excel_var[$z][$i][35]=$sum['nw'];
                        $excel_var[$z][$i][36]=$sum['hw'];
                        $excel_var[$z][$i][37]=$sum['tw'];
                        $excel_var[$z][$i][38]=$sum['esi'];
                        $excel_var[$z][$i][39]=$sum['yi'];
                        $excel_var[$z][$i][40]=$sum['mw'];

                        $excel_var[$z][$i][42]=$sum['dw'];
                        $excel_var[$z][$i][43]=$sum['tmw'];
                        $excel_var[$z][$i][44]=$sum['nmw'];

                        for($k=0;$k<sizeof($res16);$k++){
                          $temp5=$res16[$k]['rm_code'];
                          if($yarn_array[$temp5]['grand_tot']!=0){

                            echo '<tr style="text-align:center;">';
                            echo '<td colspan="2" >'.$res16[$k]['rm_code'].'</td>';
                            echo '<td colspan="2" >'.$res16[$k]['type'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['s_grey'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['s_colour'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['s_bl'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['s_black'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['s_tot'].'</td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
 
                            echo '<td>'.$yarn_array[$temp5]['b_grey'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['b_colour'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['b_bl'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['b_black'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['b_tot'].'</td>'; 
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';

                            echo '<td>'.$yarn_array[$temp5]['w_grey'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['w_colour'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['w_bl'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['w_black'].'</td>';
                            echo '<td>'.$yarn_array[$temp5]['w_tot'].'</td>'; 
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';

                            echo '<td colspan="21"></td>';
                            echo '</tr>';

                            $variety_array[$z]=$yarn_array;
                            $rmv_array[$temp5]['s_grey']   = $rmv_array[$temp5]['s_grey']+ $yarn_array[$temp5]['s_grey'];
                            $rmv_array[$temp5]['s_colour'] = $rmv_array[$temp5]['s_colour']+$yarn_array[$temp5]['s_colour'];
                            $rmv_array[$temp5]['s_black']  = $rmv_array[$temp5]['s_black']+$yarn_array[$temp5]['s_black'];
                            $rmv_array[$temp5]['s_bl']     = $rmv_array[$temp5]['s_bl']+$yarn_array[$temp5]['s_bl'];
                            $rmv_array[$temp5]['s_tot']    = $rmv_array[$temp5]['s_tot']+$yarn_array[$temp5]['s_tot'];
                            $rmv_array[$temp5]['b_grey']   = $rmv_array[$temp5]['b_grey']+$yarn_array[$temp5]['b_grey'];
                            $rmv_array[$temp5]['b_colour'] = $rmv_array[$temp5]['b_colour']+$yarn_array[$temp5]['b_colour'];
                            $rmv_array[$temp5]['b_black']  = $rmv_array[$temp5]['b_black']+$yarn_array[$temp5]['b_black'];
                            $rmv_array[$temp5]['b_bl']     = $rmv_array[$temp5]['b_bl'];+$yarn_array[$temp5]['b_bl'];
                            $rmv_array[$temp5]['b_tot']    = $rmv_array[$temp5]['b_tot']+$yarn_array[$temp5]['b_tot'];
                            $rmv_array[$temp5]['w_grey']   = $rmv_array[$temp5]['w_grey']+$yarn_array[$temp5]['w_grey'];
                            $rmv_array[$temp5]['w_colour'] = $rmv_array[$temp5]['w_colour']+$yarn_array[$temp5]['w_colour'];
                            $rmv_array[$temp5]['w_black']  = $rmv_array[$temp5]['w_black']+$yarn_array[$temp5]['w_black'];
                            $rmv_array[$temp5]['w_bl']     = $rmv_array[$temp5]['w_bl']+$yarn_array[$temp5]['w_bl'];
                            $rmv_array[$temp5]['w_tot']    = $rmv_array[$temp5]['w_tot']+$yarn_array[$temp5]['w_tot'];
                            $rmv_array[$temp5]['grand_tot']= $rmv_array[$temp5]['grand_tot']+$yarn_array[$temp5]['grand_tot'];

                          }
                        }

                        echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Grand Total</td>';
                        echo '<td>'.$rmsum['s_grey'].'</td>';
                        echo '<td>'.$rmsum['s_colour'].'</td>';
                        echo '<td>'.$rmsum['s_bl'].'</td>';
                        echo '<td>'.$rmsum['s_black'].'</td>';      
                        echo '<td>'.$rmsum['s_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td>'.$rmsum['b_grey'].'</td>';
                        echo '<td>'.$rmsum['b_colour'].'</td>';
                        echo '<td>'.$rmsum['b_bl'].'</td>';
                        echo '<td>'.$rmsum['b_black'].'</td>';      
                        echo '<td>'.$rmsum['b_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td>'.$rmsum['w_grey'].'</td>';
                        echo '<td>'.$rmsum['w_colour'].'</td>';
                        echo '<td>'.$rmsum['w_bl'].'</td>';
                        echo '<td>'.$rmsum['w_black'].'</td>';      
                        echo '<td>'.$rmsum['w_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td> </td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td><td></td><td></td><td></td></tr>";   

                        echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="4">District Total</td>';
                        echo '<td>'.$drmsum['s_grey'].'</td>';
                        echo '<td>'.$drmsum['s_colour'].'</td>';
                        echo '<td>'.$drmsum['s_bl'].'</td>';
                        echo '<td>'.$drmsum['s_black'].'</td>';      
                        echo '<td>'.$drmsum['s_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$drmsum['s_wage'].'</td>';
                        echo '<td>'.$drmsum['s_yi'].'</td>';
                        echo '<td>'.$drmsum['b_grey'].'</td>';
                        echo '<td>'.$drmsum['b_colour'].'</td>';
                        echo '<td>'.$drmsum['b_bl'].'</td>';
                        echo '<td>'.$drmsum['b_black'].'</td>';      
                        echo '<td>'.$drmsum['b_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$drmsum['b_wage'].'</td>';
                        echo '<td>'.$drmsum['b_yi'].'</td>';
                        echo '<td>'.$drmsum['w_grey'].'</td>';
                        echo '<td>'.$drmsum['w_colour'].'</td>';
                        echo '<td>'.$drmsum['w_bl'].'</td>';
                        echo '<td>'.$drmsum['w_black'].'</td>';      
                        echo '<td>'.$drmsum['w_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td>'.$drmsum['w_wage'].'</td>';
                        echo '<td>'.$drmsum['w_yi'].'</td>';
                        echo '<td>'.$dsum['no'].'</td>';
                        echo "<td>".$dsum['wf']."</td>";
                        echo "<td>".$dsum['wfrec']."</td>";
                        echo "<td>".$dsum['adrec']."</td>";
                        echo "<td>".$dsum['kcrec']."</td>";
                        echo "<td>".$dsum['covrec']."</td>";
                        echo "<td>".$dsum['lic']."</td>";
                        echo "<td>".$dsum['nw']."</td>";
                        echo "<td>".$dsum['hw']."</td>";
                        echo "<td>".$dsum['tw']."</td>";
                        echo "<td>".$dsum['esi']."</td>";
                        echo "<td>".$dsum['yi']."</td>";
                        echo "<td>".$dsum['mw']."</td>";
                        echo "<td> </td>";
                        echo "<td>".$dsum['dw']."</td>";
                        echo "<td>".$dsum['tmw']."</td>";
                        echo "<td>".$dsum['nmw']."</td>";
                        echo "<td></td><td></td><td></td><td></td></tr>";

                        $distrct_sum=array();
                        $distrct_sum[0]=$drmsum['s_grey'];
                        $distrct_sum[1]=$drmsum['s_colour'];
                        $distrct_sum[2]=$drmsum['s_bl'];
                        $distrct_sum[3]=$drmsum['s_black'];      
                        $distrct_sum[4]=$drmsum['s_tot'];
                        $distrct_sum[5]=$drmsum['s_wage'];
                        $distrct_sum[6]=$drmsum['s_yi'];
                        $distrct_sum[7]=$drmsum['b_grey'];
                        $distrct_sum[8]=$drmsum['b_colour'];
                        $distrct_sum[9]=$drmsum['b_bl'];
                        $distrct_sum[10]=$drmsum['b_black'];      
                        $distrct_sum[11]=$drmsum['b_tot'];
                        $distrct_sum[12]=$drmsum['b_wage'];
                        $distrct_sum[13]=$drmsum['b_yi'];
                        $distrct_sum[14]=$drmsum['w_grey'];
                        $distrct_sum[15]=$drmsum['w_colour'];
                        $distrct_sum[16]=$drmsum['w_bl'];
                        $distrct_sum[17]=$drmsum['w_black'];      
                        $distrct_sum[18]=$drmsum['w_tot'];
                        $distrct_sum[19]=$drmsum['w_wage'];
                        $distrct_sum[20]=$drmsum['w_yi'];
                        $distrct_sum[21]=$dsum['no'];
                        $distrct_sum[22]=$dsum['wf'];
                        $distrct_sum[23]=$dsum['wfrec'];
                        $distrct_sum[24]=$dsum['adrec'];
                        $distrct_sum[25]=$dsum['kcrec'];
                        $distrct_sum[26]=$dsum['covrec'];
                        $distrct_sum[27]=$dsum['lic'];
                        $distrct_sum[28]=$dsum['nw'];
                        $distrct_sum[29]=$dsum['hw'];
                        $distrct_sum[30]=$dsum['tw'];
                        $distrct_sum[31]=$dsum['esi'];
                        $distrct_sum[32]=$dsum['yi'];
                        $distrct_sum[33]=$dsum['mw'];
                        $distrct_sum[34]=$dsum['dw'];
                        $distrct_sum[35]=$dsum['tmw'];
                        $distrct_sum[36]=$dsum['nmw'];

                        for($k=0;$k<sizeof($res16);$k++){
                          $temp5=$res16[$k]['rm_code'];
                          if($rmv_array[$temp5]['grand_tot']!=0){

                            echo '<tr style="text-align:center;">';
                            echo '<td colspan="2" >'.$res16[$k]['rm_code'].'</td>';
                            echo '<td colspan="2" >'.$res16[$k]['type'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['s_grey'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['s_colour'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['s_bl'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['s_black'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['s_tot'].'</td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                        
                            echo '<td>'.$rmv_array[$temp5]['b_grey'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['b_colour'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['b_bl'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['b_black'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['b_tot'].'</td>'; 
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';

                            echo '<td>'.$rmv_array[$temp5]['w_grey'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['w_colour'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['w_bl'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['w_black'].'</td>';
                            echo '<td>'.$rmv_array[$temp5]['w_tot'].'</td>'; 
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';

                            echo '<td colspan="21"></td></tr>';
   
                          }
   
                        }
                        echo '<tr style="background-color: #808080; color:white; text-align: center;"><td colspan="4">Grand Total</td>';
                        echo '<td>'.$drmsum['s_grey'].'</td>';
                        echo '<td>'.$drmsum['s_colour'].'</td>';
                        echo '<td>'.$drmsum['s_bl'].'</td>';
                        echo '<td>'.$drmsum['s_black'].'</td>';      
                        echo '<td>'.$drmsum['s_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td>'.$drmsum['b_grey'].'</td>';
                        echo '<td>'.$drmsum['b_colour'].'</td>';
                        echo '<td>'.$drmsum['b_bl'].'</td>';
                        echo '<td>'.$drmsum['b_black'].'</td>';      
                        echo '<td>'.$drmsum['b_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td>'.$drmsum['w_grey'].'</td>';
                        echo '<td>'.$drmsum['w_colour'].'</td>';
                        echo '<td>'.$drmsum['w_bl'].'</td>';
                        echo '<td>'.$drmsum['w_black'].'</td>';      
                        echo '<td>'.$drmsum['w_tot'].'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td> </td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td><td></td><td></td><td></td></tr>"; 

                        echo "</tbody><tfoot></tfoot></table>"; 

                        $_SESSION["preprocessing_excel_data"]=$excel_var;
                        $_SESSION["rm_variety_excel_data"]=$variety_array;
                        $_SESSION["unitname"]=$dunitname;
                        $_SESSION["district_total"]=$distrct_sum;
                        $_SESSION["district_variety_total"]=$rmv_array;
                    
                      }
                    }
                  }// if closing of work type preprocessing
?>
</div>

<!-- <div class="text-center" >
  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
<a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
<a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
</div>
 -->
 <div class="text-center" >
          
 <a target="_blank" class="btn btn-info btn_print" href="fullreport_district_excel.php">Wages Report</a>
 <!-- <a target="_blank" class="btn btn-info btn_print" href="./calcview/full_report_unit_pdf.php">Print AS PDF</a> -->

</div>
    </section>
    
  </form>
<?php 


?>
        <!-- End your project here-->

    <!-- MDB -->
    
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
