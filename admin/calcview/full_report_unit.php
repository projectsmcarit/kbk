<?php


$res3 = retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date ='$hdate'", $con);
$res4 = retrieveData("SELECT type From kbk_work_type where work_id='$worktype'", $con);
$res5 = retrieveData("SELECT unit_code,unit_name,instructor_id From kbk_unit where unit_code='$unit'", $con);
$unitdemo = $res5[0]['unit_name'];
//session for instructor_id, instructor_name
$ab = $res5[0]['instructor_id'];
$res15 = retrieveData("SELECT name from kbk_instructor where instructor_id=$ab", $con);
$_SESSION["instructor_name"] = $res15[0]['name'];
$excel_var = array(array());
$a = strtolower($res4[0]['type']);
echo "<script> console.log('the value is $a')</script>";
$b = "kbk_" . $a;
$res1 = retrieveData("SELECT artisan_code,artisan_id,artisan_name,account_no,ifsc,bank_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from $b where s_date='$hdate')", $con);
$_SESSION["artisan_name"] = $res1[0]['artisan_name'];
$_SESSION["artisan_code"] = $res1[0]['artisan_code'];

$sum = array(
  'no'       => 0,
  'meter'    => 0,
  'sqm'      => 0,
  'sliver'   => 0,
  'yarn'     => 0,
  'wage'     => 0,
  'wf'       => 0,
  'wfrec'    => 0,
  'adrec'    => 0,
  'kcrec'    => 0,
  'covrec'   => 0,
  'lic'      => 0,
  'nw'       => 0,
  'hw'       => 0,
  'tw'       => 0,
  'esi'      => 0,
  'yi'       => 0,
  'wyi'      => 0,
  'mw'       => 0,
  'dw'       => 0,
  'tmw'      => 0,
  'nmw'      => 0,
  'val'      => 0,
  'pr'       => 0,
);
$yarnsum = array(
  'warp_grey'    => 0,
  'warp_colour'  => 0,
  'warp_black'   => 0,
  'warp_bl'      => 0,
  'warp_tot'     => 0,
  'weft_grey'    => 0,
  'weft_colour'  => 0,
  'weft_black'   => 0,
  'weft_bl'      => 0,
  'weft_tot'     => 0,
);
$rmsum = array(
  's_grey'    => 0,
  's_colour'  => 0,
  's_black'   => 0,
  's_bl'      => 0,
  's_tot'     => 0,
  's_wage'    => 0,
  's_yi'      => 0,
  'b_grey'    => 0,
  'b_colour'  => 0,
  'b_black'   => 0,
  'b_bl'      => 0,
  'b_tot'     => 0,
  'b_wage'    => 0,
  'b_yi'      => 0,
  'w_grey'    => 0,
  'w_colour'  => 0,
  'w_black'   => 0,
  'w_bl'      => 0,
  'w_tot'     => 0,
  'w_wage'    => 0,
  'w_yi'      => 0,
);

?>

<!DOCTYPE html>
<html lang="en">

<head>


  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <?php
  echo '<title>' . $res4[0]['type'] . '</title>';
  ?>
  <!-- MDB icon -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />

  <style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 2px;
      padding-bottom: 2px;
      text-align: left;
      background-color: #04AA6D;
      color: white;
    }
  </style>



</head>

<body>
  <script type="text/javascript" src="../js/mdb.min.js"></script>
  <?php
  if (sizeof($res1) == 0) {
  ?>
    <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-7">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">

                <form action="view_report.php" method="post">
                  <div class="col-md-12 my-5 d-flex align-items-center">
                    <?php
                    $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $m1 = strval(date('m', strtotime($hdate)));
                    $y1 = strval(date('Y', strtotime($hdate)));
                    echo '<span class="h5 fw-bold text-uppercase">No details are entered in ' . $unit . ' ' . $unitdemo . ' unit on the month ' . $months_name[$m1 - 1] . ' ' . $y1 . ' </span>';
                    ?>
                  </div>
                  <div class="mb-4 d-flex pt-3" style="justify-content: center;">
                    <input type="submit" name="back" id="submit" value="BACK" class="btn btn-secondary" />
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>

  <?php
  } else {
  ?>

    <!-- Start your project here-->
    <form name="spinninglistrep" action="spinning_pdf.php" method="post">
      <section class="" style="padding-top: 30px;">
        <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="">
                <div class="row g-0">
                  <div id="container_content">
                    <div class="text-black">
                      <h3 class="mb-5 text-uppercase"><?php echo $unitdemo . " Unit Monthly " . $res4[0]['type']; ?> Report</h3>

                      <div class="row">

                        <div class="col-md-2 mb-2">
                          <label class="form-label">Month and Year</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<select class="form-select" name="hdate" >';
                            $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                            $m1 = strval(date('m', strtotime($hdate)));
                            $y1 = strval(date('Y', strtotime($hdate)));
                            echo '<option value="' . $hdate . '">' . $months_name[strval($m1) - 1] . " " . $y1 . '</option></select>' . "\n";
                            //session for mydata4, mydata5
                            $htmldata = $months_name[strval($m1) - 1];
                            $_SESSION["mydata4"] = $htmldata;
                            $htmldata = $y1;
                            $_SESSION["mydata5"] = $htmldata;
                            $aaaa = strval($months_name[strval($m1) - 1] . " " . $y1);
                            // echo "<script> console.log('$aaaa');</script>";
                            $_SESSION["month_year"] = strtoupper($aaaa);
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2 mb-2">
                          <label class="form-label">Work Type</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="' . $res4[0]['type'] . '" readonly/>';
                            $_SESSION["work_type"] = $res4[0]['type'];
                            $htmldata = $res4[0]['type'];
                            $_SESSION["mydata2"] = $htmldata;
                            ?>
                          </div>
                        </div>
                        <div class="col-md-2 mb-2">
                          <label class="form-label">Unit ID</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="unitid" class="form-control form-control-lg" value="' . $unitdemo . '" readonly />';
                            $_SESSION["unit"] = strtoupper($unitdemo);
                            $htmldata = $unitdemo;
                            $_SESSION["mydata1"] = $htmldata;
                            ?>
                          </div>
                        </div>
                      </div>



                      <div class="row">

                        <div class="col-md-2 mb-2">
                          <label class="form-label">Number Of Holidays</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="hno" class="form-control form-control-lg" value="' . $res3[0]['holiday_no'] . '" readonly/>';
                            $_SESSION["holy_days"] = $res3[0]['holiday_no'];
                            ?>
                          </div>
                        </div>

                        <div class="col-md-2 mb-2">
                          <label class="form-label">Number Of Working Days</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="wno" class="form-control form-control-lg" value="' . $res3[0]['total_wd'] . '" readonly />';
                            $_SESSION["working_days"] = $res3[0]['total_wd'];
                            ?>
                          </div>
                        </div>
                      </div>




                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style="padding: 0px 50px 30px 40px;">

          <?php
          if ($res4[0]['type'] == "Spinning") {
            if (sizeof($res1) > 0) {
          ?>

              <table class="table-bordered " id="customers">
                <thead>
                  <tr>
                    <th scope="col">SI No.</th>
                    <th scope="col">Artisan Code</th>
                    <th scope="col">Artisan Name</th>
                    <th scope="col">Variety</th>
                    <th scope="col">No. of Hanks</th>
                    <th scope="col">Rate</th>
                    <th scope="col">Value</th>
                    <th scope="col">Sliver Value</th>
                    <th scope="col">Sliver Consumption</th>
                    <th scope="col">Wages</th>
                    <th scope="col">W/F Fund</th>
                    <th scope="col">w/f Recovery</th>
                    <th scope="col">Adv Recovery</th>
                    <th scope="col">Khadi Cr. Rec.</th>
                    <th scope="col">Covid adv.</th>
                    <th scope="col">LIC</th>
                    <th scope="col">Net wages</th>
                    <th scope="col">Holiday wages</th>
                    <th scope="col">Total Wages</th>
                    <th scope="col">ESI Contr</th>
                    <th scope="col">Yarn Incentive</th>
                    <th scope="col">Minimum Wages</th>
                    <th scope="col">DA Days</th>
                    <th scope="col">DA Wages</th>
                    <th scope="col">Total Minimum Wages</th>
                    <th scope="col">Net Minimum Wages</th>
                    <th scope="col">Attendance</th>
                    <th scope="col">Account Number</th>
                    <th scope="col">IFSC</th>
                    <th scope="col">Bank Name</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  // array initialization to find the variety vise total
                  $res14 = retrieveData("SELECT yarn_code,type from kbk_yarn_variety group by yarn_code", $con);

                  $yarn_array = array(array());
                  for ($k = 0; $k < sizeof($res14); $k++) {
                    $temp4 = $res14[$k]['yarn_code'];
                    $yarn_array[$temp4]['noh'] = 0;
                    $yarn_array[$temp4]['val'] = 0;
                    $yarn_array[$temp4]['sliver'] = 0;
                    $yarn_array[$temp4]['sv'] = 0;
                    $yarn_array[$temp4]['wage'] = 0;
                  }

                  $htmldata = "";
                  for ($i = 0; $i < sizeof($res1); $i++) {

                    echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                    echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';


                    echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';
                    $temp1 = strval($res1[$i]['artisan_id']);

                    $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1", $con);
                    $temp2 = $res6[0]['yarn_id'];
                    $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=1", $con);
                    $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'", $con);

                    echo '<td style="text-align: left;">' . $res7[0]['type'] . '</td>';
                    echo '<td>' . $res6[0]['noh'] . '</td>';
                    echo '<td>' . $res7[0]['rate'] . '</td>';
                    $wfund = 0;
                    if ($res8[0]['wf_yes_no'] == 1) {
                      $wfund = $res7[0]['wf'];
                    }

                    $sar1  = array(
                      'noh'    => $res6[0]['noh'],
                      'sliver' => $res7[0]['sliver'],
                      'rate'   => $res7[0]['rate'],
                      'wf'     => $wfund,
                      'wfrec'  => $res8[0]['wf_rec'],
                      'adrec'  => $res8[0]['ad_rec'],
                      'kcrec'  => $res8[0]['credit_rec'],
                      'covrec' => $res8[0]['cov_rec'],
                      'lic'    => $res8[0]['lic'],
                      'hno'    => $res3[0]['holiday_no'],
                      'wno'    => $res3[0]['total_wd'],
                      'sv'     => $res7[0]['sliver_value'],
                      'val'    => $res7[0]['value'],
                    );

                    $calc1 = spinningWageCalculation($sar1);

                    $sar2 = array(
                      'noh'     => $res6[0]['noh'],
                      'esi'     => $res7[0]['esi'],
                      'yi'      => $res7[0]['yi'],
                      'mw'      => $res7[0]['mw'],
                      'target'  => $res7[0]['target'],
                      'dawages' => $res3[0]['da_wages'],
                      'wage'    => $calc1['wage'],
                    );

                    $da = $res3[0]['total_wd'];
                    $calc2 = spinningIncentivesCalculation($sar2, $da);

                    echo '<td>' . $calc1['val'] . '</td>';
                    echo '<td>' . $calc1['sv'] . '</td>';
                    echo '<td>' . $calc1['sliver'] . '</td>';
                    echo '<td>' . $calc1['wage'] . '</td>';
                    echo  '<td>' . $calc1['wf'] . '</td>';
                    echo  '<td>' . $calc1['wfrec'] . '</td>';
                    echo  '<td>' . $calc1['adrec'] . '</td>';
                    echo  '<td>' . $calc1['kcrec'] . '</td>';
                    echo  '<td>' . $calc1['covrec'] . '</td>';
                    echo  '<td>' . $calc1['lic'] . '</td>';
                    echo  '<td>' . $calc1['netwages'] . '</td>';
                    echo  '<td>' . $calc1['hdwage'] . '</td>';
                    echo  '<td>' . $calc1['totwages'] . '</td>';
                    echo  '<td>' . $calc2['esi'] . '</td>';
                    echo  '<td>' . $calc2['yi'] . '</td>';
                    echo  '<td>' . $calc2['mw'] . '</td>';
                    echo  '<td>' . $calc2['dadays'] . '</td>';
                    echo  '<td>' . $calc2['dawages'] . '</td>';
                    echo  '<td>' . $calc2['totmw'] . '</td>';
                    echo  '<td>' . $calc2['netmw'] . '</td>';
                    echo  '<td>' . $res6[0]['attendance'] . '</td>';
                    echo '<td>' . $res1[$i]['account_no'] . '</td>';
                    echo '<td>' . $res1[$i]['ifsc'] . '</td>';
                    echo '<td>' . $res1[$i]['bank_name'] . '</td></tr>';
                    //echo  '</tr>';

                    $temp5 = $res7[0]['yarn_code'];
                    $yarn_array[$temp5]['noh'] = $yarn_array[$temp5]['noh'] + $res6[0]['noh'];
                    $yarn_array[$temp5]['val'] = $yarn_array[$temp5]['val'] + $calc1['val'];
                    $yarn_array[$temp5]['sliver'] = $yarn_array[$temp5]['sliver'] + $calc1['sliver'];
                    $yarn_array[$temp5]['sv'] = $yarn_array[$temp5]['sv'] + $calc1['sv'];
                    $yarn_array[$temp5]['wage'] = $yarn_array[$temp5]['wage'] + $calc1['wage'];

                    $sum['no'] = $sum['no'] + $res6[0]['noh'];
                    $sum['pr'] = $sum['pr'] + $calc1['sv'];
                    $sum['val'] = $sum['val'] + $calc1['val'];
                    $sum['sliver'] = $sum['sliver'] + $calc1['sliver'];
                    $sum['wage'] = $sum['wage'] + $calc1['wage'];
                    $sum['wf'] = $sum['wf'] + $calc1['wf'];
                    $sum['wfrec'] = $sum['wfrec'] + $calc1['wfrec'];
                    $sum['adrec'] = $sum['adrec'] + $calc1['adrec'];
                    $sum['kcrec'] = $sum['kcrec'] + $calc1['kcrec'];
                    $sum['covrec'] = $sum['covrec'] + $calc1['covrec'];
                    $sum['lic'] = $sum['lic'] + $calc1['lic'];
                    $sum['nw'] = $sum['nw'] + $calc1['netwages'];
                    $sum['hw'] = $sum['hw'] + $calc1['hdwage'];
                    $sum['tw'] = $sum['tw'] + $calc1['totwages'];
                    $sum['esi'] = $sum['esi'] + $calc2['esi'];
                    $sum['yi'] = $sum['yi'] + $calc2['yi'];
                    $sum['mw'] = $sum['mw'] + $calc2['mw'];
                    $sum['dw'] = $sum['dw'] + $calc2['dawages'];
                    $sum['tmw'] = $sum['tmw'] + $calc2['totmw'];
                    $sum['nmw'] = $sum['nmw'] + $calc2['netmw'];

                    $htmldata .= '<tbody><tr><td>' . strval($i + 1) . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                    $htmldata .= '<td>' . $res7[0]['type'] . '</td>';
                    $htmldata .= '<td>' . $res6[0]['noh'] . '</td>';
                    $htmldata .= '<td>' . $res7[0]['rate'] . '</td>';
                    $htmldata .= '<td>' . $calc1['val'] . '</td>';
                    $htmldata .= '<td>' . $calc1['sv'] . '</td>';
                    $htmldata .= '<td>' . $calc1['sliver'] . '</td>';
                    $htmldata .= '<td>' . $calc1['wage'] . '</td>';
                    $htmldata .= '<td>' . $calc1['wf'] . '</td>';
                    $htmldata .=  '<td>' . $calc1['wfrec'] . '</td>';
                    $htmldata .=  '<td>' . $calc1['adrec'] . '</td>';
                    $htmldata .=  '<td>' . $calc1['kcrec'] . '</td>';
                    $htmldata .= '<td>' . $calc1['covrec'] . '</td>';
                    $htmldata .= '<td>' . $calc1['lic'] . '</td>';
                    $htmldata .= '<td>' . $calc1['netwages'] . '</td>';
                    $htmldata .=  '<td>' . $calc1['hdwage'] . '</td>';
                    $htmldata .= '<td>' . $calc1['totwages'] . '</td>';
                    $htmldata .= '<td>' . $calc2['esi'] . '</td>';
                    $htmldata .= '<td>' . $calc2['yi'] . '</td>';
                    $htmldata .= '<td>' . $calc2['mw'] . '</td>';
                    $htmldata .= '<td>' . $calc2['dadays'] . '</td>';
                    $htmldata .= '<td>' . $calc2['dawages'] . '</td>';
                    $htmldata .= '<td>' . $calc2['totmw'] . '</td>';
                    $htmldata .= '<td>' . $calc2['netmw'] . '</td>';
                    $htmldata .= '<td>' . $res6[0]['attendance'] . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['account_no'] . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['ifsc'] . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['bank_name'] . '</td></tr></tbody>';

                    $excel_var[$i][0] = intval($i + 1);
                    $excel_var[$i][1] = $res1[$i]['artisan_code'];
                    $excel_var[$i][2] = $res1[$i]['artisan_name'];
                    $excel_var[$i][3] = $res7[0]['type'];
                    $excel_var[$i][4] = $res6[0]['noh'];
                    $excel_var[$i][5] = $res7[0]['rate'];
                    $excel_var[$i][6] = $calc1['val'];
                    $excel_var[$i][7] = $calc1['sv'];
                    $excel_var[$i][8] = $calc1['sliver'];
                    $excel_var[$i][9] = $calc1['wage'];
                    $excel_var[$i][10] = $calc1['wf'];
                    $excel_var[$i][11] = $calc1['wfrec'];
                    $excel_var[$i][12] = $calc1['adrec'];
                    $excel_var[$i][13] = $calc1['kcrec'];
                    $excel_var[$i][14] = $calc1['covrec'];
                    $excel_var[$i][15] = $calc1['lic'];
                    $excel_var[$i][16] = $calc1['netwages'];
                    $excel_var[$i][17] = $calc1['hdwage'];
                    $excel_var[$i][18] = $calc1['totwages'];
                    $excel_var[$i][19] = $calc2['esi'];
                    $excel_var[$i][20] = $calc2['yi'];
                    $excel_var[$i][21] = $calc2['mw'];
                    $excel_var[$i][22] = $calc2['dadays'];
                    $excel_var[$i][23] = $calc2['dawages'];
                    $excel_var[$i][24] = $calc2['totmw'];
                    $excel_var[$i][25] = $calc2['netmw'];
                    $excel_var[$i][26] = $res6[0]['attendance'];
                    $excel_var[$i][27] = $res1[$i]['account_no'];
                    $excel_var[$i][28] = $res1[$i]['ifsc'];
                    $excel_var[$i][29] = $res1[$i]['bank_name'];
                  }


                  // echo '<tr><td colspan="30"></td></tr>';
                  for ($k = 0; $k < sizeof($res14); $k++) {
                    $temp4 = $res14[$k]['yarn_code'];
                    if ($yarn_array[$temp4]['noh'] != 0) {
                      echo '<tr style="text-align:center;">';
                      echo '<td colspan="2" >' . $res14[$k]['yarn_code'] . '</td>';
                      echo '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['noh'] . '</td>';
                      echo '<td></td>';
                      echo '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['sv'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['sliver'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['wage'] . '</td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td></tr>';
                      $htmldata .= '<tr style="text-align:center;">';
                      $htmldata .= '<td colspan="2" >' . $res14[$k]['yarn_code'] . '</td>';
                      $htmldata .= '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                      $htmldata .= '<td>' . $yarn_array[$temp4]['noh'] . '</td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                      $htmldata .= '<td>' . $yarn_array[$temp4]['sv'] . '</td>';
                      $htmldata .= '<td>' . $yarn_array[$temp4]['sliver'] . '</td>';
                      $htmldata .= '<td>' . $yarn_array[$temp4]['wage'] . '</td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td>';
                      $htmldata .= '<td></td></tr>';
                    }
                  }

                  echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="4">Unit Total</td>';
                  echo "<td>" . $sum['no'] . "</td>";
                  echo "<td> </td>";
                  echo "<td>" . $sum['val'] . "</td>";
                  echo "<td>" . $sum['pr'] . "</td>";
                  echo "<td>" . $sum['sliver'] . "</td>";
                  echo "<td>" . $sum['wage'] . "</td>";
                  echo "<td>" . $sum['wf'] . "</td>";
                  echo "<td>" . $sum['wfrec'] . "</td>";
                  echo "<td>" . $sum['adrec'] . "</td>";
                  echo "<td>" . $sum['kcrec'] . "</td>";
                  echo "<td>" . $sum['covrec'] . "</td>";
                  echo "<td>" . $sum['lic'] . "</td>";
                  echo "<td>" . $sum['nw'] . "</td>";
                  echo "<td>" . $sum['hw'] . "</td>";
                  echo "<td>" . $sum['tw'] . "</td>";
                  echo "<td>" . $sum['esi'] . "</td>";
                  echo "<td>" . $sum['yi'] . "</td>";
                  echo "<td>" . $sum['mw'] . "</td>";
                  echo "<td></td>";
                  echo "<td>" . $sum['dw'] . "</td>";
                  echo "<td>" . $sum['tmw'] . "</td>";
                  echo "<td>" . $sum['nmw'] . "</td>";
                  echo "<td></td>";
                  echo "<td></td>";
                  echo "<td></td>";
                  echo "<td></td></tr>";

                  $htmldata .= '<tr style="text-align: center;"><td colspan="4"><b>Unit Total</b></td>';
                  $htmldata .= "<td><b>" . $sum['no'] . "</b></td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td><b>" . $sum['val'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['pr'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['sliver'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['wage'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['wf'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['wfrec'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['adrec'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['kcrec'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['covrec'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['lic'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['nw'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['hw'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['tw'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['esi'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['yi'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['mw'] . "</b></td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td><b>" . $sum['dw'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['tmw'] . "</b></td>";
                  $htmldata .= "<td><b>" . $sum['nmw'] . "</b></td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td></td></tr>";

                  //$htmldata.= "<td colspan='2'></td></tr></tbody><tfoot></tfoot></table>";



                  $excel_var[$i][4] = $sum['no'];
                  $excel_var[$i][6] = $sum['val'];
                  $excel_var[$i][7] = $sum['pr'];
                  $excel_var[$i][8] = $sum['sliver'];
                  $excel_var[$i][9] = $sum['wage'];
                  $excel_var[$i][10] = $sum['wf'];
                  $excel_var[$i][11] = $sum['wfrec'];
                  $excel_var[$i][12] = $sum['adrec'];
                  $excel_var[$i][13] = $sum['kcrec'];
                  $excel_var[$i][14] = $sum['covrec'];
                  $excel_var[$i][15] = $sum['lic'];
                  $excel_var[$i][16] = $sum['nw'];
                  $excel_var[$i][17] = $sum['hw'];
                  $excel_var[$i][18] = $sum['tw'];
                  $excel_var[$i][19] = $sum['esi'];
                  $excel_var[$i][20] = $sum['yi'];
                  $excel_var[$i][21] = $sum['mw'];
                  $excel_var[$i][23] = $sum['dw'];
                  $excel_var[$i][24] = $sum['tmw'];
                  $excel_var[$i][25] = $sum['nmw'];



                  $_SESSION["twist_total_excel_data"] = $yarn_array;
                  $_SESSION["twisting_excel_data"] = $excel_var;
                  $_SESSION["mydata"] = $htmldata;

                  ?>



                </tbody>
                <tfoot></tfoot>
              </table>



          <?php


            } // if closing of if(sizeof($res1)>0)

          } // if closing of work type spinning
          if ($res4[0]['type'] == "Weaving") {
            if (sizeof($res1) > 0) {
              echo '<table class="table-bordered" id="customers">

        <thead>
                        <tr>
                          <th colspan="29"></th>
                          <th colspan="5" style="text-align:center">Warp</th>
                          <th colspan="5" style="text-align:center">Weft</th>
                          <th></th>
                        </tr>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th scope="col">Artisan Name</th>
                          <th scope="col">Variety</th>
                          <th scope="col">No. of Pieces</th>
                          <th scope="col">Metre</th>
                          <th scope="col">Square Metre</th>
                          <th scope="col">Weaving Type</th>
                          <th scope="col">Rate</th>
                          <th scope="col">Prime Cost</th>
                          <th scope="col">Value</th>
                          
                          <th scope="col">Wages</th>
                          <th scope="col">W/F Fund</th>
                          <th scope="col">w/f Recovery</th>
                          <th scope="col">Adv Recovery</th>
                          <th scope="col">Khadi Cr. Rec.</th>
                          <th scope="col">Covid adv.</th>
                          <th scope="col">LIC</th>
                          <th scope="col">Net wages</th>
                          <th scope="col">Holiday wages</th>
                          <th scope="col">Total Wages</th>
                          <th scope="col">ESI Contr</th>
                          <th scope="col">Warp Yarn Incentive</th>
                          <th scope="col">Weft Yarn Incentive</th>
                          <th scope="col">Minimum Wages</th>
                          <th scope="col">DA Days</th>
                          <th scope="col">DA Wages</th>
                          <th scope="col">Total Minimum Wages</th>
                          <th scope="col">Net Minimum Wages</th>
                          <th scope="col">Grey</th>
                          <th scope="col">Colour</th>
                          <th scope="col">Black</th>
                          <th scope="col">BL</th>
                          <th scope="col">Total</th>
                           <th scope="col">Grey</th>
                          <th scope="col">Colour</th>
                          <th scope="col">Black</th>
                          <th scope="col">BL</th>
                          <th scope="col">Total</th>
                          <th scope="col">Attendance</th>
  
                        </tr>
                      </thead>
        <tbody>';

              $res14 = retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code", $con);
              $wsum = "";
              $yarn_array = array(array());
              for ($k = 0; $k < sizeof($res14); $k++) {
                $temp4 = $res14[$k]['weaving_code'];

                $yarn_array[$temp4]['mtr'] = 0;
                $yarn_array[$temp4]['sqmtr'] = 0;
                $yarn_array[$temp4]['val'] = 0;
                $yarn_array[$temp4]['pc'] = 0;
                $yarn_array[$temp4]['wage'] = 0;
                $yarn_array[$temp4]['warp_grey'] = 0;
                $yarn_array[$temp4]['warp_colour'] = 0;
                $yarn_array[$temp4]['warp_black'] = 0;
                $yarn_array[$temp4]['warp_bl'] = 0;
                $yarn_array[$temp4]['warp_tot'] = 0;
                $yarn_array[$temp4]['weft_grey'] = 0;
                $yarn_array[$temp4]['weft_colour'] = 0;
                $yarn_array[$temp4]['weft_black'] = 0;
                $yarn_array[$temp4]['weft_bl'] = 0;
                $yarn_array[$temp4]['weft_tot'] = 0;
              }
              $htmldata1 = "";

              for ($i = 0; $i < sizeof($res1); $i++) {

                $temp1 = strval($res1[$i]['artisan_id']);
                $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1", $con);
                $temp2 = intval($res9[0]['weaving_id']);

                $temp3 = intval($res9[0]['w_id']);
                $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3", $con);
                $res11 = retrieveData("SELECT weaving_code,type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2", $con);
                $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=2", $con);

                $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2", $con);
                $war3 = [
                  'warp_grey'    => $res13[0]['warp_grey'],
                  'warp_colour'  => $res13[0]['warp_colour'],
                  'warp_black'   => $res13[0]['warp_black'],
                  'warp_bl'      => $res13[0]['warp_bl'],
                  'weft_grey'    => $res13[0]['weft_grey'],
                  'weft_colour'  => $res13[0]['weft_colour'],
                  'weft_black'   => $res13[0]['weft_black'],
                  'weft_bl'      => $res13[0]['weft_bl'],
                ];
                $calc5 = weavingYICalculation($war3);
                $wfund = 0;
                if ($res12[0]['wf_yes_no'] == 1) {
                  $wfund = $res11[0]['wf'];
                }
                $war1 = [
                  'metre'  => $res9[0]['mtr'],
                  'wage'   => $res10[0]['w_wages'],
                  'wf'     => $wfund,
                  'wfrec'  => $res12[0]['wf_rec'],
                  'adrec'  => $res12[0]['ad_rec'],
                  'kcrec'  => $res12[0]['credit_rec'],
                  'covrec' => $res12[0]['cov_rec'],
                  'lic'    => $res12[0]['lic'],
                  'hno'    => $res3[0]['holiday_no'],
                  'wno'    => $res3[0]['total_wd'],
                  'sqmtr'  => $res11[0]['width'],
                  'prime'  => $res11[0]['prime_cost'],
                  'value'  => $res11[0]['rate'],
                ];
                $calc3 = weavingWageCalculation($war1);

                $war2 = [
                  'metre'   => $res9[0]['mtr'],
                  'esi'     => $res11[0]['esi'],
                  'warpyi'  => $res10[0]['warp_yi'],
                  'weftyi'  => $res10[0]['weft_yi'],
                  'mw'      => $res10[0]['min_wages'],
                  'target'  => $res10[0]['target'],
                  'dawages' => $res3[0]['da_wages'],
                  'wage'    => $calc3['wage'],
                  'warp_tot' => $calc5['warp_tot'],
                  'weft_tot' => $calc5['weft_tot'],
                ];
                $da = $res3[0]['total_wd']; //total working days (da days must be <= total working days)
                $calc4 = weavingIncentivesCalculation($war2, $da);
                $calc6 = yarnConsumptionCalculation($war3, $war1['metre']);

                echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';
                echo '<td style="text-align: left;">' . $res11[0]['type'] . '</td>';
                echo '<td>' . $res9[0]['nop'] . '</td>';
                echo '<td>' . $res9[0]['mtr'] . '</td>';
                echo '<td>' . $calc3['sqmtr'] . '</td>';
                echo '<td style="text-align: left;">' . $res10[0]['w_type'] . '</td>';
                echo '<td>' . $res10[0]['w_wages'] . '</td>';
                echo '<td>' . $calc3['prime'] . '</td>';
                echo '<td>' . $calc3['value'] . '</td>';
                echo '<td>' . $calc3['wage'] . '</td>';
                echo '<td>' . $calc3['wf'] . '</td>';
                echo '<td>' . $calc3['wfrec'] . '</td>';
                echo '<td>' . $calc3['adrec'] . '</td>';
                echo '<td>' . $calc3['kcrec'] . '</td>';
                echo '<td>' . $calc3['covrec'] . '</td>';
                echo '<td>' . $calc3['lic'] . '</td>';
                echo '<td>' . $calc3['netwages'] . '</td>';
                echo '<td>' . $calc3['hdwage'] . '</td>';
                echo '<td>' . $calc3['totwages'] . '</td>';
                echo '<td>' . $calc4['esi'] . '</td>';
                echo '<td>' . $calc4['warpyi'] . '</td>';
                echo '<td>' . $calc4['weftyi'] . '</td>';
                echo '<td>' . $calc4['mw'] . '</td>';
                echo '<td>' . $calc4['dadays'] . '</td>';
                echo '<td>' . $calc4['dawages'] . '</td>';
                echo '<td>' . $calc4['totmw'] . '</td>';
                echo '<td>' . $calc4['netmw'] . '</td>';

                echo '<td>' . $calc6['warp_grey'] . '</td>';
                echo '<td>' . $calc6['warp_colour'] . '</td>';
                echo '<td>' . $calc6['warp_black'] . '</td>';
                echo '<td>' . $calc6['warp_bl'] . '</td>';
                echo '<td>' . $calc6['warp_tot'] . '</td>';
                echo '<td>' . $calc6['weft_grey'] . '</td>';
                echo '<td>' . $calc6['weft_colour'] . '</td>';
                echo '<td>' . $calc6['weft_black'] . '</td>';
                echo '<td>' . $calc6['weft_bl'] . '</td>';
                echo '<td>' . $calc6['weft_tot'] . '</td>';

                echo '<td>' . $res9[0]['attendance'] . '</td>';
                echo '</tr>';


                $sum['no'] = $sum['no'] + $res9[0]['nop'];
                $sum['meter'] = $sum['meter'] + $res9[0]['mtr'];
                $sum['sqm'] = $sum['sqm'] + $calc3['sqmtr'];
                $sum['wage'] = $sum['wage'] + $calc3['wage'];
                $sum['val'] = $sum['val'] + $calc3['value'];
                $sum['pr'] = $sum['pr'] + $calc3['prime'];
                $sum['wf'] = $sum['wf'] + $calc3['wf'];
                $sum['wfrec'] = $sum['wfrec'] + $calc3['wfrec'];
                $sum['adrec'] = $sum['adrec'] + $calc3['adrec'];
                $sum['kcrec'] = $sum['kcrec'] + $calc3['kcrec'];
                $sum['covrec'] = $sum['covrec'] + $calc3['covrec'];
                $sum['lic'] = $sum['lic'] + $calc3['lic'];
                $sum['nw'] = $sum['nw'] + $calc3['netwages'];
                $sum['hw'] = $sum['hw'] + $calc3['hdwage'];
                $sum['tw'] = $sum['tw'] + $calc3['totwages'];
                $sum['esi'] = $sum['esi'] + $calc4['esi'];
                $sum['yi'] = $sum['yi'] + $calc4['warpyi'];
                $sum['wyi'] = $sum['wyi'] + $calc4['weftyi'];
                $sum['mw'] = $sum['mw'] + $calc4['mw'];
                $sum['dw'] = $sum['dw'] + $calc4['dawages'];
                $sum['tmw'] = $sum['tmw'] + $calc4['totmw'];
                $sum['nmw'] = $sum['nmw'] + $calc4['netmw'];
                $yarnsum['warp_grey'] = $yarnsum['warp_grey'] + $calc6['warp_grey'];
                $yarnsum['warp_colour'] = $yarnsum['warp_colour'] + $calc6['warp_colour'];
                $yarnsum['warp_black'] = $yarnsum['warp_black'] + $calc6['warp_black'];
                $yarnsum['warp_bl'] = $yarnsum['warp_bl'] + $calc6['warp_bl'];
                $yarnsum['warp_tot'] = $yarnsum['warp_tot'] + $calc6['warp_tot'];
                $yarnsum['weft_grey'] = $yarnsum['weft_grey'] + $calc6['weft_grey'];
                $yarnsum['weft_colour'] = $yarnsum['weft_colour'] + $calc6['weft_colour'];
                $yarnsum['weft_black'] = $yarnsum['weft_black'] + $calc6['weft_black'];
                $yarnsum['weft_bl'] = $yarnsum['weft_bl'] + $calc6['weft_bl'];
                $yarnsum['weft_tot'] = $yarnsum['weft_tot'] + $calc6['weft_tot'];

                $temp5 = $res11[0]['weaving_code'];
                $yarn_array[$temp5]['mtr'] = $yarn_array[$temp5]['mtr'] + $res9[0]['mtr'];
                $yarn_array[$temp5]['sqmtr'] = $yarn_array[$temp5]['sqmtr'] + $calc3['sqmtr'];
                $yarn_array[$temp5]['val'] = $yarn_array[$temp5]['val'] + $calc3['value'];
                $yarn_array[$temp5]['pc'] = $yarn_array[$temp5]['pc'] + $calc3['prime'];
                $yarn_array[$temp5]['wage'] = $yarn_array[$temp5]['wage'] + $calc3['wage'];
                $yarn_array[$temp5]['warp_grey'] = $yarn_array[$temp5]['warp_grey'] + $calc6['warp_grey'];
                $yarn_array[$temp5]['warp_colour'] = $yarn_array[$temp5]['warp_colour'] + $calc6['warp_colour'];
                $yarn_array[$temp5]['warp_black'] = $yarn_array[$temp5]['warp_black'] + $calc6['warp_black'];
                $yarn_array[$temp5]['warp_bl'] = $yarn_array[$temp5]['warp_bl'] + $calc6['warp_bl'];
                $yarn_array[$temp5]['warp_tot'] = $yarn_array[$temp5]['warp_tot'] + $calc6['warp_tot'];
                $yarn_array[$temp5]['weft_grey'] = $yarn_array[$temp5]['weft_grey'] + $calc6['weft_grey'];
                $yarn_array[$temp5]['weft_colour'] = $yarn_array[$temp5]['weft_colour'] + $calc6['weft_colour'];
                $yarn_array[$temp5]['weft_black'] = $yarn_array[$temp5]['weft_black'] + $calc6['weft_black'];
                $yarn_array[$temp5]['weft_bl'] = $yarn_array[$temp5]['weft_bl'] + $calc6['weft_bl'];
                $yarn_array[$temp5]['weft_tot'] = $yarn_array[$temp5]['weft_tot'] + $calc6['weft_tot'];

                $htmldata1 .= '<tbody><tr><td style="overflow: hidden;">' . strval($i + 1) . '</td>';
                $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res1[$i]['artisan_code'] . '</td>';
                $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res1[$i]['artisan_name'] . '</td>';
                $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res11[0]['type'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res9[0]['nop'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res9[0]['mtr'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['sqmtr'] . '</td>';
                $htmldata1 .= '<td colspan="4" style="overflow: hidden;">' . $res10[0]['w_type'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res10[0]['w_wages'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['prime'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['value'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['wage'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['wf'] . '</td>';
                $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['wfrec'] . '</td>';
                $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['adrec'] . '</td>';
                $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['kcrec'] . '</td>';
                $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['covrec'] . '</td>';
                $htmldata1 .= '<td style="overflow: hidden;">' . $calc3['lic'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['netwages'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['hdwage'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc3['totwages'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['esi'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['warpyi'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['weftyi'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['mw'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['dadays'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['dawages'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['totmw'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $calc4['netmw'] . '</td>';
                $htmldata1 .= '<td colspan="2" style="overflow: hidden;">' . $res9[0]['attendance'] . '</td>';
                $htmldata1 .=  '</tr></tbody>';

                $excel_var[$i][0] = strval($i + 1);
                $excel_var[$i][1] = $res1[$i]['artisan_code'];
                $excel_var[$i][2] = $res1[$i]['artisan_name'];
                $excel_var[$i][3] = $res11[0]['type'];
                $excel_var[$i][4] = $res9[0]['nop'];
                $excel_var[$i][5] = $res9[0]['mtr'];
                $excel_var[$i][6] = $calc3['sqmtr'];
                $excel_var[$i][7] = $res10[0]['w_type'];
                $excel_var[$i][8] = $res10[0]['w_wages'];
                $excel_var[$i][9] = $calc3['prime'];
                $excel_var[$i][10] = $calc3['value'];
                $excel_var[$i][11] = $calc3['wage'];
                $excel_var[$i][12] = $calc3['wf'];
                $excel_var[$i][13] = $calc3['wfrec'];
                $excel_var[$i][14] = $calc3['adrec'];
                $excel_var[$i][15] = $calc3['kcrec'];
                $excel_var[$i][16] = $calc3['covrec'];
                $excel_var[$i][17] = $calc3['lic'];
                $excel_var[$i][18] = $calc3['netwages'];
                $excel_var[$i][19] = $calc3['hdwage'];
                $excel_var[$i][20] = $calc3['totwages'];
                $excel_var[$i][21] = $calc4['esi'];
                $excel_var[$i][22] = $calc4['warpyi'];
                $excel_var[$i][23] = $calc4['weftyi'];
                $excel_var[$i][24] = $calc4['mw'];
                $excel_var[$i][25] = $calc4['dadays'];
                $excel_var[$i][26] = $calc4['dawages'];
                $excel_var[$i][27] = $calc4['totmw'];
                $excel_var[$i][28] = $calc4['netmw'];

                $excel_var[$i][29] = $calc6['warp_grey'];
                $excel_var[$i][30] = $calc6['warp_colour'];
                $excel_var[$i][31] = $calc6['warp_black'];
                $excel_var[$i][32] = $calc6['warp_bl'];
                $excel_var[$i][33] = $calc6['warp_tot'];
                $excel_var[$i][34] = $calc6['weft_grey'];
                $excel_var[$i][35] = $calc6['weft_colour'];
                $excel_var[$i][36] = $calc6['weft_black'];
                $excel_var[$i][37] = $calc6['weft_bl'];
                $excel_var[$i][38] = $calc6['weft_tot'];

                $excel_var[$i][39] = $res9[0]['attendance'];
                $excel_var[$i][40] = $res1[$i]['account_no'];
                $excel_var[$i][41] = $res1[$i]['ifsc'];
                $excel_var[$i][42] = $res1[$i]['bank_name'];
              }

              echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
              echo "<td>" . $sum['no'] . "</td>";
              echo "<td>" . $sum['meter'] . "</td>";
              echo "<td>" . $sum['sqm'] . "</td>";
              echo '<td colspan="2"> </td>';
              echo "<td>" . $sum['pr'] . "</td>";
              echo "<td>" . $sum['val'] . "</td>";
              echo "<td>" . $sum['wage'] . "</td>";
              echo "<td>" . $sum['wf'] . "</td>";
              echo "<td>" . $sum['wfrec'] . "</td>";
              echo "<td>" . $sum['adrec'] . "</td>";
              echo "<td>" . $sum['kcrec'] . "</td>";
              echo "<td>" . $sum['covrec'] . "</td>";
              echo "<td>" . $sum['lic'] . "</td>";
              echo "<td>" . $sum['nw'] . "</td>";
              echo "<td>" . $sum['hw'] . "</td>";
              echo "<td>" . $sum['tw'] . "</td>";
              echo "<td>" . $sum['esi'] . "</td>";
              echo "<td>" . $sum['yi'] . "</td>";
              echo "<td>" . $sum['wyi'] . "</td>";
              echo "<td>" . $sum['mw'] . "</td>";
              echo "<td></td>";
              echo "<td>" . $sum['dw'] . "</td>";
              echo "<td>" . $sum['tmw'] . "</td>";
              echo "<td>" . $sum['nmw'] . "</td>";
              echo "<td>" . $yarnsum['warp_grey'] . "</td>";
              echo "<td>" . $yarnsum['warp_colour'] . "</td>";
              echo "<td>" . $yarnsum['warp_black'] . "</td>";
              echo "<td>" . $yarnsum['warp_bl'] . "</td>";
              echo "<td>" . $yarnsum['warp_tot'] . "</td>";
              echo "<td>" . $yarnsum['weft_grey'] . "</td>";
              echo "<td>" . $yarnsum['weft_colour'] . "</td>";
              echo "<td>" . $yarnsum['weft_black'] . "</td>";
              echo "<td>" . $yarnsum['weft_bl'] . "</td>";
              echo "<td>" . $yarnsum['weft_tot'] . "</td>";
              echo "<td> </td></tr>";
              echo '<tr><td colspan="40"></td></tr>';
              for ($k = 0; $k < sizeof($res14); $k++) {
                $temp4 = $res14[$k]['weaving_code'];
                if ($yarn_array[$temp4]['mtr'] != 0) {

                  echo '<tr style="text-align:center;">';
                  echo '<td colspan="2" >' . $res14[$k]['weaving_code'] . '</td>';
                  echo '<td colspan="3" >' . $res14[$k]['type'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['mtr'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['sqmtr'] . '</td>';
                  echo '<td colspan="2"></td>';
                  echo '<td>' . $yarn_array[$temp4]['pc'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['wage'] . '</td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td>' . $yarn_array[$temp4]['warp_grey'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['warp_colour'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['warp_black'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['warp_bl'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['warp_tot'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['weft_grey'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['weft_colour'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['weft_black'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['weft_bl'] . '</td>';
                  echo '<td>' . $yarn_array[$temp4]['weft_tot'] . '</td>';

                  echo '<td></td></tr>';

                  $wsum .= '<tr style="text-align:center;">';
                  $wsum .= '<td colspan="2" >' . $res14[$k]['weaving_code'] . '</td>';
                  $wsum .= '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['mtr'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['sqmtr'] . '</td>';

                  $wsum .= '<td>' . $yarn_array[$temp4]['pc'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['wage'] . '</td>';

                  $wsum .= '<td>' . $yarn_array[$temp4]['warp_grey'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['warp_colour'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['warp_black'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['warp_bl'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['warp_tot'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['weft_grey'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['weft_colour'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['weft_black'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['weft_bl'] . '</td>';
                  $wsum .= '<td>' . $yarn_array[$temp4]['weft_tot'] . '</td>';
                  $wsum .= '</tr>';
                }
              }
              echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Grand Total</td>';
              // echo "<td>".$sum['no']."</td>";
              echo "<td></td>";
              echo "<td>" . $sum['meter'] . "</td>";
              echo "<td>" . $sum['sqm'] . "</td>";
              echo '<td colspan="2"> </td>';
              echo "<td>" . $sum['pr'] . "</td>";
              echo "<td>" . $sum['val'] . "</td>";
              echo "<td>" . $sum['wage'] . "</td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td>" . $yarnsum['warp_grey'] . "</td>";
              echo "<td>" . $yarnsum['warp_colour'] . "</td>";
              echo "<td>" . $yarnsum['warp_black'] . "</td>";
              echo "<td>" . $yarnsum['warp_bl'] . "</td>";
              echo "<td>" . $yarnsum['warp_tot'] . "</td>";
              echo "<td>" . $yarnsum['weft_grey'] . "</td>";
              echo "<td>" . $yarnsum['weft_colour'] . "</td>";
              echo "<td>" . $yarnsum['weft_black'] . "</td>";
              echo "<td>" . $yarnsum['weft_bl'] . "</td>";
              echo "<td>" . $yarnsum['weft_tot'] . "</td>";
              echo "<td> </td></tr>";

              $htmldata1 .= '<tr style="text-align: center;"><td colspan="13"><b>Unit Total</b></td>';

              $htmldata1 .= "<td colspan='2'><b>" . $sum['no'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['meter'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['sqm'] . "</b></td>";
              $htmldata1 .= "<td colspan='6'></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['pr'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['val'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['wage'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['wf'] . "</b></td>";
              $htmldata1 .= "<td><b>" . $sum['wfrec'] . "</b></td>";
              $htmldata1 .= "<td><b>" . $sum['adrec'] . "</b></td>";
              $htmldata1 .= "<td><b>" . $sum['kcrec'] . "</b></td>";
              $htmldata1 .= "<td><b>" . $sum['covrec'] . "</b></td>";
              $htmldata1 .= "<td><b>" . $sum['lic'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['nw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['hw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['tw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['esi'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['yi'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['wyi'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['mw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['dw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['tmw'] . "</b></td>";
              $htmldata1 .= "<td colspan='2'><b>" . $sum['nmw'] . "</b></td>";
              $wsum .= '<tr><td colspan="4"><b>Grand Total</b></td>';
              $wsum .= '<td><b>' . $sum['meter'] . '</b></td>';
              $wsum .= '<td><b>' . $sum['sqm'] . '</b></td>';
              $wsum .= '<td><b>' . $sum['pr'] . '</b></td>';
              $wsum .= '<td><b>' . $sum['val'] . '</b></td>';
              $wsum .= '<td><b>' . $sum['wage'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['warp_grey'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['warp_colour'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['warp_black'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['warp_bl'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['warp_tot'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['weft_grey'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['weft_colour'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['weft_black'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['weft_bl'] . '</b></td>';
              $wsum .= '<td><b>' . $yarnsum['weft_tot'] . '</b></td></tr>';

              $htmldata1 .= "<td colspan='2'></td></tr></tbody><tfoot></tfoot></table>";


              $excel_var[$i][4] = $sum['no'];
              $excel_var[$i][5] = $sum['meter'];
              $excel_var[$i][6] = $sum['sqm'];

              $excel_var[$i][9] = $sum['pr'];
              $excel_var[$i][10] = $sum['val'];
              $excel_var[$i][11] = $sum['wage'];
              $excel_var[$i][12] = $sum['wf'];
              $excel_var[$i][13] = $sum['wfrec'];
              $excel_var[$i][14] = $sum['adrec'];
              $excel_var[$i][15] = $sum['kcrec'];
              $excel_var[$i][16] = $sum['covrec'];
              $excel_var[$i][17] = $sum['lic'];
              $excel_var[$i][18] = $sum['nw'];
              $excel_var[$i][19] = $calc3['hdwage'];
              $excel_var[$i][20] = $calc3['totwages'];
              $excel_var[$i][21] = $calc4['esi'];
              $excel_var[$i][22] = $calc4['warpyi'];
              $excel_var[$i][23] = $calc4['weftyi'];
              $excel_var[$i][24] = $calc4['mw'];

              $excel_var[$i][26] = $sum['dw'];
              $excel_var[$i][27] = $sum['tmw'];
              $excel_var[$i][28] = $sum['nmw'];

              $excel_var[$i][29] = $yarnsum['warp_grey'];
              $excel_var[$i][30] = $yarnsum['warp_colour'];
              $excel_var[$i][31] = $yarnsum['warp_black'];
              $excel_var[$i][32] = $yarnsum['warp_bl'];
              $excel_var[$i][33] = $yarnsum['warp_tot'];
              $excel_var[$i][34] = $yarnsum['weft_grey'];
              $excel_var[$i][35] = $yarnsum['weft_colour'];
              $excel_var[$i][36] = $yarnsum['weft_black'];
              $excel_var[$i][37] = $yarnsum['weft_bl'];
              $excel_var[$i][38] = $yarnsum['weft_tot'];

              $_SESSION["weaving_excel_data"] = $excel_var;
              $_SESSION["weaving_total_excel_data"] = $yarn_array;
              $_SESSION['mydata3'] = $htmldata1;
              $_SESSION['weaving_var_sum'] = $wsum;

              echo "</tbody>
        <tfoot></tfoot>
    </table>";
            }
          }

          if ($res4[0]['type'] == "Preprocessing") {
            if (sizeof($res1) > 0) {
              echo '<table class="table-bordered" id="customers">
    <thead>
      <tr>
        <th colspan="4"></th>
        <th colspan="8" style="text-align:center">Sizing</th>
        <th colspan="8" style="text-align:center">Bobin Winding</th>
        <th colspan="8" style="text-align:center">Warping</th>
        <th colspan="21"></th>
      </tr>
      <tr>
        <th scope="col">SI No.</th>
        <th scope="col">Artisan Code</th>
        <th scope="col">Artisan Name</th>
        <th scope="col">Variety</th>

        <th scope="col">Grey</th>
        <th scope="col">Colour</th>
        <th scope="col">Bl</th>
        <th scope="col">Black</th>
        <th scope="col">Total</th>
        <th scope="col">Rate</th>
        <th scope="col">Wage</th>
        <th scope="col">YI</th>

        <th scope="col">Grey</th>
        <th scope="col">Colour</th>
        <th scope="col">Bl</th>
        <th scope="col">Black</th>
        <th scope="col">Total</th>
        <th scope="col">Rate</th>
        <th scope="col">Wage</th>
        <th scope="col">YI</th>

        <th scope="col">Grey</th>
        <th scope="col">Colour</th>
        <th scope="col">Bl</th>
        <th scope="col">Black</th>
        <th scope="col">Total</th>
        <th scope="col">Rate</th>
        <th scope="col">Wage</th>
        <th scope="col">YI</th>
  
                          <th scope="col">Wages</th>
        <th scope="col">W/F Fund</th>
        <th scope="col">W/F Rec.</th>
        <th scope="col">Adv Rec.</th>
        <th scope="col">Khadi Cr. Rec.</th>
        <th scope="col">Covid adv. Rec.</th>
        <th scope="col">LIC</th>
        <th scope="col">Net wages</th>
        <th scope="col">Holiday wages</th>
        <th scope="col">Total Wages</th>
        <th scope="col">ESI Contr</th>
        <th scope="col">Total Yarn Incentive</th>
        
        <th scope="col">Minimum Wages</th>
        <th scope="col">DA Days</th>
        <th scope="col">DA Wages</th>
        <th scope="col">Total Minimum Wages</th>
        <th scope="col">Net Minimum Wages</th>
        
        <th scope="col">Attendance</th>
        <th scope="col">Account Number</th>
        <th scope="col">IFSC</th>
        <th scope="col">Bank Name</th>

      </tr>
    </thead>
    <tbody>';
              $res14 = retrieveData("SELECT rm_code,type from kbk_preprocessing_variety group by rm_code", $con);
              $yarn_array = array(array());
              for ($k = 0; $k < sizeof($res14); $k++) {
                $temp4 = $res14[$k]['rm_code'];
                $yarn_array[$temp4]['s_grey'] = 0;
                $yarn_array[$temp4]['s_colour'] = 0;
                $yarn_array[$temp4]['s_black'] = 0;
                $yarn_array[$temp4]['s_bl'] = 0;
                $yarn_array[$temp4]['s_tot'] = 0;
                $yarn_array[$temp4]['b_grey'] = 0;
                $yarn_array[$temp4]['b_colour'] = 0;
                $yarn_array[$temp4]['b_black'] = 0;
                $yarn_array[$temp4]['b_bl'] = 0;
                $yarn_array[$temp4]['b_tot'] = 0;
                $yarn_array[$temp4]['w_grey'] = 0;
                $yarn_array[$temp4]['w_colour'] = 0;
                $yarn_array[$temp4]['w_black'] = 0;
                $yarn_array[$temp4]['w_bl'] = 0;
                $yarn_array[$temp4]['w_tot'] = 0;
                $yarn_array[$temp4]['grand_tot'] = 0;
              }
              $htmldata2 = "";

              for ($i = 0; $i < sizeof($res1); $i++) {
                $artid = intval($res1[$i]['artisan_id']);
                $res6 = retrieveData("SELECT wages,attendance,pre_id,preprocessing_id From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'", $con);
                $preid = intval($res6[0]['pre_id']);
                $preprocessingid = intval($res6[0]['preprocessing_id']);
                $res8 = retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3", $con);
                $res9 = retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid", $con);
                $res10 = retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid", $con);

                $par1 = [
                  'grey'  => $res10[0]['s_grey'],
                  'col'   => $res10[0]['s_col'],
                  'bl'    => $res10[0]['s_bl'],
                  'black' => $res10[0]['s_black'],
                ];
                $sizetotnoh = preprocessingTypeWage($par1);

                $par1 = [
                  'grey'  => $res10[0]['b_grey'],
                  'col'   => $res10[0]['b_col'],
                  'bl'    => $res10[0]['b_bl'],
                  'black' => $res10[0]['b_black'],
                ];
                $bobintotnoh = preprocessingTypeWage($par1);

                $par1 = [
                  'grey'  => $res10[0]['w_grey'],
                  'col'   => $res10[0]['w_col'],
                  'bl'    => $res10[0]['w_bl'],
                  'black' => $res10[0]['w_black'],
                ];
                $warptotnoh = preprocessingTypeWage($par1);

                $wfund = 0;
                if ($res8[0]['wf_yes_no'] == 1) {
                  $wfund = $res9[0]['wf'];
                }

                $par2 = [
                  'snoh'  => $sizetotnoh,
                  'bnoh'  => $bobintotnoh,
                  'wnoh'  => $warptotnoh,
                  'swage' => $res9[0]['s_wages'],
                  'bwage' => $res9[0]['b_wages'],
                  'wwage' => $res9[0]['w_wages'],
                  'wf'    => $wfund,
                  'wfrec' => $res8[0]['wf_rec'],
                  'adrec' => $res8[0]['ad_rec'],
                  'kcrec' => $res8[0]['credit_rec'],
                  'covrec' => $res8[0]['cov_rec'],
                  'lic'   => $res8[0]['lic'],
                  'hno'    => $res3[0]['holiday_no'],
                  'wno'    => $res3[0]['total_wd'],
                ];
                $calc7 = preprocessingWageCalculation($par2);

                $par3 = [
                  'snoh'     => $sizetotnoh,
                  'bnoh'     => $bobintotnoh,
                  'wnoh'     => $warptotnoh,
                  'esi'      => $res9[0]['esi'],
                  'syi'      => $res9[0]['s_yi'],
                  'byi'      => $res9[0]['b_yi'],
                  'wyi'      => $res9[0]['w_yi'],
                  'smw'      => $res9[0]['s_min_wages'],
                  'bmw'      => $res9[0]['b_min_wages'],
                  'wmw'      => $res9[0]['w_min_wages'],
                  'starget'  => $res9[0]['s_target'],
                  'btarget'  => $res9[0]['b_target'],
                  'wtarget'  => $res9[0]['w_target'],
                  'dawages'  => $res3[0]['da_wages'],
                  'wage'     => $calc7['wage'],
                  'twd'      => $res3[0]['total_wd'],
                  'tnoh'     => $calc7['totnoh'],
                ];
                $calc8 = preprocessingIncentivesCalculation($par3);

                echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';
                echo '<td style="text-align: left;">' . $res9[0]['type'] . '</td>';
                echo '<td ">' . $res10[0]['s_grey'] . '</td>';
                echo '<td ">' . $res10[0]['s_col'] . '</td>';
                echo '<td ">' . $res10[0]['s_bl'] . '</td>';
                echo '<td ">' . $res10[0]['s_black'] . '</td>';
                echo '<td ">' . $sizetotnoh . '</td>';
                echo '<td ">' . $res9[0]['s_wages'] . '</td>';
                echo '<td ">' . $calc7['swage'] . '</td>';
                echo '<td ">' . $calc8['syi'] . '</td>';

                echo '<td ">' . $res10[0]['b_grey'] . '</td>';
                echo '<td ">' . $res10[0]['b_col'] . '</td>';
                echo '<td ">' . $res10[0]['b_bl'] . '</td>';
                echo '<td ">' . $res10[0]['b_black'] . '</td>';
                echo '<td ">' . $bobintotnoh . '</td>';
                echo '<td ">' . $res9[0]['b_wages'] . '</td>';
                echo '<td ">' . $calc7['bwage'] . '</td>';
                echo '<td ">' . $calc8['byi'] . '</td>';

                echo '<td ">' . $res10[0]['w_grey'] . '</td>';
                echo '<td ">' . $res10[0]['w_col'] . '</td>';
                echo '<td ">' . $res10[0]['w_bl'] . '</td>';
                echo '<td ">' . $res10[0]['w_black'] . '</td>';
                echo '<td ">' . $warptotnoh . '</td>';
                echo '<td ">' . $res9[0]['w_wages'] . '</td>';
                echo '<td ">' . $calc7['wwage'] . '</td>';
                echo '<td ">' . $calc8['wyi'] . '</td>';


                echo '<td ">' . $calc7['wage'] . '</td>';
                echo '<td ">' . $calc7['wf'] . '</td>';
                echo '<td ">' . $res8[0]['wf_rec'] . '</td>';
                echo '<td ">' . $res8[0]['ad_rec'] . '</td>';
                echo '<td ">' . $res8[0]['credit_rec'] . '</td>';
                echo '<td ">' . $res8[0]['cov_rec'] . '</td>';
                echo '<td ">' . $res8[0]['lic'] . '</td>';

                echo '<td ">' . $calc7['netwages'] . '</td>';
                echo '<td ">' . $calc7['hdwage'] . '</td>';
                echo '<td ">' . $calc7['totwages'] . '</td>';
                echo '<td ">' . $calc8['esi'] . '</td>';
                echo '<td ">' . $calc8['yi'] . '</td>';
                echo '<td ">' . $calc8['mw'] . '</td>';
                echo '<td ">' . $calc8['dadays'] . '</td>';
                echo '<td ">' . $calc8['dawages'] . '</td>';
                echo '<td ">' . $calc8['totmw'] . '</td>';
                echo '<td ">' . $calc8['netmw'] . '</td>';
                echo '<td ">' . $res6[0]['attendance'] . '</td>';

                echo '<td>' . $res1[$i]['account_no'] . '</td>';
                echo '<td>' . $res1[$i]['ifsc'] . '</td>';
                echo '<td>' . $res1[$i]['bank_name'] . '</td></tr>';

                $htmldata2 .= '<tbody><tr><td>' . strval($i + 1) . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['type'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_black'] . '</td>';
                $htmldata2 .= '<td>' . $sizetotnoh . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['s_wages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['swage'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['syi'] . '</td>';

                $htmldata2 .= '<td>' . $res10[0]['b_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_black'] . '</td>';
                $htmldata2 .= '<td>' . $bobintotnoh . '</td>';
                $htmldata2 .=  '<td>' . $res9[0]['b_wages'] . '</td>';
                $htmldata2 .=  '<td>' . $calc7['bwage'] . '</td>';
                $htmldata2 .=  '<td>' . $calc8['byi'] . '</td>';

                $htmldata2 .= '<td>' . $res10[0]['w_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_black'] . '</td>';
                $htmldata2 .= '<td>' . $warptotnoh . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['w_wages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['wwage'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['wyi'] . '</td>';

                $htmldata2 .= '<td>' . $calc7['wage'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['wf'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['wf_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['ad_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['credit_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['cov_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['lic'] . '</td>';

                $htmldata2 .= '<td>' . $calc7['netwages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['hdwage'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['totwages'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['esi'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['yi'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['mw'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['dadays'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['dawages'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['totmw'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['netmw'] . '</td>';
                $htmldata2 .= '<td>' . $res6[0]['attendance'] . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['account_no'] . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['ifsc'] . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['bank_name'] . '</td></tr></tbody>';

                $excel_var[$i][0] = strval($i + 1);
                $excel_var[$i][1] = $res1[$i]['artisan_code'];
                $excel_var[$i][2] = $res1[$i]['artisan_name'];
                $excel_var[$i][3] = $res9[0]['type'];
                $excel_var[$i][4] = $res10[0]['s_grey'];
                $excel_var[$i][5] = $res10[0]['s_col'];
                $excel_var[$i][6] = $res10[0]['s_bl'];
                $excel_var[$i][7] = $res10[0]['s_black'];
                $excel_var[$i][8] = $sizetotnoh;
                $excel_var[$i][9] = $res9[0]['s_wages'];
                $excel_var[$i][10] = $calc7['swage'];
                $excel_var[$i][11] = $calc8['syi'];

                $excel_var[$i][12] = $res10[0]['b_grey'];
                $excel_var[$i][13] = $res10[0]['b_col'];
                $excel_var[$i][14] = $res10[0]['b_bl'];
                $excel_var[$i][15] = $res10[0]['b_black'];
                $excel_var[$i][16] = $bobintotnoh;
                $excel_var[$i][17] = $res9[0]['b_wages'];
                $excel_var[$i][18] = $calc7['bwage'];
                $excel_var[$i][19] = $calc8['byi'];

                $excel_var[$i][20] = $res10[0]['w_grey'];
                $excel_var[$i][21] = $res10[0]['w_col'];
                $excel_var[$i][22] = $res10[0]['w_bl'];
                $excel_var[$i][23] = $res10[0]['w_black'];
                $excel_var[$i][24] = $warptotnoh;
                $excel_var[$i][25] = $res9[0]['w_wages'];
                $excel_var[$i][26] = $calc7['wwage'];
                $excel_var[$i][27] = $calc8['wyi'];

                $excel_var[$i][28] = $calc7['wage'];
                $excel_var[$i][29] = $calc7['wf'];
                $excel_var[$i][30] = $res8[0]['wf_rec'];
                $excel_var[$i][31] = $res8[0]['ad_rec'];
                $excel_var[$i][32] = $res8[0]['credit_rec'];
                $excel_var[$i][33] = $res8[0]['cov_rec'];
                $excel_var[$i][34] = $res8[0]['lic'];

                $excel_var[$i][35] = $calc7['netwages'];
                $excel_var[$i][36] = $calc7['hdwage'];
                $excel_var[$i][37] = $calc7['totwages'];
                $excel_var[$i][38] = $calc8['esi'];
                $excel_var[$i][39] = $calc8['yi'];
                $excel_var[$i][40] = $calc8['mw'];
                $excel_var[$i][41] = $calc8['dadays'];
                $excel_var[$i][42] = $calc8['dawages'];
                $excel_var[$i][43] = $calc8['totmw'];
                $excel_var[$i][44] = $calc8['netmw'];
                $excel_var[$i][45] = $res6[0]['attendance'];

                $excel_var[$i][46] = $res1[$i]['account_no'];
                $excel_var[$i][47] = $res1[$i]['ifsc'];
                $excel_var[$i][48] = $res1[$i]['bank_name'];

                $rmsum['s_grey']  = $rmsum['s_grey'] + $res10[0]['s_grey'];
                $rmsum['s_colour'] = $rmsum['s_colour'] + $res10[0]['s_col'];
                $rmsum['s_black'] = $rmsum['s_black'] + $res10[0]['s_black'];
                $rmsum['s_bl']    = $rmsum['s_bl'] + $res10[0]['s_bl'];
                $rmsum['s_tot']   = $rmsum['s_tot'] + $sizetotnoh;
                $rmsum['s_wage']  = $rmsum['s_wage'] + $calc7['swage'];
                $rmsum['s_yi']    = $rmsum['s_yi'] + $calc8['syi'];
                $rmsum['b_grey']  = $rmsum['b_grey'] + $res10[0]['b_grey'];
                $rmsum['b_colour'] = $rmsum['b_colour'] + $res10[0]['b_col'];
                $rmsum['b_black'] = $rmsum['b_black'] + $res10[0]['b_black'];
                $rmsum['b_bl']    = $rmsum['b_bl'] + $res10[0]['b_bl'];
                $rmsum['b_tot']   = $rmsum['b_tot'] + $bobintotnoh;
                $rmsum['b_wage']  = $rmsum['b_wage'] + $calc7['bwage'];
                $rmsum['b_yi']    = $rmsum['b_yi'] + $calc8['byi'];
                $rmsum['w_grey']  = $rmsum['w_grey'] + $res10[0]['w_grey'];
                $rmsum['w_colour'] = $rmsum['w_colour'] + $res10[0]['w_col'];
                $rmsum['w_black'] = $rmsum['w_black'] + $res10[0]['w_black'];
                $rmsum['w_bl']    = $rmsum['w_bl'] + $res10[0]['w_bl'];
                $rmsum['w_tot']   = $rmsum['w_tot'] + $warptotnoh;
                $rmsum['w_wage']  = $rmsum['w_wage'] + $calc7['wwage'];
                $rmsum['w_yi']    = $rmsum['w_yi'] + $calc8['wyi'];

                $sum['wf'] = $sum['wf'] + $calc7['wf'];
                $sum['wfrec'] = $sum['wfrec'] + $calc7['wfrec'];
                $sum['adrec'] = $sum['adrec'] + $calc7['adrec'];
                $sum['kcrec'] = $sum['kcrec'] + $calc7['kcrec'];
                $sum['covrec'] = $sum['covrec'] + $calc7['covrec'];
                $sum['lic'] = $sum['lic'] + $calc7['lic'];
                $sum['nw'] = $sum['nw'] + $calc7['netwages'];
                $sum['hw'] = $sum['hw'] + $calc7['hdwage'];
                $sum['tw'] = $sum['tw'] + $calc7['totwages'];
                $sum['esi'] = $sum['esi'] + $calc8['esi'];
                $sum['yi'] = $sum['yi'] + $calc8['yi'];
                $sum['mw'] = $sum['mw'] + $calc8['mw'];
                $sum['dw'] = $sum['dw'] + $calc8['dawages'];
                $sum['tmw'] = $sum['tmw'] + $calc8['totmw'];
                $sum['nmw'] = $sum['nmw'] + $calc8['netmw'];
                $sum['no'] = $sum['no'] + $calc7['wage'];

                $temp5 = $res9[0]['rm_code'];
                $yarn_array[$temp5]['s_grey'] = $yarn_array[$temp5]['s_grey'] + $res10[0]['s_grey'];
                $yarn_array[$temp5]['s_colour'] = $yarn_array[$temp5]['s_colour'] + $res10[0]['s_col'];
                $yarn_array[$temp5]['s_black'] = $yarn_array[$temp5]['s_black'] + $res10[0]['s_black'];
                $yarn_array[$temp5]['s_bl'] = $yarn_array[$temp5]['s_bl'] + $res10[0]['s_bl'];
                $yarn_array[$temp5]['s_tot'] = $yarn_array[$temp5]['s_tot'] + $sizetotnoh;

                $yarn_array[$temp5]['b_grey'] = $yarn_array[$temp5]['b_grey'] + $res10[0]['b_grey'];
                $yarn_array[$temp5]['b_colour'] = $yarn_array[$temp5]['b_colour'] + $res10[0]['b_col'];
                $yarn_array[$temp5]['b_black'] = $yarn_array[$temp5]['b_black'] + $res10[0]['b_black'];
                $yarn_array[$temp5]['b_bl'] = $yarn_array[$temp5]['b_bl'] + $res10[0]['b_bl'];
                $yarn_array[$temp5]['b_tot'] = $yarn_array[$temp5]['b_tot'] + $bobintotnoh;

                $yarn_array[$temp5]['w_grey'] = $yarn_array[$temp5]['w_grey'] + $res10[0]['w_grey'];
                $yarn_array[$temp5]['w_colour'] = $yarn_array[$temp5]['w_colour'] + $res10[0]['w_col'];
                $yarn_array[$temp5]['w_black'] = $yarn_array[$temp5]['w_black'] + $res10[0]['w_black'];
                $yarn_array[$temp5]['w_bl'] = $yarn_array[$temp5]['w_bl'] + $res10[0]['w_bl'];
                $yarn_array[$temp5]['w_tot'] = $yarn_array[$temp5]['w_tot'] + $warptotnoh;
                $yarn_array[$temp5]['grand_tot'] = $sizetotnoh + $bobintotnoh + $warptotnoh;
              }

              echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
              echo '<td>' . $rmsum['s_grey'] . '</td>';
              echo '<td>' . $rmsum['s_colour'] . '</td>';
              echo '<td>' . $rmsum['s_bl'] . '</td>';
              echo '<td>' . $rmsum['s_black'] . '</td>';
              echo '<td>' . $rmsum['s_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['s_wage'] . '</td>';
              echo '<td>' . $rmsum['s_yi'] . '</td>';
              echo '<td>' . $rmsum['b_grey'] . '</td>';
              echo '<td>' . $rmsum['b_colour'] . '</td>';
              echo '<td>' . $rmsum['b_bl'] . '</td>';
              echo '<td>' . $rmsum['b_black'] . '</td>';
              echo '<td>' . $rmsum['b_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['b_wage'] . '</td>';
              echo '<td>' . $rmsum['b_yi'] . '</td>';
              echo '<td>' . $rmsum['w_grey'] . '</td>';
              echo '<td>' . $rmsum['w_colour'] . '</td>';
              echo '<td>' . $rmsum['w_bl'] . '</td>';
              echo '<td>' . $rmsum['w_black'] . '</td>';
              echo '<td>' . $rmsum['w_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['w_wage'] . '</td>';
              echo '<td>' . $rmsum['w_yi'] . '</td>';
              echo '<td>' . $sum['no'] . '</td>';
              echo "<td>" . $sum['wf'] . "</td>";
              echo "<td>" . $sum['wfrec'] . "</td>";
              echo "<td>" . $sum['adrec'] . "</td>";
              echo "<td>" . $sum['kcrec'] . "</td>";
              echo "<td>" . $sum['covrec'] . "</td>";
              echo "<td>" . $sum['lic'] . "</td>";
              echo "<td>" . $sum['nw'] . "</td>";
              echo "<td>" . $sum['hw'] . "</td>";
              echo "<td>" . $sum['tw'] . "</td>";
              echo "<td>" . $sum['esi'] . "</td>";
              echo "<td> </td>";
              echo "<td>" . $sum['mw'] . "</td>";
              echo "<td></td>";
              echo "<td>" . $sum['dw'] . "</td>";
              echo "<td>" . $sum['tmw'] . "</td>";
              echo "<td>" . $sum['nmw'] . "</td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td>";
              echo "<td></td></tr>";

              $htmldata2 .= '<tr style="text-align:center;"><td colspan="4"><b>Unit Total</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['no'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['wf'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['wfrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['adrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['kcrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['covrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['lic'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['nw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['hw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['tw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['esi'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $sum['mw'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $sum['dw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['tmw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['nmw'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td></td></tr>';

              $excel_var[$i][4] = $rmsum['s_grey'];
              $excel_var[$i][5] = $rmsum['s_colour'];
              $excel_var[$i][6] = $rmsum['s_bl'];
              $excel_var[$i][7] = $rmsum['s_black'];
              $excel_var[$i][8] = $rmsum['s_tot'];

              $excel_var[$i][10] = $rmsum['s_wage'];
              $excel_var[$i][11] = $rmsum['s_yi'];
              $excel_var[$i][12] = $rmsum['b_grey'];
              $excel_var[$i][13] = $rmsum['b_colour'];
              $excel_var[$i][14] = $rmsum['b_bl'];
              $excel_var[$i][15] = $rmsum['b_black'];
              $excel_var[$i][16] = $rmsum['b_tot'];

              $excel_var[$i][18] = $rmsum['b_wage'];
              $excel_var[$i][19] = $rmsum['b_yi'];
              $excel_var[$i][20] = $rmsum['w_grey'];
              $excel_var[$i][21] = $rmsum['w_colour'];
              $excel_var[$i][22] = $rmsum['w_bl'];
              $excel_var[$i][23] = $rmsum['w_black'];
              $excel_var[$i][24] = $rmsum['w_tot'];

              $excel_var[$i][26] = $rmsum['w_wage'];
              $excel_var[$i][27] = $rmsum['w_yi'];
              $excel_var[$i][28] = $sum['no'];
              $excel_var[$i][29] = $sum['wf'];
              $excel_var[$i][30] = $sum['wfrec'];
              $excel_var[$i][31] = $sum['adrec'];
              $excel_var[$i][32] = $sum['kcrec'];
              $excel_var[$i][33] = $sum['covrec'];
              $excel_var[$i][34] = $sum['lic'];
              $excel_var[$i][35] = $sum['nw'];
              $excel_var[$i][36] = $sum['hw'];
              $excel_var[$i][37] = $sum['tw'];
              $excel_var[$i][38] = $sum['esi'];
              $excel_var[$i][39] = $sum['yi'];
              $excel_var[$i][40] = $sum['mw'];

              $excel_var[$i][42] = $sum['dw'];
              $excel_var[$i][43] = $sum['tmw'];
              $excel_var[$i][44] = $sum['nmw'];


              for ($k = 0; $k < sizeof($res14); $k++) {
                $temp4 = $res14[$k]['rm_code'];
                if ($yarn_array[$temp4]['grand_tot'] != 0) {
                  echo '<tr style="text-align:center;">';
                  echo '<td colspan="2" >' . $res14[$k]['rm_code'] . '</td>';
                  echo '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['s_grey'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['s_colour'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['s_bl'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['s_black'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['s_tot'] . '</td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';

                  echo '<td>' . $yarn_array[$temp5]['b_grey'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['b_colour'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['b_bl'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['b_black'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['b_tot'] . '</td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';

                  echo '<td>' . $yarn_array[$temp5]['w_grey'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['w_colour'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['w_bl'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['w_black'] . '</td>';
                  echo '<td>' . $yarn_array[$temp5]['w_tot'] . '</td>';

                  echo '<td colspan="24"></td>';
                  echo '</tr>';

                  $htmldata2 .= '<tr style="text-align:center;">';
                  $htmldata2 .= '<td colspan="2" >' . $res14[$k]['rm_code'] . '</td>';
                  $htmldata2 .= '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_tot'] . '</td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';

                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_tot'] . '</td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';

                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_tot'] . '</td>';

                  $htmldata2 .= '<td colspan="24"></td>';
                  $htmldata2 .= '</tr>';
                }
              }
              for ($k = 0; $k < sizeof($res14); $k++) {
                $temp4 = $res14[$k]['rm_code'];
                if ($yarn_array[$temp4]['grand_tot'] != 0) {
                  echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Grand Total</td>';
                  echo '<td>' . $rmsum['s_grey'] . '</td>';
                  echo '<td>' . $rmsum['s_colour'] . '</td>';
                  echo '<td>' . $rmsum['s_bl'] . '</td>';
                  echo '<td>' . $rmsum['s_black'] . '</td>';
                  echo '<td>' . $rmsum['s_tot'] . '</td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td>' . $rmsum['b_grey'] . '</td>';
                  echo '<td>' . $rmsum['b_colour'] . '</td>';
                  echo '<td>' . $rmsum['b_bl'] . '</td>';
                  echo '<td>' . $rmsum['b_black'] . '</td>';
                  echo '<td>' . $rmsum['b_tot'] . '</td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td></td>';
                  echo '<td>' . $rmsum['w_grey'] . '</td>';
                  echo '<td>' . $rmsum['w_colour'] . '</td>';
                  echo '<td>' . $rmsum['w_bl'] . '</td>';
                  echo '<td>' . $rmsum['w_black'] . '</td>';
                  echo '<td>' . $rmsum['w_tot'] . '</td>';

                  echo '<td colspan="24"></td></tr>';

                  $htmldata2 .= '<tr style="text-align:center;"><td colspan="4"><b>Grand Total</b></td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['s_tot'] . '</td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';

                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['b_tot'] . '</td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';
                  $htmldata2 .= '<td></td>';

                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_grey'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_colour'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_bl'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_black'] . '</td>';
                  $htmldata2 .= '<td>' . $yarn_array[$temp5]['w_tot'] . '</td>';

                  $htmldata2 .= '<td colspan="24"></td>';
                  $htmldata2 .= '</tr>';
                }
              }

              $_SESSION["preprocessing_excel_data"] = $excel_var;
              $_SESSION["rm_variety_excel_data"] = $yarn_array;
              $_SESSION["mydata"] = $htmldata2;

              echo "</tbody>
    <tfoot></tfoot>
  </table>";
            }
          }
          if ($res4[0]['type'] == "Twisting") {
            if (sizeof($res1) > 0) {
          ?>

              <table class="table-bordered " id="customers">
                <thead>
                  <tr>
                    <th scope="col">SI No.</th>
                    <th scope="col">Artisan Code</th>
                    <th scope="col">Artisan Name</th>
                    <th scope="col">Variety</th>
                    <th scope="col">No. of Hanks</th>
                    <th scope="col">Rate</th>
                    <th scope="col">Value</th>
                    <th scope="col">Basic Yarn Value</th>
                    <th scope="col">Yarn Consumption</th>
                    <th scope="col">Wages</th>
                    <th scope="col">W/F Fund</th>
                    <th scope="col">w/f Recovery</th>
                    <th scope="col">Adv Recovery</th>
                    <th scope="col">Khadi Cr. Rec.</th>
                    <th scope="col">Covid adv.</th>
                    <th scope="col">LIC</th>
                    <th scope="col">Net wages</th>
                    <th scope="col">Holiday wages</th>
                    <th scope="col">Total Wages</th>
                    <th scope="col">ESI Contr</th>
                    <th scope="col">Yarn Incentive</th>
                    <th scope="col">Minimum Wages</th>
                    <th scope="col">DA Days</th>
                    <th scope="col">DA Wages</th>
                    <th scope="col">Total Minimum Wages</th>
                    <th scope="col">Net Minimum Wages</th>
                    <th scope="col">Attendance</th>
                    <th scope="col">Account Number</th>
                    <th scope="col">IFSC</th>
                    <th scope="col">Bank Name</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  // array initialization to find the variety vise total
                  $res14 = retrieveData("SELECT twisting_code,type from kbk_twisting_variety group by twisting_code", $con);

                  $yarn_array = array(array());
                  for ($k = 0; $k < sizeof($res14); $k++) {
                    $temp4 = $res14[$k]['twisting_code'];
                    $yarn_array[$temp4]['noh'] = 0;
                    $yarn_array[$temp4]['val'] = 0;
                    $yarn_array[$temp4]['yarn'] = 0;
                    $yarn_array[$temp4]['yv'] = 0;
                    $yarn_array[$temp4]['wage'] = 0;
                  }

                  $htmldata3 = "";
                  for ($i = 0; $i < sizeof($res1); $i++) {

                    echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                    echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';


                    echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';
                    $temp1 = strval($res1[$i]['artisan_id']);

                    $res6 =  retrieveData("SELECT wages,attendance,noh,twist_id From kbk_twisting where s_date='$hdate' and artisan_id=$temp1", $con);
                    $temp2 = $res6[0]['twist_id'];
                    $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1 and work_id=4", $con);
                    $res7 = retrieveData("SELECT * From kbk_twisting_variety where twist_id='$temp2'", $con);

                    echo '<td style="text-align: left;">' . $res7[0]['type'] . '</td>';
                    echo '<td>' . $res6[0]['noh'] . '</td>';
                    echo '<td>' . $res7[0]['rate'] . '</td>';
                    $wfund = 0;
                    if ($res8[0]['wf_yes_no'] == 1) {
                      $wfund = $res7[0]['wf'];
                    }

                    $sar1  = array(
                      'noh'    => $res6[0]['noh'],
                      'yarn' => $res7[0]['yarn'],
                      'rate'   => $res7[0]['rate'],
                      'wf'     => $wfund,
                      'wfrec'  => $res8[0]['wf_rec'],
                      'adrec'  => $res8[0]['ad_rec'],
                      'kcrec'  => $res8[0]['credit_rec'],
                      'covrec' => $res8[0]['cov_rec'],
                      'lic'    => $res8[0]['lic'],
                      'hno'    => $res3[0]['holiday_no'],
                      'wno'    => $res3[0]['total_wd'],
                      'yv'     => $res7[0]['yarn_value'],
                      'val'    => $res7[0]['value'],
                    );

                    $calc1 = twistingWageCalculation($sar1);

                    $sar2 = array(
                      'noh'     => $res6[0]['noh'],
                      'esi'     => $res7[0]['esi'],
                      'yi'      => $res7[0]['yi'],
                      'mw'      => $res7[0]['mw'],
                      'target'  => $res7[0]['target'],
                      'dawages' => $res3[0]['da_wages'],
                      'wage'    => $calc1['wage'],
                    );

                    $da = $res3[0]['total_wd'];
                    $calc2 = twistingIncentivesCalculation($sar2, $da);

                    echo '<td>' . $calc1['val'] . '</td>';
                    echo '<td>' . $calc1['yv'] . '</td>';
                    echo '<td>' . $calc1['yarn'] . '</td>';
                    echo '<td>' . $calc1['wage'] . '</td>';
                    echo  '<td>' . $calc1['wf'] . '</td>';
                    echo  '<td>' . $calc1['wfrec'] . '</td>';
                    echo  '<td>' . $calc1['adrec'] . '</td>';
                    echo  '<td>' . $calc1['kcrec'] . '</td>';
                    echo  '<td>' . $calc1['covrec'] . '</td>';
                    echo  '<td>' . $calc1['lic'] . '</td>';
                    echo  '<td>' . $calc1['netwages'] . '</td>';
                    echo  '<td>' . $calc1['hdwage'] . '</td>';
                    echo  '<td>' . $calc1['totwages'] . '</td>';
                    echo  '<td>' . $calc2['esi'] . '</td>';
                    echo  '<td>' . $calc2['yi'] . '</td>';
                    echo  '<td>' . $calc2['mw'] . '</td>';
                    echo  '<td>' . $calc2['dadays'] . '</td>';
                    echo  '<td>' . $calc2['dawages'] . '</td>';
                    echo  '<td>' . $calc2['totmw'] . '</td>';
                    echo  '<td>' . $calc2['netmw'] . '</td>';
                    echo  '<td>' . $res6[0]['attendance'] . '</td>';
                    echo '<td>' . $res1[$i]['account_no'] . '</td>';
                    echo '<td>' . $res1[$i]['ifsc'] . '</td>';
                    echo '<td>' . $res1[$i]['bank_name'] . '</td></tr>';
                    //echo  '</tr>';

                    $temp5 = $res7[0]['twisting_code'];
                    $yarn_array[$temp5]['noh'] = $yarn_array[$temp5]['noh'] + $res6[0]['noh'];
                    $yarn_array[$temp5]['val'] = $yarn_array[$temp5]['val'] + $calc1['val'];
                    $yarn_array[$temp5]['yarn'] = $yarn_array[$temp5]['yarn'] + $calc1['yarn'];
                    $yarn_array[$temp5]['yv'] = $yarn_array[$temp5]['yv'] + $calc1['yv'];
                    $yarn_array[$temp5]['wage'] = $yarn_array[$temp5]['wage'] + $calc1['wage'];

                    $sum['no'] = $sum['no'] + $res6[0]['noh'];
                    $sum['pr'] = $sum['pr'] + $calc1['yv'];
                    $sum['val'] = $sum['val'] + $calc1['val'];
                    $sum['yarn'] = $sum['yarn'] + $calc1['yarn'];
                    $sum['wage'] = $sum['wage'] + $calc1['wage'];
                    $sum['wf'] = $sum['wf'] + $calc1['wf'];
                    $sum['wfrec'] = $sum['wfrec'] + $calc1['wfrec'];
                    $sum['adrec'] = $sum['adrec'] + $calc1['adrec'];
                    $sum['kcrec'] = $sum['kcrec'] + $calc1['kcrec'];
                    $sum['covrec'] = $sum['covrec'] + $calc1['covrec'];
                    $sum['lic'] = $sum['lic'] + $calc1['lic'];
                    $sum['nw'] = $sum['nw'] + $calc1['netwages'];
                    $sum['hw'] = $sum['hw'] + $calc1['hdwage'];
                    $sum['tw'] = $sum['tw'] + $calc1['totwages'];
                    $sum['esi'] = $sum['esi'] + $calc2['esi'];
                    $sum['yi'] = $sum['yi'] + $calc2['yi'];
                    $sum['mw'] = $sum['mw'] + $calc2['mw'];
                    $sum['dw'] = $sum['dw'] + $calc2['dawages'];
                    $sum['tmw'] = $sum['tmw'] + $calc2['totmw'];
                    $sum['nmw'] = $sum['nmw'] + $calc2['netmw'];

                    $htmldata3 .= '<tbody><tr><td>' . strval($i + 1) . '</td>';
                    $htmldata3 .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                    $htmldata3 .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                    $htmldata3 .= '<td>' . $res7[0]['type'] . '</td>';
                    $htmldata3 .= '<td>' . $res6[0]['noh'] . '</td>';
                    $htmldata3 .= '<td>' . $res7[0]['rate'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['val'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['yv'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['yarn'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['wage'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['wf'] . '</td>';
                    $htmldata3 .=  '<td>' . $calc1['wfrec'] . '</td>';
                    $htmldata3 .=  '<td>' . $calc1['adrec'] . '</td>';
                    $htmldata3 .=  '<td>' . $calc1['kcrec'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['covrec'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['lic'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['netwages'] . '</td>';
                    $htmldata3 .=  '<td>' . $calc1['hdwage'] . '</td>';
                    $htmldata3 .= '<td>' . $calc1['totwages'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['esi'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['yi'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['mw'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['dadays'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['dawages'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['totmw'] . '</td>';
                    $htmldata3 .= '<td>' . $calc2['netmw'] . '</td>';
                    $htmldata3 .= '<td>' . $res6[0]['attendance'] . '</td>';
                    $htmldata3 .= '<td>' . $res1[$i]['account_no'] . '</td>';
                    $htmldata3 .= '<td>' . $res1[$i]['ifsc'] . '</td>';
                    $htmldata3 .= '<td>' . $res1[$i]['bank_name'] . '</td></tr></tbody>';

                    $excel_var[$i][0] = intval($i + 1);
                    $excel_var[$i][1] = $res1[$i]['artisan_code'];
                    $excel_var[$i][2] = $res1[$i]['artisan_name'];
                    $excel_var[$i][3] = $res7[0]['type'];
                    $excel_var[$i][4] = $res6[0]['noh'];
                    $excel_var[$i][5] = $res7[0]['rate'];
                    $excel_var[$i][6] = $calc1['val'];
                    $excel_var[$i][7] = $calc1['yv'];
                    $excel_var[$i][8] = $calc1['yarn'];
                    $excel_var[$i][9] = $calc1['wage'];
                    $excel_var[$i][10] = $calc1['wf'];
                    $excel_var[$i][11] = $calc1['wfrec'];
                    $excel_var[$i][12] = $calc1['adrec'];
                    $excel_var[$i][13] = $calc1['kcrec'];
                    $excel_var[$i][14] = $calc1['covrec'];
                    $excel_var[$i][15] = $calc1['lic'];
                    $excel_var[$i][16] = $calc1['netwages'];
                    $excel_var[$i][17] = $calc1['hdwage'];
                    $excel_var[$i][18] = $calc1['totwages'];
                    $excel_var[$i][19] = $calc2['esi'];
                    $excel_var[$i][20] = $calc2['yi'];
                    $excel_var[$i][21] = $calc2['mw'];
                    $excel_var[$i][22] = $calc2['dadays'];
                    $excel_var[$i][23] = $calc2['dawages'];
                    $excel_var[$i][24] = $calc2['totmw'];
                    $excel_var[$i][25] = $calc2['netmw'];
                    $excel_var[$i][26] = $res6[0]['attendance'];
                    $excel_var[$i][27] = $res1[$i]['account_no'];
                    $excel_var[$i][28] = $res1[$i]['ifsc'];
                    $excel_var[$i][29] = $res1[$i]['bank_name'];
                  }


                  // echo '<tr><td colspan="30"></td></tr>';
                  for ($k = 0; $k < sizeof($res14); $k++) {
                    $temp4 = $res14[$k]['twisting_code'];
                    if ($yarn_array[$temp4]['noh'] != 0) {
                      echo '<tr style="text-align:center;">';
                      echo '<td colspan="2" >' . $res14[$k]['twisting_code'] . '</td>';
                      echo '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['noh'] . '</td>';
                      echo '<td></td>';
                      echo '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['yv'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['yarn'] . '</td>';
                      echo '<td>' . $yarn_array[$temp4]['wage'] . '</td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td>';
                      echo '<td></td></tr>';
                      $htmldata3 .= '<tr style="text-align:center;">';
                      $htmldata3 .= '<td colspan="2" >' . $res14[$k]['twisting_code'] . '</td>';
                      $htmldata3 .= '<td colspan="2" >' . $res14[$k]['type'] . '</td>';
                      $htmldata3 .= '<td>' . $yarn_array[$temp4]['noh'] . '</td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td>' . $yarn_array[$temp4]['val'] . '</td>';
                      $htmldata3 .= '<td>' . $yarn_array[$temp4]['yv'] . '</td>';
                      $htmldata3 .= '<td>' . $yarn_array[$temp4]['yarn'] . '</td>';
                      $htmldata3 .= '<td>' . $yarn_array[$temp4]['wage'] . '</td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td>';
                      $htmldata3 .= '<td></td></tr>';
                    }
                  }

                  echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="4">Unit Total</td>';
                  echo "<td>" . $sum['no'] . "</td>";
                  echo "<td> </td>";
                  echo "<td>" . $sum['val'] . "</td>";
                  echo "<td>" . $sum['pr'] . "</td>";
                  echo "<td>" . $sum['yarn'] . "</td>";
                  echo "<td>" . $sum['wage'] . "</td>";
                  echo "<td>" . $sum['wf'] . "</td>";
                  echo "<td>" . $sum['wfrec'] . "</td>";
                  echo "<td>" . $sum['adrec'] . "</td>";
                  echo "<td>" . $sum['kcrec'] . "</td>";
                  echo "<td>" . $sum['covrec'] . "</td>";
                  echo "<td>" . $sum['lic'] . "</td>";
                  echo "<td>" . $sum['nw'] . "</td>";
                  echo "<td>" . $sum['hw'] . "</td>";
                  echo "<td>" . $sum['tw'] . "</td>";
                  echo "<td>" . $sum['esi'] . "</td>";
                  echo "<td>" . $sum['yi'] . "</td>";
                  echo "<td>" . $sum['mw'] . "</td>";
                  echo "<td></td>";
                  echo "<td>" . $sum['dw'] . "</td>";
                  echo "<td>" . $sum['tmw'] . "</td>";
                  echo "<td>" . $sum['nmw'] . "</td>";
                  echo "<td></td>";
                  echo "<td></td>";
                  echo "<td></td>";
                  echo "<td></td></tr>";

                  $htmldata3 .= '<tr style="text-align: center;"><td colspan="4"><b>Unit Total</b></td>';
                  $htmldata3 .= "<td><b>" . $sum['no'] . "</b></td>";
                  $htmldata3 .= "<td> </td>";
                  $htmldata3 .= "<td><b>" . $sum['val'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['pr'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['yarn'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['wage'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['wf'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['wfrec'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['adrec'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['kcrec'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['covrec'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['lic'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['nw'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['hw'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['tw'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['esi'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['yi'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['mw'] . "</b></td>";
                  $htmldata3 .= "<td> </td>";
                  $htmldata3 .= "<td><b>" . $sum['dw'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['tmw'] . "</b></td>";
                  $htmldata3 .= "<td><b>" . $sum['nmw'] . "</b></td>";
                  $htmldata3 .= "<td> </td>";
                  $htmldata3 .= "<td> </td>";
                  $htmldata3 .= "<td> </td>";
                  $htmldata3 .= "<td></td></tr>";

                  //$htmldata.= "<td colspan='2'></td></tr></tbody><tfoot></tfoot></table>";



                  $excel_var[$i][4] = $sum['no'];
                  $excel_var[$i][6] = $sum['val'];
                  $excel_var[$i][7] = $sum['pr'];
                  $excel_var[$i][8] = $sum['yarn'];
                  $excel_var[$i][9] = $sum['wage'];
                  $excel_var[$i][10] = $sum['wf'];
                  $excel_var[$i][11] = $sum['wfrec'];
                  $excel_var[$i][12] = $sum['adrec'];
                  $excel_var[$i][13] = $sum['kcrec'];
                  $excel_var[$i][14] = $sum['covrec'];
                  $excel_var[$i][15] = $sum['lic'];
                  $excel_var[$i][16] = $sum['nw'];
                  $excel_var[$i][17] = $sum['hw'];
                  $excel_var[$i][18] = $sum['tw'];
                  $excel_var[$i][19] = $sum['esi'];
                  $excel_var[$i][20] = $sum['yi'];
                  $excel_var[$i][21] = $sum['mw'];
                  $excel_var[$i][23] = $sum['dw'];
                  $excel_var[$i][24] = $sum['tmw'];
                  $excel_var[$i][25] = $sum['nmw'];



                  $_SESSION["yarn_total_excel_data"] = $yarn_array;
                  $_SESSION["twisting_excel_data"] = $excel_var;
                  $_SESSION["mydata"] = $htmldata3;

                  ?>



                </tbody>
                <tfoot></tfoot>
              </table>



          <?php


            } // if closing of if(sizeof($res1)>0)

          } 
          ?>
        </div>
        </div>
        <div class="text-center" id="print_container">
          <!--  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">-->
          <a target="_blank" class="btn btn-info btn_print" href="fullreport_unit_excel.php" id="excel_btn">Wages Report</a>
          <!-- print buton -->
          <!--<button class="btn btn-info btn_print" id="print_btn">Print As PDF</button>-->
          <a target="_blank" class="btn btn-info btn_print" href="./calcview/full_report_unit_pdf.php">Print AS PDF</a>

          <a href="finalize.php?month=<?= $m1 ?>&year=<?= $y1 ?>&work_type=<?= $_SESSION['mydata2'] ?>" class="btn btn-info">Finalize</a>
        </div>
        <!-- <div class="text-center" >
  <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
<a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
<a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
</div>
 -->
        <!-- <div class="text-center" >
 
<a target="_blank" class="btn btn-info btn_print" href="./calcview/full_report_unit_pdf.php">Wages Report</a>
</div>-->


      </section>

    </form>
  <?php
  }

  ?>
  <!-- <script>
  const printbtn=document.getElementById('print_btn');
  printbtn.addEventListener('click',openPrintDialog);
  function openPrintDialog(){
    window.print();
  }
  </script> -->

  <!-- End your project here-->

  <!-- MDB -->
  <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
  <!-- Custom scripts -->
  <script type="text/javascript"></script>
</body>

</html>