<?php

$res3 = retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'", $con);
$res4 = retrieveData("SELECT type From kbk_work_type where work_id=$worktype", $con);
$res5 = retrieveData("SELECT unit_code,unit_name From kbk_unit where unit_code='$unit'", $con);
$unitdemo = $res5[0]['unit_name'];
$a = strtolower($res4[0]['type']);
$b = "kbk_" . $a;
$res1 = retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from $b where s_date='$hdate')", $con);

$sum = array(
  'no'       => 0,
  'mtr'      => 0,
  'wage'     => 0,
  'wf'       => 0,
  'wfrec'    => 0,
  'adrec'    => 0,
  'kcrec'    => 0,
  'covrec'   => 0,
  'lic'      => 0,
  'nw'       => 0,
  'hw'       => 0,
  'tw'       => 0,
  'mw'       => 0,
  'dw'       => 0,
  'tmw'      => 0,
  'nmw'      => 0,
);

$rmsum = array(
  's_grey'    => 0,
  's_colour'  => 0,
  's_black'   => 0,
  's_bl'      => 0,
  's_tot'     => 0,
  's_wage'    => 0,
  's_yi'      => 0,
  'b_grey'    => 0,
  'b_colour'  => 0,
  'b_black'   => 0,
  'b_bl'      => 0,
  'b_tot'     => 0,
  'b_wage'    => 0,
  'b_yi'      => 0,
  'w_grey'    => 0,
  'w_colour'  => 0,
  'w_black'   => 0,
  'w_bl'      => 0,
  'w_tot'     => 0,
  'w_wage'    => 0,
  'w_yi'      => 0,
);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <?php
  echo '<title>' . $res4[0]['type'] . '</title>';
  ?>
  <!-- MDB icon -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />

  <style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 2px;
      padding-bottom: 2px;
      text-align: left;
      background-color: #04AA6D;
      color: white;
    }
  </style>
</head>

<body>
  <script type="text/javascript" src="../js/mdb.min.js"></script>
  <?php
  if (sizeof($res1) == 0) {
  ?>
    <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-7">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <form action="view_report.php" method="post">

                  <div class="col-md-12 my-5 d-flex align-items-center">
                    <?php
                    $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $m1 = strval(date('m', strtotime($hdate)));
                    $y1 = strval(date('Y', strtotime($hdate)));
                    echo '<span class="h5 fw-bold text-uppercase">No details are entered in ' . $unit . ' ' . $unitdemo . ' unit on the month ' . $months_name[$m1 - 1] . ' ' . $y1 . ' </span>';
                    ?>
                  </div>

                  <div class="mb-4 d-flex pt-3" style="justify-content: center;">
                    <input type="submit" name="back" id="submit" value="BACK" class="btn btn-secondary" />
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>

  <?php
  } else {
  ?>

    <!-- Start your project here-->
    <form name="spinninglistrep" action="spinning_pdf.php" method="post">
      <section class="" style="padding-top: 30px;">
        <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col">
              <div class="">
                <div class="row g-0">
                  <div id="container_content">
                    <div class="text-black">
                      <h3 class="mb-5 text-uppercase">Minimum Wage Report For <?php echo $res4[0]['type']; ?></h3>

                      <div class="row">
                        <div class="col-md-2 mb-2">
                          <label class="form-label">Month and Year</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<select class="form-select" name="hdate" >';
                            $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                            $m1 = strval(date('m', strtotime($hdate)));
                            $y1 = strval(date('Y', strtotime($hdate)));
                            echo '<option value="' . $hdate . '">' . $months_name[strval($m1) - 1] . " " . $y1 . '</option></select>' . "\n";
                            $htmldata = $months_name[strval($m1) - 1];
                            $_SESSION["mydata4"] = $htmldata;
                            $htmldata = $y1;
                            $_SESSION["mydata5"] = $htmldata;
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2 mb-2">
                          <label class="form-label">Work Type</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="' . $res4[0]['type'] . '" readonly/>';
                            $htmldata = $res4[0]['type'];
                            $_SESSION["mydata2"] = $htmldata;
                            ?>
                          </div>
                        </div>

                        <div class="col-md-2 mb-2">
                          <label class="form-label">Unit ID</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="unitid" class="form-control form-control-lg" value="' . $unitdemo . '" readonly />';
                            $htmldata = $unitdemo;
                            $_SESSION["mydata1"] = $htmldata;
                            ?>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2 mb-2">
                          <label class="form-label">Number Of Holidays</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="hno" class="form-control form-control-lg" value="' . $res3[0]['holiday_no'] . '" readonly/>';
                            ?>
                          </div>
                        </div>

                        <div class="col-md-2 mb-2">
                          <label class="form-label">Number Of Working Days</label>
                        </div>
                        <div class="col-md-3 mb-2">
                          <div class="form-outline ">
                            <?php
                            echo '<input type="text" name="wno" class="form-control form-control-lg" value="' . $res3[0]['total_wd'] . '" readonly />';
                            ?>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style="padding: 0px 50px 30px 40px;">

          <?php
          if ($res4[0]['type'] == "Spinning") {
            if (sizeof($res1) > 0) {
          ?>

              <table class="table-bordered " id="customers">
                <thead>
                  <tr>
                    <th scope="col">SI No.</th>
                    <th scope="col">Artisan Code</th>
                    <th scope="col">Artisan Name</th>
                    <th scope="col">Variety</th>
                    <th scope="col">No. of Hanks</th>
                    <th scope="col">Rate</th>
                    <!-- <th scope="col">Sliver Consumption</th> -->
                    <th scope="col">Wages Paid</th>
                    <!--<th scope="col">W/F Fund</th>
              <th scope="col">w/f Recovery</th>
              <th scope="col">Adv Recovery</th>
              <th scope="col">Khadi Cr. Rec.</th>
              <th scope="col">Covid adv.</th>
              <th scope="col">LIC</th>
              <th scope="col">Net wages</th>
              <th scope="col">Holiday wages</th>
              <th scope="col">Total Wages</th>
              <th scope="col">ESI Contr</th>
              <th scope="col">Yarn Incentive</th> -->
                    <th scope="col">Minimum Wages</th>
                    <th scope="col">DA Days</th>
                    <th scope="col">DA Wages</th>
                    <th scope="col">Total Minimum Wages</th>
                    <th scope="col">Net Minimum Wages</th>
                    <!-- <th scope="col">Attendance</th>  -->
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $htmldata = "";

                  for ($i = 0; $i < sizeof($res1); $i++) {
                    $temp1 = strval($res1[$i]['artisan_id']);
                    $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1", $con);
                    $temp2 = $res6[0]['yarn_id'];
                    $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1", $con);
                    $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'", $con);

                    $sar1  = array(
                      'noh'    => $res6[0]['noh'],
                      'sliver' => $res7[0]['sliver'],
                      'rate'   => $res7[0]['rate'],
                      'wf'     => $res7[0]['wf'],
                      'wfrec'  => $res8[0]['wf_rec'],
                      'adrec'  => $res8[0]['ad_rec'],
                      'kcrec'  => $res8[0]['credit_rec'],
                      'covrec' => $res8[0]['cov_rec'],
                      'lic'    => $res8[0]['lic'],
                      'hno'    => $res3[0]['holiday_no'],
                      'wno'    => $res3[0]['total_wd'],
                      'val'    => $res7[0]['value']
                    );

                    $calc1 = spinningWageCalculation($sar1);

                    $sar2 = array(
                      'noh'     => $res6[0]['noh'],
                      'esi'     => $res7[0]['esi'],
                      'yi'      => $res7[0]['yi'],
                      'mw'      => $res7[0]['mw'],
                      'target'  => $res7[0]['target'],
                      'dawages' => $res3[0]['da_wages'],
                      'wage'    => $calc1['wage'],
                    );

                    $da = $res3[0]['total_wd'];
                    $calc2 = spinningIncentivesCalculation($sar2, $da);

                    echo  '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                    echo  '<td style="text-align:left;">' . $res1[$i]['artisan_code'] . '</td>';
                    echo  '<td style="text-align:left;">' . $res1[$i]['artisan_name'] . '</td>';
                    echo  '<td style="text-align:left;">' . $res7[0]['type'] . '</td>';
                    echo  '<td>' . $res6[0]['noh'] . '</td>';
                    echo  '<td>' . $res7[0]['rate'] . '</td>';
                    $htmldata .= '<tr><td>' . strval($i + 1) . '</td>';
                    $htmldata .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                    $htmldata .=  '<td>' . $res1[$i]['artisan_name'] . '</td>';
                    $htmldata .= '<td>' . $res7[0]['type'] . '</td>';
                    $htmldata .=  '<td>' . $res6[0]['noh'] . '</td>';
                    $htmldata .= '<td>' . $res7[0]['rate'] . '</td>';
                    // echo  '<td>'.$calc1['sliver'].'</td>';
                    echo  '<td>' . $calc1['wage'] . '</td>';
                    $htmldata .=  '<td>' . $calc1['wage'] . '</td>';
                    // echo  '<td>'.$calc1['wf'].'</td>';
                    // echo  '<td>'.$calc1['wfrec'].'</td>';
                    // echo  '<td>'.$calc1['adrec'].'</td>';
                    // echo  '<td>'.$calc1['kcrec'].'</td>';
                    // echo  '<td>'.$calc1['covrec'].'</td>';
                    // echo  '<td>'.$calc1['lic'].'</td>';
                    // echo  '<td>'.$calc1['netwages'].'</td>';
                    // echo  '<td>'.$calc1['hdwage'].'</td>';
                    // echo  '<td>'.$calc1['totwages'].'</td>';
                    // echo  '<td>'.$calc2['esi'].'</td>';
                    // echo  '<td>'.$calc2['yi'].'</td>';
                    echo  '<td>' . $calc2['mw'] . '</td>';
                    echo  '<td>' . $calc2['dadays'] . '</td>';
                    echo  '<td>' . $calc2['dawages'] . '</td>';
                    echo  '<td>' . $calc2['totmw'] . '</td>';
                    echo  '<td>' . $calc2['netmw'] . '</td>';
                    $htmldata .= '<td>' . $calc2['mw'] . '</td>';
                    $htmldata .= '<td>' . $calc2['dadays'] . '</td>';
                    $htmldata .= '<td>' . $calc2['dawages'] . '</td>';
                    $htmldata .=  '<td>' . $calc2['totmw'] . '</td>';
                    $htmldata .=  '<td>' . $calc2['netmw'] . '</td>';
                    // echo  '<td>'.$res6[0]['attendance'].'</td>';
                    echo  '</tr>';
                    $htmldata .= '</tr>';

                    $sum['wage'] = $sum['wage'] + $calc1['wage'];
                    $sum['mw'] = $sum['mw'] + $calc2['mw'];
                    $sum['dw'] = $sum['dw'] + $calc2['dawages'];
                    $sum['tmw'] = $sum['tmw'] + $calc2['totmw'];
                    $sum['nmw'] = $sum['nmw'] + $calc2['netmw'];
                  }

                  echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="6">Unit Total</td>';
                  echo "<td>" . $sum['wage'] . "</td>";
                  echo "<td>" . $sum['mw'] . "</td>";
                  echo "<td> </td>";
                  echo "<td>" . $sum['dw'] . "</td>";
                  echo "<td>" . $sum['tmw'] . "</td>";
                  echo "<td>" . $sum['nmw'] . "</td>";
                  echo "</tr>";

                  $htmldata .= '<tr style="text-align:center;"><td colspan="6"><b>Unit Total</b></td>';
                  $htmldata .= "<td>" . $sum['wage'] . "</td>";
                  $htmldata .= "<td>" . $sum['mw'] . "</td>";
                  $htmldata .= "<td> </td>";
                  $htmldata .= "<td>" . $sum['dw'] . "</td>";
                  $htmldata .= "<td>" . $sum['tmw'] . "</td>";
                  $htmldata .= "<td>" . $sum['nmw'] . "</td>";
                  $htmldata .= "</tr>";

                  $_SESSION["mydata"] = $htmldata;
                  ?>
                </tbody>
                <tfoot></tfoot>
              </table>

          <?php
            } // if closing of if(sizeof($res1)>0)
          } // if closing of work type spinning
          if ($res4[0]['type'] == "Weaving") {
            if (sizeof($res1) > 0) {
              echo '<table class="table-bordered" id="customers">
              <thead>
                <tr>
                  <th scope="col">SI No.</th>
                  <th scope="col">Artisan Code</th>
                  <th scope="col">Artisan Name</th>
                  <th scope="col">Variety</th>
                  <th scope="col">No. of Pieces</th>
                  <th scope="col">Metre</th>
                  
                  <th scope="col">Weaving Type</th>
                  <th scope="col">Rate</th>
                  
                  <th scope="col">Wages Paid</th>
                  <th scope="col">Minimum Wages</th>
                  <th scope="col">DA Days</th>
                  <th scope="col">DA Wages</th>
                  <th scope="col">Total Minimum Wages</th>
                  <th scope="col">Net Minimum Wages</th>
                </tr>
              </thead>
              <tbody>';

              $htmldata1 = "";

              for ($i = 0; $i < sizeof($res1); $i++) {
                $temp1 = strval($res1[$i]['artisan_id']);
                $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1", $con);
                $temp2 = $res9[0]['weaving_id'];
                $temp3 = $res9[0]['w_id'];
                $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3", $con);
                $res11 = retrieveData("SELECT type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2", $con);
                $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic From kbk_deduction where d_date='$hdate' and artisan_id=$temp1", $con);
                $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2", $con);

                $war3 = [
                  'warp_grey'    => $res13[0]['warp_grey'],
                  'warp_colour'  => $res13[0]['warp_colour'],
                  'warp_black'   => $res13[0]['warp_black'],
                  'warp_bl'      => $res13[0]['warp_bl'],
                  'weft_grey'    => $res13[0]['weft_grey'],
                  'weft_colour'  => $res13[0]['weft_colour'],
                  'weft_black'   => $res13[0]['weft_black'],
                  'weft_bl'      => $res13[0]['weft_bl'],
                ];

                $calc5 = weavingYICalculation($war3);

                $war1 = [
                  'metre'  => $res9[0]['mtr'],
                  'wage'   => $res10[0]['w_wages'],
                  'wf'     => $res11[0]['wf'],
                  'wfrec'  => $res12[0]['wf_rec'],
                  'adrec'  => $res12[0]['ad_rec'],
                  'kcrec'  => $res12[0]['credit_rec'],
                  'covrec' => $res12[0]['cov_rec'],
                  'lic'    => $res12[0]['lic'],
                  'hno'    => $res3[0]['holiday_no'],
                  'wno'    => $res3[0]['total_wd'],
                  'sqmtr'  => $res11[0]['width'],
                  'prime'  => $res11[0]['prime_cost'],
                  'value'  => $res11[0]['rate'],
                ];

                $calc3 = weavingWageCalculation($war1);

                $war2 = [
                  'metre'   => $res9[0]['mtr'],
                  'esi'     => $res11[0]['esi'],
                  'warpyi'  => $res10[0]['warp_yi'],
                  'weftyi'  => $res10[0]['weft_yi'],
                  'mw'      => $res10[0]['min_wages'],
                  'target'  => $res10[0]['target'],
                  'dawages' => $res3[0]['da_wages'],
                  'wage'    => $calc3['wage'],
                  'warp_tot' => $calc5['warp_tot'],
                  'weft_tot' => $calc5['weft_tot'],
                ];

                $da = $res3[0]['total_wd'];
                $calc4 = weavingIncentivesCalculation($war2, $da);

                echo '<tr style="text-align:center;"><td>' . strval($i + 1) . '</td>';
                $htmldata1 .= '<tr style="text-align:center;"><td>' . strval($i + 1) . '</td>';
                echo '<td style="text-align:left;">' . $res1[$i]['artisan_code'] . '</td>';
                $htmldata1 .= '<td style="text-align:left;">' . $res1[$i]['artisan_code'] . '</td>';
                echo '<td  style="text-align:left;">' . $res1[$i]['artisan_name'] . '</td>';
                $htmldata1 .= '<td  style="text-align:left;">' . $res1[$i]['artisan_name'] . '</td>';
                echo '<td style="text-align:left;">' . $res11[0]['type'] . '</td>';
                $htmldata1 .= '<td style="text-align:left;">' . $res11[0]['type'] . '</td>';
                echo '<td>' . $res9[0]['nop'] . '</td>';
                $htmldata1 .= '<td>' . $res9[0]['nop'] . '</td>';
                echo '<td>' . $res9[0]['mtr'] . '</td>';
                $htmldata1 .= '<td>' . $res9[0]['mtr'] . '</td>';
                // echo '<td>'.$calc3['sqmtr'].'</td>';
                echo '<td style="text-align:left;">' . $res10[0]['w_type'] . '</td>';
                $htmldata1 .= '<td style="text-align:left;">' . $res10[0]['w_type'] . '</td>';
                echo '<td>' . $res10[0]['w_wages'] . '</td>';
                $htmldata1 .= '<td>' . $res10[0]['w_wages'] . '</td>';
                echo '<td>' . $calc3['wage'] . '</td>';
                $htmldata1 .= '<td>' . $calc3['wage'] . '</td>';
                // echo '<td>'.$calc3['wf'].'</td>';
                // echo '<td>'.$calc3['wfrec'].'</td>';
                // echo '<td>'.$calc3['adrec'].'</td>';
                // echo '<td>'.$calc3['kcrec'].'</td>';
                // echo '<td>'.$calc3['covrec'].'</td>';
                // echo '<td>'.$calc3['lic'].'</td>';
                // echo '<td>'.$calc3['netwages'].'</td>';
                // echo '<td>'.$calc3['hdwage'].'</td>';
                // echo '<td>'.$calc3['totwages'].'</td>';
                // echo '<td>'.$calc4['esi'].'</td>';
                // echo '<td>'.$calc4['warpyi'].'</td>';
                // echo '<td>'.$calc4['weftyi'].'</td>';
                echo '<td>' . $calc4['mw'] . '</td>';
                $htmldata1 .= '<td>' . $calc4['mw'] . '</td>';
                echo '<td>' . $calc4['dadays'] . '</td>';
                $htmldata1 .= '<td>' . $calc4['dadays'] . '</td>';
                echo '<td>' . $calc4['dawages'] . '</td>';
                $htmldata1 .= '<td>' . $calc4['dawages'] . '</td>';
                echo '<td>' . $calc4['totmw'] . '</td>';
                $htmldata1 .= '<td>' . $calc4['totmw'] . '</td>';
                echo '<td>' . $calc4['netmw'] . '</td>';
                $htmldata1 .=  '<td>' . $calc4['netmw'] . '</td>';
                // echo '<td>'.$res9[0]['attendance'].'</td>';
                echo '</tr>';

                $sum['no'] = $sum['no'] + $res9[0]['nop'];
                $sum['mtr'] = $sum['mtr'] + $res9[0]['mtr'];
                $sum['wage'] = $sum['wage'] + $calc3['wage'];
                $sum['mw'] = $sum['mw'] + $calc4['mw'];
                $sum['dw'] = $sum['dw'] + $calc4['dawages'];
                $sum['tmw'] = $sum['tmw'] + $calc4['totmw'];
                $sum['nmw'] = $sum['nmw'] + $calc4['netmw'];
              }
              echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
              echo "<td>" . $sum['no'] . "</td>";
              echo "<td>" . $sum['mtr'] . "</td>";
              echo '<td colspan="2"></td>';
              echo "<td>" . $sum['wage'] . "</td>";
              echo "<td>" . $sum['mw'] . "</td>";
              echo "<td> </td>";
              echo "<td>" . $sum['dw'] . "</td>";
              echo "<td>" . $sum['tmw'] . "</td>";
              echo "<td>" . $sum['nmw'] . "</td>";
              echo "</tr>";
              echo "</tbody><tfoot></tfoot></table>";

              $htmldata1 .= '<tr style="text-align:center;"><td colspan="8"><b>Unit Total</b></td>';
              $htmldata1 .= "<td>" . $sum['wage'] . "</td>";
              $htmldata1 .= "<td>" . $sum['mw'] . "</td>";
              $htmldata1 .= "<td> </td>";
              $htmldata1 .= "<td>" . $sum['dw'] . "</td>";
              $htmldata1 .= "<td>" . $sum['tmw'] . "</td>";
              $htmldata1 .= "<td>" . $sum['nmw'] . "</td>";
              $htmldata1 .= "</tr>";
              $htmldata1 .= "</tbody><tfoot></tfoot></table>";

              $_SESSION['mydata3'] = $htmldata1;
            }
          }

          if ($res4[0]['type'] == "Preprocessing") {
            if (sizeof($res1) > 0) {
              echo '<table class="table-bordered" id="customers">
              <thead>
                <tr>
                  <th colspan="4"></th>
                  <th colspan="8" style="text-align:center">Sizing</th>
                  <th colspan="8" style="text-align:center">Bobin Winding</th>
                  <th colspan="8" style="text-align:center">Warping</th>
                  <th colspan="15"></th>
                </tr>
                <tr>
                  <th scope="col">SI No.</th>
                  <th scope="col">Artisan Code</th>
                  <th scope="col">Artisan Name</th>
                  <th scope="col">Variety</th>

                  <th scope="col">Grey</th>
                  <th scope="col">Colour</th>
                  <th scope="col">Bl</th>
                  <th scope="col">Black</th>
                  <th scope="col">Total</th>
                  <th scope="col">Rate</th>
                  <th scope="col">Wage</th>
                  <th scope="col">YI</th>

                  <th scope="col">Grey</th>
                  <th scope="col">Colour</th>
                  <th scope="col">Bl</th>
                  <th scope="col">Black</th>
                  <th scope="col">Total</th>
                  <th scope="col">Rate</th>
                  <th scope="col">Wage</th>
                  <th scope="col">YI</th>

                  <th scope="col">Grey</th>
                  <th scope="col">Colour</th>
                  <th scope="col">Bl</th>
                  <th scope="col">Black</th>
                  <th scope="col">Total</th>
                  <th scope="col">Rate</th>
                  <th scope="col">Wage</th>
                  <th scope="col">YI</th>

                  <th scope="col">Wages</th>
                  <th scope="col">W/F Fund</th>
                  <th scope="col">W/F Rec.</th>
                  <th scope="col">Adv Rec.</th>
                  <th scope="col">Khadi Cr. Rec.</th>
                  <th scope="col">Covid adv. Rec.</th>
                  <th scope="col">LIC</th>
                  <th scope="col">Net wages</th>
                  <th scope="col">Holiday wages</th>
                  <th scope="col">Total Wages</th>
                  <th scope="col">Minimum Wages</th>
                  <th scope="col">DA Days</th>
                  <th scope="col">DA Wages</th>
                  <th scope="col">Total Minimum Wages</th>
                  <th scope="col">Net Minimum Wages</th>
                </tr>
              </thead>
            <tbody>';

              $htmldata2 = '';

              for ($i = 0; $i < sizeof($res1); $i++) {
                $artid = intval($res1[$i]['artisan_id']);
                $res6 = retrieveData("SELECT wages,attendance,pre_id,preprocessing_id From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'", $con);
                $preid = intval($res6[0]['pre_id']);
                $preprocessingid = intval($res6[0]['preprocessing_id']);
                $res8 = retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3", $con);
                $res9 = retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid", $con);
                $res10 = retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid", $con);

                $par1 = [
                  'grey'  => $res10[0]['s_grey'],
                  'col'   => $res10[0]['s_col'],
                  'bl'    => $res10[0]['s_bl'],
                  'black' => $res10[0]['s_black'],
                ];

                $sizetotnoh = preprocessingTypeWage($par1);

                $par1 = [
                  'grey'  => $res10[0]['b_grey'],
                  'col'   => $res10[0]['b_col'],
                  'bl'    => $res10[0]['b_bl'],
                  'black' => $res10[0]['b_black'],
                ];

                $bobintotnoh = preprocessingTypeWage($par1);

                $par1 = [
                  'grey'  => $res10[0]['w_grey'],
                  'col'   => $res10[0]['w_col'],
                  'bl'    => $res10[0]['w_bl'],
                  'black' => $res10[0]['w_black'],
                ];

                $warptotnoh = preprocessingTypeWage($par1);

                $wfund = 0;
                if ($res8[0]['wf_yes_no'] == 1) {
                  $wfund = $res9[0]['wf'];
                }

                $par2 = [
                  'snoh'  => $sizetotnoh,
                  'bnoh'  => $bobintotnoh,
                  'wnoh'  => $warptotnoh,
                  'swage' => $res9[0]['s_wages'],
                  'bwage' => $res9[0]['b_wages'],
                  'wwage' => $res9[0]['w_wages'],
                  'wf'    => $wfund,
                  'wfrec' => $res8[0]['wf_rec'],
                  'adrec' => $res8[0]['ad_rec'],
                  'kcrec' => $res8[0]['credit_rec'],
                  'covrec' => $res8[0]['cov_rec'],
                  'lic'   => $res8[0]['lic'],
                  'hno'    => $res3[0]['holiday_no'],
                  'wno'    => $res3[0]['total_wd'],
                ];

                $calc7 = preprocessingWageCalculation($par2);

                $par3 = [
                  'snoh'     => $sizetotnoh,
                  'bnoh'     => $bobintotnoh,
                  'wnoh'     => $warptotnoh,
                  'esi'      => $res9[0]['esi'],
                  'syi'      => $res9[0]['s_yi'],
                  'byi'      => $res9[0]['b_yi'],
                  'wyi'      => $res9[0]['w_yi'],
                  'smw'      => $res9[0]['s_min_wages'],
                  'bmw'      => $res9[0]['b_min_wages'],
                  'wmw'      => $res9[0]['w_min_wages'],
                  'starget'  => $res9[0]['s_target'],
                  'btarget'  => $res9[0]['b_target'],
                  'wtarget'  => $res9[0]['w_target'],
                  'dawages'  => $res3[0]['da_wages'],
                  'wage'     => $calc7['wage'],
                  'twd'      => $res3[0]['total_wd'],
                  'tnoh'     => $calc7['totnoh'],
                ];

                $calc8 = preprocessingIncentivesCalculation($par3);

                echo '<tr style="text-align: center;"><td>' . strval($i + 1) . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_code'] . '</td>';
                echo '<td style="text-align: left;">' . $res1[$i]['artisan_name'] . '</td>';
                echo '<td style="text-align: left;">' . $res9[0]['type'] . '</td>';

                echo '<td ">' . $res10[0]['s_grey'] . '</td>';
                echo '<td ">' . $res10[0]['s_col'] . '</td>';
                echo '<td ">' . $res10[0]['s_bl'] . '</td>';
                echo '<td ">' . $res10[0]['s_black'] . '</td>';
                echo '<td ">' . $sizetotnoh . '</td>';
                echo '<td ">' . $res9[0]['s_wages'] . '</td>';
                echo '<td ">' . $calc7['swage'] . '</td>';
                echo '<td ">' . $calc8['syi'] . '</td>';

                echo '<td ">' . $res10[0]['b_grey'] . '</td>';
                echo '<td ">' . $res10[0]['b_col'] . '</td>';
                echo '<td ">' . $res10[0]['b_bl'] . '</td>';
                echo '<td ">' . $res10[0]['b_black'] . '</td>';
                echo '<td ">' . $bobintotnoh . '</td>';
                echo '<td ">' . $res9[0]['b_wages'] . '</td>';
                echo '<td ">' . $calc7['bwage'] . '</td>';
                echo '<td ">' . $calc8['byi'] . '</td>';

                echo '<td ">' . $res10[0]['w_grey'] . '</td>';
                echo '<td ">' . $res10[0]['w_col'] . '</td>';
                echo '<td ">' . $res10[0]['w_bl'] . '</td>';
                echo '<td ">' . $res10[0]['w_black'] . '</td>';
                echo '<td ">' . $warptotnoh . '</td>';
                echo '<td ">' . $res9[0]['w_wages'] . '</td>';
                echo '<td ">' . $calc7['wwage'] . '</td>';
                echo '<td ">' . $calc8['wyi'] . '</td>';

                echo '<td ">' . $calc7['wage'] . '</td>';
                echo '<td ">' . $calc7['wf'] . '</td>';
                echo '<td ">' . $res8[0]['wf_rec'] . '</td>';
                echo '<td ">' . $res8[0]['ad_rec'] . '</td>';
                echo '<td ">' . $res8[0]['credit_rec'] . '</td>';
                echo '<td ">' . $res8[0]['cov_rec'] . '</td>';
                echo '<td ">' . $res8[0]['lic'] . '</td>';
                echo '<td ">' . $calc7['netwages'] . '</td>';
                echo '<td ">' . $calc7['hdwage'] . '</td>';
                echo '<td ">' . $calc7['totwages'] . '</td>';
                echo '<td ">' . $calc8['mw'] . '</td>';
                echo '<td ">' . $calc8['dadays'] . '</td>';
                echo '<td ">' . $calc8['dawages'] . '</td>';
                echo '<td ">' . $calc8['totmw'] . '</td>';
                echo '<td ">' . $calc8['netmw'] . '</td></tr>';

                $htmldata2 .= '<tbody><tr><td>' . strval($i + 1) . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['artisan_code'] . '</td>';
                $htmldata2 .= '<td>' . $res1[$i]['artisan_name'] . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['type'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['s_black'] . '</td>';
                $htmldata2 .= '<td>' . $sizetotnoh . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['s_wages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['swage'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['syi'] . '</td>';

                $htmldata2 .= '<td>' . $res10[0]['b_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['b_black'] . '</td>';
                $htmldata2 .= '<td>' . $bobintotnoh . '</td>';
                $htmldata2 .=  '<td>' . $res9[0]['b_wages'] . '</td>';
                $htmldata2 .=  '<td>' . $calc7['bwage'] . '</td>';
                $htmldata2 .=  '<td>' . $calc8['byi'] . '</td>';

                $htmldata2 .= '<td>' . $res10[0]['w_grey'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_col'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_bl'] . '</td>';
                $htmldata2 .= '<td>' . $res10[0]['w_black'] . '</td>';
                $htmldata2 .= '<td>' . $warptotnoh . '</td>';
                $htmldata2 .= '<td>' . $res9[0]['w_wages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['wwage'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['wyi'] . '</td>';

                $htmldata2 .= '<td>' . $calc7['wage'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['wf'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['wf_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['ad_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['credit_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['cov_rec'] . '</td>';
                $htmldata2 .= '<td>' . $res8[0]['lic'] . '</td>';

                $htmldata2 .= '<td>' . $calc7['netwages'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['hdwage'] . '</td>';
                $htmldata2 .= '<td>' . $calc7['totwages'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['mw'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['dadays'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['dawages'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['totmw'] . '</td>';
                $htmldata2 .= '<td>' . $calc8['netmw'] . '</td></tr></tbody>';

                $rmsum['s_grey']  = $rmsum['s_grey'] + $res10[0]['s_grey'];
                $rmsum['s_colour'] = $rmsum['s_colour'] + $res10[0]['s_col'];
                $rmsum['s_black'] = $rmsum['s_black'] + $res10[0]['s_black'];
                $rmsum['s_bl']    = $rmsum['s_bl'] + $res10[0]['s_bl'];
                $rmsum['s_tot']   = $rmsum['s_tot'] + $sizetotnoh;
                $rmsum['s_wage']  = $rmsum['s_wage'] + $calc7['swage'];
                $rmsum['s_yi']    = $rmsum['s_yi'] + $calc8['syi'];
                $rmsum['b_grey']  = $rmsum['b_grey'] + $res10[0]['b_grey'];
                $rmsum['b_colour'] = $rmsum['b_colour'] + $res10[0]['b_col'];
                $rmsum['b_black'] = $rmsum['b_black'] + $res10[0]['b_black'];
                $rmsum['b_bl']    = $rmsum['b_bl'] + $res10[0]['b_bl'];
                $rmsum['b_tot']   = $rmsum['b_tot'] + $bobintotnoh;
                $rmsum['b_wage']  = $rmsum['b_wage'] + $calc7['bwage'];
                $rmsum['b_yi']    = $rmsum['b_yi'] + $calc8['byi'];
                $rmsum['w_grey']  = $rmsum['w_grey'] + $res10[0]['w_grey'];
                $rmsum['w_colour'] = $rmsum['w_colour'] + $res10[0]['w_col'];
                $rmsum['w_black'] = $rmsum['w_black'] + $res10[0]['w_black'];
                $rmsum['w_bl']    = $rmsum['w_bl'] + $res10[0]['w_bl'];
                $rmsum['w_tot']   = $rmsum['w_tot'] + $warptotnoh;
                $rmsum['w_wage']  = $rmsum['w_wage'] + $calc7['wwage'];
                $rmsum['w_yi']    = $rmsum['w_yi'] + $calc8['wyi'];

                $sum['wf'] = $sum['wf'] + $calc7['wf'];
                $sum['wfrec'] = $sum['wfrec'] + $calc7['wfrec'];
                $sum['adrec'] = $sum['adrec'] + $calc7['adrec'];
                $sum['kcrec'] = $sum['kcrec'] + $calc7['kcrec'];
                $sum['covrec'] = $sum['covrec'] + $calc7['covrec'];
                $sum['lic'] = $sum['lic'] + $calc7['lic'];
                $sum['nw'] = $sum['nw'] + $calc7['netwages'];
                $sum['hw'] = $sum['hw'] + $calc7['hdwage'];
                $sum['tw'] = $sum['tw'] + $calc7['totwages'];
                $sum['mw'] = $sum['mw'] + $calc8['mw'];
                $sum['dw'] = $sum['dw'] + $calc8['dawages'];
                $sum['tmw'] = $sum['tmw'] + $calc8['totmw'];
                $sum['nmw'] = $sum['nmw'] + $calc8['netmw'];
                $sum['no'] = $sum['no'] + $calc7['wage'];
              }
              echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="4">Unit Total</td>';
              echo '<td>' . $rmsum['s_grey'] . '</td>';
              echo '<td>' . $rmsum['s_colour'] . '</td>';
              echo '<td>' . $rmsum['s_bl'] . '</td>';
              echo '<td>' . $rmsum['s_black'] . '</td>';
              echo '<td>' . $rmsum['s_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['s_wage'] . '</td>';
              echo '<td>' . $rmsum['s_yi'] . '</td>';
              echo '<td>' . $rmsum['b_grey'] . '</td>';
              echo '<td>' . $rmsum['b_colour'] . '</td>';
              echo '<td>' . $rmsum['b_bl'] . '</td>';
              echo '<td>' . $rmsum['b_black'] . '</td>';
              echo '<td>' . $rmsum['b_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['b_wage'] . '</td>';
              echo '<td>' . $rmsum['b_yi'] . '</td>';
              echo '<td>' . $rmsum['w_grey'] . '</td>';
              echo '<td>' . $rmsum['w_colour'] . '</td>';
              echo '<td>' . $rmsum['w_bl'] . '</td>';
              echo '<td>' . $rmsum['w_black'] . '</td>';
              echo '<td>' . $rmsum['w_tot'] . '</td>';
              echo '<td></td>';
              echo '<td>' . $rmsum['w_wage'] . '</td>';
              echo '<td>' . $rmsum['w_yi'] . '</td>';
              echo '<td>' . $sum['no'] . '</td>';
              echo "<td>" . $sum['wf'] . "</td>";
              echo "<td>" . $sum['wfrec'] . "</td>";
              echo "<td>" . $sum['adrec'] . "</td>";
              echo "<td>" . $sum['kcrec'] . "</td>";
              echo "<td>" . $sum['covrec'] . "</td>";
              echo "<td>" . $sum['lic'] . "</td>";
              echo "<td>" . $sum['nw'] . "</td>";
              echo "<td>" . $sum['hw'] . "</td>";
              echo "<td>" . $sum['tw'] . "</td>";
              echo "<td>" . $sum['mw'] . "</td>";
              echo "<td></td>";
              echo "<td>" . $sum['dw'] . "</td>";
              echo "<td>" . $sum['tmw'] . "</td>";
              echo "<td>" . $sum['nmw'] . "</td></tr>";
              echo "</tbody><tfoot></tfoot></table>";

              $htmldata2 .= '<tr style="text-align:center;"><td colspan="4"><b>Unit Total</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['s_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['b_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_grey'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_colour'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_bl'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_black'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_tot'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_wage'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $rmsum['w_yi'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['no'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['wf'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['wfrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['adrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['kcrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['covrec'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['lic'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['nw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['hw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['tw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['mw'] . '</b></td>';
              $htmldata2 .= '<td></td>';
              $htmldata2 .= '<td><b>' . $sum['dw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['tmw'] . '</b></td>';
              $htmldata2 .= '<td><b>' . $sum['nmw'] . '</b></td></tr></tbody><tfoot></tfoot></table>';

              $_SESSION["myData"] = $htmldata2;
            }
          }
          ?>
        </div>

        <!-- <div class="text-center" >
      <input type="submit" name="submit" value="Print" class="btn btn-info btn_print">
      <a target="_blank" class="btn btn-info btn_print" href="spinningwages_unit_pdf.php">Wages Report</a>
      <a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a>
    </div>-->

        <div class="text-center" id="print_container">
          <a target="_blank" class="btn btn-info btn_print" href="./calcview/min_wage_unit_pdf.php">Minimum Wages Report</a>
          <!-- <button class="btn btn-info btn_print" id="print_btn">PRINT</button> -->
          <a href="finalize.php?month=<?= $m1 ?>&year=<?= $y1 ?>&work_type=<?= $_SESSION['mydata2'] ?>" class="btn btn-info">Finalize</a>

        </div>

      </section>
    </form>

  <?php
  }
  ?>

  <!-- End your project here-->

  <!-- MDB -->
  <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
  <!-- Custom scripts -->
  <script type="text/javascript"></script>
</body>

</html>