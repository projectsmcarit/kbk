<?php

  $res3= retrieveData("SELECT holiday_no,total_wd,da_wages From kbk_holidays where h_date='$hdate'",$con);
  $res4= retrieveData("SELECT type From kbk_work_type where work_id=$worktype",$con);
  $res5= retrieveData("SELECT unit_code,unit_name From kbk_unit where unit_code='$unit'",$con);
  $unitdemo=$res5[0]['unit_name'];
  $a=strtolower($res4[0]['type']);
  $b="kbk_".$a;
  $res1= retrieveData("SELECT artisan_code,artisan_id,artisan_name From kbk_artisan where unit_code='$unit' and artisan_id in (select artisan_id from $b where s_date='$hdate')",$con);
  
  $sum = array(
    'wage'     => 0,
    'wfrec'    => 0,
    'adrec'    => 0,
    'kcrec'    => 0,
    'covrec'   => 0,
  );

?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <?php
      echo '<title>'.$res4[0]['type'].'</title>';
    ?>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
          
   <style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 2px;
  padding-bottom: 2px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>

     
    
  </head>
  <body>
<script type="text/javascript" src="../js/mdb.min.js"></script>
<?php
 if(sizeof($res1)==0)
 {
    ?>
       <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-7">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
               
                  <form action="view_report.php" method="post">  
                <div class="col-md-12 my-5 d-flex align-items-center">
                 
    
                  
    <?php
         $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         $m1= strval(date('m',strtotime($hdate)));
         $y1= strval(date('Y',strtotime($hdate)));
     
          echo '<span class="h5 fw-bold text-uppercase">No details are entered in '.$unit.' '.$unitdemo.' unit on the month '.$months_name[$m1-1].' '.$y1.' </span>';
                        
                        
     ?>                   
                      </div>                   
                         
                      <div class="mb-4 d-flex pt-3" style="justify-content: center;">
                        <input type="submit" name="back" id="submit" value="BACK" class="btn btn-secondary" />
                      </div>
                   </form>
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


<?php
 }
 else
 {

?>


  
    <!-- Start your project here-->
   
    <form name="spinninglistrep" action="spinning_pdf.php" method="post"> 
   
    
    
   
    <section class="" style="padding-top: 30px;">
      <div class="py-5" style="padding-left: 100px;padding-right: 500px;">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col">
            <div class="">
              <div class="row g-0">
               <div  id="container_content">
                 <div class="text-black">
                   <h3 class="mb-5 text-uppercase"><?php echo $unitdemo." Unit Monthly ".$res4[0]['type']; ?> Report</h3>            

  <div class="row">
     
     <div class="col-md-2 mb-2">                       
     <label class="form-label" >Month and Year</label>                   
     </div>
     <div class="col-md-3 mb-2">
       <div class="form-outline ">
       <?php 
         echo '<select class="form-select" name="hdate" >';
         $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         $m1= strval(date('m',strtotime($hdate)));
         $y1= strval(date('Y',strtotime($hdate)));
         echo '<option value="'.$hdate.'">'.$months_name[strval($m1)-1]." ".$y1.'</option></select>'."\n";
         
       ?>
       </div>
     </div>
     </div>
<div class="row">
 
 <div class="col-md-2 mb-2">                       
  <label class="form-label" >Work Type</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
      echo  '<input type="text" name="worktype" class="form-control form-control-lg" value="'.$res4[0]['type'].'" readonly/>';
                  
    ?>
    </div>
  </div>
  
  <div class="col-md-2 mb-2">                       
  <label class="form-label" >Unit ID</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
      echo '<input type="text" name="unitid" class="form-control form-control-lg" value="'.$unitdemo.'" readonly />';  
                 
    ?>
    </div>
  </div>
  </div>



 <div class="row">
 
 <div class="col-md-2 mb-2">                       
  <label class="form-label">Number Of Holidays</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
        <?php
        echo '<input type="text" name="hno" class="form-control form-control-lg" value="'.$res3[0]['holiday_no'].'" readonly/>';                    
        ?>
    </div>
  </div>
  
  <div class="col-md-2 mb-2">                       
  <label class="form-label">Number Of Working Days</label>                   
  </div>
  <div class="col-md-3 mb-2">
    <div class="form-outline ">
    <?php
        echo '<input type="text" name="wno" class="form-control form-control-lg" value="'.$res3[0]['total_wd'].'" readonly />';
       
    ?>                  
    </div>
  </div>
  </div>




                  </div>          
                </div>
              </div>                
            </div>
          </div>            
        </div>
      </div>

<div style="padding: 0px 50px 30px 40px;">
  
<?php
if($res4[0]['type']=="Spinning")
{
if(sizeof($res1)>0)
{
?>

    <table class="table-bordered " id="customers">
        <thead>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th scope="col">Artisan Name</th>
                  
                          <th scope="col">Wages</th>
                         
                          <th scope="col">w/f Recovery</th>
                          <th scope="col">Adv Recovery</th>
                          <th scope="col">Khadi Cr. Rec.</th>
                          <th scope="col">Covid adv.</th>
                   
  
                        </tr>
                      </thead>
        <tbody>
<?php
    for($i=0;$i<sizeof($res1);$i++)
    {
        
        echo '<tr style="text-align:center;"><td>'.strval($i+1).'</td>';
        echo '<td style="text-align:left;">'.$res1[$i]['artisan_code'].'</td>';
        echo '<td style="text-align:left;">'.$res1[$i]['artisan_name'].'</td>';
        $temp1=strval($res1[$i]['artisan_id']);
        
        $res6 =  retrieveData("SELECT wages,attendance,noh,yarn_id From kbk_spinning where s_date='$hdate' and artisan_id=$temp1",$con);
        $temp2=$res6[0]['yarn_id'];
        $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic  From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
        $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_id='$temp2'",$con);
   
        $sar1  = array(
          'noh'    => $res6[0]['noh'],
          'sliver' => $res7[0]['sliver'],
          'rate'   => $res7[0]['rate'],
          'wf'     => $res7[0]['wf'],
          'wfrec'  => $res8[0]['wf_rec'],
          'adrec'  => $res8[0]['ad_rec'],
          'kcrec'  => $res8[0]['credit_rec'],
          'covrec' => $res8[0]['cov_rec'],
          'lic'    => $res8[0]['lic'],
          'hno'    => $res3[0]['holiday_no'],
          'wno'    => $res3[0]['total_wd'],
          'val'    => $res7[0]['value']
        );

        $calc1=spinningWageCalculation($sar1);
      
        $sar2 = array(
          'noh'     => $res6[0]['noh'],
          'esi'     => $res7[0]['esi'],
          'yi'      => $res7[0]['yi'],
          'mw'      => $res7[0]['mw'],
          'target'  => $res7[0]['target'],
          'dawages' => $res3[0]['da_wages'],
          'wage'    => $calc1['wage'],
        );
        
$da=$res3[0]['total_wd'];
        $calc2=spinningIncentivesCalculation($sar2,$da);

     
        echo '<td>'.$calc1['wage'].'</td>';
  
        echo  '<td>'.$calc1['wfrec'].'</td>';
        echo  '<td>'.$calc1['adrec'].'</td>';
        echo  '<td>'.$calc1['kcrec'].'</td>';
        echo '<td>'.$calc1['covrec'].'</td>';

        echo  '</tr>';
        
         $sum['wage']=$sum['wage']+$calc1['wage'];
        
         $sum['wfrec']=$sum['wfrec']+$calc1['wfrec'];
         $sum['adrec']=$sum['adrec']+$calc1['adrec'];
         $sum['kcrec']=$sum['kcrec']+$calc1['kcrec'];
         $sum['covrec']=$sum['covrec']+$calc1['covrec'];

        
    }
    echo '<tr style="background-color: #454545; color:white;text-align: center;"><td colspan="3">Unit Total</td>';
  
    echo "<td>".$sum['wage']."</td>";
   
    echo "<td>".$sum['wfrec']."</td>";
    echo "<td>".$sum['adrec']."</td>";
    echo "<td>".$sum['kcrec']."</td>";
    echo "<td>".$sum['covrec']."</td>";
   
    echo "</tr>";


?>  



        </tbody>
        <tfoot></tfoot>
    </table> 
  


<?php

}// if closing of if(sizeof($res1)>0)

}// if closing of work type spinning
if($res4[0]['type']=="Weaving")
{
  if(sizeof($res1)>0)
{
  echo '<table class="table-bordered" id="customers">

        <thead>
                        <tr>
                          <th scope="col">SI No.</th>
                          <th scope="col">Artisan Code</th>
                          <th scope="col">Artisan Name</th>
               
                          
                          <th scope="col">Wages</th>
                      
                          <th scope="col">w/f Recovery</th>
                          <th scope="col">Adv Recovery</th>
                          <th scope="col">Khadi Cr. Rec.</th>
                          <th scope="col">Covid adv.</th>
     
  
                        </tr>
                      </thead>
        <tbody>';
        for($i=0;$i<sizeof($res1);$i++)
        {
          
          $temp1=strval($res1[$i]['artisan_id']);
          $res9 = retrieveData("SELECT attendance,nop,mtr,weaving_id,w_id from kbk_weaving where s_date='$hdate' and artisan_id=$temp1",$con);
          $temp2 = $res9[0]['weaving_id'];
          $temp3 = $res9[0]['w_id'];
          $res10 = retrieveData("SELECT w_wages,warp_yi,weft_yi,min_wages,target,w_type from kbk_weaving_type where w_id=$temp3",$con);
          $res11 = retrieveData("SELECT type,rate,countable,length,wf,esi,prime_cost,width,wef,yarn_warp,yarn_weft from kbk_weaving_variety where weaving_id=$temp2",$con);
          $res12 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
          $res13 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$temp2",$con);
          $war3 =[
            'warp_grey'    => $res13[0]['warp_grey'],
            'warp_colour'  => $res13[0]['warp_colour'],
            'warp_black'   => $res13[0]['warp_black'],
            'warp_bl'      => $res13[0]['warp_bl'],
            'weft_grey'    => $res13[0]['weft_grey'],
            'weft_colour'  => $res13[0]['weft_colour'],
            'weft_black'   => $res13[0]['weft_black'],
            'weft_bl'      => $res13[0]['weft_bl'],
          ];
          $calc5 = weavingYICalculation($war3);
          $war1 = [
            'metre'  => $res9[0]['mtr'],
            'wage'   => $res10[0]['w_wages'],
            'wf'     => $res11[0]['wf'],
            'wfrec'  => $res12[0]['wf_rec'],
            'adrec'  => $res12[0]['ad_rec'],
            'kcrec'  => $res12[0]['credit_rec'],
            'covrec' => $res12[0]['cov_rec'],
            'lic'    => $res12[0]['lic'],
            'hno'    => $res3[0]['holiday_no'],
            'wno'    => $res3[0]['total_wd'],
            'sqmtr'  => $res11[0]['width'],
            'prime'  => $res11[0]['prime_cost'],
            'value'  => $res11[0]['rate'],
          ];
          $calc3=weavingWageCalculation($war1);

          $war2 = [
                'metre'   => $res9[0]['mtr'],
                'esi'     => $res11[0]['esi'],
                'warpyi'  => $res10[0]['warp_yi'],
                'weftyi'  => $res10[0]['weft_yi'],
                'mw'      => $res10[0]['min_wages'],
                'target'  => $res10[0]['target'],
                'dawages' => $res3[0]['da_wages'],
                'wage'    => $calc3['wage'],
                'warp_tot'=> $calc5['warp_tot'],
                'weft_tot'=> $calc5['weft_tot'],
            ];
$da=$res3[0]['total_wd'];
          $calc4=weavingIncentivesCalculation($war2,$da);

          echo '<tr style="text-align:center;"><td>'.strval($i+1).'</td>';
          echo '<td style="text-align:left;">'.$res1[$i]['artisan_code'].'</td>';
          echo '<td style="text-align:left;">'.$res1[$i]['artisan_name'].'</td>';
  
          echo '<td>'.$calc3['wage'].'</td>';
  
          echo '<td>'.$calc3['wfrec'].'</td>';
          echo '<td>'.$calc3['adrec'].'</td>';
          echo '<td>'.$calc3['kcrec'].'</td>';
          echo '<td>'.$calc3['covrec'].'</td>';
         
          echo '</tr>';

         $sum['wage']=$sum['wage']+$calc3['wage'];
        
         $sum['wfrec']=$sum['wfrec']+$calc3['wfrec'];
         $sum['adrec']=$sum['adrec']+$calc3['adrec'];         
         $sum['kcrec']=$sum['kcrec']+$calc3['kcrec'];
         $sum['covrec']=$sum['covrec']+$calc3['covrec'];


        }
    echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="3">Unit Total</td>';
    echo "<td>".$sum['wage']."</td>";
 
    echo "<td>".$sum['wfrec']."</td>";
    echo "<td>".$sum['adrec']."</td>";
    echo "<td>".$sum['kcrec']."</td>";
    echo "<td>".$sum['covrec']."</td>";
   
    echo "</tr>";

        echo "</tbody>
        <tfoot></tfoot>
    </table>"; 
      }
    }

?>
<?php
  if($res4[0]['type']=="Preprocessing"){
    if(sizeof($res1)>0){
      echo '<table class="table-bordered" id="customers">
        <thead>
        <tr>
          <th scope="col">SI No.</th>
          <th scope="col">Artisan Code</th>
          <th scope="col">Artisan Name</th>
          <th scope="col">Wages</th>
                      
          <th scope="col">w/f Recovery</th>
          <th scope="col">Adv Recovery</th>
          <th scope="col">Khadi Cr. Rec.</th>
          <th scope="col">Covid adv.</th>
        </tr>
        </thead>
        <tbody>';

        for($i=0;$i<sizeof($res1);$i++)
        {
          $artid=intval($res1[$i]['artisan_id']);
          $res6 = retrieveData("SELECT wages,attendance,pre_id,preprocessing_id From kbk_preprocessing where s_date='$hdate' and artisan_id=$artid and unit_code='$unit'",$con);
          $preid=intval($res6[0]['pre_id']);
          $preprocessingid=intval($res6[0]['preprocessing_id']);
          $res8 = retrieveData("SELECT ad_rec,credit_rec,cov_rec,wf_rec,lic,wf_yes_no  From kbk_deduction where d_date='$hdate' and artisan_id=$artid and work_id=3",$con);
          $res9=retrieveData("SELECT * FROM kbk_preprocessing_variety where pre_id=$preid",$con);
          $res10=retrieveData("SELECT * FROM kbk_preprocessing_type_entry where preprocessing_id=$preprocessingid",$con);
    
            $par1 = [
              'grey'  => $res10[0]['s_grey'],
              'col'   => $res10[0]['s_col'],
              'bl'    => $res10[0]['s_bl'],
              'black' => $res10[0]['s_black'],
            ];
            $sizetotnoh=preprocessingTypeWage($par1);
    
            $par1 = [
              'grey'  => $res10[0]['b_grey'],
              'col'   => $res10[0]['b_col'],
              'bl'    => $res10[0]['b_bl'],
              'black' => $res10[0]['b_black'],
            ];
            $bobintotnoh=preprocessingTypeWage($par1);
    
            $par1 = [
              'grey'  => $res10[0]['w_grey'],
              'col'   => $res10[0]['w_col'],
              'bl'    => $res10[0]['w_bl'],
              'black' => $res10[0]['w_black'],
            ];
            $warptotnoh=preprocessingTypeWage($par1);
    
            $wfund=0;
            if($res8[0]['wf_yes_no']==1){
              $wfund=$res9[0]['wf'];
            }
    
            $par2 = [
              'snoh'  => $sizetotnoh,
              'bnoh'  => $bobintotnoh,
              'wnoh'  => $warptotnoh,
              'swage' => $res9[0]['s_wages'],
              'bwage' => $res9[0]['b_wages'],
              'wwage' => $res9[0]['w_wages'],
              'wf'    => $wfund,
              'wfrec' => $res8[0]['wf_rec'],
              'adrec' => $res8[0]['ad_rec'],
              'kcrec' => $res8[0]['credit_rec'],
              'covrec'=> $res8[0]['cov_rec'],
              'lic'   => $res8[0]['lic'],
              'hno'    => $res3[0]['holiday_no'],
              'wno'    => $res3[0]['total_wd'],
          ];
            $calc7=preprocessingWageCalculation($par2);
    
            $par3 = [
              'snoh'     => $sizetotnoh,
              'bnoh'     => $bobintotnoh,
              'wnoh'     => $warptotnoh,
              'esi'      => $res9[0]['esi'],
              'syi'      => $res9[0]['s_yi'],
              'byi'      => $res9[0]['b_yi'],
              'wyi'      => $res9[0]['w_yi'],              
              'smw'      => $res9[0]['s_min_wages'],
              'bmw'      => $res9[0]['b_min_wages'],
              'wmw'      => $res9[0]['w_min_wages'],
              'starget'  => $res9[0]['s_target'],
              'btarget'  => $res9[0]['b_target'],
              'wtarget'  => $res9[0]['w_target'],
              'dawages'  => $res3[0]['da_wages'],
              'wage'     => $calc7['wage'],
              'twd'      => $res3[0]['total_wd'],
              'tnoh'     => $calc7['totnoh'],  
            ];
            $calc8=preprocessingIncentivesCalculation($par3);

        echo '<tr style="text-align: center;"><td>'.strval($i+1).'</td>';
        echo '<td style="text-align: left;">'.$res1[$i]['artisan_code'].'</td>';
        echo '<td style="text-align: left;">'.$res1[$i]['artisan_name'].'</td>';
        echo '<td ">'.$calc7['wage'].'</td>';
        echo '<td ">'.$res8[0]['wf_rec'].'</td>';
        echo '<td ">'.$res8[0]['ad_rec'].'</td>';
        echo '<td ">'.$res8[0]['credit_rec'].'</td>';
        echo '<td ">'.$res8[0]['cov_rec'].'</td>';
        echo '</tr>';

        $sum['wage']=$sum['wage']+$calc7['wage'];
        $sum['wfrec']=$sum['wfrec']+$calc7['wfrec'];
        $sum['adrec']=$sum['adrec']+$calc7['adrec'];         
        $sum['kcrec']=$sum['kcrec']+$calc7['kcrec'];
        $sum['covrec']=$sum['covrec']+$calc7['covrec'];
      }
      echo '<tr style="background-color: #454545; color:white; text-align: center;"><td colspan="3">Unit Total</td>';
      echo "<td>".$sum['wage']."</td>";
      echo "<td>".$sum['wfrec']."</td>";
      echo "<td>".$sum['adrec']."</td>";
      echo "<td>".$sum['kcrec']."</td>";
      echo "<td>".$sum['covrec']."</td>";
      echo "</tr>";
      echo "</tbody><tfoot></tfoot></table>";
    }
  }  
?>
</div>

<div class="text-center" >
  <!-- <input type="submit" name="submit" value="Print" class="btn btn-info btn_print"> -->
<a target="_blank" class="btn btn-info btn_print" href="./calcview/_unit_pdf.php">Wages Report</a>
<!-- <a target="_blank" class="btn btn-info btn_print" href="spinning_minwgs_pdf.php">Minimum Wages Report</a> -->
</div>
 
 


    </section>
    
  </form>
<?php 
}

?>
        <!-- End your project here-->

    <!-- MDB -->
    <!-- <script type="text/javascript" src="../js/mdb.min.js"></script> -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
