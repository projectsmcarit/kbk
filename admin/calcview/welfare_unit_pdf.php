<?php
include('../connection.php');
include('../kbk_library.php');
session_start();

require_once("../dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$html=""; 
$html.='<html>';
$html.='<body>';
$html.='<form>'; 
$html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
$html.='<br>';
$r1=$_SESSION["mydata2"];
$r2=$_SESSION["mydata4"];
$r3=$_SESSION["mydata5"];
$html.='<h3 align="center">'.$r1.' Welfare Fund Report '.$_SESSION["mydata1"].'</h3>';

$html.='<h3 align="center">'.$r2.' '.$r3.'</h3>';

if($r1=="Spinning")
              {
                $html.='<table style="border-collapse:collapse" border="1" align="center" width=90%>
<thead>
                <tr style="width:10.17mm">
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Membership Number</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisans Contribution</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Boards Contribution</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total</th>
                  
               

                </tr>
              </thead>';
                $html.=$_SESSION["mydata"];
              }

              else if($r1=="Weaving")
              {
                $html.='<table style="border-collapse:collapse" border="1" align="center" width=90%>
<thead>
                <tr style="width:10.17mm">
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Membership Number</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisans Contribution</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Boards Contribution</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total</th>
                  
                 
                </tr>
              </thead>';
              $html.=$_SESSION["mydata3"];

              }
              else if($r1 == "Preprocessing"){

                $html.='<table style="border-collapse:collapse" border="1" align="center" width=90%>
                <thead>
                                <tr style="width:10.17mm">
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Membership Number</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Wages</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisans Contribution</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Boards Contribution</th>
                                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total</th>
                                  
                                 
                                </tr>
                              </thead>';
                              $html.=$_SESSION["myData"];
                
              }




$html.='</table>';
$html.="</form>";
$html.="</body>";
$html.="</html>";
$dompdf->loadHtml(html_entity_decode($html));	
$dompdf->setPaper('A4', 'landscape'); //portrait or landscape
$dompdf->render();
ob_end_clean();
$dompdf->stream("Welfare Fund Report",array("Attachment"=>0));

?>    