<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    
   
    <!-- Font Awesome -->
   
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body >
    <!-- Start your project here-->
<!--Main Navigation-->
<header>

    <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:darkslategrey; position:fixed;z-index:1; right:0;left: 0;margin-bottom: 50px;"  >
    <!-- Container wrapper -->
    <div class="container-fluid" style="z-index: 1;" >
      <!-- Toggle button -->
      <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i class="fas fa-bars"></i>
      </button>
  
      <!-- Collapsible wrapper -->
     
        <!-- Left links -->
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="view_admin_index.php" style="color: white"
>Home</a>
          </li>
          <!-- Navbar dropdown -->
          <li class="nav-item dropdown">
            <a
              class="nav-link dropdown-toggle"
              href="#"
              style="color: white"
              id="navbarDropdown"
              role="button"
              data-mdb-toggle="dropdown"
              aria-expanded="false"
              
            >
            Registration
            </a>
            <!-- Dropdown menu -->
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="view_artisan_reg.php" style="color: black">Artisan</a>
              </li>
              <li>
                <a class="dropdown-item" href="view_centre_reg.php" style="color: black">Centre</a>
              </li>
              <li>
                <a class="dropdown-item" href="view_instructor_registration.php" style="color: black">Instructor </a>
              </li>
              <li>
                <a class="dropdown-item" href="view_esi_centre_registration.php" style="color: black">Esi Center </a>
                <li>
                <a class="dropdown-item" href="view_unit_reg.php" style="color: black">Unit </a>
              </li>
              </li>
            </ul>
            <!-- Navbar dropdown -->
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                style="color: white"
                id="navbarDropdown"
                role="button"
                data-mdb-toggle="dropdown"
                aria-expanded="false"
              >
              View
            Details
              </a>
              <!-- Dropdown menu -->
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <a class="dropdown-item" href="Artisan_View.php" style="color: black">Artisan</a>
                </li>
                <li>
                  <a class="dropdown-item" href="view_Centre_Search.php" style="color: black">Centre</a>
                </li>
                <li>
                  <a class="dropdown-item" href="Instructor_View.php" style="color: black">Instructor </a>
                </li>
                <li>
                  <a class="dropdown-item" href="view_edit_unit_reg.php" style="color: black">Unit </a>
                </li>
                <li>
                  <a class="dropdown-item" href="view_add_remove_artisan.php" style="color: black">Add or Remove Artisan </a>
                </li>
        </ul>
        <li class="nav-item dropdown">
            <a
              class="nav-link dropdown-toggle"
              href="#"
              style="color: white"
              id="navbarDropdown"
              role="button"
              data-mdb-toggle="dropdown"
              aria-expanded="false"
            >
            Wage Parameter
            </a>
            <!-- Dropdown menu -->
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="view_yarn_variety.php" style="color: black">Spinning</a>
              </li>
              <li>
                <a class="dropdown-item" href="view_twisting_variety.php" style="color: black">Twisting</a>
              </li>
              
              <li>
                <a class="dropdown-item" href="View_Weaving_variety.php" style="color: black">Weaving</a>
              </li>
              <li>
                <a class="dropdown-item" href="view_preprocessing_parameter.php" style="color: black">Pre-Processing </a>
              </li>
               <li>
                <a class="dropdown-item" href="yarn_variety_search.php" style="color: black">Spinning Edit </a>
              </li>
              <li>
                <a class="dropdown-item" href="twisting_variety_search.php" style="color: black">Twisting Edit </a>
              </li>
               <li>
                <a class="dropdown-item" href="weaving_edit.php" style="color: black">Weaving Edit </a>
              </li>
              <!--  <li>
                <a class="dropdown-item" href="view_preprocessing_parameter.php" style="color: black">Pre-Processing Edit</a>
              </li> -->
               <li>
                <a class="dropdown-item" href="view_spinning_parameter.php" style="color: black">View Spinning</a>
              </li>
              <li>
                <a class="dropdown-item" href="view_twisting_parameter.php" style="color: black">View Twisting</a>
              </li>
               <li>
                <a class="dropdown-item" href="view_weaving_parameter.php" style="color: black">View Weaving </a>
              </li>
              <li>
                <a class="dropdown-item" href="view_preprocessing_variety.php" style="color:black">View Preprocessing </a>
              </li>
                <!--<li>
                <a class="dropdown-item" href="view_preprocessing_parameter.php" style="color: black">View Pre-Processing </a>
              </li> -->
      </ul>
      <li class="nav-item dropdown">
        <a
          class="nav-link dropdown-toggle"
          href="#"
          style="color: white"
          id="navbarDropdown"
          role="button"
          data-mdb-toggle="dropdown"
          aria-expanded="false"
        >
        Works
        </a>
        <!-- Dropdown menu -->
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown" >
          <li>
           <!-- <a class="dropdown-item" href="view_wage_parameters.php" style="color: black">Parameters</a>-->
          </li>
          <li>
            <a class="dropdown-item" href="view_holiday.php" style="color: black">Holiday/Working days</a>
          </li>
          <li>
            <a class="dropdown-item" href="view_report_spinning_entry.php" style="color: black">Work Details </a>
          </li>
  </ul>
  <li class="nav-item dropdown">
        <a
          class="nav-link dropdown-toggle"
          href="#"
          style="color: white"
          id="navbarDropdown"
          role="button"
          data-mdb-toggle="dropdown"
          aria-expanded="false"
        >
        View All Details
        </a>
        <!-- Dropdown menu -->
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown" >
          <li>
            <a class="dropdown-item" href="view_all_artisan.php" style="color: black">Artisan</a>
          </li>
          <li>
            <a class="dropdown-item" href="view_all_instructor.php" style="color: black">Instructor</a>
          </li>
          <li>
            <a class="dropdown-item" href="view_all_centre.php" style="color: black">Center</a>
          </li>
          <li>
            <a class="dropdown-item" href="view_all_unit.php" style="color: black">Unit</a>
          </li>
  </ul>
 
  <li class="nav-item">
    <a class="nav-link" href="#" style="color: white">Miscellaneous Charges</a>
  </li>
 <li class="nav-item">
    <a class="nav-link" href="view_report.php" style="color: white">Reports</a>
  </li>


        <!-- Left links -->
     
      <!-- Collapsible wrapper -->
  
      </div>
  
        
        <!-- Avatar -->
        <div class="dropdown " style="z-index: 1;" >
          <a
            class="dropdown-toggle d-flex align-items-center hidden-arrow"
            href="#"
            id="navbarDropdownMenuAvatar"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            <img
              src="../images/1.jpg"
              class="rounded-circle"
              height="25"
              alt="Khadi Board Kottayam"
              loading="lazy"
            />
          </a>
          <ul
            class="dropdown-menu dropdown-menu-end"
            aria-labelledby="navbarDropdownMenuAvatar"
          >
          <a class="dropdown-item" href="./view_change_password.php">Change Password</a>
              <a class="dropdown-item" href="../logout.php">Logout</a>
            </li>
          </ul>
        </div>
      
      <!-- Right elements -->
    
    <!-- Container wrapper -->
  </nav>
</header>
   <!-- <script type="text/javascript" src="../js/mdb.min.js"></script>  -->
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>