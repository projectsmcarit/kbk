<html>

<head>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <style>
        .checked-0{
            background-color:red;
            color:whitesmoke
        }
        .checked-1{
            background-color:green;
            color:whitesmoke
        }
    </style>

</head>
<?php

include('../connection.php');

if (isset($_REQUEST["term"])) {
    // Prepare a select statement
    //$unit = $_POST['unit'];
    $sql = "select * from kbk_instructor WHERE name LIKE ? OR instructor_id LIKE ? OR phone LIKE ?"; 
    
    if($stmt = mysqli_prepare($con, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "sss", $param_term, $param_term, $param_term);
        
        // Set parameters
        $param_term = '%' . $_REQUEST["term"] . '%';
        
        // Attempt to execute the prepared statement
        if (mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            // Check number of rows in the result set
            if (mysqli_num_rows($result) > 0) {
                // Fetch result rows as an associative array
?>
                <center>
                    <table class='table table-hover'>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Pincode</th>
                            <th>Phone</th>
                        </tr>
                        <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
                            <tr>
                                <td><mark><?php echo $row['instructor_id']; ?></mark></td>
                                <td><mark><?php echo $row['name']; ?></mark></td>
                                <td><mark><?php echo $row['address']; ?></mark></td>
                                <td><mark><?php echo $row['pin']; ?></mark></td>
                                <td><mark><?php echo $row['phone']; ?></mark></td>
                                <td><a href="view_instructor.php?id=<?php echo $row['instructor_id']; ?>"><input type="button" value="View/Update" class="btn btn-primary"></a></td>
                                
                                <td><a href="delete_instructor.php?id=<?php echo $row['instructor_id']; ?>" onclick="return confirm('Do yout want to delete the record');"><input type="button" value='Delete' class="btn btn-danger"></a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </center>
<?php
            } else {
                echo "<p>No matches found</p>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
        }
    }
    // Close statement
    mysqli_stmt_close($stmt);
}

// close connection
mysqli_close($con);
?>

</html>