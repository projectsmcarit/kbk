<?php
 session_start();
include("../connection.php");
include("../kbk_library.php");
    $type=$_SESSION["work_type"];
    $date=$_SESSION["month_year"];
    $unit=$_SESSION["unit"];

 if($type=='Spinning')
{
    $ar=$_SESSION["spinning_excel_data"];
    $yarn_array= $_SESSION["yarn_total_excel_data"];
     $classid='Aneesh';
     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Spinning Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A3:BB3')->getFont()->setBold(true)->setSize(12);

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A3:BB3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AD1');
    $objSheet->mergeCells('A2:AD2');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('SPINNING WAGES FOR - '.$unit.' UNIT '.$date);

    $objSheet->getCell('A3')->setValue('SI No');            
    $objSheet->getCell('B3')->setValue('Artisan Code');
    $objSheet->getCell('C3')->setValue('Artisan Name');
    $objSheet->getCell('D3')->setValue('Variety');
    $objSheet->getCell('E3')->setValue('No. of Hanks');
    $objSheet->getCell('F3')->setValue('Rate');
    $objSheet->getCell('G3')->setValue('Value');
    $objSheet->getCell('H3')->setValue('Sliver Value');
    $objSheet->getCell('I3')->setValue('Sliver Consumption');
    $objSheet->getCell('J3')->setValue('Wages');
    $objSheet->getCell('K3')->setValue('W/F Fund');
    $objSheet->getCell('L3')->setValue('w/f Recovery');
    $objSheet->getCell('M3')->setValue('Adv Recovery');
    $objSheet->getCell('N3')->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('O3')->setValue('Covid adv.');
    $objSheet->getCell('P3')->setValue('LIC');
    $objSheet->getCell('Q3')->setValue('Net wages');
    $objSheet->getCell('R3')->setValue('Holiday wages');
    $objSheet->getCell('S3')->setValue('Total Wages Payable');
    $objSheet->getCell('T3')->setValue('ESI Contr');
    $objSheet->getCell('U3')->setValue('Yarn Incentive');
    $objSheet->getCell('V3')->setValue('Minimum Wages');
    $objSheet->getCell('W3')->setValue('DA Days');
    $objSheet->getCell('X3')->setValue('DA Wages');
    $objSheet->getCell('Y3')->setValue('Total Minimum Wages');
    $objSheet->getCell('Z3')->setValue('Net Minimum Wages');
    $objSheet->getCell('AA3')->setValue('Attendance');
    $objSheet->getCell('AB3')->setValue('Account Number');
    $objSheet->getCell('AC3')->setValue('IFSC');
    $objSheet->getCell('AD3')->setValue('Bank Name');



    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);
    $objSheet->getRowDimension('3')->setRowHeight(45);
    $objSheet->getStyle('A3:BB3')->getAlignment()->setWrapText(true);


    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(15);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(15);
    $objSheet->getColumnDimension('I')->setWidth(14);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setAutoSize(true);
    $objSheet->getColumnDimension('AB')->setAutoSize(true);
    $objSheet->getColumnDimension('AC')->setAutoSize(true);
    $objSheet->getColumnDimension('AD')->setAutoSize(true);

    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        foreach($temp as $j)
        {
            if($j=='AB')
            {
                $objSheet->setCellValueExplicit('AB'.($i+4),$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.($i+4))->setValue($ar[$i][$k]);
               $k=$k+1; 
            }
            
        }
    }
    $k=$i+4;
    // $objSheet->mergeCells('A'.$k.':AA'.$k);


    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Unit Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($k))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($k))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($k))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($k))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($k))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($k))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($k))->setValue($ar[$i][16]);
    $objSheet->getCell('R'.($k))->setValue($ar[$i][17]);
    $objSheet->getCell('S'.($k))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($k))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($k))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($k))->setValue($ar[$i][21]);
    $objSheet->getCell('X'.($k))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($k))->setValue($ar[$i][24]);
    $objSheet->getCell('Z'.($k))->setValue($ar[$i][25]);


    $res14=retrieveData("SELECT yarn_code,type from kbk_yarn_variety group by yarn_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['yarn_code'];
        if($yarn_array[$temp4]['noh']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$k.':D'.$k)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$k.':B'.$k);
            $objSheet->mergeCells('C'.$k.':D'.$k);
            $objSheet->getCell('A'.($k))->setValue($res14[$l]['yarn_code']);
            $objSheet->getCell('C'.($k))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($k))->setValue($yarn_array[$temp4]['noh']);
            $objSheet->getCell('G'.($k))->setValue($yarn_array[$temp4]['val']);
            $objSheet->getCell('H'.($k))->setValue($yarn_array[$temp4]['sv']);
            $objSheet->getCell('I'.($k))->setValue($yarn_array[$temp4]['sliver']);
            $objSheet->getCell('J'.($k))->setValue($yarn_array[$temp4]['wage']);

        }
   
    }
    $k=$k+1;
    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Grand Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Spinning_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}

if($type=='Weaving')
{
    $ar=$_SESSION["weaving_excel_data"];

    $weav_array= $_SESSION["weaving_total_excel_data"];
     $classid='Aneesh';
     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Weaving Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A3:BB3')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A4:BB4')->getFont()->setBold(true)->setSize(12);

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A3:BB3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AO1');
    $objSheet->mergeCells('A2:AQ2');
    $objSheet->mergeCells('AD3:AH3');
    $objSheet->mergeCells('AI3:AM3');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('WEAVING WAGES FOR - '.$unit.' UNIT '.$date);
    $objSheet->getCell('AD3')->setValue('Warp');
    $objSheet->getCell('AI3')->setValue('Weft');  

    $objSheet->getCell('A4')->setValue('SI No');            
    $objSheet->getCell('B4')->setValue('Artisan Code');
    $objSheet->getCell('C4')->setValue('Artisan Name');
    $objSheet->getCell('D4')->setValue('Variety');
    $objSheet->getCell('E4')->setValue('No. of Pieces');
    $objSheet->getCell('F4')->setValue('Metre');
    $objSheet->getCell('G4')->setValue('Square Metre');
    $objSheet->getCell('H4')->setValue('Weaving Type');
    $objSheet->getCell('I4')->setValue('Rate');
    $objSheet->getCell('J4')->setValue('Prime Cost');
    $objSheet->getCell('K4')->setValue('Value');
    $objSheet->getCell('L4')->setValue('Wages');
    $objSheet->getCell('M4')->setValue('W/F Fund');
    $objSheet->getCell('N4')->setValue('w/f Recovery');
    $objSheet->getCell('O4')->setValue('Adv Recovery');
    $objSheet->getCell('P4')->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('Q4')->setValue('Covid adv.');
    $objSheet->getCell('R4')->setValue('LIC');
    $objSheet->getCell('S4')->setValue('Net wages');
    $objSheet->getCell('T4')->setValue('Holiday wages');
    $objSheet->getCell('U4')->setValue('Total Wages Payable');
    $objSheet->getCell('V4')->setValue('ESI Contr');
    $objSheet->getCell('W4')->setValue('Warp Yarn Incentive');
    $objSheet->getCell('X4')->setValue('Weft Yarn Incentive');
    $objSheet->getCell('Y4')->setValue('Minimum Wages');
    $objSheet->getCell('Z4')->setValue('DA Days');
    $objSheet->getCell('AA4')->setValue('DA Wages');
    $objSheet->getCell('AB4')->setValue('Total Minimum Wages');
    $objSheet->getCell('AC4')->setValue('Net Minimum Wages');
    
    $objSheet->getCell('AD4')->setValue('Grey');
    $objSheet->getCell('AE4')->setValue('Colour');
    $objSheet->getCell('AF4')->setValue('Black');
    $objSheet->getCell('AG4')->setValue('BL');
    $objSheet->getCell('AH4')->setValue('Total');
    $objSheet->getCell('AI4')->setValue('Grey');
    $objSheet->getCell('AJ4')->setValue('Colour');
    $objSheet->getCell('AK4')->setValue('Black');
    $objSheet->getCell('AL4')->setValue('BL');
    $objSheet->getCell('AM4')->setValue('Total');
    $objSheet->getCell('AN4')->setValue('Attendance');
    $objSheet->getCell('AO4')->setValue('Account Number');
    $objSheet->getCell('AP4')->setValue('IFSC');
    $objSheet->getCell('AQ4')->setValue('Bank Name');
    



    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);
    $objSheet->getRowDimension('3')->setRowHeight(20);
    $objSheet->getRowDimension('4')->setRowHeight(45);
    $objSheet->getStyle('A4:BB4')->getAlignment()->setWrapText(true);


    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(20);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(20);
    $objSheet->getColumnDimension('I')->setWidth(14);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setWidth(20);
    $objSheet->getColumnDimension('AB')->setWidth(20);
    $objSheet->getColumnDimension('AC')->setWidth(15);
    $objSheet->getColumnDimension('AD')->setWidth(15);
    $objSheet->getColumnDimension('AE')->setWidth(15);
    $objSheet->getColumnDimension('AF')->setWidth(15);
    $objSheet->getColumnDimension('AG')->setWidth(15);
    $objSheet->getColumnDimension('AH')->setWidth(15);
    $objSheet->getColumnDimension('AI')->setWidth(15);
    $objSheet->getColumnDimension('AJ')->setWidth(15);
    $objSheet->getColumnDimension('AK')->setWidth(15);
    $objSheet->getColumnDimension('AL')->setWidth(15);
    $objSheet->getColumnDimension('AM')->setWidth(15);
    $objSheet->getColumnDimension('AN')->setWidth(15);
    $objSheet->getColumnDimension('AO')->setAutoSize(true);
    $objSheet->getColumnDimension('AP')->setAutoSize(true);
    $objSheet->getColumnDimension('AQ')->setAutoSize(true);
    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        foreach($temp as $j)
        {
            if($j=='AO')
            {
                $objSheet->setCellValueExplicit('AO'.($i+5),$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.($i+5))->setValue($ar[$i][$k]);
            $k=$k+1; 
            }
        }
    }

    $k=$i+5;

        $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Unit Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($k))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
  
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($k))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($k))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($k))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($k))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($k))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($k))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($k))->setValue($ar[$i][16]);
    $objSheet->getCell('R'.($k))->setValue($ar[$i][17]);
    $objSheet->getCell('S'.($k))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($k))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($k))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($k))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($k))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($k))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($k))->setValue($ar[$i][24]);
    $objSheet->getCell('AA'.($k))->setValue($ar[$i][26]);
    $objSheet->getCell('AB'.($k))->setValue($ar[$i][27]);
    $objSheet->getCell('AC'.($k))->setValue($ar[$i][28]);
    $objSheet->getCell('AD'.($k))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($k))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($k))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($k))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($k))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($k))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($k))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($k))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($k))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($k))->setValue($ar[$i][38]);

    $res14=retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['weaving_code'];
        if($weav_array[$temp4]['mtr']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$k.':E'.$k)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$k.':B'.$k);
            $objSheet->mergeCells('C'.$k.':E'.$k);
            $objSheet->getCell('A'.($k))->setValue($res14[$l]['weaving_code']);
            $objSheet->getCell('C'.($k))->setValue($res14[$l]['type']);
            $objSheet->getCell('F'.($k))->setValue($weav_array[$temp4]['mtr']);
            $objSheet->getCell('G'.($k))->setValue($weav_array[$temp4]['sqmtr']);
            $objSheet->getCell('K'.($k))->setValue($weav_array[$temp4]['val']);
            $objSheet->getCell('J'.($k))->setValue($weav_array[$temp4]['pc']);
            $objSheet->getCell('AD'.($k))->setValue($weav_array[$temp4]['warp_grey']);
            $objSheet->getCell('AE'.($k))->setValue($weav_array[$temp4]['warp_colour']);
            $objSheet->getCell('AF'.($k))->setValue($weav_array[$temp4]['warp_black']);
            $objSheet->getCell('AG'.($k))->setValue($weav_array[$temp4]['warp_bl']);
            $objSheet->getCell('AH'.($k))->setValue($weav_array[$temp4]['warp_tot']);
            $objSheet->getCell('AI'.($k))->setValue($weav_array[$temp4]['weft_grey']);
            $objSheet->getCell('AJ'.($k))->setValue($weav_array[$temp4]['weft_colour']);
            $objSheet->getCell('AK'.($k))->setValue($weav_array[$temp4]['weft_black']);
            $objSheet->getCell('AL'.($k))->setValue($weav_array[$temp4]['weft_bl']);
            $objSheet->getCell('AM'.($k))->setValue($weav_array[$temp4]['weft_tot']);

            

        }
   
    }
    $k=$k+1;
   
    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Grand Total');
    // $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($k))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
  
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($k))->setValue($ar[$i][10]);
   
    $objSheet->getCell('AD'.($k))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($k))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($k))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($k))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($k))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($k))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($k))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($k))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($k))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($k))->setValue($ar[$i][38]);



    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Weaving_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}


 if($type=='Preprocessing')
{
     $ar=$_SESSION["preprocessing_excel_data"];
    $yarn_array= $_SESSION["rm_variety_excel_data"];
     $classid='Aneesh';
     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Preprocessing Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A3:BB3')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A4:BB4')->getFont()->setBold(true)->setSize(12);

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A3:BB3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A4:BB4')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AW1');
    $objSheet->mergeCells('A2:AW2');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('PREPROCESSING WAGES FOR - '.$unit.' UNIT '.$date);

    $objSheet->mergeCells('E3:L3');
    $objSheet->mergeCells('M3:T3');
    $objSheet->mergeCells('U3:AB3');
   

    $objSheet->getCell('E3')->setValue('Sizing');
    $objSheet->getCell('M3')->setValue('Bobin Winding');
    $objSheet->getCell('U3')->setValue('Warping');

    $objSheet->getCell('A4')->setValue('SI No');            
    $objSheet->getCell('B4')->setValue('Artisan Code');
    $objSheet->getCell('C4')->setValue('Artisan Name');
    $objSheet->getCell('D4')->setValue('Variety');

    $objSheet->getCell('E4')->setValue('Grey');
    $objSheet->getCell('F4')->setValue('Colour');
    $objSheet->getCell('G4')->setValue('BL');
    $objSheet->getCell('H4')->setValue('Black');
    $objSheet->getCell('I4')->setValue('Total');
    $objSheet->getCell('J4')->setValue('Rate');
    $objSheet->getCell('K4')->setValue('Wages');
    $objSheet->getCell('L4')->setValue('YI');

    $objSheet->getCell('M4')->setValue('Grey');
    $objSheet->getCell('N4')->setValue('Colour');
    $objSheet->getCell('O4')->setValue('BL');
    $objSheet->getCell('P4')->setValue('Black');
    $objSheet->getCell('Q4')->setValue('Total');
    $objSheet->getCell('R4')->setValue('Rate');
    $objSheet->getCell('S4')->setValue('Wages');
    $objSheet->getCell('T4')->setValue('YI');

    $objSheet->getCell('U4')->setValue('Grey');
    $objSheet->getCell('V4')->setValue('Colour');
    $objSheet->getCell('W4')->setValue('BL');
    $objSheet->getCell('X4')->setValue('Black');
    $objSheet->getCell('Y4')->setValue('Total');
    $objSheet->getCell('Z4')->setValue('Rate');
    $objSheet->getCell('AA4')->setValue('Wages');
    $objSheet->getCell('AB4')->setValue('YI');

    $objSheet->getCell('AC4')->setValue('Wages');
    $objSheet->getCell('AD4')->setValue('W/F Fund');
    $objSheet->getCell('AE4')->setValue('w/f Recovery');
    $objSheet->getCell('AF4')->setValue('Adv Recovery');
    $objSheet->getCell('AG4')->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('AH4')->setValue('Covid adv.');
    $objSheet->getCell('AI4')->setValue('LIC');
    $objSheet->getCell('AJ4')->setValue('Net wages');
    $objSheet->getCell('AK4')->setValue('Holiday wages');
    $objSheet->getCell('AL4')->setValue('Total Wages Payable');
    $objSheet->getCell('AM4')->setValue('ESI Contr');
    $objSheet->getCell('AN4')->setValue('Total Yarn Incentive');
    $objSheet->getCell('AO4')->setValue('Minimum Wages');
    $objSheet->getCell('AP4')->setValue('DA Days');
    $objSheet->getCell('AQ4')->setValue('DA Wages');
    $objSheet->getCell('AR4')->setValue('Total Minimum Wages');
    $objSheet->getCell('AS4')->setValue('Net Minimum Wages');
    $objSheet->getCell('AT4')->setValue('Attendance');
    $objSheet->getCell('AU4')->setValue('Account Number');
    $objSheet->getCell('AV4')->setValue('IFSC');
    $objSheet->getCell('AW4')->setValue('Bank Name');



    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);
    $objSheet->getRowDimension('3')->setRowHeight(20);
    $objSheet->getRowDimension('4')->setRowHeight(45);
    $objSheet->getStyle('A3:BB3')->getAlignment()->setWrapText(true);
    $objSheet->getStyle('A4:BB4')->getAlignment()->setWrapText(true);

    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(15);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(15);
    $objSheet->getColumnDimension('I')->setWidth(15);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setWidth(15);
    $objSheet->getColumnDimension('AB')->setWidth(15);

    $objSheet->getColumnDimension('AC')->setWidth(15);
    $objSheet->getColumnDimension('AD')->setWidth(15);
    $objSheet->getColumnDimension('AE')->setWidth(15);
    $objSheet->getColumnDimension('AF')->setWidth(15);
    $objSheet->getColumnDimension('AG')->setWidth(15);
    $objSheet->getColumnDimension('AH')->setWidth(15);
    $objSheet->getColumnDimension('AI')->setWidth(15);
    $objSheet->getColumnDimension('AJ')->setWidth(15);
    $objSheet->getColumnDimension('AK')->setWidth(15);
    $objSheet->getColumnDimension('AL')->setWidth(15);
    $objSheet->getColumnDimension('AM')->setWidth(15);
    $objSheet->getColumnDimension('AN')->setWidth(15);
    $objSheet->getColumnDimension('AO')->setWidth(15);
    $objSheet->getColumnDimension('AP')->setWidth(15);
    $objSheet->getColumnDimension('AQ')->setWidth(15);
    $objSheet->getColumnDimension('AR')->setWidth(15);
    $objSheet->getColumnDimension('AS')->setWidth(15);
    $objSheet->getColumnDimension('AT')->setAutoSize(true);
    $objSheet->getColumnDimension('AU')->setAutoSize(true);
    $objSheet->getColumnDimension('AV')->setAutoSize(true);
    $objSheet->getColumnDimension('AW')->setAutoSize(true);
   
    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        // $objSheet->getStyle('AU'.($i+5))->getNumberFormat()->setFormatCode('0');
        // $objSheet->setCellValueExplicit('AU'.($i+5),PHPExcel_Cell_DataType::TYPE_STRING);
        foreach($temp as $j)
        {
            if($j=='AU')
            {
                $objSheet->setCellValueExplicit('AU'.($i+5),$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.($i+5))->setValue($ar[$i][$k]);
            $k=$k+1; 
            }
            
        }
    }
    $k=$i+5;

    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Unit Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($k))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);

    $objSheet->getCell('K'.($k))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($k))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($k))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($k))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($k))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($k))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($k))->setValue($ar[$i][16]);

    $objSheet->getCell('S'.($k))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($k))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($k))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($k))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($k))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($k))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($k))->setValue($ar[$i][24]);
   
 
    $objSheet->getCell('AA'.($k))->setValue($ar[$i][26]);
    $objSheet->getCell('AB'.($k))->setValue($ar[$i][27]);
    $objSheet->getCell('AC'.($k))->setValue($ar[$i][28]);
    $objSheet->getCell('AD'.($k))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($k))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($k))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($k))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($k))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($k))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($k))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($k))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($k))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($k))->setValue($ar[$i][38]);
    $objSheet->getCell('AO'.($k))->setValue($ar[$i][39]);
   
    $objSheet->getCell('AQ'.($k))->setValue($ar[$i][40]);
    $objSheet->getCell('AR'.($k))->setValue($ar[$i][42]);
    $objSheet->getCell('AS'.($k))->setValue($ar[$i][43]);
    $objSheet->getCell('AT'.($k))->setValue($ar[$i][44]);


    $res14=retrieveData("SELECT rm_code,type from kbk_preprocessing_variety group by rm_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['rm_code'];
        if($yarn_array[$temp4]['grand_tot']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$k.':D'.$k)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$k.':B'.$k);
            $objSheet->mergeCells('C'.$k.':D'.$k);
            $objSheet->getCell('A'.($k))->setValue($res14[$l]['rm_code']);
            $objSheet->getCell('C'.($k))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($k))->setValue($yarn_array[$temp4]['s_grey']);
            $objSheet->getCell('F'.($k))->setValue($yarn_array[$temp4]['s_colour']);
            $objSheet->getCell('G'.($k))->setValue($yarn_array[$temp4]['s_bl']);
            $objSheet->getCell('H'.($k))->setValue($yarn_array[$temp4]['s_black']);
            $objSheet->getCell('I'.($k))->setValue($yarn_array[$temp4]['s_tot']);

            $objSheet->getCell('M'.($k))->setValue($yarn_array[$temp4]['b_grey']);
            $objSheet->getCell('N'.($k))->setValue($yarn_array[$temp4]['b_colour']);
            $objSheet->getCell('O'.($k))->setValue($yarn_array[$temp4]['b_bl']);
            $objSheet->getCell('P'.($k))->setValue($yarn_array[$temp4]['b_black']);
            $objSheet->getCell('Q'.($k))->setValue($yarn_array[$temp4]['b_tot']);

            $objSheet->getCell('U'.($k))->setValue($yarn_array[$temp4]['w_grey']);
            $objSheet->getCell('V'.($k))->setValue($yarn_array[$temp4]['w_colour']);
            $objSheet->getCell('W'.($k))->setValue($yarn_array[$temp4]['w_bl']);
            $objSheet->getCell('X'.($k))->setValue($yarn_array[$temp4]['w_black']);
            $objSheet->getCell('Y'.($k))->setValue($yarn_array[$temp4]['w_tot']);
            

        }
   
    }

    $k=$k+1;

    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Grand Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($k))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);

    $objSheet->getCell('M'.($k))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($k))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($k))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($k))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($k))->setValue($ar[$i][16]);

    $objSheet->getCell('U'.($k))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($k))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($k))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($k))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($k))->setValue($ar[$i][24]);
   
 



    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Preprocessing_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}
if($type=='Twisting')
{
    $ar=$_SESSION["twisting_excel_data"];
    $yarn_array= $_SESSION["twist_total_excel_data"];
     $classid='Aneesh';
     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Twisting Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    $objSheet->getStyle('A3:BB3')->getFont()->setBold(true)->setSize(12);

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A3:BB3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AD1');
    $objSheet->mergeCells('A2:AD2');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('TWISTING WAGES FOR - '.$unit.' UNIT '.$date);

    $objSheet->getCell('A3')->setValue('SI No');            
    $objSheet->getCell('B3')->setValue('Artisan Code');
    $objSheet->getCell('C3')->setValue('Artisan Name');
    $objSheet->getCell('D3')->setValue('Variety');
    $objSheet->getCell('E3')->setValue('No. of Hanks');
    $objSheet->getCell('F3')->setValue('Rate');
    $objSheet->getCell('G3')->setValue('Value');
    $objSheet->getCell('H3')->setValue('Basic Yarn Value');
    $objSheet->getCell('I3')->setValue('Yarn Consumption');
    $objSheet->getCell('J3')->setValue('Wages');
    $objSheet->getCell('K3')->setValue('W/F Fund');
    $objSheet->getCell('L3')->setValue('w/f Recovery');
    $objSheet->getCell('M3')->setValue('Adv Recovery');
    $objSheet->getCell('N3')->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('O3')->setValue('Covid adv.');
    $objSheet->getCell('P3')->setValue('LIC');
    $objSheet->getCell('Q3')->setValue('Net wages');
    $objSheet->getCell('R3')->setValue('Holiday wages');
    $objSheet->getCell('S3')->setValue('Total Wages Payable');
    $objSheet->getCell('T3')->setValue('ESI Contr');
    $objSheet->getCell('U3')->setValue('Yarn Incentive');
    $objSheet->getCell('V3')->setValue('Minimum Wages');
    $objSheet->getCell('W3')->setValue('DA Days');
    $objSheet->getCell('X3')->setValue('DA Wages');
    $objSheet->getCell('Y3')->setValue('Total Minimum Wages');
    $objSheet->getCell('Z3')->setValue('Net Minimum Wages');
    $objSheet->getCell('AA3')->setValue('Attendance');
    $objSheet->getCell('AB3')->setValue('Account Number');
    $objSheet->getCell('AC3')->setValue('IFSC');
    $objSheet->getCell('AD3')->setValue('Bank Name');



    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);
    $objSheet->getRowDimension('3')->setRowHeight(45);
    $objSheet->getStyle('A3:BB3')->getAlignment()->setWrapText(true);


    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(15);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(15);
    $objSheet->getColumnDimension('I')->setWidth(14);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setAutoSize(true);
    $objSheet->getColumnDimension('AB')->setAutoSize(true);
    $objSheet->getColumnDimension('AC')->setAutoSize(true);
    $objSheet->getColumnDimension('AD')->setAutoSize(true);

    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        foreach($temp as $j)
        {
            if($j=='AB')
            {
                $objSheet->setCellValueExplicit('AB'.($i+4),$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.($i+4))->setValue($ar[$i][$k]);
               $k=$k+1; 
            }
            
        }
    }
    $k=$i+4;
    // $objSheet->mergeCells('A'.$k.':AA'.$k);


    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Unit Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($k))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($k))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($k))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($k))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($k))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($k))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($k))->setValue($ar[$i][16]);
    $objSheet->getCell('R'.($k))->setValue($ar[$i][17]);
    $objSheet->getCell('S'.($k))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($k))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($k))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($k))->setValue($ar[$i][21]);
    $objSheet->getCell('X'.($k))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($k))->setValue($ar[$i][24]);
    $objSheet->getCell('Z'.($k))->setValue($ar[$i][25]);


    $res14=retrieveData("SELECT twisting_code,type from kbk_twisting_variety group by twisting_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['twisting_code'];
        if($yarn_array[$temp4]['noh']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$k.':D'.$k)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$k.':B'.$k);
            $objSheet->mergeCells('C'.$k.':D'.$k);
            $objSheet->getCell('A'.($k))->setValue($res14[$l]['twisting_code']);
            $objSheet->getCell('C'.($k))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($k))->setValue($yarn_array[$temp4]['noh']);
            $objSheet->getCell('G'.($k))->setValue($yarn_array[$temp4]['val']);
            $objSheet->getCell('H'.($k))->setValue($yarn_array[$temp4]['yv']);
            $objSheet->getCell('I'.($k))->setValue($yarn_array[$temp4]['yarn']);
            $objSheet->getCell('J'.($k))->setValue($yarn_array[$temp4]['wage']);

        }
   
    }
    $k=$k+1;
    $objSheet->getStyle($k)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($k).':D'.($k));
    $objSheet->getCell('A'.($k))->setValue('Grand Total');
    $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Twisting_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}


?>
