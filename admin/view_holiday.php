  <?php
$noh = "";
$now = "";
$daw = "";
$hmonth="";
$hyear="";




        include('../connection.php'); // Using database connection file here
        if(isset($_POST['search']))
            {
                $hmonth = $_POST['hmonth'];  
                $hyear = $_POST['hyear'];
                $hday=01;
                date_default_timezone_set('Asia/Kolkata');
                $htime= strtotime("$hyear-$hmonth-$hday");

                $hdate=date("Y/m/d",$htime);
                $sql = "select * from kbk_holidays WHERE h_date = '$hdate'";  
                
                $result = mysqli_query($con, $sql);
                $row = mysqli_fetch_array($result); 

                
                
                $noh     = $row['holiday_no'];
                $now     = $row['total_wd'];
                $daw  = $row['da_wages'];

            }
            if(isset($_POST['update']))
             {
              $hmonth = $_POST['hmonth'];  
              $hyear = $_POST['hyear'];
              $hday=01;
              date_default_timezone_set('Asia/Kolkata');
              $htime= strtotime("$hyear-$hmonth-$hday");

              $hdate=date("Y/m/d",$htime);

              $holidayno1 = $_POST['hno'];
                $totalno1 = $_POST['wno'];
                $dawages1 = floatval($_POST['dawage']);

                $sql1 = "UPDATE kbk_holidays SET holiday_no = $holidayno1, total_wd = $totalno1, da_wages = $dawages1 WHERE h_date = '$hdate' ";
                $re=$con->query($sql1);

                if($re)
                {    
 
                    echo "<script>alert($hdate)</script>";
                    
                }
                else
                {
                    echo "<script>alert('Error!!! Try Again')</script>";
                    
                }
            }
?>
                                   


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Holiday</title>
   
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />


    
    
      
   <?php 
   include('header.php');  
   ?>
   
   
  </head>
  
  <body>
    <!-- Start your project here-->
    <!-- Tabs navs -->
<div style="padding-top: 50px;">
<ul class="nav nav-tabs nav-fill mb-3" id="ex1" role="tablist">
  <li class="nav-item" role="presentation">
    <a
      class="nav-link active"
      id="ex1-tab-1"
      data-mdb-toggle="tab"
      href="#ex1-tabs-1"
      role="tab"
      aria-controls="ex1-tabs-1"
      aria-selected="true"
      >Add Holyday/working days </a
    >
  </li>
  <li class="nav-item" role="presentation">
    <a
      class="nav-link"
      id="ex1-tab-2"
      data-mdb-toggle="tab"
      href="#ex1-tabs-2"
      role="tab"
      aria-controls="ex1-tabs-2"
      aria-selected="false"
      >Edit Holyday/working days </a
    >
  </li>
  
</ul></div>
<!-- Tabs navs -->

<!-- Tabs content -->
<div class="tab-content" id="ex1-content">
  <div
    class="tab-pane fade show active"
    id="ex1-tabs-1"
    role="tabpanel"
    aria-labelledby="ex1-tab-1"
  >
  <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <div class="col-md-6 col-lg-5 d-none d-md-block">
                  <img
                    src="../images/mahatma_gandhi.jpg"
                    alt="Holiday form"
                    class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                  />
                </div>
                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                  <div class="card-body p-4 p-lg-5 text-black">
    
                    <form action="holiday.php" method="post"> 
    
                      <div class="d-flex align-items-center mb-3 pb-1">
                        
                        <span class="h1 fw-bold mb-0">Holidays</span>
                      </div>
                      
                      
                      <div class="row">
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="hmonth" aria-label="Default select example" required> 
                          <option disabled selected value="">-- Select Month--</option>
                          <?php
                              $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                              for ($i_month = 1; $i_month <= 12; $i_month++)
                                {
                                 
                                 echo '<option value="'.$i_month.'">'. $months_name[$i_month-1].'</option>'."\n";
                                 
                                }	
                            ?>  
                        </select>
                       </div>
                       <div class="col-md-6 mb-4">
                      
                      <select class="form-select" name="hyear" aria-label="Default select example" required>
                          <option disabled selected value="">-- Select Year--</option>
                          <?php
                              
                            
                              for ($i_year = date('Y')-5; $i_year <= date('Y')+5; $i_year++)
                                {
                                 

                                 echo '<option value="'.$i_year.'">'.$i_year.'</option>'."\n";
                                
                                }	
                            ?>  
                        </select>
                      </div>                                           
                      </div>

                      <div class="form-outline mb-4">
                        <input type="number" min="0" max="31" name="hno" id="hno" class="form-control form-control-lg" required/>
                        <label class="form-label" for="form2Example17" >Number of Holidays</label>
                      </div>

                      <div class="form-outline mb-4">
                        <input type="number" min="0" max="31" name="tno" id="tno" class="form-control form-control-lg" required />
                        <label class="form-label" for="form2Example17" >Number of Working Days</label>
                      </div>


                      <div class="form-outline mb-4">
                        <input type="text" min="0"  name="da_wage" id="dawage" class="form-control form-control-lg" required />
                        <label class="form-label" for="form2Example17" >DA Wages</label>
                      </div>


    
                      <!-- <div class="form-outline mb-4">
                        <input type="month" id="date" class="form-control form-control-lg" />
                        <label class="form-label" for="form2Example27">Month and Year</label>
                      </div> -->
                         
                      <div class="pt-1 mb-4">
                        <input type="submit" name="submit" value="SUBMIT" class="btn btn-dark btn-lg btn-block" />
                      </div>
                                          </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>  


  </div>
  <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
    
  <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <div class="col-md-6 col-lg-5 d-none d-md-block">
                  <img
                    src="../images/mahatma_gandhi.jpg"
                    alt="Holiday form"
                    class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                  />
                </div>
                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                  <div class="card-body p-4 p-lg-5 text-black">
    
                    <form method="post">
    
                      <div class="d-flex align-items-center mb-3 pb-1">
                        
                        <span class="h1 fw-bold mb-0">Holidays</span>
                      </div>


                      


                        <div class="row">
                        <div class="col-md-4 mb-4">
                          <label>Select Month and Year</label>
                              </div> 
                              </div>   
                      <div class="row">
                      <div class="col-md-4 mb-4">
    
                      <select class="form-select" name="hmonth" aria-label="Default select example">
                      
                          <option disabled >-- Select Month--</option> 
                          <?php
                              $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                                echo '<option value="'.$hmonth.'">'. $months_name[$hmonth-1].'</option>'."\n";
                              for ($i_month = 1; $i_month <= 12; $i_month++)
                                {
                                 
                                 echo '<option value="'.$i_month.'">'. $months_name[$i_month-1].'</option>'."\n";
                                 
                                }	
                            ?>  
                        </select>
                       </div>
                       <div class="col-md-4 mb-4">
                      
                      <select class="form-select" name="hyear" aria-label="Default select example">
                          <option disabled>-- Select Year--</option>
                          <?php
                              
                              echo '<option value="'.$hyear.'">'. $hyear.'</option>'."\n";
                              for ($i_year = 2000; $i_year <= date('Y')+5; $i_year++)
                                {
                                 

                                 echo '<option value="'.$i_year.'">'.$i_year.'</option>'."\n";
                                
                                }	
                            ?>  
                        </select>
                      </div>

                      <div class="col-md-4 mb-4"> 
                      <button type="submit" class="btn btn-warning btn-lg ms-2" name="search" method="post"  > Search </button>                                  
                      </div>
                      </div>

                      
                      

                         <!--<div class="input-group">
                        <div class="form-outline"> 
                           <input type="month" id="month" class="form-control" />
                          <label class="form-label" for="form1">Month</label>
                        </div>
                        <button type="button" class="btn btn-primary">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>-->



                      <div id="refresh-section">
                      <div class="row">
                      <div class="col-md-6 mb-4">                       
                      <label class="form-label" for="noh">Number Of Holidays</label>                   
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline ">
                          <input type="number" name="hno" id="numberofholidays" value="<?php echo $noh; ?>" class="form-control form-control-lg" />                    
                        </div>
                      </div>
                      </div>
                      
                      <div class="row">
                      <div class="col-md-6 mb-4">                       
                      <label class="form-label" for="noh">Number Of Working Days</label>                   
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline ">
                          <input type="number" name="wno" id="numberofholidays" value="<?php echo $now; ?>" class="form-control form-control-lg" />                    
                        </div>
                      </div></div>

                      <div class="row">
                      <div class="col-md-6 mb-4">                       
                      <label class="form-label" for="noh">DA Wages</label>                   
                      </div>
                      <div class="col-md-6 mb-4 ">
                        <div class="form-outline ">
                          <input type="text" name="dawage" id="dawages" value="<?php echo $daw; ?>" class="form-control form-control-lg" />                    
                        </div>
                      </div></div>

                      <div class="d-flex justify-content-end pt-3">
                      <input type="reset" class="btn btn-light btn-lg "></button>
                        <input type="submit" name="update" class="btn btn-warning btn-lg ms-2" value="Update">
                        
                      </div>
                      </div>
                      </div>
                      <script type="text/javascript">
                        
                       $(function() {
  
  $('a[data-mdb-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('lastTab', $(this).attr('href'));
  });
  var lastTab = localStorage.getItem('lastTab');
  
  if (lastTab) {
    $('[href="' + lastTab + '"]').tab('show');
  }
  
});
                          
                        

     
       </script>
                                          </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  
</div>

<!-- Tabs content -->
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>