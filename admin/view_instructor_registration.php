<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>INSTRUCTOR REGISTRATION</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->
    <form name="instructorregistration" action="instructor_registration.php" method="post"> 
    <header>
      <!-- Navbar -->
      <?php 
        include('header.php')
      ?>
      
      </header>
    
    
      <section class="h-100 bg-dark">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col">
        <div class="card card-registration my-4">
          <div class="row g-0">
            <div class="col-xl-6 d-none d-xl-block">
              <img
                src="../images/Mahatma_Gandhi.jpg"
                alt="Sample photo"
                class="img-fluid"
                style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
              />
            </div>
            <div class="col-xl-6">
              <div class="card-body p-md-5 text-black">
                <h3 class="mb-5 text-uppercase">Instructor registration</h3>
                
                <div class="form-outline mb-4">
                  <input type="text" name="name" id="name" class="form-control form-control-lg"  onchange="nameValidate()"  required />
                  <label class="form-label" for="name"><span style="color:#ff0000">*</span>Name</label>
                </div>
                <span id="errorname"></span>

                

                <div class="form-outline mb-4">
                  <input type="text" name="address" id="address" class="form-control form-control-lg" required />
                  <label class="form-label" for="address"><span style="color:#ff0000">*</span>Address</label>
                </div>

                
                <div class="form-outline mb-4">
                  <input type="number" name="pincode" id="pin" class="form-control form-control-lg" onchange="pinValidate()"   required />
                  <label class="form-label" for="pin"><span style="color:#ff0000">*</span>Pin Code</label>
                </div>
                <span id="errorpin"></span>
                
                <div class="form-outline mb-4">
                  <input type="number" name="phone" id="contact" class="form-control form-control-lg" onchange="phoneValidate()"  required />
                  <label class="form-label" for="contact">Phone No</label>
                </div>
                <span id="errorphone"></span>

                <div class="form-outline mb-4">
                  <input type="email" name="email" id="email" class="form-control form-control-lg" onchange="emailValidate()" />
                  <label class="form-label" for="email">Email</label>
                </div>
                <span id="erroremail"></span>

                <div class="form-outline mb-4">
                  <input type="number" step="any" name="wages" id="wages" class="form-control form-control-lg"  />
                  <label class="form-label" for="pass">Wages</label>
                </div>

                <div class="form-outline mb-4">
                  <input type="text" name="user_name" id="username" class="form-control form-control-lg" required />
                  <label class="form-label" for="name"><span style="color:#ff0000">*</span>User Name</label>
                </div>
                 
                
                <div class="form-outline mb-4">
                  <input type="password"  step="any" name="inspassword" id="pass" class="form-control form-control-lg"  onfocus="passFocus()" onchange="passValidate()" required />
                  <label class="form-label" for="pass"><span style="color:#ff0000">*</span>Password</label>
                </div>
                <span id="errorpass"></span>
                <div class="form-outline mb-4">
                  <input type="text" step="any" name="cnfrmpass" id="cnfrmpass" class="form-control form-control-lg" onkeyup="matchPassword()"   required />
                  <label class="form-label" for="pass"><span style="color:#ff0000">*</span>Confirm Password</label>
                </div>
                <span id='message'></span>

                

                <div class="d-flex justify-content-end pt-3">
                  <button type="reset" class="btn btn-light btn-lg">Reset all</button>
                  <input type="submit" name="submit" value="Submit" class="btn btn-warning btn-lg ms-2">
                </div>
     
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</form>
 <!-- End your project here-->
  <!-- validation javascript -->
  <script type="text/javascript" src="validation.js"></script>

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
