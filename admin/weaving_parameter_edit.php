<?php
include('../connection.php');
session_start();
if(isset($_POST['update']))
{

	$weavingid=intval($_POST['updateid']);
	$rowid=$_POST['rowid'];
	$weavingcode=$_POST['weavingcode'.$rowid];
	$type=$_POST['type'.$rowid];
	$countable=intval($_POST['countable'.$rowid]);
	$length=floatval($_POST['length'.$rowid]);
	$width=floatval($_POST['width'.$rowid]);
	$rate=floatval($_POST['rate'.$rowid]);
	$primecost=floatval($_POST['primecost'.$rowid]);
	$esi=floatval($_POST['esi'.$rowid]);
	$wf=floatval($_POST['wf'.$rowid]);
	$ai=floatval($_POST['ai'.$rowid]);
	$yarnwarp=intval($_POST['yarnwarp'.$rowid]);
	$yarnweft=intval($_POST['yarnweft'.$rowid]);
	$threadgroup=$_POST['threadgroup'.$rowid];
	
	$month=$_POST['month'.$rowid];
	$year=$_POST['year'.$rowid];
	$day=01;
    date_default_timezone_set('Asia/Kolkata');
    $htime= strtotime("$year-$month-$day");
    $hdate=date("Y/m/d",$htime);

    $sql1 = "UPDATE kbk_weaving_variety SET weaving_code='$weavingcode',type='$type',rate=$rate,countable=$countable,length=$length,wf=$wf,esi=$esi,prime_cost=$primecost,width=$width,wef='$hdate',ai=$ai,yarn_warp=$yarnwarp,yarn_weft=$yarnweft,thread_group='$threadgroup' where weaving_id=$weavingid";
    $res1 = $con -> query($sql1);
    
	
	//weaving type
	$fw_wage=floatval($_POST['fw_wage'.$rowid]);
	$fw_warpyi=floatval($_POST['fw_warpyi'.$rowid]);
	$fw_weftyi=floatval($_POST['fw_weftyi'.$rowid]);
	$fw_mw=floatval($_POST['fw_mw'.$rowid]);
	$fw_target=floatval($_POST['fw_target'.$rowid]);

	$sql2 = "UPDATE kbk_weaving_type SET w_wages=$fw_wage,warp_yi=$fw_warpyi,weft_yi=$fw_weftyi,min_wages=$fw_mw,target=$fw_target,wef='$hdate' where w_type='FULL WEAVING' and weaving_id=$weavingid";
	$res2 = $con -> query($sql2);

	$rm_wage=floatval($_POST['rm_wage'.$rowid]);
	$rm_warpyi=floatval($_POST['rm_warpyi'.$rowid]);
	$rm_weftyi=floatval($_POST['rm_weftyi'.$rowid]);
	$rm_mw=floatval($_POST['rm_mw'.$rowid]);
	$rm_target=floatval($_POST['rm_target'.$rowid]);
	$sql3 = "UPDATE kbk_weaving_type SET w_wages=$rm_wage,warp_yi=$rm_warpyi,weft_yi=$rm_weftyi,min_wages=$rm_mw,target=$rm_target,wef='$hdate' where w_type='RM WARP WEAVING' and weaving_id=$weavingid";
	$res3 = $con -> query($sql3);
	var_dump($res3);

	$mw_wage=floatval($_POST['mw_wage'.$rowid]);
	$mw_warpyi=floatval($_POST['mw_warpyi'.$rowid]);
	$mw_weftyi=floatval($_POST['mw_weftyi'.$rowid]);
	$mw_mw=floatval($_POST['mw_mw'.$rowid]);
	$mw_target=floatval($_POST['mw_target'.$rowid]);
	$sql4 = "UPDATE kbk_weaving_type SET w_wages=$mw_wage,warp_yi=$mw_warpyi,weft_yi=$mw_weftyi,min_wages=$mw_mw,target=$mw_target,wef='$hdate' where w_type='MEAR WEAVING' and weaving_id=$weavingid";
	$res4 = $con -> query($sql4);

	//yarn consumption
	$warp_grey=floatval($_POST['warp_grey'.$rowid]);
	$warp_colour=floatval($_POST['warp_colour'.$rowid]);
	$warp_black=floatval($_POST['warp_black'.$rowid]);
	$warp_bl=floatval($_POST['warp_bl'.$rowid]);
	$weft_grey=floatval($_POST['weft_grey'.$rowid]);
	$weft_colour=floatval($_POST['weft_colour'.$rowid]);
	$weft_black=floatval($_POST['weft_black'.$rowid]);
	$weft_bl=floatval($_POST['weft_bl'.$rowid]);
	$sql5 = "UPDATE kbk_yarn_consumption SET warp_grey=$warp_grey,warp_colour=$warp_colour,warp_black=$warp_black,warp_bl=$warp_bl,weft_grey=$weft_grey,weft_colour=$weft_colour,weft_black=$weft_black,weft_bl=$weft_bl,wef='$hdate' where weaving_id=$weavingid";
	$res5 = $con -> query($sql5);

	if($res1&&$res2&&$res3&&$res4&&$res5)
    {
    	echo "<script>alert('Success')</script>";
    	echo "<script>window.location.href='view_weaving_parameter.php'</script>";
    }
    else
    {
    	echo "<script>alert('Error!!! Try Again')</script>";
    	echo "<script>window.location.href='view_weaving_parameter.php'</script>";
    }

}


?>