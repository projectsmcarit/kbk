<!DOCTYPE html>
<html lang="en">
  <head>
   
 
      <title>Preprocessing Report</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
   
      <!-- MDB icon -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <?php 
        include('header.php');
      ?>
</head>
<body>


<section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col col-xl-7">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
               
                <form action="view_report_spinning_entry.php" method="post">  
                    <div class="" style="padding:100px 100px 100px 100px;">
                        <div class="row" > 
                            <div class="col-md-8 mb-6">
                               <label for="">Full Report View</label>
                            </div>
                            <div class="col-md-4 mb-4">
                            <input type="submit" name="monthlyreport" id="monthlyreport" value="Full Report" class="btn btn-secondary" style="width:200px;">
                            </div>
                        </div>  
                        
                        <div class="row">
                            <div class="col-md-8 mb-6">
                               <label >Monthly Report</label>
                            </div>
                            <div class="col-md-4 mb-4">
                            <input type="submit" name="monthlyreport" id="monthlyreport" value="Report" class="btn btn-secondary" style="width:200px;">
                            </div>
                        </div>  

                    </div>                   
                         
                    <div class="mb-4 d-flex pt-3" style="justify-content: center;">
                        <input type="submit" name="back" id="submit" value="BACK" class="btn btn-secondary" />
                    </div>
                </form>
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    </body>
</html>