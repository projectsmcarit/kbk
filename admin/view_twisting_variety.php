<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Twisting Variety</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
   
     <?php 
   include('header.php');  
   ?>
  </head>
  <body>
    <!-- Start your project here-->
    <form name="twistingvariety" action="twisting_variety.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    
                    <img
                      src="../images/twisting_3.jpg"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .30rem; border-bottom-left-radius: .30rem;"
                    />
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                        <h3 class="mb-5 text-uppercase">Twisting Variety</h3>
      
                     <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="twistingcode" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n">Twisting Code</label>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="twistingvariety" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n">Twisting Variety</label><!--variety=type in table-->
                          </div>
                        </div>
                        
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="rate" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">Wage per Hank </label>
                      </div>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="hankvalue" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">Value per Hank </label>
                      </div>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="yarnvalue" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">Basic Yarn Value </label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="yarnconsumption" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">Yarn Consumption </label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="esi" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">ESI</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="welfare" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">Welfare fund </label>
                            </div>
                          </div>
                      </div>
      
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="yarnincentive" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Yarn Incentive </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="annualincentive" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Annual Incentive </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="minimumwage" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Minimum wage </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="target" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Target </label>
                        </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                          <input type="date" name="witheffectfrom" id="form3Example1m1" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">With Effect From </label>
                            
                          </div>
                        </div>
                        
                      </div>
      
                     
                      
      
                      <div class="d-flex justify-content-end pt-3">
                        <button type="submit" name="submit" class="btn btn-warning btn-lg ms-2">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>

                            