<?php
 session_start();
include("../connection.php");
include("../kbk_library.php");
    $type=$_SESSION["work_type"];
    $date=$_SESSION["month_year"];

    $unit=$_SESSION["unitname"];

 if($type=='Spinning')
{
   

  $spinning_array=$_SESSION["district_total_spinning_variety"];
  $dsum=$_SESSION["district_total_spinning"];
    // $yarn_array= $_SESSION["yarn_total_excel_data"];
     
     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Spinning Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A3:BB3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AD1');
    $objSheet->mergeCells('A2:AD2');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('SPINNING WAGES FOR - KOTTAYAM DISTRICT '.$date);

    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(15);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(15);
    $objSheet->getColumnDimension('I')->setWidth(14);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setAutoSize(true);
    $objSheet->getColumnDimension('AB')->setWidth(20);
    $objSheet->getColumnDimension('AC')->setWidth(15);
    $objSheet->getColumnDimension('AD')->setWidth(15);

 $spin_array=$_SESSION["spinning_excel_data"];
 $spin_variety=$_SESSION["spinning_variety_data"];
$y=3;

  for($z=0;$z<(sizeof($spin_array));$z=$z+1)
  {
    if($spin_array[$z][0][0]!=0)
      {      
        $ar=$spin_array[$z];
        $yarn_array=$spin_variety[$z];
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);
    $objSheet->mergeCells('A'.$y.':AD'.$y);
    $objSheet->getCell('A'.$y)->setValue($unit[$z].' UNIT'); 
$y=$y+1;
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);

    $objSheet->getCell('A'.$y)->setValue('SI No');            
    $objSheet->getCell('B'.$y)->setValue('Artisan Code');
    $objSheet->getCell('C'.$y)->setValue('Artisan Name');
    $objSheet->getCell('D'.$y)->setValue('Variety');
    $objSheet->getCell('E'.$y)->setValue('No. of Hanks');
    $objSheet->getCell('F'.$y)->setValue('Rate');
    $objSheet->getCell('G'.$y)->setValue('Value');
    $objSheet->getCell('H'.$y)->setValue('Sliver Value');
    $objSheet->getCell('I'.$y)->setValue('Sliver Consumption');
    $objSheet->getCell('J'.$y)->setValue('Wages');
    $objSheet->getCell('K'.$y)->setValue('W/F Fund');
    $objSheet->getCell('L'.$y)->setValue('w/f Recovery');
    $objSheet->getCell('M'.$y)->setValue('Adv Recovery');
    $objSheet->getCell('N'.$y)->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('O'.$y)->setValue('Covid adv.');
    $objSheet->getCell('P'.$y)->setValue('LIC');
    $objSheet->getCell('Q'.$y)->setValue('Net wages');
    $objSheet->getCell('R'.$y)->setValue('Holiday wages');
    $objSheet->getCell('S'.$y)->setValue('Total Wages Payable');
    $objSheet->getCell('T'.$y)->setValue('ESI Contr');
    $objSheet->getCell('U'.$y)->setValue('Yarn Incentive');
    $objSheet->getCell('V'.$y)->setValue('Minimum Wages');
    $objSheet->getCell('W'.$y)->setValue('DA Days');
    $objSheet->getCell('X'.$y)->setValue('DA Wages');
    $objSheet->getCell('Y'.$y)->setValue('Total Minimum Wages');
    $objSheet->getCell('Z'.$y)->setValue('Net Minimum Wages');
    $objSheet->getCell('AA'.$y)->setValue('Attendance');
    $objSheet->getCell('AB'.$y)->setValue('Account Number');
    $objSheet->getCell('AC'.$y)->setValue('IFSC');
    $objSheet->getCell('AD'.$y)->setValue('Bank Name');



    $objSheet->getRowDimension($y)->setRowHeight(20);
    $objSheet->getRowDimension($y)->setRowHeight(20);
    $objSheet->getRowDimension($y)->setRowHeight(45);
    $objSheet->getStyle('A'.$y.':BB'.$y)->getAlignment()->setWrapText(true);


    

    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $y=$y+1;
        $k=0;
        
        foreach($temp as $j)
        {
            if($j=='AB')
            {
                $objSheet->setCellValueExplicit('AB'.$y,$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.($y))->setValue($ar[$i][$k]);
               $k=$k+1;
            }
            
        }
    
    }
    
    $y=$y+1;
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Unit Total');
    $objSheet->getCell('E'.($y))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($y))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($y))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($y))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($y))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($y))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($y))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($y))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($y))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($y))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($y))->setValue($ar[$i][16]);
    $objSheet->getCell('R'.($y))->setValue($ar[$i][17]);
    $objSheet->getCell('S'.($y))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($y))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($y))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($y))->setValue($ar[$i][21]);
    $objSheet->getCell('X'.($y))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($y))->setValue($ar[$i][24]);
    $objSheet->getCell('Z'.($y))->setValue($ar[$i][25]);

 $y=$y+1;
    $res14=retrieveData("SELECT yarn_code,type from kbk_yarn_variety group by yarn_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['yarn_code'];
        if($yarn_array[$temp4]['noh']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$y.':D'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':D'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['yarn_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($y))->setValue($yarn_array[$temp4]['noh']);
            $objSheet->getCell('G'.($y))->setValue($yarn_array[$temp4]['val']);
            $objSheet->getCell('H'.($y))->setValue($yarn_array[$temp4]['sv']);
            $objSheet->getCell('I'.($y))->setValue($yarn_array[$temp4]['sliver']);
            $objSheet->getCell('J'.($y))->setValue($yarn_array[$temp4]['wage']);
            $y=$y+1;
        }
   
    }

    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Grand Total');
    $objSheet->getCell('E'.($y))->setValue($ar[$i][4]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($y))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($y))->setValue($ar[$i][8]);
    $objSheet->getCell('J'.($y))->setValue($ar[$i][9]);
    $y=$y+1;
    // $k=$k+1;
    // $objSheet->getStyle($k)->getFont()->setBold(true);
    // $objSheet->mergeCells('A'.($k).':D'.($k));
    // $objSheet->getCell('A'.($k))->setValue('Grand Total');
    // $objSheet->getCell('E'.($k))->setValue($ar[$i][4]);
    // $objSheet->getCell('G'.($k))->setValue($ar[$i][6]);
    // $objSheet->getCell('H'.($k))->setValue($ar[$i][7]);
    // $objSheet->getCell('I'.($k))->setValue($ar[$i][8]);
    // $objSheet->getCell('J'.($k))->setValue($ar[$i][9]);
}
}// for loop of every unit
 $y=$y+1;
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('District Total');
    $objSheet->getCell('E'.($y))->setValue($dsum['no']);
    $objSheet->getCell('G'.($y))->setValue($dsum['val']);
    $objSheet->getCell('H'.($y))->setValue($dsum['sv']);
    $objSheet->getCell('I'.($y))->setValue($dsum['sliver']);
    $objSheet->getCell('J'.($y))->setValue($dsum['wage']);
    $objSheet->getCell('K'.($y))->setValue($dsum['wf']);
    $objSheet->getCell('L'.($y))->setValue($dsum['wfrec']);
    $objSheet->getCell('M'.($y))->setValue($dsum['adrec']);
    $objSheet->getCell('N'.($y))->setValue($dsum['kcrec']);
    $objSheet->getCell('O'.($y))->setValue($dsum['covrec']);
    $objSheet->getCell('P'.($y))->setValue($dsum['lic']);
    $objSheet->getCell('Q'.($y))->setValue($dsum['nw']);
    $objSheet->getCell('R'.($y))->setValue($dsum['hw']);
    $objSheet->getCell('S'.($y))->setValue($dsum['tw']);
    $objSheet->getCell('T'.($y))->setValue($dsum['esi']);
    $objSheet->getCell('U'.($y))->setValue($dsum['yi']);
    $objSheet->getCell('V'.($y))->setValue($dsum['mw']);
    $objSheet->getCell('X'.($y))->setValue($dsum['dw']);
    $objSheet->getCell('Y'.($y))->setValue($dsum['tmw']);
    $objSheet->getCell('Z'.($y))->setValue($dsum['nmw']);
    $y=$y+1;
$res14=retrieveData("SELECT yarn_code,type from kbk_yarn_variety group by yarn_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['yarn_code'];
        if($spinning_array[$temp4]['noh']!=0)
        {
            $k=$k+1;
            $objSheet->getStyle('A'.$y.':D'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':D'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['yarn_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($y))->setValue($spinning_array[$temp4]['noh']);
            $objSheet->getCell('G'.($y))->setValue($spinning_array[$temp4]['val']);
            $objSheet->getCell('H'.($y))->setValue($spinning_array[$temp4]['sv']);
            $objSheet->getCell('I'.($y))->setValue($spinning_array[$temp4]['sliver']);
            $objSheet->getCell('J'.($y))->setValue($spinning_array[$temp4]['wage']);
            $y=$y+1;
        }
   
    }
     $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('District Total');
    $objSheet->getCell('E'.($y))->setValue($dsum['no']);
    $objSheet->getCell('G'.($y))->setValue($dsum['val']);
    $objSheet->getCell('H'.($y))->setValue($dsum['sv']);
    $objSheet->getCell('I'.($y))->setValue($dsum['sliver']);
    $objSheet->getCell('J'.($y))->setValue($dsum['wage']);
    


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Spinning_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}




if($type=='Weaving')
{
    $weav_array=$_SESSION["weaving_excel_data"];
    
   
    $ut_variety=$_SESSION["weaving_variety_data"];
  

     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Weaving Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
   
    

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
  

   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AO1');
    $objSheet->mergeCells('A2:AQ2');
    

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('WEAVING WAGES FOR - KOTTAYAM DISTRICT '.$date);

    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setAutoSize(true);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(20);
    $objSheet->getColumnDimension('I')->setWidth(14);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setWidth(20);
    $objSheet->getColumnDimension('AB')->setWidth(20);
    $objSheet->getColumnDimension('AC')->setWidth(15);
    $objSheet->getColumnDimension('AD')->setWidth(15);
    $objSheet->getColumnDimension('AE')->setWidth(15);
    $objSheet->getColumnDimension('AF')->setWidth(15);
    $objSheet->getColumnDimension('AG')->setWidth(15);
    $objSheet->getColumnDimension('AH')->setWidth(15);
    $objSheet->getColumnDimension('AI')->setWidth(15);
    $objSheet->getColumnDimension('AJ')->setWidth(15);
    $objSheet->getColumnDimension('AK')->setWidth(15);
    $objSheet->getColumnDimension('AL')->setWidth(15);
    $objSheet->getColumnDimension('AM')->setWidth(15);
    $objSheet->getColumnDimension('AN')->setWidth(15);
    $objSheet->getColumnDimension('AO')->setWidth(25);
    $objSheet->getColumnDimension('AP')->setAutoSize(true);
    $objSheet->getColumnDimension('AQ')->setAutoSize(true);


    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);
    
$y=3;

 for($z=0;$z<(sizeof($weav_array));$z=$z+1)
 {
   if($weav_array[$z][0][0]!=0)
   {      
    $ar=$weav_array[$z];
    $ut_var=$ut_variety[$z];
$objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);
$objSheet->mergeCells('A'.$y.':AQ'.$y);
$objSheet->getCell('A'.$y)->setValue($unit[$z].' UNIT');
    $y=$y+1;
     
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);

    $objSheet->mergeCells('AD'.$y.':AH'.$y);
    $objSheet->mergeCells('AI'.$y.':AM'.$y);

    $objSheet->getCell('AD'.$y)->setValue('Warp');
    $objSheet->getCell('AI'.$y)->setValue('Weft'); 

     $objSheet->getRowDimension($y)->setRowHeight(20); 
 $y=$y+1;
   
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);
    $objSheet->getRowDimension($y)->setRowHeight(45);
    $objSheet->getStyle('A'.$y.':BB'.$y)->getAlignment()->setWrapText(true);
    
    $objSheet->getCell('A'.$y)->setValue('SI No');            
    $objSheet->getCell('B'.$y)->setValue('Artisan Code');
    $objSheet->getCell('C'.$y)->setValue('Artisan Name');
    $objSheet->getCell('D'.$y)->setValue('Variety');
    $objSheet->getCell('E'.$y)->setValue('No. of Pieces');
    $objSheet->getCell('F'.$y)->setValue('Metre');
    $objSheet->getCell('G'.$y)->setValue('Square Metre');
    $objSheet->getCell('H'.$y)->setValue('Weaving Type');
    $objSheet->getCell('I'.$y)->setValue('Rate');
    $objSheet->getCell('J'.$y)->setValue('Prime Cost');
    $objSheet->getCell('K'.$y)->setValue('Value');
    $objSheet->getCell('L'.$y)->setValue('Wages');
    $objSheet->getCell('M'.$y)->setValue('W/F Fund');
    $objSheet->getCell('N'.$y)->setValue('w/f Recovery');
    $objSheet->getCell('O'.$y)->setValue('Adv Recovery');
    $objSheet->getCell('P'.$y)->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('Q'.$y)->setValue('Covid adv.');
    $objSheet->getCell('R'.$y)->setValue('LIC');
    $objSheet->getCell('S'.$y)->setValue('Net wages');
    $objSheet->getCell('T'.$y)->setValue('Holiday wages');
    $objSheet->getCell('U'.$y)->setValue('Total Wages Payable');
    $objSheet->getCell('V'.$y)->setValue('ESI Contr');
    $objSheet->getCell('W'.$y)->setValue('Warp Yarn Incentive');
    $objSheet->getCell('X'.$y)->setValue('Weft Yarn Incentive');
    $objSheet->getCell('Y'.$y)->setValue('Minimum Wages');
    $objSheet->getCell('Z'.$y)->setValue('DA Days');
    $objSheet->getCell('AA'.$y)->setValue('DA Wages');
    $objSheet->getCell('AB'.$y)->setValue('Total Minimum Wages');
    $objSheet->getCell('AC'.$y)->setValue('Net Minimum Wages');
    
    $objSheet->getCell('AD'.$y)->setValue('Grey');
    $objSheet->getCell('AE'.$y)->setValue('Colour');
    $objSheet->getCell('AF'.$y)->setValue('Black');
    $objSheet->getCell('AG'.$y)->setValue('BL');
    $objSheet->getCell('AH'.$y)->setValue('Total');
    $objSheet->getCell('AI'.$y)->setValue('Grey');
    $objSheet->getCell('AJ'.$y)->setValue('Colour');
    $objSheet->getCell('AK'.$y)->setValue('Black');
    $objSheet->getCell('AL'.$y)->setValue('BL');
    $objSheet->getCell('AM'.$y)->setValue('Total');
    $objSheet->getCell('AN'.$y)->setValue('Attendance');

    $objSheet->getCell('AO'.$y)->setValue('Account Number');

    
    $objSheet->getCell('AP'.$y)->setValue('IFSC');
    $objSheet->getCell('AQ'.$y)->setValue('Bank Name');

   
    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        $y=$y+1;
        
        foreach($temp as $j)
        {
            if($j=='AO')
            {
                $objSheet->setCellValueExplicit('AO'.$y,$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.$y)->setValue($ar[$i][$k]);
            $k=$k+1;
            }
            
           
        }
    }
    $y=$y+1;
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Unit Total');
    $objSheet->getCell('E'.($y))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($y))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
  
    $objSheet->getCell('J'.($y))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($y))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($y))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($y))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($y))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($y))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($y))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($y))->setValue($ar[$i][16]);
    $objSheet->getCell('R'.($y))->setValue($ar[$i][17]);
    $objSheet->getCell('S'.($y))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($y))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($y))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($y))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($y))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($y))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($y))->setValue($ar[$i][24]);
    $objSheet->getCell('AA'.($y))->setValue($ar[$i][26]);
    $objSheet->getCell('AB'.($y))->setValue($ar[$i][27]);
    $objSheet->getCell('AC'.($y))->setValue($ar[$i][28]);
    $objSheet->getCell('AD'.($y))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($y))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($y))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($y))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($y))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($y))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($y))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($y))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($y))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($y))->setValue($ar[$i][38]);


    // $objSheet->mergeCells('A'.$k.':AA'.$k);

    $res14=retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['weaving_code'];
        if($ut_var[$temp4]['mtr']!=0)
        {
            $y=$y+1;
            $objSheet->getStyle('A'.$y.':E'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':E'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['weaving_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('F'.($y))->setValue($ut_var[$temp4]['mtr']);
            $objSheet->getCell('G'.($y))->setValue($ut_var[$temp4]['sqmtr']);
            $objSheet->getCell('K'.($y))->setValue($ut_var[$temp4]['val']);
            $objSheet->getCell('J'.($y))->setValue($ut_var[$temp4]['pc']);
            $objSheet->getCell('AD'.($y))->setValue($ut_var[$temp4]['warp_grey']);
            $objSheet->getCell('AE'.($y))->setValue($ut_var[$temp4]['warp_colour']);
            $objSheet->getCell('AF'.($y))->setValue($ut_var[$temp4]['warp_black']);
            $objSheet->getCell('AG'.($y))->setValue($ut_var[$temp4]['warp_bl']);
            $objSheet->getCell('AH'.($y))->setValue($ut_var[$temp4]['warp_tot']);
            $objSheet->getCell('AI'.($y))->setValue($ut_var[$temp4]['weft_grey']);
            $objSheet->getCell('AJ'.($y))->setValue($ut_var[$temp4]['weft_colour']);
            $objSheet->getCell('AK'.($y))->setValue($ut_var[$temp4]['weft_black']);
            $objSheet->getCell('AL'.($y))->setValue($ut_var[$temp4]['weft_bl']);
            $objSheet->getCell('AM'.($y))->setValue($ut_var[$temp4]['weft_tot']);

            

        }
   
    }

     $y=$y+1;
   
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Grand Total');
   
    $objSheet->getCell('F'.($y))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
  
    $objSheet->getCell('J'.($y))->setValue($ar[$i][9]);
    $objSheet->getCell('K'.($y))->setValue($ar[$i][10]);
  
    $objSheet->getCell('AD'.($y))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($y))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($y))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($y))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($y))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($y))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($y))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($y))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($y))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($y))->setValue($ar[$i][38]);
    
    $y=$y+1;
    

  }
}
$y=$y+1;
$ar=$_SESSION["district_total_weaving"];
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('District Total');
    $objSheet->getCell('E'.($y))->setValue($ar[4]);
    $objSheet->getCell('F'.($y))->setValue($ar[5]);
    $objSheet->getCell('G'.($y))->setValue($ar[6]);
  
    $objSheet->getCell('J'.($y))->setValue($ar[9]);
    $objSheet->getCell('K'.($y))->setValue($ar[10]);
    $objSheet->getCell('L'.($y))->setValue($ar[11]);
    $objSheet->getCell('M'.($y))->setValue($ar[12]);
    $objSheet->getCell('N'.($y))->setValue($ar[13]);
    $objSheet->getCell('O'.($y))->setValue($ar[14]);
    $objSheet->getCell('P'.($y))->setValue($ar[15]);
    $objSheet->getCell('Q'.($y))->setValue($ar[16]);
    $objSheet->getCell('R'.($y))->setValue($ar[17]);
    $objSheet->getCell('S'.($y))->setValue($ar[18]);
    $objSheet->getCell('T'.($y))->setValue($ar[19]);
    $objSheet->getCell('U'.($y))->setValue($ar[20]);
    $objSheet->getCell('V'.($y))->setValue($ar[21]);
    $objSheet->getCell('W'.($y))->setValue($ar[22]);
    $objSheet->getCell('X'.($y))->setValue($ar[23]);
    $objSheet->getCell('Y'.($y))->setValue($ar[24]);
    $objSheet->getCell('AA'.($y))->setValue($ar[26]);
    $objSheet->getCell('AB'.($y))->setValue($ar[27]);
    $objSheet->getCell('AC'.($y))->setValue($ar[28]);
    $objSheet->getCell('AD'.($y))->setValue($ar[29]);
    $objSheet->getCell('AE'.($y))->setValue($ar[30]);
    $objSheet->getCell('AF'.($y))->setValue($ar[31]);
    $objSheet->getCell('AG'.($y))->setValue($ar[32]);
    $objSheet->getCell('AH'.($y))->setValue($ar[33]);
    $objSheet->getCell('AI'.($y))->setValue($ar[34]);
    $objSheet->getCell('AJ'.($y))->setValue($ar[35]);
    $objSheet->getCell('AK'.($y))->setValue($ar[36]);
    $objSheet->getCell('AL'.($y))->setValue($ar[37]);
    $objSheet->getCell('AM'.($y))->setValue($ar[38]);
 $ut_var=$_SESSION["district_total_weaving_variety"];
    $res14=retrieveData("SELECT weaving_code,type from kbk_weaving_variety group by weaving_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['weaving_code'];
        if($ut_var[$temp4]['mtr']!=0)
        {
            $y=$y+1;
            $objSheet->getStyle('A'.$y.':E'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':E'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['weaving_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('F'.($y))->setValue($ut_var[$temp4]['mtr']);
            $objSheet->getCell('G'.($y))->setValue($ut_var[$temp4]['sqmtr']);
            $objSheet->getCell('K'.($y))->setValue($ut_var[$temp4]['val']);
            $objSheet->getCell('J'.($y))->setValue($ut_var[$temp4]['pc']);
            $objSheet->getCell('AD'.($y))->setValue($ut_var[$temp4]['warp_grey']);
            $objSheet->getCell('AE'.($y))->setValue($ut_var[$temp4]['warp_colour']);
            $objSheet->getCell('AF'.($y))->setValue($ut_var[$temp4]['warp_black']);
            $objSheet->getCell('AG'.($y))->setValue($ut_var[$temp4]['warp_bl']);
            $objSheet->getCell('AH'.($y))->setValue($ut_var[$temp4]['warp_tot']);
            $objSheet->getCell('AI'.($y))->setValue($ut_var[$temp4]['weft_grey']);
            $objSheet->getCell('AJ'.($y))->setValue($ut_var[$temp4]['weft_colour']);
            $objSheet->getCell('AK'.($y))->setValue($ut_var[$temp4]['weft_black']);
            $objSheet->getCell('AL'.($y))->setValue($ut_var[$temp4]['weft_bl']);
            $objSheet->getCell('AM'.($y))->setValue($ut_var[$temp4]['weft_tot']);

            

        }
   
    }
$y=$y+1;
    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('District Grand Total');
    
    $objSheet->getCell('F'.($y))->setValue($ar[5]);
    $objSheet->getCell('G'.($y))->setValue($ar[6]);
  
    $objSheet->getCell('J'.($y))->setValue($ar[9]);
    $objSheet->getCell('K'.($y))->setValue($ar[10]);
   
    $objSheet->getCell('AD'.($y))->setValue($ar[29]);
    $objSheet->getCell('AE'.($y))->setValue($ar[30]);
    $objSheet->getCell('AF'.($y))->setValue($ar[31]);
    $objSheet->getCell('AG'.($y))->setValue($ar[32]);
    $objSheet->getCell('AH'.($y))->setValue($ar[33]);
    $objSheet->getCell('AI'.($y))->setValue($ar[34]);
    $objSheet->getCell('AJ'.($y))->setValue($ar[35]);
    $objSheet->getCell('AK'.($y))->setValue($ar[36]);
    $objSheet->getCell('AL'.($y))->setValue($ar[37]);
    $objSheet->getCell('AM'.($y))->setValue($ar[38]);


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Weaving_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}

if($type=='Preprocessing')
{
     

     $temp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW');

    include_once('PHPExcel/IOFactory.php');

    $objPHPExcel = new PHPExcel;

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    $numberFormat = '#,#0.##;[Red]-#,#0.##';



    $objSheet = $objPHPExcel->getActiveSheet();

    $objSheet->setTitle(' Preprocessing Report');

    $objSheet->getStyle('A1:BB1')->getFont()->setBold(true)->setSize(13);
    $objSheet->getStyle('A2:BB2')->getFont()->setBold(true)->setSize(12);
    

   $objSheet->getStyle('A1:BB1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
   $objSheet->getStyle('A2:BB2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);


   $objSheet->getStyle('1:10000')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);



    $objSheet->mergeCells('A1:AW1');
    $objSheet->mergeCells('A2:AW2');

    $objSheet->getCell('A1')->setValue('DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM');
    $objSheet->getCell('A2')->setValue('PREPROCESSING WAGES FOR - KOTTAYAM DISTRICT '.$date);
    $objSheet->getColumnDimension('A')->setWidth(7);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setWidth(15);
    $objSheet->getColumnDimension('E')->setWidth(15);
    $objSheet->getColumnDimension('F')->setWidth(15);
    $objSheet->getColumnDimension('G')->setWidth(15);
    $objSheet->getColumnDimension('H')->setWidth(15);
    $objSheet->getColumnDimension('I')->setWidth(15);
    $objSheet->getColumnDimension('J')->setWidth(15);
    $objSheet->getColumnDimension('K')->setWidth(15);
    $objSheet->getColumnDimension('L')->setWidth(15);
    $objSheet->getColumnDimension('M')->setWidth(15);
    $objSheet->getColumnDimension('N')->setWidth(15);
    $objSheet->getColumnDimension('O')->setWidth(15);
    $objSheet->getColumnDimension('P')->setWidth(15);
    $objSheet->getColumnDimension('Q')->setWidth(15);
    $objSheet->getColumnDimension('R')->setWidth(15);
    $objSheet->getColumnDimension('S')->setWidth(15);
    $objSheet->getColumnDimension('T')->setWidth(15);
    $objSheet->getColumnDimension('U')->setWidth(15);
    $objSheet->getColumnDimension('V')->setWidth(15);
    $objSheet->getColumnDimension('W')->setWidth(15);
    $objSheet->getColumnDimension('X')->setWidth(15);
    $objSheet->getColumnDimension('Y')->setWidth(15);
    $objSheet->getColumnDimension('Z')->setWidth(15);
    $objSheet->getColumnDimension('AA')->setWidth(15);
    $objSheet->getColumnDimension('AB')->setWidth(15);

    $objSheet->getColumnDimension('AC')->setWidth(15);
    $objSheet->getColumnDimension('AD')->setWidth(15);
    $objSheet->getColumnDimension('AE')->setWidth(15);
    $objSheet->getColumnDimension('AF')->setWidth(15);
    $objSheet->getColumnDimension('AG')->setWidth(15);
    $objSheet->getColumnDimension('AH')->setWidth(15);
    $objSheet->getColumnDimension('AI')->setWidth(15);
    $objSheet->getColumnDimension('AJ')->setWidth(15);
    $objSheet->getColumnDimension('AK')->setWidth(15);
    $objSheet->getColumnDimension('AL')->setWidth(15);
    $objSheet->getColumnDimension('AM')->setWidth(15);
    $objSheet->getColumnDimension('AN')->setWidth(15);
    $objSheet->getColumnDimension('AO')->setWidth(15);
    $objSheet->getColumnDimension('AP')->setWidth(15);
    $objSheet->getColumnDimension('AQ')->setWidth(15);
    $objSheet->getColumnDimension('AR')->setWidth(15);
    $objSheet->getColumnDimension('AS')->setWidth(15);
    $objSheet->getColumnDimension('AT')->setAutoSize(true);
    $objSheet->getColumnDimension('AU')->setAutoSize(true);
    $objSheet->getColumnDimension('AV')->setAutoSize(true);
    $objSheet->getColumnDimension('AW')->setAutoSize(true);

    $objSheet->getRowDimension('1')->setRowHeight(20);
    $objSheet->getRowDimension('2')->setRowHeight(20);

$pre_array=$_SESSION["preprocessing_excel_data"];
$ut_variety= $_SESSION["rm_variety_excel_data"];

$y=3;

for($z=0;$z<(sizeof($pre_array));$z=$z+1)
{
  if($pre_array[$z][0][0]!=0)
  {      
    $ar=$pre_array[$z];
    $ut_var=$ut_variety[$z];
    
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);
    $objSheet->mergeCells('A'.$y.':AW'.$y);
    $objSheet->getCell('A'.$y)->setValue($unit[$z].' UNIT');
    $y=$y+1;

    $objSheet->mergeCells('E'.$y.':L'.$y);
    $objSheet->mergeCells('M'.$y.':T'.$y);
    $objSheet->mergeCells('U'.$y.':AB'.$y);
   
$objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);

    $objSheet->getCell('E'.$y)->setValue('Sizing');
    $objSheet->getCell('M'.$y)->setValue('Bobin Winding');
    $objSheet->getCell('U'.$y)->setValue('Warping');
    $objSheet->getRowDimension($y)->setRowHeight(20);
    $objSheet->getStyle('A'.$y.':BB'.$y)->getAlignment()->setWrapText(true);
    
    
  $y=$y+1;
    $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true)->setSize(12);
    $objSheet->getRowDimension($y)->setRowHeight(45);
    $objSheet->getStyle('A'.$y.':BB'.$y)->getAlignment()->setWrapText(true);
    
    $objSheet->getCell('A'.$y)->setValue('SI No');            
    $objSheet->getCell('B'.$y)->setValue('Artisan Code');
    $objSheet->getCell('C'.$y)->setValue('Artisan Name');
    $objSheet->getCell('D'.$y)->setValue('Variety');
    $objSheet->getCell('E'.$y)->setValue('Grey');
    $objSheet->getCell('F'.$y)->setValue('Colour');
    $objSheet->getCell('G'.$y)->setValue('BL');
    $objSheet->getCell('H'.$y)->setValue('Black');
    $objSheet->getCell('I'.$y)->setValue('Total');
    $objSheet->getCell('J'.$y)->setValue('Rate');
    $objSheet->getCell('K'.$y)->setValue('Wages');
    $objSheet->getCell('L'.$y)->setValue('YI');
    $objSheet->getCell('M'.$y)->setValue('Grey');
    $objSheet->getCell('N'.$y)->setValue('Colour');
    $objSheet->getCell('O'.$y)->setValue('BL');
    $objSheet->getCell('P'.$y)->setValue('Black');
    $objSheet->getCell('Q'.$y)->setValue('Total');
    $objSheet->getCell('R'.$y)->setValue('Rate');
    $objSheet->getCell('S'.$y)->setValue('Wages');
    $objSheet->getCell('T'.$y)->setValue('YI');
    $objSheet->getCell('U'.$y)->setValue('Grey');
    $objSheet->getCell('V'.$y)->setValue('Colour');
    $objSheet->getCell('W'.$y)->setValue('BL');
    $objSheet->getCell('X'.$y)->setValue('Black');
    $objSheet->getCell('Y'.$y)->setValue('Total');
    $objSheet->getCell('Z'.$y)->setValue('Rate');
    $objSheet->getCell('AA'.$y)->setValue('Wages');
    $objSheet->getCell('AB'.$y)->setValue('YI');
    $objSheet->getCell('AC'.$y)->setValue('Wages');
    $objSheet->getCell('AD'.$y)->setValue('W/F Fund');
    $objSheet->getCell('AE'.$y)->setValue('w/f Recovery');
    $objSheet->getCell('AF'.$y)->setValue('Adv Recovery');
    $objSheet->getCell('AG'.$y)->setValue('Khadi Cr. Rec.');
    $objSheet->getCell('AH'.$y)->setValue('Covid adv.');
    $objSheet->getCell('AI'.$y)->setValue('LIC');
    $objSheet->getCell('AJ'.$y)->setValue('Net wages');
    $objSheet->getCell('AK'.$y)->setValue('Holiday wages');
    $objSheet->getCell('AL'.$y)->setValue('Total Wages Payable');
    $objSheet->getCell('AM'.$y)->setValue('ESI Contr');
    $objSheet->getCell('AN'.$y)->setValue('Total Yarn Incentive');
    $objSheet->getCell('AO'.$y)->setValue('Minimum Wages');
    $objSheet->getCell('AP'.$y)->setValue('DA Days');
    $objSheet->getCell('AQ'.$y)->setValue('DA Wages');
    $objSheet->getCell('AR'.$y)->setValue('Total Minimum Wages');
    $objSheet->getCell('AS'.$y)->setValue('Net Minimum Wages');
    $objSheet->getCell('AT'.$y)->setValue('Attendance');
    $objSheet->getCell('AU'.$y)->setValue('Account Number');
    $objSheet->getCell('AV'.$y)->setValue('IFSC');
    $objSheet->getCell('AW'.$y)->setValue('Bank Name');
   
    for($i=0;$i<sizeof($ar)-1;$i++)
    {
        $k=0;
        $y=$y+1;

        foreach($temp as $j)
        {
            if($j=='AU')
            {
                $objSheet->setCellValueExplicit('AU'.$y,$ar[$i][$k],PHPExcel_Cell_DataType::TYPE_STRING);
                $k=$k+1;
            }
            else
            {
               $objSheet->getCell($j.$y)->setValue($ar[$i][$k]);
            $k=$k+1; 
            }
            
        }
    }
    $y=$y+1;

    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Unit Total');
    $objSheet->getCell('E'.($y))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($y))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($y))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($y))->setValue($ar[$i][8]);

    $objSheet->getCell('K'.($y))->setValue($ar[$i][10]);
    $objSheet->getCell('L'.($y))->setValue($ar[$i][11]);
    $objSheet->getCell('M'.($y))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($y))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($y))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($y))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($y))->setValue($ar[$i][16]);

    $objSheet->getCell('S'.($y))->setValue($ar[$i][18]);
    $objSheet->getCell('T'.($y))->setValue($ar[$i][19]);
    $objSheet->getCell('U'.($y))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($y))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($y))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($y))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($y))->setValue($ar[$i][24]);
   
 
    $objSheet->getCell('AA'.($y))->setValue($ar[$i][26]);
    $objSheet->getCell('AB'.($y))->setValue($ar[$i][27]);
    $objSheet->getCell('AC'.($y))->setValue($ar[$i][28]);
    $objSheet->getCell('AD'.($y))->setValue($ar[$i][29]);
    $objSheet->getCell('AE'.($y))->setValue($ar[$i][30]);
    $objSheet->getCell('AF'.($y))->setValue($ar[$i][31]);
    $objSheet->getCell('AG'.($y))->setValue($ar[$i][32]);
    $objSheet->getCell('AH'.($y))->setValue($ar[$i][33]);
    $objSheet->getCell('AI'.($y))->setValue($ar[$i][34]);
    $objSheet->getCell('AJ'.($y))->setValue($ar[$i][35]);
    $objSheet->getCell('AK'.($y))->setValue($ar[$i][36]);
    $objSheet->getCell('AL'.($y))->setValue($ar[$i][37]);
    $objSheet->getCell('AM'.($y))->setValue($ar[$i][38]);
    $objSheet->getCell('AO'.($y))->setValue($ar[$i][39]);

    $objSheet->getCell('AQ'.($y))->setValue($ar[$i][40]);
    $objSheet->getCell('AR'.($y))->setValue($ar[$i][42]);
    $objSheet->getCell('AS'.($y))->setValue($ar[$i][43]);
    $objSheet->getCell('AT'.($y))->setValue($ar[$i][44]);

    $res14=retrieveData("SELECT rm_code,type from kbk_preprocessing_variety group by rm_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['rm_code'];
        if($ut_var[$temp4]['grand_tot']!=0)
        {
            $y=$y+1;
            $objSheet->getStyle('A'.$y.':D'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':D'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['rm_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($y))->setValue($ut_var[$temp4]['s_grey']);
            $objSheet->getCell('F'.($y))->setValue($ut_var[$temp4]['s_colour']);
            $objSheet->getCell('G'.($y))->setValue($ut_var[$temp4]['s_bl']);
            $objSheet->getCell('H'.($y))->setValue($ut_var[$temp4]['s_black']);
            $objSheet->getCell('I'.($y))->setValue($ut_var[$temp4]['s_tot']);

            $objSheet->getCell('M'.($y))->setValue($ut_var[$temp4]['b_grey']);
            $objSheet->getCell('N'.($y))->setValue($ut_var[$temp4]['b_colour']);
            $objSheet->getCell('O'.($y))->setValue($ut_var[$temp4]['b_bl']);
            $objSheet->getCell('P'.($y))->setValue($ut_var[$temp4]['b_black']);
            $objSheet->getCell('Q'.($y))->setValue($ut_var[$temp4]['b_tot']);

            $objSheet->getCell('U'.($y))->setValue($ut_var[$temp4]['w_grey']);
            $objSheet->getCell('V'.($y))->setValue($ut_var[$temp4]['w_colour']);
            $objSheet->getCell('W'.($y))->setValue($ut_var[$temp4]['w_bl']);
            $objSheet->getCell('X'.($y))->setValue($ut_var[$temp4]['w_black']);
            $objSheet->getCell('Y'.($y))->setValue($ut_var[$temp4]['w_tot']);
        

        }
   
    }

    $y=$y+1;

    $objSheet->getStyle($y)->getFont()->setBold(true);
    $objSheet->mergeCells('A'.($y).':D'.($y));
    $objSheet->getCell('A'.($y))->setValue('Grand Total');
    $objSheet->getCell('E'.($y))->setValue($ar[$i][4]);
    $objSheet->getCell('F'.($y))->setValue($ar[$i][5]);
    $objSheet->getCell('G'.($y))->setValue($ar[$i][6]);
    $objSheet->getCell('H'.($y))->setValue($ar[$i][7]);
    $objSheet->getCell('I'.($y))->setValue($ar[$i][8]);

    $objSheet->getCell('M'.($y))->setValue($ar[$i][12]);
    $objSheet->getCell('N'.($y))->setValue($ar[$i][13]);
    $objSheet->getCell('O'.($y))->setValue($ar[$i][14]);
    $objSheet->getCell('P'.($y))->setValue($ar[$i][15]);
    $objSheet->getCell('Q'.($y))->setValue($ar[$i][16]);

    $objSheet->getCell('U'.($y))->setValue($ar[$i][20]);
    $objSheet->getCell('V'.($y))->setValue($ar[$i][21]);
    $objSheet->getCell('W'.($y))->setValue($ar[$i][22]);
    $objSheet->getCell('X'.($y))->setValue($ar[$i][23]);
    $objSheet->getCell('Y'.($y))->setValue($ar[$i][24]);
    $y=$y+1;
   
 }
}
$y=$y+1;
$objSheet->mergeCells('A'.($y).':D'.($y));
$objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true);
$objSheet->getCell('A'.($y))->setValue('District Total');
$distrct_sum=$_SESSION["district_total"];
    $k=0;
        foreach($temp as $j)
        {
            
            if(!($j=='A'||$j=='B'||$j=='C'||$j=='D'||$j=='J'||$j=='R'||$j=='Z'||$j=='AN'||$j=='AP'||$j=='AU'||$j=='AV'||$j=='AW'))
            {
               $objSheet->getCell($j.$y)->setValue($distrct_sum[$k]);
            $k=$k+1; 
            }
            
        }
$ut_var=$_SESSION["district_variety_total"];
$res14=retrieveData("SELECT rm_code,type from kbk_preprocessing_variety group by rm_code",$con);
    for($l=0;$l<sizeof($res14);$l++)
    {
        $temp4=$res14[$l]['rm_code'];
        if($ut_var[$temp4]['grand_tot']!=0)
        {
            $y=$y+1;
            $objSheet->getStyle('A'.$y.':D'.$y)->getFont()->setBold(true);
            $objSheet->mergeCells('A'.$y.':B'.$y);
            $objSheet->mergeCells('C'.$y.':D'.$y);
            $objSheet->getCell('A'.($y))->setValue($res14[$l]['rm_code']);
            $objSheet->getCell('C'.($y))->setValue($res14[$l]['type']);
            $objSheet->getCell('E'.($y))->setValue($ut_var[$temp4]['s_grey']);
            $objSheet->getCell('F'.($y))->setValue($ut_var[$temp4]['s_colour']);
            $objSheet->getCell('G'.($y))->setValue($ut_var[$temp4]['s_bl']);
            $objSheet->getCell('H'.($y))->setValue($ut_var[$temp4]['s_black']);
            $objSheet->getCell('I'.($y))->setValue($ut_var[$temp4]['s_tot']);

            $objSheet->getCell('M'.($y))->setValue($ut_var[$temp4]['b_grey']);
            $objSheet->getCell('N'.($y))->setValue($ut_var[$temp4]['b_colour']);
            $objSheet->getCell('O'.($y))->setValue($ut_var[$temp4]['b_bl']);
            $objSheet->getCell('P'.($y))->setValue($ut_var[$temp4]['b_black']);
            $objSheet->getCell('Q'.($y))->setValue($ut_var[$temp4]['b_tot']);

            $objSheet->getCell('U'.($y))->setValue($ut_var[$temp4]['w_grey']);
            $objSheet->getCell('V'.($y))->setValue($ut_var[$temp4]['w_colour']);
            $objSheet->getCell('W'.($y))->setValue($ut_var[$temp4]['w_bl']);
            $objSheet->getCell('X'.($y))->setValue($ut_var[$temp4]['w_black']);
            $objSheet->getCell('Y'.($y))->setValue($ut_var[$temp4]['w_tot']);
        

        }
   
    }
  
  $y=$y+1;
  $objSheet->getStyle('A'.$y.':BB'.$y)->getFont()->setBold(true);
  $objSheet->mergeCells('A'.($y).':D'.($y));
  $objSheet->getCell('A'.($y))->setValue('District Grand Total');
            $objSheet->getCell('E'.($y))->setValue($distrct_sum[0]);
            $objSheet->getCell('F'.($y))->setValue($distrct_sum[1]);
            $objSheet->getCell('G'.($y))->setValue($distrct_sum[2]);
            $objSheet->getCell('H'.($y))->setValue($distrct_sum[3]);
            $objSheet->getCell('I'.($y))->setValue($distrct_sum[4]);

            $objSheet->getCell('M'.($y))->setValue($distrct_sum[7]);
            $objSheet->getCell('N'.($y))->setValue($distrct_sum[8]);
            $objSheet->getCell('O'.($y))->setValue($distrct_sum[9]);
            $objSheet->getCell('P'.($y))->setValue($distrct_sum[10]);
            $objSheet->getCell('Q'.($y))->setValue($distrct_sum[11]);

            $objSheet->getCell('U'.($y))->setValue($distrct_sum[14]);
            $objSheet->getCell('V'.($y))->setValue($distrct_sum[15]);
            $objSheet->getCell('W'.($y))->setValue($distrct_sum[16]);
            $objSheet->getCell('X'.($y))->setValue($distrct_sum[17]);
            $objSheet->getCell('Y'.($y))->setValue($distrct_sum[18]);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Preprocessing_Report.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit;

}





?>
