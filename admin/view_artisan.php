<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Artisan</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
</head>

<body>
    <header>
        <?php
        include("header.php");
        ?>
        <form>

            <?php
            include('../connection.php');
            $artisan_name    = "";
            $contact_no      = "";
            $aadhar_no       = "";
            $aadhar          = "";
            $photo           = "";
            $doj             = "";
            $dob             = "";
            $addressln1      = "";
            $addressln2      = "";
            $gender          = "";
            $pin_number      = "";
            $father_spouse   = "";
            $lic             = "";
            $esi             = "";
            $account_no      = "";
            $bank_name       = "";
            $ifsc            = "";
            $wf_reg_no       = "";
            $email           = "";
            $unit_code        = "";
            $category        = "";
            $mmdi           = "";
            $esi_centr       = "";
            $artisan_id       = "";
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $sql = "SELECT * FROM kbk_artisan WHERE artisan_id = $id";
                $result = mysqli_query($con, $sql);
                $row = mysqli_fetch_array($result);
                $artisan_name    = $row['artisan_name'];
                $contact_no      = $row['contact_no'];
                $aadhar_no       = $row['aadhar_no'];
                $aadhar          = $row['aadhar'];
                $photo           = $row['photo'];
                $doj             = $row['doj'];
                $dob             = $row['dob'];
                $addressln1      = $row['addressln1'];
                $addressln2      = $row['addressln2'];
                $gender          = $row['gender'];
                $pin_number      = $row['pin_number'];
                $father_spouse   = $row['father_spouse'];
                $lic             = $row['lic'];
                $esi             = $row['esi'];
                $account_no      = $row['account_no'];
                $bank_name       = $row['bank_name'];
                $ifsc            = $row['ifsc'];
                $wf_reg_no       = $row['wf_reg_no'];
                $email           = $row['email'];
                $unit_code       = $row['unit_code'];
                $category        = $row['caste'];
                $mmdi            = $row['mmdi'];
                $esi_centr       = $row['esi_centr'];
                $artisan_id      = $row['artisan_id'];
            }
            ?>
            <section class="" style="background-color:aquamarine;">
                <div class="container py-5">
                    <div class="row d-flex justify-content-center align-items-center">
                        <div class="col">
                            <div class="card card-registration my-4">
                                <div class="row g-0">
                                    <div class="col-xl-8">
                                        <div class="card-body p-md-5 text-black">
                                            <div class="row">
                                                <div class="col-md-8 mb-4">
                                                    <label class="form-label">Name</label>
                                                    <div class="form-outline">
                                                        <input type="text" name="art_name" id="art_name" class="form-control form-control-lg" value="<?php echo $artisan_name; ?>" readonly />

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-4">
                                                        <label class="form-label">Artisan ID</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="art_id_disp" id="art_id_disp" class="form-control form-control-lg" value="<?php echo $id; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 mb-4">
                                                        <label class="form-label" for="form3Example1n">Email ID</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="email" id="email" class="form-control form-control-lg" value="<?php echo $email; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 mb-4">
                                                        <label class="form-label" for="form3Example8">Address Line 1</label>
                                                        <div class="form-outline mb-4">
                                                            <input type="text" name="addressln1" id="addressln1" class="form-control form-control-lg" value="<?php echo $addressln1; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 mb-4">
                                                        <label class="form-label" for="form3Example8">Address Line 2</label>
                                                        <div class="form-outline mb-4">
                                                            <input type="text" name="addressln2" id="addressln2" class="form-control form-control-lg" value="<?php echo $addressln2; ?>" readonly />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6 mb-4">
                                                            <label class="form-label" for="form3Example8">Contact Number</label>
                                                            <div class="form-outline">
                                                                <input type="number" name="contact" id="contact" class="form-control form-control-lg" min=0 max=9999999999 value="<?php echo $contact_no; ?>" readonly />

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label " for="form3Example8">Caste</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="category" id="category" class="form-control form-control-lg" min=0 max=999999 value="<?php echo $category; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1m1">Pin Number</label>
                                                        <div class="form-outline">
                                                            <input type="number" name="pin" id="pin" class="form-control form-control-lg" min=0 max=999999 value="<?php echo $pin_number; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">Aadhar number</label>
                                                        <div class="form-outline">
                                                            <input type="number" name="adar" id="adar" class="form-control form-control-lg" min=0 max=999999999999 value="<?php echo $aadhar_no; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1m1">Date of birth</label>
                                                        <div class="form-outline">
                                                            <input type="date" name="dob" id="dob" class="form-control form-control-lg" value="<?php echo $dob; ?>" disabled="true" />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">Father/Spouse</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="fos" id="fos" class="form-control form-control-lg" value="<?php echo $father_spouse; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="d-md-flex justify-content-start align-items-center mb-4 py-2">

                                                    <h6 class="mb-0 me-4">Gender: </h6>

                                                    <div class="form-check form-check-inline mb-0 me-4">
                                                        <input class="form-check-input" type="radio" name="gender" id="gender1" disabled="true" value="Female" <?php if ($gender == 'Female') echo 'checked="checked"'; ?> />
                                                        <label class="form-check-label" for="femaleGender">Female</label>
                                                    </div>

                                                    <div class="form-check form-check-inline mb-0 me-4">
                                                        <input class="form-check-input" type="radio" name="gender" id="gender2" disabled="true" value="Male" <?php if ($gender == 'Male') echo 'checked="checked"'; ?> />
                                                        <label class="form-check-label" for="maleGender">Male</label>
                                                    </div>

                                                    <div class="form-check form-check-inline mb-0">
                                                        <input class="form-check-input" type="radio" name="gender" id="gender3" disabled="true" value="Other" <?php if ($gender == 'Other') echo 'checked="checked"'; ?> />
                                                        <label class="form-check-label" for="otherGender">Other</label>
                                                    </div>

                                                </div>



                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1m1">LIC Number</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="lic" id="lic" class="form-control form-control-lg" value="<?php echo $lic; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">ESI Number</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="esi" id="esi" class="form-control form-control-lg" value="<?php echo $esi; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1m1">*Bank account number</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="accno" id="accno" class="form-control form-control-lg" value="<?php echo $account_no; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">*Bank name</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="bankname" id="bankname" class="form-control form-control-lg" value="<?php echo $bank_name; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <div class="form-outline">
                                                            <label for="formFile" class="form-label">Aadhar image</label>
                                                            <input class="form-control" name="aadharpic" type="file" id="aadharpic" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <div class="form-outline">
                                                            <label for="formFile" class="form-label">Photo</label>
                                                            <input class="form-control" name="pic" type="file" id="pic" readonly />
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <img <?php
                                                                if ($aadhar) {
                                                                    echo "src='../upload_images/" . strval($row['aadhar']) . "'";
                                                                } ?> alt="Sample photo" class="img-thumbnail" name="aadhar_img" style="height: 200px; width: 400px;" readonly />
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <img <?php if ($photo) {
                                                                    echo "src='../upload_images/" . strval($row['photo']) . "'";
                                                                } ?> alt="Sample photo" class="img-thumbnail" name="photo_img" style=" height: 200px; width: 200px;" readonly />
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1m1">IFSC</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="ifsc" id="ifsc" class="form-control form-control-lg" value="<?php echo $value = "$ifsc"; ?>" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">Welfare number</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="wf" id="wf" class="form-control form-control-lg" value="<?php echo $wf_reg_no; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 mb-4">
                                                        <label class="form-label" for="form3Example1n1">Unit name</label>
                                                        <div class="form-outline">
                                                            <input type="text" name="unitid" id="unitid" class="form-control form-control-lg" value="<?php echo $unit_code; ?>" readonly />

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-4">
                                                <label class="form-label" for="form3Example1n1">Date of joining</label>
                                                <div class="form-outline">
                                                    <input type="date" name="doj" id="doj" class="form-control form-control-lg" value="<?php echo $doj; ?>" disabled="true" />

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 mb-4">
                                                    <label class="form-label" for="form3Example1n1">Esi centre</label>
                                                    <div class="form-outline">
                                                        <input type="text" name="esi_center" id="esi_center" class="form-control form-control-lg" value="<?php echo $esi_centr; ?>" readonly />

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-4">
                                                <label class="form-label" for="form3Example1n1">MMDI Number</label>
                                                <div class="form-outline">
                                                    <input type="text" name="mmdi" id="mmdi" class="form-control form-control-lg" value="<?php echo $mmdi; ?>" readonly />

                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end pt-3">
                                                <a href="Artisan_View.php"><input type="button" value="Back" class="btn btn-warning bg-lg ms-2"></a>
                                                <a href="view_update_artisan.php?id=<?php echo $artisan_id; ?>"><input type="button" value="Edit" class="btn btn-warning bg-lg ms-2" data-mdb-target="#exampleModal" name="edit"></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>

            </section>

        </form>
    </header>
    <script type="text/javascript" src="../js/mdb.min.js"></script>
</body>

</html>