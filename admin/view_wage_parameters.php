<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Material Design for Bootstrap</title>
    <!-- MDB icon -->
    
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->

    <!-- Navbar -->
<?php 
include('header.php'); 
?>
</nav>
<!-- Navbar -->


    <section class="h-100 bg-dark">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col">
            <div class="card card-registration my-4">
              <div class="row g-0">
                <div class="col-xl-6 d-none d-xl-block">
                  <img
                    src="../images/wgp.png"
                    alt="Sample photo"
                    class="img-fluid"
                    style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                  />
                </div>
                <div class="col-xl-6">
                  <div class="card-body p-md-5 text-black">
                    <h3 class="mb-5 text-uppercase">Wage Parameters</h3>
    
                    <div class="form-outline mb-4">
                      <input type="text" id="form3Example9" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example9">Parameter ID</label>
                    </div>
    
                    <div class="form-outline mb-4">
                      <input type="text" id="form3Example90" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example90">LIC</label>
                    </div>
    
                    <div class="form-outline mb-4">
                      <input type="text" id="form3Example99" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example99">Work ID</label>
                    </div>
    
                
                    <div class="form-outline mb-4">
                      <input type="text" id="form3Example90" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example90">With Effect From</label>
                    </div>
    
                    <div class="d-flex justify-content-end pt-3">
                      <button type="button" class="btn btn-light btn-lg">Cancel</button>
                      <button type="button" class="btn btn-warning btn-lg ms-2">Submit</button>
                    </div>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
