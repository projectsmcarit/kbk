<?php
include('../connection.php');
include('../kbk_library.php');
include('header.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>


  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>Weaving Paramater View</title>
  <!-- MDB icon -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />

  <style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
      text-align: center;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 2px;
      padding-bottom: 2px;

      background-color: #04AA6D;
      color: white;

    }

    #table-responsive {
      display: table;
      /* margin-left: 50px; */
      /* max-width: 100vw; */
    }

    .table-wrapper {
      /* margin-left: auto; */
      max-height: 500px;
      max-width: 100vw;
      overflow: auto;
      display: inline-block;
    }

    .table-wrapper thead tr {
      position: sticky;
      top: 0;
    }
  </style>



</head>

<body>
  <script type="text/javascript" src="../js/mdb.min.js"></script>

  <script type="text/javascript">
    var count = 0;

    function displayReport(demo) {

      // window.aa=demo;
      for (var i = 1; i < count + 1; i = i + 1) {
        document.getElementById(i).style.display = "none";
      }
      document.getElementById('update' + demo).style.display = "block";
      var weavid = document.getElementById('weavingid' + demo).value;
      document.getElementById('updateid').value = weavid;
      document.getElementById('rowid').value = demo;

      FieldEnable(demo);

    }

    function FieldEnable(temp) {

      document.getElementById('weavingcode' + temp).readOnly = false;
      document.getElementById('type' + temp).readOnly = false;
      document.getElementById('countable' + temp).disabled = false;
      document.getElementById('length' + temp).readOnly = false;
      document.getElementById('width' + temp).readOnly = false;
      document.getElementById('rate' + temp).readOnly = false;
      document.getElementById('primecost' + temp).readOnly = false;
      document.getElementById('esi' + temp).readOnly = false;
      document.getElementById('wf' + temp).readOnly = false;
      document.getElementById('ai' + temp).readOnly = false;
      document.getElementById('yarnwarp' + temp).disabled = false;
      document.getElementById('yarnweft' + temp).disabled = false;
      document.getElementById('threadgroup' + temp).disabled = false;
      document.getElementById('month' + temp).disabled = false;
      document.getElementById('year' + temp).disabled = false;
      document.getElementById('fw_wage' + temp).readOnly = false;
      document.getElementById('fw_warpyi' + temp).readOnly = false;
      document.getElementById('fw_weftyi' + temp).readOnly = false;
      document.getElementById('fw_mw' + temp).readOnly = false;
      document.getElementById('fw_target' + temp).readOnly = false;
      document.getElementById('rm_wage' + temp).readOnly = false;
      document.getElementById('rm_warpyi' + temp).readOnly = false;
      document.getElementById('rm_weftyi' + temp).readOnly = false;
      document.getElementById('rm_mw' + temp).readOnly = false;
      document.getElementById('rm_target' + temp).readOnly = false;
      document.getElementById('mw_wage' + temp).readOnly = false;
      document.getElementById('mw_warpyi' + temp).readOnly = false;
      document.getElementById('mw_weftyi' + temp).readOnly = false;
      document.getElementById('mw_mw' + temp).readOnly = false;
      document.getElementById('mw_target' + temp).readOnly = false;
      document.getElementById('warp_grey' + temp).readOnly = false;
      document.getElementById('warp_colour' + temp).readOnly = false;
      document.getElementById('warp_black' + temp).readOnly = false;
      document.getElementById('warp_bl' + temp).readOnly = false;
      document.getElementById('weft_grey' + temp).readOnly = false;
      document.getElementById('weft_colour' + temp).readOnly = false;
      document.getElementById('weft_black' + temp).readOnly = false;
      document.getElementById('weft_bl' + temp).readOnly = false;

    }
  </script>
  <form name="spinningparaview" action="weaving_parameter_edit.php" method="post">
    <section class="" style="padding-top: 30px;">
      <div class="py-5" style="padding-left: 30px;padding-right: 500px;">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col">
            <div class="">
              <div class="row g-0">
                <div id="container_content">
                  <div class="text-black">
                    <h3 class="mb-5 text-uppercase" style="padding-left: 100px;">Weaving Parameters</h3>
                    <div class="table-responsive" id="table-responsive">
                      <table class=" table table-hover table-wrapper table-bordered " id="customers">
                        <thead>
                          <tr>
                            <th colspan="15"> </th>
                            <th colspan="5">Full Weaving</th>
                            <th colspan="5">RM Warp Weaving</th>
                            <th colspan="5">Mear Weaving</th>
                            <th colspan="4">Warp</th>
                            <th colspan="4">Weft</th>
                            <th colspan="2" style="background-color: white;border-color: white;"> </th>
                          </tr>
                          <tr>
                            <th>SI No.</th>
                            <th style="min-width: 110px;">Weaving Code</th>
                            <th style="min-width: 150px;">Weaving Variety</th>
                            <th style="min-width: 120px;">Measurable/ Countable</th>
                            <th style="min-width: 80px;">Length</th>
                            <th style="min-width: 80px;">Width in Meter</th>
                            <th style="min-width: 80px;">Value Per Meter</th>
                            <th style="min-width: 80px;">Prime Cost Per Meter</th>
                            <th style="min-width: 80px;">ESI</th>
                            <th style="min-width: 80px;">Welfare Fund</th>

                            <th style="min-width: 80px;">Annual Incentive</th>
                            <th style="min-width: 80px;">Warp Yarn</th>
                            <th style="min-width: 80px;">Weft Yarn</th>
                            <th style="min-width: 85px;">Type</th>
                            <th style="min-width: 118px;">With Effect From</th>

                            <th style="min-width: 80px;">Wages Per Meter</th>
                            <th style="min-width: 80px;">Warp YI</th>
                            <th style="min-width: 80px;">Weft YI</th>
                            <th style="min-width: 80px;">Min Wages</th>
                            <th style="min-width: 80px;">Target</th>
                            <th style="min-width: 80px;">Wages Per Meter</th>
                            <th style="min-width: 80px;">Warp YI</th>
                            <th style="min-width: 80px;">Weft YI</th>
                            <th style="min-width: 80px;">Min Wages</th>
                            <th style="min-width: 80px;">Target</th>
                            <th style="min-width: 80px;">Wages Per Meter</th>
                            <th style="min-width: 80px;">Warp YI</th>
                            <th style="min-width: 80px;">Weft YI</th>
                            <th style="min-width: 80px;">Min Wages</th>
                            <th style="min-width: 80px;">Target</th>

                            <th style="min-width: 80px;">Grey</th>
                            <th style="min-width: 80px;">Colour</th>
                            <th style="min-width: 80px;">Black</th>
                            <th style="min-width: 80px;">Bleach</th>

                            <th style="min-width: 80px;">Grey</th>
                            <th style="min-width: 80px;">Colour</th>
                            <th style="min-width: 80px;">Black</th>
                            <th style="min-width: 80px;">Bleach</th>

                            <th colspan="2" style="background-color: white;border-color: white;"> </th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php

                          $res1 = retrieveData("SELECT * from kbk_weaving_variety", $con);
                          for ($i = 0; $i < sizeof($res1); $i++) {
                          ?>
                            <script type="text/javascript">
                              count = count + 1;
                            </script>
                            <?php

                            echo '<tr><td><input type="text"  name="weavingid' . strval($i + 1) . '" id="weavingid' . strval($i + 1) . '" value="' . $res1[$i]['weaving_id'] . '" class="form-control-plaintext form-control-lg" required hidden />' . strval($i + 1) . '</td>';
                            echo '<td ><input type="text"  name="weavingcode' . strval($i + 1) . '" id="weavingcode' . strval($i + 1) . '" value="' . $res1[$i]['weaving_code'] . '" class="form-control-plaintext form-control-lg" required readonly /></td>';
                            echo '<td ><input type="text" name="type' . strval($i + 1) . '" id="type' . strval($i + 1) . '"value="' . $res1[$i]['type'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            if ($res1[$i]['countable'] == 1) {
                              echo '<td> <select class="form-control-plaintext"  name="countable' . strval($i + 1) . '" id="countable' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option selected value="1">Countable</option value="0"><option>Measurable</option></select></td>';
                            } else {
                              echo '<td> <select class="form-control-plaintext"  name="countable' . strval($i + 1) . '" id="countable' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option value="0"selected>Measurable</option value="1"><option>Countable</option></select></td>';
                            }
                            echo '<td ><input type="text" name="length' . strval($i + 1) . '" id="length' . strval($i + 1) . '" value="' . $res1[$i]['length'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo '<td><input type="text" name="width' . strval($i + 1) . '" id="width' . strval($i + 1) . '" value="' . $res1[$i]['width'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo '<td ><input type="text" name="rate' . strval($i + 1) . '" id="rate' . strval($i + 1) . '" value="' . $res1[$i]['rate'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo '<td><input type="text" name="primecost' . strval($i + 1) . '" id="primecost' . strval($i + 1) . '" value="' . $res1[$i]['prime_cost'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo '<td><input type="text" name="esi' . strval($i + 1) . '" id="esi' . strval($i + 1) . '" value="' . $res1[$i]['esi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo '<td><input type="text" name="wf' . strval($i + 1) . '" id="wf' . strval($i + 1) . '" value="' . $res1[$i]['wf'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';

                            echo  '<td><input type="text" name="ai' . strval($i + 1) . '" id="ai' . strval($i + 1) . '" value="' . $res1[$i]['ai'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            $yid1 = $res1[$i]['yarn_warp'];
                            $yid2 = $res1[$i]['yarn_weft'];
                            $res2 = retrieveData("SELECT yarn_id,yarn_code,type from kbk_yarn_variety where yarn_id=$yid1", $con);
                            $res3 = retrieveData("SELECT yarn_id,yarn_code,type from kbk_yarn_variety where yarn_id=$yid2", $con);
                            $res10 = retrieveData("SELECT yarn_id,yarn_code,type from kbk_yarn_variety group by yarn_code", $con);
                            echo '<td> <select class="form-control-plaintext"  name="yarnwarp' . strval($i + 1) . '" id="yarnwarp' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option selected value="' . $res2[0]['yarn_id'] . '">' . $res2[0]['type'] . '</option>';

                            for ($j = 0; $j < sizeof($res10); $j++) {
                              echo "<option value='" . $res10[$j]['yarn_id'] . "'>" . $res10[$j]['type'] . "</option>";  // displaying data in option menu
                            }
                            echo '</select></td>';
                            echo '<td> <select class="form-control-plaintext"  name="yarnweft' . strval($i + 1) . '" id="yarnweft' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option selected value="' . $res3[0]['yarn_id'] . '">' . $res3[0]['type'] . '</option>';

                            for ($j = 0; $j < sizeof($res10); $j++) {
                              echo "<option value='" . $res10[$j]['yarn_id'] . "'>" . $res10[$j]['type'] . "</option>";  // displaying data in option menu
                            }
                            echo '</select></td>';
                            echo '<td> <select class="form-control-plaintext"  name="threadgroup' . strval($i + 1) . '" id="threadgroup' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option selected value="' . $res1[$i]['thread_group'] . '">' . $res1[$i]['thread_group'] . '</option><option value="NMC">NMC</option><option value="MUSLIN">MUSLIN</option><option value="POLY">POLY</option><option value="SILK">SILK</option></select></td>';
                            // echo '<td><input type="date" name="dob" id="dob" class="form-control form-control-lg" value="'.$res1[$i]['wef'].'"/></td>';

                            echo '<td><select class="form-control-plaintext" name="month' . strval($i + 1) . '" id="month' . strval($i + 1) . '" aria-label="Default select example" disabled="true">';

                            $months_name = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                            $hdate = $res1[$i]['wef'];
                            $m1 = strval(date('m', strtotime($hdate)));
                            $y1 = strval(date('Y', strtotime($hdate)));
                            echo '<option value="' . $m1 . '" selected>' . $months_name[strval($m1) - 1] . '</option>' . "\n";
                            for ($i_month = 1; $i_month <= 12; $i_month++) {
                              // if($m1==$i_month){
                              //   continue;
                              // }                
                              echo '<option value="' . $i_month . '">' . $months_name[$i_month - 1] . '</option>' . "\n";
                            }
                            echo '</select>';
                            echo '<select class="form-control-plaintext" name="year' . strval($i + 1) . '" id="year' . strval($i + 1) . '" aria-label="Default select example" disabled="true"> <option selected value="' . $y1 . '">' . $y1 . '</option>';

                            for ($i_year = date('Y') - 10; $i_year <= date('Y') + 10; $i_year++) {
                              // if($y1==$i_year){
                              //   continue;
                              // }           
                              echo '<option value="' . $i_year . '">' . $i_year . '</option>' . "\n";
                            }
                            echo '</select></td>';


                            $wid = $res1[$i]['weaving_id'];
                            $wef = $res1[$i]['wef'];
                            $res4 = retrieveData("SELECT * from kbk_weaving_type where weaving_id=$wid and wef='$wef' and w_type='full weaving'", $con);
                            echo  '<td><input type="text" name="fw_wage' . strval($i + 1) . '" id="fw_wage' . strval($i + 1) . '" value="' . $res4[0]['w_wages'] . '" class="form-control-plaintext form-control-lg" required readonly /></td>';
                            echo  '<td><input type="text" name="fw_warpyi' . strval($i + 1) . '" id="fw_warpyi' . strval($i + 1) . '" value="' . $res4[0]['warp_yi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="fw_weftyi' . strval($i + 1) . '" id="fw_weftyi' . strval($i + 1) . '" value="' . $res4[0]['weft_yi'] . '" class="form-control-plaintext form-control-lg" required readonly /></td>';
                            echo  '<td><input type="text" name="fw_mw' . strval($i + 1) . '" id="fw_mw' . strval($i + 1) . '" value="' . $res4[0]['min_wages'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="fw_target' . strval($i + 1) . '" id="fw_target' . strval($i + 1) . '" value="' . $res4[0]['target'] . '" class="form-control-plaintext form-control-lg" required readonly /></td>';

                            $res5 = retrieveData("SELECT * from kbk_weaving_type where weaving_id=$wid and wef='$wef' and w_type='rm warp weaving'", $con);
                            echo  '<td><input type="text" name="rm_wage' . strval($i + 1) . '" id="rm_wage' . strval($i + 1) . '" value="' . $res5[0]['w_wages'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="rm_warpyi' . strval($i + 1) . '" id="rm_warpyi' . strval($i + 1) . '" value="' . $res5[0]['warp_yi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="rm_weftyi' . strval($i + 1) . '" id="rm_weftyi' . strval($i + 1) . '" value="' . $res5[0]['weft_yi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="rm_mw' . strval($i + 1) . '" id="rm_mw' . strval($i + 1) . '" value="' . $res5[0]['min_wages'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="rm_target' . strval($i + 1) . '" id="rm_target' . strval($i + 1) . '" value="' . $res5[0]['target'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';

                            $res6 = retrieveData("SELECT * from kbk_weaving_type where weaving_id=$wid and wef='$wef' and w_type='mear weaving'", $con);
                            echo  '<td><input type="text" name="mw_wage' . strval($i + 1) . '" id="mw_wage' . strval($i + 1) . '" value="' . $res6[0]['w_wages'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="mw_warpyi' . strval($i + 1) . '" id="mw_warpyi' . strval($i + 1) . '" value="' . $res6[0]['warp_yi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="mw_weftyi' . strval($i + 1) . '" id="mw_weftyi' . strval($i + 1) . '" value="' . $res6[0]['weft_yi'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="mw_mw' . strval($i + 1) . '" id="mw_mw' . strval($i + 1) . '" value="' . $res6[0]['min_wages'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="mw_target' . strval($i + 1) . '" id="mw_target' . strval($i + 1) . '" value="' . $res6[0]['target'] . '" class="form-control-plaintext form-control-lg" required readonly /></td>';

                            $res7 = retrieveData("SELECT * from kbk_yarn_consumption where weaving_id=$wid and wef='$wef'", $con);
                            echo  '<td><input type="text" name="warp_grey' . strval($i + 1) . '" id="warp_grey' . strval($i + 1) . '" value="' . $res7[0]['warp_grey'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="warp_colour' . strval($i + 1) . '" id="warp_colour' . strval($i + 1) . '" value="' . $res7[0]['warp_colour'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="warp_black' . strval($i + 1) . '" id="warp_black' . strval($i + 1) . '" value="' . $res7[0]['warp_black'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="warp_bl' . strval($i + 1) . '" id="warp_bl' . strval($i + 1) . '" value="' . $res7[0]['warp_bl'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';

                            echo  '<td><input type="text" name="weft_grey' . strval($i + 1) . '" id="weft_grey' . strval($i + 1) . '" value="' . $res7[0]['weft_grey'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="weft_colour' . strval($i + 1) . '" id="weft_colour' . strval($i + 1) . '" value="' . $res7[0]['weft_colour'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="weft_black' . strval($i + 1) . '" id="weft_black' . strval($i + 1) . '" value="' . $res7[0]['weft_black'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            echo  '<td><input type="text" name="weft_bl' . strval($i + 1) . '" id="weft_bl' . strval($i + 1) . '" value="' . $res7[0]['weft_bl'] . '" class="form-control-plaintext form-control-lg" required readonly/></td>';
                            ?>

                            <td style="border-color: white;background-color: white;">
                              <input type="button" id="<?php echo strval($i + 1); ?>" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="edit" value="Edit" onclick="displayReport(this.id)">
                            </td>
                          <?php
                            echo '<td style="border-color: white;background-color: white;"><input type="submit"  id="update' . strval($i + 1) . '" class="btn btn-warning btn-lg ms-2" data-mdb-target="#exampleModal" name="update" value="Update" style="display:none;"></td></tr>';
                          }

                          echo '<input type="text" name="updateid" id="updateid" value="" class="form-control-plaintext form-control-lg" required hidden/>';
                          echo '<input type="text" name="rowid" id="rowid" value="" class="form-control-plaintext form-control-lg" required hidden/>';
                          ?>



                        </tbody>
                        <tfoot></tfoot>
                      </table>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </form>
</body>

</html>


<!-- <div style="padding: 0px 50px 30px 40px;">
 -->