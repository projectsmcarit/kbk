<?php
include('../connection.php');
include('../kbk_library.php');
session_start();

require_once("dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();


 

$html=""; 
$html.='<html>';
$html.='<body>';
$html.='<form>'; 
$html.='<h2 align="center">DISTRICT KHADI & VILLAGE INDUSTRIES OFFICE, KOTTAYAM</h2>';      
$html.='<br>';
$html.='<h3 align="center">'.$_SESSION["mydata2"].' Wages For '.$_SESSION["mydata1"].'</h3>';
$html.='<br>';
$html.='<h3 align="center">'.$_SESSION["mydata4"]." ".$_SESSION["mydata5"].'</h3>';
//$html.=$_SESSION["mydata2"];
//$html.=' <table style="border-collapse:collapse" border="1" align="center" width=47%;>';
/*
$res1= retrieveData("SELECT artisan_id,artisan_name From kbk_artisan where unit_id='$unitid'",$con);
$res6 =  retrieveData("SELECT total_wages,attendance,noh,yarn_code From kbk_spinning where s_date='$hdate' and artisan_id=$temp1",$con);
        $temp2=$res6[0]['yarn_code'];
        $res8 =  retrieveData("SELECT ad_rec,credit_rec,cov_rec From kbk_deduction where d_date='$hdate' and artisan_id=$temp1",$con);
        $res7 = retrieveData("SELECT * From kbk_yarn_variety where yarn_code='$temp2'",$con);*/
$html.='<table style="border-collapse:collapse" border="1" align="center" width=47%>
<thead>
                <tr style="width:10.17mm">
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">SI No.</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Code</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Artisan Name</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Verity</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">No. of Hanks</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Rate</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Sliver Consumption</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Fund</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">W/F Recovery</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Adv Recovery </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Khadi Credit Recovery </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm"> Covid Recovery </th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">LIC</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Net wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Holiday wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Total Wages</th>
                  <th scope="col" align="center" halign="middle" style="width:10.17mm">Attendance</th>

                </tr>
              </thead>';
//$html.=' <tbody>';
/*
for($i=0;$i<sizeof($res1);$i++)
{
  $html.=' <tr>
  <td>'.strval($i+1).'</td>
  <td>'.$unitid.$res1[$i]['artisan_id'].'</td>
  <td>'.$res1[$i]['artisan_name'].'</td>';
  $temp1=strval($res1[$i]['artisan_id']);
  echo '<td>'.$res7[0]['type'].'</td>';
  echo '<td>'.$res6[0]['noh'].'</td>';
  echo '<td>'.$res7[0]['rate'].'</td>';
  $sar[0]=$res6[0]['noh'];
  $sar[1]=$res7[0]['sliver'];
  $sar[2]=$res7[0]['rate'];
  $sar[3]=$res7[0]['wf'];
  $sar[4]=$res8[0]['ad_rec'];
  $sar[5]=$res8[0]['cov_rec'];
  $sar[6]=$res8[0]['credit_rec'];
  $sar[7]=0.0;
  $sar[8]=$res7[0]['esi'];
  $sar[9]=$res7[0]['yi'];
  $sar[10]=$res4[0]['lic'];
  $sar[11]=0.0;
  $sar[12]=$res3[0]['holiday_no'];
  $sar[13]=$res3[0]['total_wd'];
  $sar[14]=0.0;
  $calc1=spinningCalculation($sar);
  echo '<td>'.$calc1[1].'</td>';
  echo '<td>'.$calc1[2].'</td>';
  echo '<td>'.$calc1[3].'</td>';
  echo '<td>'.$calc1[4].'</td>';
  echo '<td>'.$calc1[5].'</td>';
  echo '<td>'.$calc1[6].'</td>';
  echo '<td>'.$calc1[7].'</td>';
  echo '<td>'.$calc1[8].'</td>';
  echo '<td>'.$calc1[9].'</td>';
  echo '<td>'.$calc1[10].'</td>';
  echo '<td>'.$calc1[11].'</td>';
  echo '<td>'.$calc1[12].'</td>';
  echo '<td>'.$calc1[14].'</td>';
  echo '<td>'.$res6[0]['attendance'].'</td>';
  echo '</tr>';
  $html.='</tr>';
}
*/
//$html.='</tbody>';
$html.=$_SESSION["mydata"];
$html.='</table>';


$html.="</form>";
$html.="</body>";
$html.="</html>";
$dompdf->loadHtml(html_entity_decode($html));	
$dompdf->setPaper('A4', 'landscape'); //portrait or landscape
$dompdf->render();
ob_end_clean();
$dompdf->stream("SPINING REPORT",array("Attachment"=>0));

?>       
