<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Readymade Warp</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
   
     <?php 
   include('header.php');  
   ?>
  </head>
  <body>
    <!-- Start your project here-->
    <form name="rmvariety" action="rm_variety.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    
                    <img
                      src="../images/Charkha1.jpg"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .30rem; border-bottom-left-radius: .30rem;"
                    />
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                        <h3 class="mb-5 text-uppercase">Readymade Warp </h3>
      
                     <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="rmcode" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n">readymade  Code</label>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="type" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n">type</label><!--variety=type in table-->
                          </div>
                        </div>
                        
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="s_wage" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">sizing wages </label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="s_min_wage" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">sizing minimum wages </label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="s_target" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">sizing target</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="s_yi" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">sizing yarn incentive </label>
                            </div>
                          </div>
                      </div>
      
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="b_wage" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">bobbin winding wages </label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="b_min_wage" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">bobbin winding minimum wages </label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="b_target" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">bobbin winding target</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="b_yi" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">bobbin winding yarn incentive </label>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="w_wage" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example8">warping  wages </label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="w_min_wage" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">warping minimum wages </label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="w_target" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">warping target</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="w_yi" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">warping yarn incentive </label>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="welfare" class="form-control form-control-lg" />
                              <label class="form-label" for="form3Example1n1">Welfare fund </label>
                            </div>
                          </div>
                          <div class="col-md-6 mb-4">
                            <div class="form-outline">
                            <input type="number" step="any" min="0" name="annualincentive" class="form-control form-control-lg" />
                          <label class="form-label" for="form3Example1n1">Annual Incentive </label>
                            </div>
                          </div>
                          <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="esi" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n1">ESI</label>
                         </div>
                          </div>
                    
      
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                          <input type="date" name="witheffectfrom" id="form3Example1m1" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1m1">With Effect From </label>
                            
                          </div>
                        </div>
                        
                      </div>
      
                     
                      
      
                      <div class="d-flex justify-content-end pt-3">
                        <button type="submit" name="submit" class="btn btn-warning btn-lg ms-2">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
