<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Artisans</title>
    <!-- MDB icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <style>
        .cont {
            background: white;
            position: relative;
            margin-top: 100px;
            padding-top: 25px;
            padding-bottom: 25px;
            margin-right: 50px;
            margin-left: 50px;
            margin-bottom: 500px
        }

        h4{
            color: black;
        }

        .table-wrapper {
            max-height: 300px;
            overflow: auto;
            display: inline-block;
        }

        .table-wrapper thead tr {
            position: sticky;
            top: 0;
        }

    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
</head>
<body>
    <header>
        <?php
            include("header.php");
            include("../connection.php");
        ?>   
    </header>
    <!-- <section class=""> -->
        <div
         id="intro"
         class="bg-image"
         style="
                background-image: url(../images/2.jpg);
                height: 100vh;
                "
         >
            <div class="mask text-white" style="background-color: rgba(0, 0, 0, 0.8)">
                <div class="container-flex cont">
                    <div class="row justify-content-center" >
                        <div class="col-md-12" >
                            <div class="card mt-5" >
                                <div class="card-header" >
                                    <center><h4>UNIT</h4></center>
                                </div>
                                <center>
                                <div class="card-body" style="width: 1000px; margin-left: auto;" >
                                    <div class="table-responsive">
                                        <table id="tableData" class="table table-hover table-sm table-wrapper" >
                                            <thead class="table-dark">
                                                <tr>
                                                    <th>Unit Code</th>
                                                    <th>Name</th>
                                                    <th>Activity</th>
                                                    <th>Instructor</th>
                                                    <th>Centre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql = "SELECT a.*, b.name, c.centre_name FROM kbk_unit AS a INNER JOIN kbk_instructor AS b ON (a.instructor_id = b.instructor_id) INNER JOIN kbk_centre AS c ON (a.centre_id = c.centre_code)";
                                                    $result = $con->query($sql);
                                                    if($result->num_rows > 0) {
                                                        while($row = $result->fetch_assoc()) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['unit_code']; ?></td>
                                                    <td><?php echo $row['unit_name']; ?></td>
                                                    <td><?php echo $row['activity']; ?></td>
                                                    <td><?php echo $row['name']; ?></td>
                                                    <td><?php echo $row['centre_name']; ?></td>
                                                </tr>

                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button class="btn btn-primary" onclick="Export();">EXPORT</button>
                                    </div>
                                </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- </section> -->
    <script type="text/javascript">
        function Export() {
            $(document).ready(function () {
                $("#tableData").table2excel({
                    filename: "units.xls"
                });
            });
        }
    </script>
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <script type="text/javascript" src="validation.js"></script>
</body>
</html>