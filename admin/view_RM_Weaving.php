<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>RM Weaving</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
   
    
  </head>
  <body>
    <!-- Start your project here-->
    <form name="Weaving" action="weaving.php" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    <img
                      src="../images/wgp.png"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
                    />
    
    
                    
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                      <h3 class="mb-5 text-uppercase">RM Warp Weaving</h3>
      
                     
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example90">RM Warp Weaving Wages</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_ww_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Warp Weaving yi</label>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" name="rm_wf_yi" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example99">RM Weft Weaving yi</label>
                      </div>
      
                      <div class="form-outline mb-4">
                        <input type="text" name="min_wages" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example97">Minimum Wages</label>
                      </div>
                      
    
                      <div class="form-outline mb-4">
                        <input type="text" name="target" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example9">Target</label>
                      </div>
                      <div class="col-md-6 mb-4">
                      <div class="form-outline">
                        <input type="date" name="witheffectfrom" class="form-control form-control-lg" />
                        <label class="form-label" for="form3Example1m1">With Effect From </label>
                      </div>
                    </div>
                      <div class="col-md-6 mb-4">
    
                      <select class="form-select" name="code" aria-label="Default select example" id="code">
                          <option disabled selected>-- Weaving code --</option>
                          <?php
                              include('../connection.php'); // Using database connection file here
                              $cen= mysqli_query($con, "SELECT weaving_code,weaving_id From kbk_weaving_variety");  // Use select query here 

                              while($center = mysqli_fetch_array($cen))
                                {
                                 echo "<option value='". $center['weaving_id'] ."'>" .$center['weaving_code'] ." </option>";  // displaying data in option menu
                                }	
                            ?>  
                        </select>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <button type="reset" class="btn btn-light btn-lg">Reset</button>
                        <button type="submit" class="btn btn-warning btn-lg ms-2" name="rmsubmit">Submit</button>
                      </div>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>