<?php
include('../connection.php');
include('../kbk_library.php');
// $code=$_POST['yarncode'];
// $sql = "SELECT * from kbk_yarn_variety WHERE  yarn_code = '$code'";
// $result = mysqli_query($con, $sql);
// $row = mysqli_fetch_array($result);  


$twistid=0;
$variety =""; 
$rate ="";  
$yarn ="";
$esi =""; 
$wf =""; 
$yi ="";
$ai="";
$mw="";  
$target ="";
$wef ="";
if(isset($_POST['search']))
{
    $code=$_POST['twistingcode'];
    $sql="SELECT * from kbk_twisting_variety where twisting_code='$code' and wef = (SELECT max(wef) from kbk_twisting_variety where twisting_code='$code')";
    //$sql = "SELECT * from kbk_yarn_variety WHERE  yarn_code = '$code'";
    $result = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($result);  
    $twistid =$row['twist_id'];
    $variety =$row['type']; 
    $rate =$row['rate'];  
    $yarn =$row['yarn'];
    $esi =$row['esi']; 
    $wf =$row['wf']; 
    $yi =$row['yi'];
    $ai=$row['ai'];
    $mw=$row['mw'];  
    $target =$row['target'];
    $wef =$row['wef'];
}
if(isset($_POST['update']))
 {
  $tid=intval($_POST['twistid']);
  $variety = $_POST['twistingvariety']; 
  $rate = $_POST['rate'];  
  $yarn = $_POST['yarnconsumption'];
  $esi = $_POST['esi']; 
  $wf = $_POST['welfare']; 
  $yi = $_POST['yarnincentive'];
  $ai=$_POST['annualincentive'];
  $mw=$_POST['minimumwage'];  
  $target = $_POST['target'];
  $wef = $_POST['witheffectfrom']; 
  $sql = "UPDATE kbk_twisting_variety SET type = '$variety', rate = $rate, yarn = $yarn, esi= $esi, wf = $wf,yi = $yi,ai=$ai,mw=$mw,target=$target WHERE twist_id=$tid";
                
      $re=$con->query($sql);
      //var_dump($re);
      if($re)
      {    

          echo "<script>alert('success')</script>";
          //echo "<script>window.location.href='view_admin_index.php'</script>";
      }
      else
      {
          echo "<script>alert('Error!!! Try Again')</script>";
          //echo "<script>window.location.href='view_artisan_reg.php'</script>";
      }

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Twisting_Variety_Search</title>
      
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
   
     <?php 
   include('header.php');  
   ?>
  </head>
  <body>
    <!-- Start your project here-->
    <form name="twistingvariety" action="" method="post">
    <section class="h-100 bg-dark">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col">
              <div class="card card-registration my-4">
                <div class="row g-0">
                  <div class="col-xl-6 d-none d-xl-block">
                    
                    <img
                      src="../images/Charkha1.jpg"
                      alt="Sample photo"
                      class="img-fluid"
                      style="border-top-left-radius: .30rem; border-bottom-left-radius: .30rem;"
                    />
                  </div>
                  <div class="col-xl-6">
                    <div class="card-body p-md-5 text-black">
                        <h3 class="mb-5 text-uppercase">Twisting Variety Search</h3>
                        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                    <script>
$(document).ready(function(){
    $('.form-outline input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("twisting_variety_backend.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".form-outline").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
                  </script>
                     <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="twistingcode" class="form-control form-control-lg" />
                            <label class="form-label" for="form3Example1n">Twisting Code</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                      <div class="col-md-6 mb-4">
                        <input type="submit" class="btn btn-warning btn-lg ms-2" name="search" value="Search" >
                  
                      
                    </div >
                     <input type="text" name="twistid" value="<?php echo $twistid; ?>" hidden />
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="text" name="twistingvariety" id="twistingvariety" class="form-control form-control-lg" value="<?php echo $variety; ?>"  readonly />
                            <label class="form-label" for="form3Example1n">Twisting Variety</label><!--variety=type in table-->
                          </div>
                        </div>
                        
                      <div class="col-md-6 mb-4">
                      <div class="form-outline ">
                        <input type="number" step="any" min="0" name="rate" id="rate" class="form-control form-control-lg"value="<?php echo $rate; ?>"  readonly  />
                        <label class="form-label" for="form3Example8">Rate </label>
                      </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="yarnconsumption" id="yarnconsumption" class="form-control form-control-lg"value="<?php echo $yarn; ?>"  readonly  />
                            <label class="form-label" for="form3Example1m1">Yarn Consumption </label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                            <input type="number" step="any" min="0" name="esi" id="esi" class="form-control form-control-lg" value="<?php echo $esi; ?>"  readonly />
                            <label class="form-label" for="form3Example1n1">ESI</label>
                          </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="form-outline">
                              <input type="number" step="any" min="0" name="welfare" id="welfare" class="form-control form-control-lg"value="<?php echo $wf; ?>"  readonly  />
                              <label class="form-label" for="form3Example1n1">Welfare fund </label>
                            </div>
                          </div>
                      </div>
      
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="yarnincentive" id="yarnincentive" class="form-control form-control-lg"value="<?php echo $yi; ?>"  readonly  />
                          <label class="form-label" for="form3Example1n1">Yarn Incentive </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="annualincentive" id="annualincentive" class="form-control form-control-lg"value="<?php echo $ai; ?>"  readonly  />
                          <label class="form-label" for="form3Example1n1">Annual Incentive </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="minimumwage" id="minimumwage" class="form-control form-control-lg" value="<?php echo $mw; ?>"  readonly  />
                          <label class="form-label" for="form3Example1n1">Minimum wage </label>
                        </div>
                      </div>
                      <div class="col-md-6 mb-4">
                        <div class="form-outline">
                          <input type="number" step="any" min="0" name="target" id="target" class="form-control form-control-lg" value="<?php echo $target; ?>"  readonly />
                          <label class="form-label" for="form3Example1n1">Target </label>
                        </div>
                      </div>
                      <div>
                        <div class="col-md-6 mb-4">
                          <div class="form-outline">
                          <input type="date" name="witheffectfrom" id="witheffectfrom" class="form-control form-control-lg"value="<?php echo $wef; ?>"  readonly  />
                            <label class="form-label" for="form3Example1m1">With Effect From </label>
                            
                          </div>
                        </div>
                        
                      </div>
      
                     
                      
      
                      </div>

                    
                    <div class="d-flex justify-content-end pt-3">
                      <!-- <button type="reset" class="btn btn-light btn-lg">Reset All</button> -->
                      <input type="button"  name="edit" id="edit" class="btn btn-warning btn-lg ms-2" onclick=formUpdate() method="post" value="EDIT"></button>
                      <input type="submit" name="update" class="btn btn-warning btn-lg ms-2" value="UPDATE" >
                      <script>
                        function formUpdate()
                        {
                     
                          
                          document.getElementById("twistingvariety").readOnly = false;
                          document.getElementById("rate").readOnly = false;
                          document.getElementById("yarnconsumption").readOnly = false;
                          document.getElementById("esi").readOnly = false;
                          document.getElementById("welfare").readOnly = false;
                          document.getElementById("yarnincentive").readOnly = false;
                          document.getElementById("annualincentive").readOnly = false;
                          document.getElementById("minimumwage").readOnly = false;
                          document.getElementById("target").readOnly = false;
                          document.getElementById("witheffectfrom").readOnly = false;
                          
                        }
                      </script>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="../js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
