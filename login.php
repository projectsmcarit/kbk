<!DOCTYPE html>
<?php 
    include('connection.php');
?>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Login</title>
    <!-- MDB icon -->
    <!--<link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon" /> -->
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
    <!-- Google Fonts Roboto -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
    />
    <!-- MDB -->
    <link rel="stylesheet" href="css/mdb.min.css" />
  </head>
  <body>
    <!-- Start your project here-->
    <section class="vh-100" style="background-color: black;">
      <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
              <div class="row g-0">
                <div class="col-md-6 col-lg-5 d-none d-md-block">
                  <img
                    src="images/1.jpg"
                    alt="login form"
                    class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                  />
                </div>
                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                  <div class="card-body p-4 p-lg-5 text-black">
    
                    <form action="authentication.php" method="post"> 
    
                      <div class="d-flex align-items-center mb-3 pb-1">
                        
                        <span class="h1 fw-bold mb-0">Login</span>
                      </div>
                      <div class="form-outline mb-4">
                        <input type="text" id="form2Example17" name="uname" class="form-control form-control-lg" required="" />
                        <label class="form-label" for="form2Example17">User Name</label>
                      </div>
    
                      <div class="form-outline mb-4">
                        <input type="password" id="form2Example27" name="psw" class="form-control form-control-lg" required="" />
                        <label class="form-label" for="form2Example27">Password</label>
                      </div>
                         <div class="form-outline mb-4">
                      <input id="user" name="user" value="admin" type="radio" required="" >Admin 
                      <input id="user" name="user" value="instructor" type="radio" required="" > Unit Instructor
                      </div>
                      <div class="pt-1 mb-4">
                        <input type="submit" class="btn btn-dark btn-lg btn-block" name="submit" value="Login">
                      </div>
                      <a class="small text-muted" href="forgot_pswd.php">Forgot password?</a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End your project here-->

    <!-- MDB -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
